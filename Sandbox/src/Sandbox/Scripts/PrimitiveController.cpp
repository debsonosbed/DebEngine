#include "PrimitiveController.h"


void PrimitiveController::OnCreate()
{
	m_RNG = std::mt19937(m_Device());
	m_Distribution = std::uniform_real_distribution<f64>(0.0, 1.0);

	f32 randVal = m_Distribution(m_RNG);
	m_Angle = randVal * 360.0f;

	m_Radius += m_Distribution(m_RNG) * 0.1f * (f32)GetScene()->GetEntitiesCount();

	GetComponent<TransformComponent>().Position = glm::vec3(
		glm::sin(m_Angle) * m_Radius,
		(randVal - 0.5f) * m_Radius * 2.0f,
		glm::cos(m_Angle) * m_Radius);

	f64 r = m_Distribution(m_RNG);
	f64 g = m_Distribution(m_RNG);
	f64 b = m_Distribution(m_RNG);
	GetComponent<MeshComponent>().Mesh->SetColor({ r, g, b, 1.0f });
}

void PrimitiveController::OnUpdate()
{
	auto& tc = GetComponent<TransformComponent>();
	const f32 radius = 10.0f;

	f32 speedFactor = 0.001f;
	m_Angle += 50.0f * Time::DeltaTime() * speedFactor;
	f32 stepX = glm::sin(m_Angle) * m_Radius;
	f32 stepZ = glm::cos(m_Angle) * m_Radius;
	tc.Position = glm::vec3(stepX, tc.Position.y, stepZ);
}

void PrimitiveController::OnDestroy()
{

}
