#pragma once

#include <DebEngine.h>
using namespace DebEngine;

#include <random>

class PrimitiveController : public ScriptableEntity
{
public:
	virtual ~PrimitiveController() = default;

	ASSIGN_NAME(PrimitiveController)
protected:
	void OnCreate() override;
	void OnUpdate() override;
	void OnDestroy() override;
private:
	std::random_device m_Device;
	std::mt19937 m_RNG;
	std::uniform_real_distribution<f64> m_Distribution;
	f32 m_Angle = 0.0f;
	f32 m_Radius = 10.0f;
};

