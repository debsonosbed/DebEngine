#include "SceneWindow.h"

#include "DebEditor/Utils/EditorUtils.h"

namespace DebEditor
{
	using namespace Utils;

	SceneWindow::SceneWindow(Scene* scene)
		: Window(scene)
	{

	}

	void SceneWindow::OnRender(b8* isOpen)
	{
		if (ImGui::Begin("Scene", isOpen))
		{
			auto& sceneOptions = m_Scene->GetOptions();

			ImGui::Columns(2, 0, false);

			ImGui::SetColumnWidth(0, 85);
			ImGui::Text("Scene Name: ");

			ImGui::NextColumn();
			ImGui::SetColumnWidth(1, 150);
			ImGui::PushItemWidth(-1);

			Buffer buffer(nullptr, 256);
			buffer.Write((u8*)sceneOptions.SceneName.c_str(), sceneOptions.SceneName.size());
			if (ImGui::InputText("##scenename", (char*)buffer.Data, buffer.Size))
			{
				sceneOptions.SceneName = std::string((char*)buffer.Data);
			}
			buffer.Clear();

			ImGui::PopItemWidth();

			ImGui::Columns(1);

			ImGui::Text("Entities: %d", m_Scene->GetEntitiesCount());

			ImGui::Columns(2, 0, false);
			ImGui::SetColumnWidth(0, 85);

			Control("Show Grid: ", sceneOptions.ShowGrid);
			Control("Force scripts: ", sceneOptions.PlayScriptsInEditorMode);

			ImGui::Columns(1);

			ImGui::End();
		}
	}
}