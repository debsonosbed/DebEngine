#pragma once

#include "Window.h"

namespace DebEditor
{
	class EnvironmentWindow : public Window
	{
	public:
		EnvironmentWindow() = default;
		EnvironmentWindow(Scene* scene);
		virtual void OnRender(b8* isOpen) override;
	};
}

