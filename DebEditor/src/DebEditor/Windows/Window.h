#pragma once

#include "DebEngine.h"

#include "imgui/imgui.h"

using namespace DebEngine;

namespace DebEditor
{
	class Window
	{
	public:
		Window() = default;
		Window(Scene* scene) : m_Scene(scene) { }
		virtual ~Window() = default;
		virtual void OnRender(b8* isOpen) = 0;
	protected:
		Scene* m_Scene = nullptr;
	};
}