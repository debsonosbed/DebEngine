#pragma once

#include "Window.h"

namespace DebEditor
{
	class SceneWindow : public Window
	{
	public:
		SceneWindow() = default;
		SceneWindow(Scene* scene);
		virtual void OnRender(b8* isOpen) override;
	};
}