#pragma once

#include "Window.h"

namespace DebEditor
{
	class StatisticsWindow : public Window
	{
	public:
		StatisticsWindow() = default;
		StatisticsWindow(Scene* scene);
		virtual void OnRender(b8* isOpen) override;
	};
}

