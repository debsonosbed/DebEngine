#include "StatisticsWindow.h"

#include "imgui/imgui.h"

namespace DebEditor
{

	StatisticsWindow::StatisticsWindow(Scene* scene)
		: Window(scene)
	{

	}

	void StatisticsWindow::OnRender(b8* isOpen)
	{
		if (ImGui::Begin("Statistics", isOpen))
		{
			auto nodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_Selected | ImGuiTreeNodeFlags_DefaultOpen;
			if (ImGui::TreeNodeEx("##statisticsgraphics", nodeFlags, "Graphics"))
			{
				ImGui::Text("%.1lf FPS (%.1lfms)", Statistics::GetFPS(), Statistics::GetFrameTime());
				ImGui::Text("Main Thread: %.1lfms", Statistics::GetMainThreadFrameTime());
				ImGui::Text("Render Thread: %.1lfms", Statistics::GetRenderThreadFrameTime());
				auto [gameViewportWidth, gameViewportHeight] = Statistics::GetGameViewportDimensions();
				ImGui::Text("Game viewport: %dx%d - %.1f MB", gameViewportWidth, gameViewportHeight, (f32)BYTES_TO_MB(Statistics::GetGameViewportSize()));

				ImGui::TreePop();
			}
			ImGui::End();
		}
	}
}