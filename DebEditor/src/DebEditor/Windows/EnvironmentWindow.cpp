#include "EnvironmentWindow.h"

#include "DebEditor/Utils/EditorUtils.h"

#include <filesystem>

#include "imgui/imgui.h"

namespace DebEditor
{
	using namespace Utils;

	EnvironmentWindow::EnvironmentWindow(Scene* scene)
		: Window(scene)
	{

	}

	void EnvironmentWindow::OnRender(b8* isOpen)
	{
		if (ImGui::Begin("Environment", isOpen))
		{
			auto& sceneOptions = m_Scene->GetOptions();
			auto labelColumnWidth = 60;
			auto nodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_Selected | ImGuiTreeNodeFlags_DefaultOpen;
			if (ImGui::TreeNodeEx("##environment", nodeFlags, "Environment"))
			{
				ImGui::Columns(3, nullptr, false);

				ImGui::SetColumnWidth(0, labelColumnWidth);
				ImGui::Text("Path: ");

				ImGui::NextColumn();
				ImGui::SetColumnWidth(1, 200);
				ImGui::PushItemWidth(-1);
				if (sceneOptions.EnvironmentMap.first)
					ImGui::InputText("##envmapfilepath", (char*)sceneOptions.EnvProperties.Path.c_str(), 256, ImGuiInputTextFlags_ReadOnly);
				else
					ImGui::InputText("##envmapfilepath", (char*)"Null", 256, ImGuiInputTextFlags_ReadOnly);
				ImGui::PopItemWidth();

				ImGui::NextColumn();
				ImGui::SetColumnWidth(2, 40);
				if (ImGui::Button("...##openenvmap"))
				{
					auto envMapPath = Filesystem::OpenFileDialog(std::filesystem::current_path().string() + "\\assets\\environment\\", "HDRi Files (*.hdr)\0*.hdr\0");
					if (!envMapPath.empty())
					{
						m_Scene->LoadEnvironmentMap(envMapPath);
					}
;				}

				ImGui::Columns(2, nullptr, false);
				ImGui::AlignTextToFramePadding();

				ImGui::SetColumnWidth(0, labelColumnWidth);
				Control("LOD", sceneOptions.EnvProperties.LOD, 0.0f, 11.0f, ControlType::Slider);
				Control("Rotation", sceneOptions.EnvProperties.Rotation, -360.0f, 360.0f, ControlType::Slider);
				ImGui::Columns(1);

				ImGui::TreePop();
			}

			if (ImGui::TreeNodeEx("##directionallight", nodeFlags, "DirectionalLight"))
			{
				ImGui::Columns(2, nullptr, false);
				ImGui::AlignTextToFramePadding();
				auto& dirLight = sceneOptions.DirectionalLight;

				ImGui::SetColumnWidth(0, labelColumnWidth);
				Control("Direction", dirLight.Direction, ControlType::Slider);
				Control("Radiance", dirLight.Radiance, ControlType::Color);
				Control("Intensity", dirLight.Intensity, 0.0f, 5.0f, ControlType::Slider);
				ImGui::Columns(1);

				ImGui::TreePop();
			}

			if (ImGui::TreeNodeEx("##camera", nodeFlags, "Camera"))
			{
				ImGui::Columns(2, 0, false);
				ImGui::AlignTextToFramePadding();

				ImGui::SetColumnWidth(0, labelColumnWidth);
				if (Control("Exposure", m_Scene->GetEditorCamera()->GetExposure(), 0.0f, 5.0f, ControlType::Slider))
				{
					if (m_Scene->GetMainCamera())
					{
						auto& mainCameraExposure = m_Scene->GetMainCamera()->GetExposure();
						mainCameraExposure = m_Scene->GetEditorCamera()->GetExposure();
					}
				}
				ImGui::Columns(1);

				ImGui::TreePop();
			}

			ImGui::End();
		}
	}
}