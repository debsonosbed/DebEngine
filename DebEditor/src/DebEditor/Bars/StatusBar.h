#pragma once

#include "DebEngine.h"

namespace DebEditor
{
	using namespace DebEngine;

	class StatusBar
	{
	public:
		StatusBar() = default;

		void OnImGuiRender();
		void SetStatusMessage(const std::string& statusMsg);

		void SetSize(f32 size);
		f32 GetSize() const;
	private:
		std::string m_StatusMessage = "DebEngine " + std::string(DEBENGINE_BUILD_ID);
		f32 m_Size = 20.0f;
	};
}

