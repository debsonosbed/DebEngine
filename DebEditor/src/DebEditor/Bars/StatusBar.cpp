#include "StatusBar.h"

namespace DebEditor
{

	void StatusBar::OnImGuiRender()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
		ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0);
		ImGui::SetCursorPosY(ImGui::GetCursorPosY() - 5.0f);
		ImGui::BeginChildFrame(1, ImVec2(0, 0));
		ImGui::PopStyleVar(2);

		auto availRegion = ImGui::GetContentRegionAvail();
		f32 rightMargin = 5.0f;
		auto textSize = ImGui::CalcTextSize(m_StatusMessage.c_str());
		auto cursorPosX = ImGui::GetCursorPosX() + availRegion.x - textSize.x - rightMargin;
		auto cursorPosY = ImGui::GetCursorPosY() + (availRegion.y - textSize.y) / 2;
		ImGui::SetCursorPos(ImVec2(cursorPosX, cursorPosY));
		ImGui::Text(m_StatusMessage.c_str());
		ImGui::EndChildFrame();
	}

	void StatusBar::SetStatusMessage(const std::string& statusMsg)
	{
		m_StatusMessage = statusMsg;
	}

	void StatusBar::SetSize(f32 size)
	{
		m_Size = size;
	}

	f32 StatusBar::GetSize() const
	{
		return m_Size;
	}
}