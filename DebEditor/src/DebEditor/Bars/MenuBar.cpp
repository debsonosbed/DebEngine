#include "MenuBar.h"

#include "DebEngine/Core/Statistics.h"
#include "DebEditor/Utils/EditorUtils.h"

#include "imgui/imgui_internal.h"

#include <filesystem>

namespace DebEditor
{
	MenuBar::MenuBar(Scene* scene)
		: m_Scene(scene),
		m_EnvironmentWindow(scene),
		m_StatisticsWidndow(scene),
		m_SceneWindow(scene)
	{
		for (const auto& entry : std::filesystem::directory_iterator(m_SceneDirPath)) {
			auto path = entry.path();
			if (path.filename().extension().string() == m_SceneFileExt)
			{
				m_AvilableScenes.insert({ path.stem().string(), path.string() });
			}
		}
	}

	void MenuBar::OnImGuiRender()
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("New"))
				{
					
					m_OpenNewScenePopup = true;
				}

				if (ImGui::MenuItem("Save"))
				{
					SaveScene();
					m_OpenSceneSavedPopup = true;
				}

				if (ImGui::BeginMenu("Load"))
				{
					if (m_AvilableScenes.size() > 0)
					{
						for (const auto& [sceneName, path] : m_AvilableScenes)
						{
							if (ImGui::MenuItem(sceneName.c_str()))
							{
								m_Scene->LoadFromFile(path);
								m_OpenSceneLoadedPopup = true;
							}
						}
					}
					else
					{
						ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
						ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
						ImGui::MenuItem("No scenes available...");
						ImGui::PopItemFlag();
						ImGui::PopStyleVar();

					}
					ImGui::EndMenu();
				}

				if (ImGui::MenuItem("Exit"))
				{

				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Window"))
			{
				if (ImGui::MenuItem("Statistics"))
				{
					m_ShowStatisticsWindow = true;
				}

				if (ImGui::MenuItem("Environment"))
				{
					m_ShowEnvironmentWindow = true;
				}

				if (ImGui::MenuItem("Scene"))
				{
					m_ShowSceneWindow = true;
				}

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}

		if (m_OpenSceneSavedPopup)
		{
			ImGui::OpenPopup("Scene save");
			m_OpenSceneSavedPopup = false;
		}
		auto mainWindowPos = ImGui::GetWindowPos();
		auto mainWindowSize = ImGui::GetWindowSize();
		if (ImGui::BeginPopupModal("Scene save", nullptr, ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
		{
			ImVec2 windowSize = { 200, 100 };
			ImGui::SetWindowPos({ (mainWindowSize.x - windowSize.x) / 2.0f + mainWindowPos.x, (mainWindowSize.y - windowSize.y) / 2.0f + mainWindowPos.y });
			ImGui::SetWindowSize(windowSize);

			auto sceneName = m_Scene->GetOptions().SceneName;
			const std::string modalText = "Scene \"" + sceneName + "\" saved successfully!";

			ImGui::TextWrapped(modalText.c_str());

			f32 margin = 5.0f;
			ImVec2 buttonSize = { 40, 25 };
			ImVec2 buttonPos = { windowSize.x - buttonSize.x - margin, windowSize.y - buttonSize.y - margin };
			ImGui::SetCursorPos(buttonPos);
			if (ImGui::Button("Ok", buttonSize))
			{
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}

		if (m_OpenSceneLoadedPopup)
		{
			ImGui::OpenPopup("Scene loaded");
			m_OpenSceneLoadedPopup = false;
		}

		if (ImGui::BeginPopupModal("Scene loaded", nullptr, ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
		{
			ImVec2 windowSize = { 200, 100 };
			ImGui::SetWindowPos({ (mainWindowSize.x - windowSize.x) / 2.0f + mainWindowPos.x, (mainWindowSize.y - windowSize.y) / 2.0f + mainWindowPos.y });
			ImGui::SetWindowSize(windowSize);

			auto sceneName = m_Scene->GetOptions().SceneName;
			const std::string modalText = "Scene \"" + sceneName + "\" loaded successfully!";

			ImGui::TextWrapped(modalText.c_str());

			f32 margin = 5.0f;
			ImVec2 buttonSize = { 40, 25 };
			ImVec2 buttonPos = { windowSize.x - buttonSize.x - margin, windowSize.y - buttonSize.y - margin };
			ImGui::SetCursorPos(buttonPos);
			if (ImGui::Button("Ok", buttonSize))
			{
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}

		if (m_OpenNewScenePopup)
		{
			ImGui::OpenPopup("New Scene");
			m_OpenNewScenePopup = false;
		}

		if (ImGui::BeginPopupModal("New Scene", nullptr, ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
		{
			ImVec2 windowSize = { 200, 100 };
			ImGui::SetWindowPos({ (mainWindowSize.x - windowSize.x) / 2.0f + mainWindowPos.x, (mainWindowSize.y - windowSize.y) / 2.0f + mainWindowPos.y });
			ImGui::SetWindowSize(windowSize);

			auto sceneName = m_Scene->GetOptions().SceneName;
			const std::string modalText = "Do you wish to save current scene?";

			ImGui::TextWrapped(modalText.c_str());

			f32 margin = 5.0f;
			f32 itemSpacing = 5.0f;
			ImVec2 buttonSize = { 40, 25 };
			ImVec2 buttonPos = { windowSize.x -  2 * buttonSize.x - itemSpacing - margin, windowSize.y - buttonSize.y - margin };
			ImGui::SetCursorPos(buttonPos);
			if (ImGui::Button("No", buttonSize))
			{
				m_Scene->LoadDefault();

				ImGui::CloseCurrentPopup();
			}

			ImGui::SameLine(buttonPos.x + buttonSize.x, itemSpacing);
			if (ImGui::Button("Yes", buttonSize))
			{
				SaveScene();
				m_Scene->LoadDefault();

				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}

		if (m_ShowStatisticsWindow)
		{
			m_StatisticsWidndow.OnRender(&m_ShowStatisticsWindow);
		}

		if (m_ShowEnvironmentWindow)
		{
			m_EnvironmentWindow.OnRender(&m_ShowEnvironmentWindow);
		}

		if (m_ShowSceneWindow)
		{
			m_SceneWindow.OnRender(&m_ShowSceneWindow);
		}
	}

	void MenuBar::SetOnSceneLoadCallback(std::function<void(Scene*)> callback)
	{
		m_SceneLoadCallback = callback;
	}

	void MenuBar::SetOnSceneSaveCallback(std::function<void()> callback)
	{
		m_SceneSaveCallback = callback;
	}

	void MenuBar::SetOnEditorExitCallback(std::function<void()> callback)
	{
		m_EditorExitCallback = callback;
	}

	void MenuBar::SaveScene()
	{
		auto sceneName = m_Scene->GetOptions().SceneName;
		auto scenePath = m_SceneDirPath + sceneName + m_SceneFileExt;
		m_Scene->SaveToFile(scenePath);
		m_AvilableScenes[sceneName] = scenePath;
	}
}