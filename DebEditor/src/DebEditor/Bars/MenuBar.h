#pragma once

#include "DebEngine/Gameplay/Scene.h"
#include "DebEditor/Windows/StatisticsWindow.h"
#include "DebEditor/Windows/EnvironmentWindow.h"
#include "DebEditor/Windows/SceneWindow.h"

namespace DebEditor
{
	using namespace DebEngine;

	class MenuBar
	{
	public:
		MenuBar() = default;
		MenuBar(Scene* scene);

		void OnImGuiRender();
		void SetOnSceneLoadCallback(std::function<void(Scene*)> callback);
		void SetOnSceneSaveCallback(std::function<void()> callback);
		void SetOnEditorExitCallback(std::function<void()> callback);
	public:
		void SaveScene();

	private:
		Scene* m_Scene;

		std::function<void(Scene*)> m_SceneLoadCallback;
		std::function<void()> m_SceneSaveCallback;
		std::function<void()> m_EditorExitCallback;

		// Temporary data for scene loading - ImGuiFileDialog will be implemented later 
		std::unordered_map<std::string, std::string> m_AvilableScenes;
		std::string m_SceneDirPath = "assets/scenes/";
		std::string m_SceneFileExt = ".debsc";

		b8 m_OpenSceneSavedPopup = false;
		b8 m_OpenSceneLoadedPopup = false;

		StatisticsWindow m_StatisticsWidndow;
		b8 m_ShowStatisticsWindow = false;
		EnvironmentWindow m_EnvironmentWindow;
		b8 m_ShowEnvironmentWindow = false;
		SceneWindow m_SceneWindow;
		b8 m_ShowSceneWindow = false;

		b8 m_OpenNewScenePopup = false;
	};
}
