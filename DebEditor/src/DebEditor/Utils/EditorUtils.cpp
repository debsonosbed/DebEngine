#include "EditorUtils.h"

#include <imgui.h>
#include <imgui/imgui_internal.h>
#include <glm/gtc/type_ptr.hpp>

namespace DebEditor
{
	namespace Utils
	{
		// Partly insipred by Hazel Game Engine
		void Vector3Control(const std::string& label, glm::vec3& vec, f32 defaultValue, f32 minVal, f32 maxVal, f32 columnWidth)
		{

			ImGuiIO& io = ImGui::GetIO();
			auto boldFont = io.Fonts->Fonts[1];

			ImGui::PushID(label.c_str());

			ImGui::Columns(2, 0, false);
			ImGui::SetColumnWidth(0, columnWidth);
			ImGui::Text(label.c_str());
			ImGui::NextColumn();

			ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

			float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			ImVec2 buttonSize = { lineHeight + 3.0f, lineHeight };

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("X", buttonSize))
			{
				vec.x = defaultValue;
			}

			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			ImGui::DragFloat("##X", &vec.x, 0.1f, minVal, maxVal, "%.2f");
			ImGui::PopItemWidth();
			ImGui::SameLine();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("Y", buttonSize))
			{
				vec.y = defaultValue;
			}

			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			ImGui::DragFloat("##Y", &vec.y, 0.1f, minVal, maxVal, "%.2f");
			ImGui::PopItemWidth();
			ImGui::SameLine();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.35f, 0.9f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("Z", buttonSize))
			{
				vec.z = defaultValue;
			}

			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			ImGui::DragFloat("##Z", &vec.z, 0.1f, minVal, maxVal, "%.2f");
			ImGui::PopItemWidth();

			ImGui::PopStyleVar();

			ImGui::Columns(1);

			ImGui::PopID();
		}

		void MeshContent(std::shared_ptr<Mesh>& mesh)
		{

		}

		void MaterialContent(std::shared_ptr<Material>& mesh)
		{

		}

		b8 Control(const std::string& name, const char* value)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			ImGui::SameLine();
			std::string id = "##" + name;
			ImGui::InputText("##meshfilepath", (char*)value, 256, ImGuiInputTextFlags_ReadOnly);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return true;
		}

		b8 Control(const std::string& name, const std::string& value)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			ImGui::InputText("##meshfilepath", (char*)value.c_str(), 256, ImGuiInputTextFlags_ReadOnly);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return true;
		}

		b8 Control(const std::string& name, b8& value)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			bool result = ImGui::Checkbox(id.c_str(), &value);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return result;
		}

		b8 Control(const std::string& name, f32& value, f32 min, f32 max, ControlType type)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			bool changed = false;
			if (type == ControlType::Slider)
				changed = ImGui::SliderFloat(id.c_str(), &value, min, max);
			else
				changed = ImGui::DragFloat(id.c_str(), &value, 1.0f, min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return changed;
		}

		b8 Control(const std::string& name, glm::vec2& value, ControlType type)
		{
			return Control(name, value, -1.0f, 1.0f, type);
		}

		b8 Control(const std::string& name, glm::vec2& value, f32 min, f32 max, ControlType type)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			bool changed = false;
			if (type == ControlType::Slider)
				changed = ImGui::SliderFloat2(id.c_str(), glm::value_ptr(value), min, max);
			else
				changed = ImGui::DragFloat2(id.c_str(), glm::value_ptr(value), 1.0f, min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return changed;
		}

		b8 Control(const std::string& name, glm::vec3& value, ControlType type)
		{
			return Control(name, value, -1.0f, 1.0f, type);
		}

		b8 Control(const std::string& name, glm::vec3& value, f32 min, f32 max, ControlType type)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			bool changed = false;
			if ((u32)type & (u32)ControlType::Color)
				changed = ImGui::ColorEdit3(id.c_str(), glm::value_ptr(value), ImGuiColorEditFlags_NoInputs);
			else if (type == ControlType::Slider)
				changed = ImGui::SliderFloat3(id.c_str(), glm::value_ptr(value), min, max);
			else
				changed = ImGui::DragFloat3(id.c_str(), glm::value_ptr(value), 1.0f, min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return changed;
		}

		b8 Control(const std::string& name, glm::vec4& value, ControlType type)
		{
			return Control(name, value, -1.0f, 1.0f, type);
		}

		b8 Control(const std::string& name, glm::vec4& value, f32 min, f32 max, ControlType type)
		{
			ImGui::Text(name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			std::string id = "##" + name;
			bool changed = false;
			if ((int)type & (int)ControlType::Color)
				changed = ImGui::ColorEdit4(id.c_str(), glm::value_ptr(value), ImGuiColorEditFlags_NoInputs);
			else if (type == ControlType::Slider)
				changed = ImGui::SliderFloat4(id.c_str(), glm::value_ptr(value), min, max);
			else
				changed = ImGui::DragFloat4(id.c_str(), glm::value_ptr(value), 1.0f, min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

			return changed;
		}

		void TextCenter(const std::string& text)
		{
			ImGui::CalcTextSize(text.c_str());
			f32 textWidth = ImGui::CalcTextSize(text.c_str()).x;
			ImGui::SameLine((ImGui::GetWindowSize().x - textWidth) / 2.0f);
			ImGui::Text(text.c_str());
		}

		void CenterWidget(f32 widgetWidth)
		{
			ImGui::SameLine((ImGui::GetWindowSize().x - widgetWidth) / 2.0f);
		}
	}
}