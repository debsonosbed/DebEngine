#pragma once

#include "DebEngine.h"

#include "DebEngine/Graphics/Mesh.h"

namespace DebEditor
{
	namespace Utils
	{
		using namespace DebEngine;

		void Vector3Control(const std::string& label, glm::vec3& vec, f32 defaultValue = 0.f, f32 minVal = 0.f, f32 maxVal = 0.f, f32 columnWidth = 80.0f);
		void MeshContent(std::shared_ptr<Mesh>& mesh);
		void MaterialContent(std::shared_ptr<Material>& mesh);

		enum class ControlType
		{
			None	= 0, 
			Color	= 1, 
			Drag	= 2, 
			Slider	= 4
		};


		// ImGui UI helpers
		b8 Control(const std::string& name, const char* value);
		b8 Control(const std::string& name, const std::string& value);
		b8 Control(const std::string& name, b8& value);
		b8 Control(const std::string& name, f32& value, f32 min = -1.0f, f32 max = 1.0f, ControlType type = ControlType::None);
		b8 Control(const std::string& name, glm::vec2& value, ControlType type);
		b8 Control(const std::string& name, glm::vec2& value, f32 min = -1.0f, f32 max = 1.0f, ControlType type = ControlType::None);
		b8 Control(const std::string& name, glm::vec3& value, ControlType type);
		b8 Control(const std::string& name, glm::vec3& value, f32 min = -1.0f, f32 max = 1.0f, ControlType type = ControlType::None);
		b8 Control(const std::string& name, glm::vec4& value, ControlType type);
		b8 Control(const std::string& name, glm::vec4& value, f32 min = -1.0f, f32 max = 1.0f, ControlType type = ControlType::None);

		void TextCenter(const std::string& text);
		void CenterWidget(f32 widgetWidth);
	}
}