#pragma once

#include "DebEngine.h"

#include "DebEngine/Graphics/Material.h"
#include "DebEngine/Graphics/Shader.h"
#include "DebEngine/Graphics/Texture.h"
#include "DebEngine/Graphics/Mesh.h"
#include "DebEngine/Editor/EditorCamera.h"
#include "DebEngine/Gameplay/Scene.h"
#include "DebEngine/Gameplay/Entity.h"

#include <imgui/imgui.h>

#include "DebEngine/ImGui/ImGuizmo.h"

#include "Panels/SceneGraphPanel.h"
#include "Panels/AssetsManagerPanel.h"
#include "Panels/PropertiesPanel.h"
#include "Panels/ConsolePanel.h"
#include "Panels/CodeEditorPanel.h"
#include "Panels/SceneViewportPanel.h"
#include "Panels/RuntimeViewportPanel.h"

#include "Bars/MenuBar.h"
#include "Bars/StatusBar.h"

using namespace DebEngine;

namespace DebEditor
{
#ifndef DEB_DIST
	class EditorLayer : public Layer
	{
	public:
		EditorLayer(Scene* scene);
		~EditorLayer() = default;

		void OnAttach() override;
		void OnDetach() override;
		void OnUpdate() override;
		void OnEvent(Event& event) override;
		void OnImGuiRender() override;
	private:
		EditorCamera m_EditorCamera;
		Scene* m_ActiveScene;

		ImGuizmo::OPERATION m_CurrentGizmoOperation = ImGuizmo::OPERATION::TRANSLATE;

		std::shared_ptr<Material> m_Material;
		Entity m_Gun;
		Entity m_Cube;
		Entity m_DirectionalLight;
		Entity m_Camera;
		Entity m_Sprite;
		std::vector<Entity> m_CubesContainer;

		ImFont* m_FontRoboto = nullptr;

		SceneGraphPanel m_SceneGraphPanel;
		AssetsManagerPanel m_AssetsManagerPanel;
		PropertiesPanel m_PropertiesPanel;
		ConsolePanel m_ConsolePanel;
		CodeEditorPanel m_CodeEditorPanel;
		SceneViewportPanel m_SceneViewportPanel;
		RuntimeViewportPanel m_RuntimeViewportPanel;
		MenuBar m_MenuBar;
		StatusBar m_StatusBar;
	};
#else
	class EditorLayer : public Layer
	{
	public:
		EditorLayer(Scene* scene) {};
		~EditorLayer() = default;

		void OnAttach() override {};
		void OnDetach() override {};
		void OnUpdate() override {};
		void OnEvent(Event& event) override {};
		void OnImGuiRender() override {};
	};
#endif
}
