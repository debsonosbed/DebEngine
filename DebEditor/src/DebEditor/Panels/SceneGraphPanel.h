#pragma once

#include "DebEngine/Gameplay/Scene.h"
#include "DebEngine/Gameplay/Entity.h"


namespace DebEditor
{
	using namespace DebEngine;

	class SceneGraphPanel
	{
	public:
		SceneGraphPanel() = default;
		SceneGraphPanel(Scene* scene);

		void OnImGuiRender();
	private:
		Scene* m_Scene;
		u32 m_ViewportWidth = 0;
		u32 m_ViewportHeight = 0;

		Entity m_HoveredEntity{};
	};
}

