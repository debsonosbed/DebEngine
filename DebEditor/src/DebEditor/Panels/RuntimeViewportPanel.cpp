#include "RuntimeViewportPanel.h"

#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Graphics/SceneRenderer.h"

#include "imgui/imgui.h"

namespace DebEditor
{

	RuntimeViewportPanel::RuntimeViewportPanel(Scene* scene)
		: m_Scene(scene)
	{

	}

	void RuntimeViewportPanel::OnImGuiRender()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::SetNextWindowViewport(ImGui::GetMainViewport()->ID);
		m_Scene->GetOptions().DisplayRuntimeViewport = ImGui::Begin("Game Viewport");

		if (m_Scene->GetOptions().DisplayRuntimeViewport)
		{
			if (Input::IsKeyPressed(KeyCode::F11))
			{
				Application::Get().GetWindow().TrapMouse(true);
			}
			if (Input::IsKeyPressed(KeyCode::ESCAPE))
			{
				Application::Get().GetWindow().TrapMouse(false);
			}

			auto viewportSize = ImGui::GetContentRegionAvail();
			if (m_ViewportWidth != viewportSize.x || m_ViewportHeight != viewportSize.y)
			{
				m_ViewportWidth = viewportSize.x;
				m_ViewportHeight = viewportSize.y;

				SceneRenderer::SetRuntimeViewportSize(m_ViewportWidth, m_ViewportHeight);
			}

			// Frame: 0 - first ImGui render
			// Frame: 1 = initial resize
			// Frame: 2 - resizing framebuffer that has ImGui texture
			// Frame: 3 - ready to display
			// Didn't figure out yet, why it needs to be done on the initialization.
			// After viewport resize for the next frame ImGui texture should not be drawn
			// But why it is not an issue after first 2 frames?
			if (m_InitialFrameCount > 2)
			{
				ImTextureID runtimeTextureId = SceneRenderer::GetRuntimeImGuiTextureID();
				ImGui::Image(runtimeTextureId, viewportSize);
			}
			else
			{
				m_InitialFrameCount++;
			}
		}

		ImGui::End();
		ImGui::PopStyleVar();
	}
}