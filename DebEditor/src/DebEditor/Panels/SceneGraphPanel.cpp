#include "SceneGraphPanel.h"

#include "DebEngine.h"

#include "imgui/imgui.h"
#include <imgui/imgui_internal.h>
#include "DebEngine/Graphics/Renderer.h"

namespace DebEditor
{
	SceneGraphPanel::SceneGraphPanel(Scene* scene)
		: m_Scene(scene)
	{

	}

	void SceneGraphPanel::OnImGuiRender()
	{
		ImGui::Begin("Scene Graph");

		auto& selectedEntity = m_Scene->GetSelectedEntity();

		b8 isAnyEntityHovered = false;
		m_Scene->m_Registry.each([&](auto entityId)
			{
				Entity entity = { entityId, m_Scene };
				auto& tc = entity.GetComponent<TagComponent>();

				ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | (selectedEntity == entity ? ImGuiTreeNodeFlags_Selected : 0);
				b8 isNodeOpened = ImGui::TreeNodeEx((void*)(u64)(u32)entityId, flags, tc.Tag.c_str());
				if (ImGui::IsItemClicked())
				{
					m_Scene->SetSelectedEntity(entity);
				}

				if (ImGui::IsItemHovered())
				{
					isAnyEntityHovered = true;
					m_HoveredEntity = entity;
				}

				if (isNodeOpened) ImGui::TreePop();
			});

		ImGuiPopupFlags popupFlags = ImGuiPopupFlags_None;
		if (ImGui::BeginPopupContextWindow("SceneGraphPopup"))
		{
			b8 selected = false;
			if (m_HoveredEntity) m_Scene->SetSelectedEntity(m_HoveredEntity);

			if (ImGui::MenuItem("Copy"))
			{

			}

			if (ImGui::MenuItem("Paste"))
			{

			}

			ImGui::Separator();

			if (ImGui::MenuItem("Rename", 0, &selected, (b8)m_HoveredEntity))
			{

			}

			if (ImGui::MenuItem("Duplicate", 0, &selected, (b8)m_HoveredEntity))
			{

			}

			if (ImGui::MenuItem("Delete", 0, &selected, (b8)m_HoveredEntity))
			{
				// Delete entity after it's been rendered to avoid submitting empty data for render
				auto hoveredEntity = m_HoveredEntity;
				Renderer::Submit([this, hoveredEntity]()
					{
						m_Scene->DeleteEntity(hoveredEntity);
						m_Scene->SetSelectedEntity({});
					});
			}

			ImGui::Separator();

			if (ImGui::MenuItem("Create Empty"))
			{
				Entity entity = m_Scene->CreateEntity("Entity");
			}

			if (ImGui::BeginMenu("3D Object"))
			{
				if (ImGui::MenuItem("Cube"))
				{
					Entity cube = m_Scene->CreateEntity("Cube");
					cube.AddComponent<MeshComponent>("assets/models/cube.fbx");

					m_Scene->SetSelectedEntity(cube);
				}

				if (ImGui::MenuItem("Sphere"))
				{
					Entity sphere = m_Scene->CreateEntity("Sphere");
					sphere.AddComponent<MeshComponent>("assets/models/sphere.fbx");

					m_Scene->SetSelectedEntity(sphere);
				}

				if (ImGui::MenuItem("Plane"))
				{
					Entity plane = m_Scene->CreateEntity("Plane");
					plane.AddComponent<MeshComponent>("assets/models/cube.fbx");
					auto& tc = plane.GetComponent<TransformComponent>();
					f32 size = 15.0f;
					tc.Scale = glm::vec3(size, 0.1f, size);

					m_Scene->SetSelectedEntity(plane);
				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("2D Object"))
			{
				if (ImGui::MenuItem("Sprite"))
				{

				}

				ImGui::EndMenu();
			}

			ImGui::EndPopup();
		}
		else if (!isAnyEntityHovered)
		{
			m_HoveredEntity = {};
		}

		ImGui::End();
	}
}