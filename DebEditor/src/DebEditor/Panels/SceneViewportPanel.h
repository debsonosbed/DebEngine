#pragma once

#include "DebEngine/Gameplay/Scene.h"

#include "DebEngine/ImGui/ImGuizmo.h"

namespace DebEditor
{
	using namespace DebEngine;

	class SceneViewportPanel
	{
	public:
		SceneViewportPanel() = default;
		SceneViewportPanel(Scene* scene, EditorCamera& editorCamera);

		void OnImGuiRender();
		void SetOnViewportSizeChangedCallback(std::function<void(u32, u32)> callback);
		b8 IsWindowHovered() const;
	public:
		Scene* m_Scene;
		EditorCamera* m_EditorCamera;
		u32 m_ViewportWidth = 0;
		u32 m_ViewportHeight = 0;

		u32 m_InitialFrameCount = 0;

		b8 m_IsWindowHovered = false;

		std::function<void(u32, u32)> m_ViewportSizeChangedCallback;
		ImGuizmo::OPERATION m_CurrentGizmoOperation = ImGuizmo::OPERATION::TRANSLATE;
	};
}

