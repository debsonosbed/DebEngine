#pragma once

#include "DebEngine/Gameplay/Scene.h"


namespace DebEditor
{
	using namespace DebEngine;

	class RuntimeViewportPanel
	{
	public:
		RuntimeViewportPanel() = default;
		RuntimeViewportPanel(Scene* scene);

		void OnImGuiRender();
	public:
		Scene* m_Scene;
		u32 m_ViewportWidth = 0;
		u32 m_ViewportHeight = 0;

		u32 m_InitialFrameCount = 0;
		b8 m_IsCapturingMouseEnabled = false;
		b8 m_MouseCapturingInterrupted = false;
	};
}

