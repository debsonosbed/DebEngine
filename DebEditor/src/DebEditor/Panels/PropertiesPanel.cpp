﻿#include "PropertiesPanel.h"

#include "DebEngine/Gameplay/Entity.h"
#include "DebEngine/Gameplay/Components.h"
#include "DebEngine/Core/Filesystem.h"

#include "entt.hpp"

#include "DebEditor/Utils/EditorUtils.h"
#include <glm/gtx/matrix_decompose.hpp>

#include <filesystem>

namespace DebEditor
{
	using namespace Utils;

	PropertiesPanel::PropertiesPanel(Scene* scene)
		: m_Scene(scene)
	{
		// Build list of available components
	}

	void PropertiesPanel::OnImGuiRender()
	{
		// TODO: Replace use of columns with tables....


		ImGui::Begin("Properties");

		auto& selectedEntity = m_Scene->GetSelectedEntity();
		if (selectedEntity)
		{
			if (selectedEntity.HasComponent<TagComponent>())
			{
				auto& tc = selectedEntity.GetComponent<TagComponent>();

				Buffer buffer(nullptr, 256);
				buffer.Write((u8*)tc.Tag.c_str(), tc.Tag.size());
				if (ImGui::InputText("Tag", (char*)buffer.Data, buffer.Size))
				{
					tc.Tag = std::string((char*)buffer.Data);
				}
				buffer.Clear();
			}

			if (selectedEntity.HasComponent<TransformComponent>())
			{
				DrawComponentNode("Transform", m_IsTransformTreeOpenedInitial, [](Entity& selectedEntity)
					{
						auto& tc = selectedEntity.GetComponent<TransformComponent>();

						glm::vec3 scale, position, skew;
						glm::vec4 perspective;
						glm::quat orientation;

						glm::decompose(tc.GetTransform(), scale, orientation, position, skew, perspective);

						Utils::Vector3Control("Translation", position, 0.0f, 70.0f);
						glm::vec3 rotation = glm::degrees(glm::eulerAngles(orientation));
						Utils::Vector3Control("Rotation", rotation, 0.0f, 70.0f);
						Utils::Vector3Control("Scale", scale, 1.0f, 70.0f);

						if (scale.x <= 0) scale.x = 0.01f;
						if (scale.y <= 0) scale.y = 0.01f;
						if (scale.z <= 0) scale.z = 0.01f;

						tc.Position = position;
						tc.Rotation = glm::radians(rotation);
						tc.Scale = scale;
					}, GlobalComponentsData.Components.at(TransformComponent::GetName()));
			}

			if (selectedEntity.HasComponent<CameraComponent>())
			{
				DrawComponentNode("Camera", m_IsCameraTreeOpenedInitial, [](Entity& selectedEntity)
					{
						auto& cc = selectedEntity.GetComponent<CameraComponent>();

						ImGui::Checkbox("Primary", &cc.IsMainCamera);
					}, GlobalComponentsData.Components.at(CameraComponent::GetName()));
			}

			if (selectedEntity.HasComponent<SpriteRendererComponent>())
			{
				DrawComponentNode("Sprite Renderer", m_IsSpriteRendererTreeOpenedInitial, [](Entity& selectedEntity)
					{
						auto& src = selectedEntity.GetComponent<SpriteRendererComponent>();

					}, GlobalComponentsData.Components.at(SpriteRendererComponent::GetName()));
			}

			if (selectedEntity.HasComponent<MeshComponent>())
			{
				DrawComponentNode("Mesh", m_IsMeshTreeOpenedInitial, [&](Entity& selectedEntity)
					{
						auto& mc = selectedEntity.GetComponent<MeshComponent>();

						ImGui::Columns(3, nullptr, false);

						ImGui::SetColumnWidth(0, 70);
						ImGui::Text("File Path");

						ImGui::NextColumn();
						ImGui::SetColumnWidth(1, 170);
						ImGui::PushItemWidth(-1);
						if (mc.Mesh)
							ImGui::InputText("##meshfilepath", (char*)mc.Mesh->GetPath().c_str(), 256, ImGuiInputTextFlags_ReadOnly);
						else
							ImGui::InputText("##meshfilepath", (char*)"Null", 256, ImGuiInputTextFlags_ReadOnly);
						ImGui::PopItemWidth();

						ImGui::NextColumn();
						ImGui::SetColumnWidth(2, 40);
						if (ImGui::Button("...##openmesh"))
						{
							auto meshPath = Filesystem::OpenFileDialog(std::filesystem::current_path().string() + "\\assets\\models\\", "Mesh Files (*.fbx, *.obj)\0*.fbx;*.obj\0");
							if (!meshPath.empty())
							{
								// For now assume that every path is a valid one...
								selectedEntity.RemoveComponent<MeshComponent>();
								selectedEntity.AddComponent<MeshComponent>(meshPath);
							}
						}

						if (mc.Mesh)
						{
							ImGui::Columns(2, nullptr, false);
							ImGui::AlignTextToFramePadding();
							ImGui::SetColumnWidth(0, 70.0f);

							auto meshColor = mc.Mesh->GetColor();
							if (Control("Color", meshColor, ControlType::Color))
							{
								mc.Mesh->SetColor(meshColor);
							}

							auto meshRoughness = mc.Mesh->GetRoughness();
							if (Control("Roughness", meshRoughness, 0.0f, 1.0f, ControlType::Slider))
							{
								mc.Mesh->SetRoughness(meshRoughness);
							}

							auto meshMetalness = mc.Mesh->GetMetalness();
							if (Control("Metalness", meshMetalness, 0.0f, 1.0f, ControlType::Slider))
							{
								mc.Mesh->SetMetalness(meshMetalness);
							}
						}

						ImGui::Columns(1);

						/*auto& material = mc.Mesh->GetMaterial();
						if (material != nullptr)
						{
							ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | (m_IsMaterialsTreeOpenedInitial ? ImGuiTreeNodeFlags_DefaultOpen : 0);
							b8 isMaterialNodeOpened = ImGui::TreeNodeEx("Materials", flags, "Materials");

							if (isMaterialNodeOpened)
							{
								ImGui::Columns(2, 0, false);
								ImGui::SetColumnWidth(0, 70);
								ImGui::Text("Material");

								ImGui::NextColumn();
								ImGui::SetColumnWidth(1, 100);
								ImGui::PushItemWidth(-1);

								ImGui::InputText("##materialattachment", (char*)material->GetName().c_str(), 256, ImGuiInputTextFlags_ReadOnly);

								ImGui::PopItemWidth();

								ImGui::TreePop();
							}
						}*/
					}, GlobalComponentsData.Components.at(MeshComponent::GetName()));
			}

			if (selectedEntity.HasComponent<NativeScriptComponent>())
			{
				DrawComponentNode("Script", m_IsScriptTreeOpenedInitial, [&](Entity& selectedEntity)
					{
						auto& nsc = selectedEntity.GetComponent<NativeScriptComponent>();
						Control("Name: ", nsc.GetScriptName(&nsc));

					}, GlobalComponentsData.Components.at(NativeScriptComponent::GetName()));
			}

			// Add component button
			{
				auto cursorPos = ImGui::GetCursorPos();
				auto windowSize = ImGui::GetWindowSize();
				f32 buttonMarginTop = 15.0f;
				f32 buttonMarginSide = 20.0f;
				ImVec2 buttonSize = { 150.0f, 25.0f };
				ImVec2 buttonCursorPos = { (windowSize.x - buttonSize.x) / 2.0f, cursorPos.y + buttonMarginTop };
				ImGui::SetCursorPos(buttonCursorPos);

				b8 componentsPopupOpen = ImGui::IsPopupOpen("##componentspopup");
				if (componentsPopupOpen) ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.0f);
				if (ImGui::Button("Add Component", buttonSize))
				{
					ImGui::SetNextWindowPos({ ImGui::GetWindowPos().x + buttonCursorPos.x, ImGui::GetCursorScreenPos().y - buttonSize.y });
					ImGui::OpenPopup("##componentspopup");
				}
				if (componentsPopupOpen) ImGui::PopStyleVar();

				ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
				if (ImGui::BeginPopup("##componentspopup", ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoScrollbar))
				{
					ImVec2 popupSize = { buttonSize.x, 150.0f };
					ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.0f);
					ImGui::BeginChildFrame(1, popupSize, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
					{
						ImGui::PopStyleVar();
						//ImGui::SetWindowPos({ windowPos.x + buttonCursorPos.x, windowPos.y + buttonCursorPos.y });
						//ImGui::SetWindowSize({ 300, 300 });

						ImGui::SetCursorPos({ ImGui::GetCursorPos().x , ImGui::GetCursorPos().y + 3.0f });
						ImVec2 p_min = ImGui::GetCursorScreenPos();
						ImVec2 p_max = ImVec2(p_min.x + ImGui::GetContentRegionAvailWidth(), p_min.y + ImGui::GetFrameHeight());
						ImGui::GetWindowDrawList()->AddRectFilled(p_min, p_max, IM_COL32(255, 155, 0, 255), 3.0f, ImDrawCornerFlags_All);
						auto componentsText = "Components";
						auto textSize = ImGui::CalcTextSize(componentsText);
						ImGui::SetCursorPos({ (ImGui::GetContentRegionAvailWidth() - textSize.x) / 2.0f, ImGui::GetCursorPos().y });
						ImGui::Text(componentsText);

						{
							ImGui::SetCursorPos({ ImGui::GetCursorPos().x - 3.0f, ImGui::GetCursorPos().y + 5.0f });
							ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarRounding, 3.0f);
							ImVec2 listSize = { buttonSize.x - 5.0f, ImGui::GetContentRegionAvail().y };
							ImGui::BeginChild("##componentslist", listSize);
							{
								f32 textMarginSide = 5.0f;
								for (const auto& [componentName, componentAccessManager] : GlobalComponentsData.Components)
								{
									if (componentAccessManager.ExistIn(selectedEntity)) continue;

									ImGui::SetCursorPos({ ImGui::GetCursorPos().x + 3.0f, ImGui::GetCursorPos().y });
									if (componentName == NativeScriptComponent::GetName())
									{
										ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(5.0f, 5.0f));
										ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 3.0f);
										if (ImGui::BeginMenu(NativeScriptComponent::GetName().c_str()))
										{
											for (auto& [scriptName, scriptManager] : ScriptRegistry::GetAll())
											{
												if (ImGui::MenuItem(scriptName.c_str()))
												{
													componentAccessManager.AddTo(selectedEntity);
													scriptManager.BindTo(selectedEntity);
													ImGui::CloseCurrentPopup();
												}
											}

											ImGui::EndMenu();
										}
										ImGui::PopStyleVar(2);
									}
									else
									{
										if (ImGui::MenuItem(componentName.c_str()))
										{
											componentAccessManager.AddTo(selectedEntity);

											ImGui::CloseCurrentPopup();
										}
									}
								}
							}
							ImGui::EndChild();
							ImGui::PopStyleVar();
						}
					}
					ImGui::EndChildFrame();
					ImGui::EndPopup();
				}
				ImGui::PopStyleVar();
			}
		}


		ImGui::End();
		//ImGui::PopStyleVar();
	}

	void PropertiesPanel::DrawComponentNode(const std::string& title, b8 isOpenedInitial, NodeContentFunc nodeContentFunc, const ComponentAccessManager& cam)
	{
		auto& selectedEntity = m_Scene->GetSelectedEntity();
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_AllowItemOverlap | (isOpenedInitial ? ImGuiTreeNodeFlags_DefaultOpen : 0);
		b8 isNodeOpened = ImGui::TreeNodeEx(title.c_str(), flags, title.c_str());

		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.0f);
		ImVec2 buttonSize = { 20.0f, 20.0f };
		ImGui::SameLine(ImGui::GetWindowWidth() - 25.0f);
		if (ImGui::Button("+", buttonSize))
		{
			ImGui::OpenPopup("##componentoptions");
		}
		ImGui::PopStyleVar();

		b8 componentRemoved = false;
		if (ImGui::BeginPopup("##componentoptions", ImGuiWindowFlags_NoMove))
		{
			if (ImGui::MenuItem("Remove Component"))
			{
				if (cam.Removable())
				{
					cam.RemoveFrom(selectedEntity);
					componentRemoved = true;
				}
			}

			ImGui::EndPopup();
		}

		if (isNodeOpened)
		{
			if (!componentRemoved)
				nodeContentFunc(selectedEntity);

			ImGui::TreePop();
		}
	}

}