#include "ConsolePanel.h"

#include "DebEngine.h"

#include "imgui/imgui.h"
#include "DebEngine/Core/Utils.h"

namespace DebEditor
{

	ConsolePanel::ConsolePanel(Scene* scene)
		: m_Scene(scene)
	{

	}

	void ConsolePanel::OnImGuiRender()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
		ImGui::Begin("Console");
		{
			auto& logLines = Logger::GetLogLines();
			ImGui::PopStyleVar();
			ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
			if (ImGui::Button("Clear"))
			{
				logLines.clear();
			}
			ImGui::SameLine(0.0f, 0.0f);

			b8 shouldScrollBottom = false;
			if (m_ScrollOnNewLog)
			{
				ImVec4 buttonColor = { 0.25f, 0.25f, 0.25f, 1.0f };
				ImVec4 buttonActiveColor = { 0.3f, 0.3f, 0.3f, 1.0f };
				ImGui::PushID("Auto-Scroll");
				ImGui::PushStyleColor(ImGuiCol_Button, buttonColor);
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, buttonColor);
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, buttonActiveColor);
				ImGui::Button("Auto-Scroll");
				if (ImGui::IsItemClicked(0))
				{
					m_ScrollOnNewLog = !m_ScrollOnNewLog;
				}
				ImGui::PopStyleColor(3);
				ImGui::PopID();
			}
			else
			{
				if (ImGui::Button("Auto-Scroll"))
				{
					m_ScrollOnNewLog = true;
					shouldScrollBottom = true;
				}
			}
			ImGui::PopStyleVar();

			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(5, 0));
			ImGui::BeginChild("##ScrollArea", ImVec2(0, 0),false, ImGuiWindowFlags_AlwaysUseWindowPadding);
			{
				ImGui::PopStyleVar();
				b8 logContentChanged = false;
				if (logLines.size() != m_LogLinesCount)
				{
					logContentChanged = true;
					m_LogLinesCount = logLines.size();
				}

				f32 logItemHeight = ImGui::GetTextLineHeightWithSpacing();
				u32 linesCount = logLines.size();
				s32 logLineIndexStart = 0;
				s32 logLineIndexEnd = linesCount;
				ImGui::CalcListClipping(linesCount, logItemHeight, &logLineIndexStart, &logLineIndexEnd);
				ImGui::SetCursorPosY(ImGui::GetCursorPosY() + logLineIndexStart * logItemHeight);

				for (u32 i = logLineIndexStart; i < logLineIndexEnd; i++)
				{
					auto& [logType, logLine] = logLines[i];
					auto logColor = GetLogTypeColor(logType);
					ImGui::PushStyleColor(ImGuiCol_Text, { logColor.r, logColor.g, logColor.b, logColor.a });
					ImGui::Selectable(logLine.c_str());
					ImGui::PopStyleColor();

					if (ImGui::BeginPopupContextItem(std::to_string(i).c_str()))
					{
						if (ImGui::MenuItem("Copy Log"))
						{

						}

						ImGui::EndPopup();
					}
				}
				ImGui::SetCursorPosY(ImGui::GetCursorPosY() + (linesCount - logLineIndexEnd) * logItemHeight);

				if (m_ScrollOnNewLog && (logContentChanged || shouldScrollBottom))
				{
					auto& contentRegion = ImGui::GetContentRegionAvail();
					ImGui::SetScrollY(-contentRegion.y);
				}
			}
			ImGui::EndChild();
		}
		ImGui::End();

	}

	glm::vec4 ConsolePanel::GetLogTypeColor(LogType logType) const
	{
		glm::ivec4 color(255);

		switch (logType)
		{
		case LogType::Trace:
			color = { 255, 255, 255, 255 };
			break;
		case LogType::Info:
			color = { 0, 128, 0, 255 };
			break;
		case LogType::Warn:
			color = { 255, 204, 0, 255 };
			break;
		case LogType::Error:
			color = { 255, 0, 0, 255 };
			break;
		case LogType::Critical:
			color = { 255, 0, 0, 255 };
			break;
		}

		return Utils::FromRGBA(color);
	}

	void ConsolePanel::ScrollBottom()
	{

	}

}