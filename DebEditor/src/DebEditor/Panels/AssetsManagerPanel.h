#pragma once

#include "DebEngine/Gameplay/Scene.h"

#include "imgui/imgui.h"

namespace DebEditor
{
	using namespace DebEngine;

	class AssetsManagerPanel
	{
	public:
		AssetsManagerPanel() = default;
		AssetsManagerPanel(Scene* scene);

		void OnImGuiRender();
	public:
		Scene* m_Scene;
	};
}

