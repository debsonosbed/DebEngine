#pragma once

#include "DebEngine/Gameplay/Scene.h"

namespace DebEditor
{
	using namespace DebEngine;

	class ConsolePanel
	{
	public:
		ConsolePanel() = default;
		ConsolePanel(Scene* scene);

		void OnImGuiRender();
	private:
		glm::vec4 GetLogTypeColor(LogType logType) const;
		void ScrollBottom();
	private:
		Scene* m_Scene;
		u32 m_MaxConsoleDisplayLinesCount = 50;
		b8 m_ScrollOnNewLog = true;

		u32 m_LogLinesCount = 0;
	};
}

