#pragma once

#include "DebEngine/Gameplay/Scene.h"


namespace DebEditor
{
	using namespace DebEngine;

	class CodeEditorPanel
	{
	public:
		CodeEditorPanel() = default;
		CodeEditorPanel(Scene* scene);

		void OnImGuiRender();
	public:
		Scene* m_Scene;
	};
}

