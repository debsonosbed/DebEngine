#pragma once

#include "DebEngine/Gameplay/Scene.h"
#include "DebEngine/Gameplay/Components.h"

namespace DebEditor
{
	using namespace DebEngine;

	class PropertiesPanel
	{
	public:
		PropertiesPanel() = default;
		PropertiesPanel(Scene* scene);

		void OnImGuiRender();
	private:
		typedef std::function<void(Entity&)> NodeContentFunc;

		void DrawComponentNode(const std::string& title, b8 isOpenedInitial, NodeContentFunc nodeContentFunc, const ComponentAccessManager& cam);

	private:
		Scene* m_Scene;
		b8 m_IsTransformTreeOpenedInitial = true;
		b8 m_IsCameraTreeOpenedInitial = true;
		b8 m_IsSpriteRendererTreeOpenedInitial = true;
		b8 m_IsMeshTreeOpenedInitial = true;
		b8 m_IsMaterialsTreeOpenedInitial = true;
		b8 m_IsScriptTreeOpenedInitial = true;
		b8 m_IsAddComponentPopupOpened = false;
	};
}

