#include "SceneViewportPanel.h"

#include "DebEngine.h"
#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Graphics/SceneRenderer.h"
#include "DebEngine/Gameplay/Entity.h"

#include "imgui/imgui.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace DebEditor
{
	SceneViewportPanel::SceneViewportPanel(Scene* scene, EditorCamera& editorCamera)
		: m_Scene(scene)
	{
		m_EditorCamera = &editorCamera;
	}

	void SceneViewportPanel::OnImGuiRender()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
		ImGui::SetNextWindowViewport(ImGui::GetMainViewport()->ID);
		m_Scene->GetOptions().DisplayEditorViewport = ImGui::Begin("Scene Viewport");

		if (m_Scene->GetOptions().DisplayEditorViewport)
		{
			m_IsWindowHovered = ImGui::IsWindowHovered();

			// If button pressed and mouse isnt over any of the gizmos
			if (Input::IsMouseButtonPressed(KeyCode::MouseLeft) && !ImGuizmo::IsOver() && !Input::IsKeyDown(KeyCode::LCTRL))
			{
				// Replace with input get mouse pos?
				auto& mousePos = ImGui::GetMousePos();
				//DEB_LOG("Mouse X: {0} Mouse Y: {1}", mousePos.x, mousePos.y);
				auto& windowPos = ImGui::GetWindowPos();

				auto& windowSize = ImGui::GetWindowSize();

				// Verify only available area
				ImVec2 contentRegionMin = ImGui::GetWindowContentRegionMin();
				f32 relativeMouseX = mousePos.x - windowPos.x - contentRegionMin.x;
				f32 relativeMouseY = mousePos.y - windowPos.y - contentRegionMin.y;
				windowSize.y -= contentRegionMin.y;
				if (relativeMouseX > 0 && relativeMouseX < windowSize.x && relativeMouseY > 0 && relativeMouseY < windowSize.y)
				{
					m_Scene->SelectEntityAt(relativeMouseX, relativeMouseY);
				}
			}

			// Resize camera's projection matrix to the actual viewport size
			auto viewportSize = ImGui::GetContentRegionAvail();
			// should be scene camera if exists
			if (m_ViewportWidth != viewportSize.x || m_ViewportHeight != viewportSize.y)
			{
				m_ViewportWidth = viewportSize.x;
				m_ViewportHeight = viewportSize.y;

				SceneRenderer::SetEditorViewportSize(m_ViewportWidth, m_ViewportHeight);

				m_ViewportSizeChangedCallback(m_ViewportWidth, m_ViewportHeight);
			}

			// Frame: 0 - first ImGui render
			// Frame: 1 = initial resize
			// Frame: 2 - resizing framebuffer that has ImGui texture
			// Frame: 3 - ready to display
			// Didn't figure out yet, why it needs to be done on the initialization.
			// After viewport resize for the next frame ImGui texture should not be drawn
			// But why it is not an issue after first 2 frames?
			if (m_InitialFrameCount > 2)
			{
				// Display editor viewport 
				ImTextureID editorTextureId = SceneRenderer::GetEditorImGuiTextureID();
				ImGui::Image(editorTextureId, viewportSize);
			}
			else
			{
				m_InitialFrameCount++;
			}

			auto selectedEntity = m_Scene->GetSelectedEntity();
			if (selectedEntity)
			{
				ImGuizmo::SetOrthographic(false);
				ImGuizmo::SetDrawlist();

				const auto& windowSize = ImGui::GetWindowSize();
				const auto& windowPos = ImGui::GetWindowPos();
				ImGuizmo::SetRect(windowPos.x, windowPos.y, windowSize.x, windowSize.y);

				const auto& cameraProjectionMatrix = m_EditorCamera->GetProjectionMatrix();
				const auto& cameraViewMatrix = m_EditorCamera->GetViewMatrix();

				auto& entityTC = selectedEntity.GetComponent<TransformComponent>();
				auto entityTransform = entityTC.GetTransform();
		
				if (ImGui::IsWindowFocused() || m_IsWindowHovered)
				{
					if (Input::IsKeyPressed(KeyCode::W)) m_CurrentGizmoOperation = ImGuizmo::OPERATION::TRANSLATE;
					if (Input::IsKeyPressed(KeyCode::E)) m_CurrentGizmoOperation = ImGuizmo::OPERATION::ROTATE;
					if (Input::IsKeyPressed(KeyCode::R)) m_CurrentGizmoOperation = ImGuizmo::OPERATION::SCALE;
				}

				ImGuizmo::Manipulate(glm::value_ptr(cameraViewMatrix), glm::value_ptr(cameraProjectionMatrix),
					m_CurrentGizmoOperation, ImGuizmo::LOCAL, glm::value_ptr(entityTransform));

				if (ImGuizmo::IsUsing())
				{
					glm::vec3 scale, position, skew;
					glm::vec4 perspective;
					glm::quat orientation;

					glm::decompose(entityTransform, scale, orientation, position, skew, perspective);
					glm::vec3 rotation = glm::eulerAngles(orientation);

					entityTC.Position = position;
					entityTC.Rotation = rotation;
					entityTC.Scale = scale;
				}
			}
		}

		ImGui::End();
		ImGui::PopStyleVar();
	}

	void SceneViewportPanel::SetOnViewportSizeChangedCallback(std::function<void(u32, u32)> callback)
	{
		m_ViewportSizeChangedCallback = callback;
	}

	b8 SceneViewportPanel::IsWindowHovered() const
	{
		return m_IsWindowHovered;
	}
}