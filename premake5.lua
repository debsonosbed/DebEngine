workspace "DebEngine"
    architecture "x64"
    startproject "Sandbox"

    configurations
    {
        "Debug",
        "Release",
        "Dist"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["spdlog"] = "DebEngine/vendor/spdlog/include"
IncludeDir["SDL2"] = "DebEngine/vendor/SDL2/include"
IncludeDir["glm"] = "DebEngine/vendor/glm"
IncludeDir["ImGui"] = "DebEngine/vendor/imgui"
IncludeDir["ImGuizmo"] = "DebEngine/vendor/ImGuizmo"
IncludeDir["Vulkan"] = "DebEngine/vendor/Vulkan/include"
IncludeDir["shaderc"] = "DebEngine/vendor/shaderc/include"
IncludeDir["SPIRV_Cross"] = "DebEngine/vendor/SPIRV-Cross"
IncludeDir["Assimp"] = "DebEngine/vendor/assimp/include"
IncludeDir["stb"] = "DebEngine/vendor/stb/include"
IncludeDir["entt"] = "DebEngine/vendor/entt/include"
IncludeDir["yaml"] = "DebEngine/vendor/yaml/include"

-- Project specifics path
LibraryDir = {}
LibraryDir["SDL2"] = "vendor/SDL2/bin/" .. outputdir .. "/SDL2/SDL2.dll"
LibraryDir["Vulkan"] = "vendor/Vulkan/lib/vulkan-1.lib"


LibraryDebugDir = {}
LibraryDebugDir["shaderc"] = "vendor/shaderc/lib/Debug/shaderc.lib"
LibraryDebugDir["shaderc_util"] = "vendor/shaderc/lib/Debug/shaderc_util.lib"
LibraryDebugDir["glslang"] = "vendor/glslang/lib/Debug/glslang.lib"
LibraryDebugDir["glslang_MachineIndependent"] = "vendor/glslang/lib/Debug/MachineIndependent.lib"
LibraryDebugDir["glslang_SPIRV"] = "vendor/glslang/lib/Debug/SPIRV.lib"
LibraryDebugDir["glslang_OGLCompiler"] = "vendor/glslang/lib/Debug/OGLCompiler.lib"
LibraryDebugDir["glslang_OSDependent"] = "vendor/glslang/lib/Debug/OSDependent.lib"
LibraryDebugDir["glslang_GenericCodeGen"] = "vendor/glslang/lib/Debug/GenericCodeGen.lib"
LibraryDebugDir["SPIRV_Tools"] = "vendor/SPIRV-Tools/lib/Debug/SPIRV-Tools.lib"
LibraryDebugDir["SPIRV_Tools_opt"] = "vendor/SPIRV-Tools/lib/Debug/SPIRV-Tools-opt.lib"

LibraryReleaseDir = {}
LibraryReleaseDir["shaderc"] = "vendor/shaderc/lib/Release/shaderc.lib"
LibraryReleaseDir["shaderc_util"] = "vendor/shaderc/lib/Release/shaderc_util.lib"
LibraryReleaseDir["glslang"] = "vendor/glslang/lib/Release/glslang.lib"
LibraryReleaseDir["glslang_MachineIndependent"] = "vendor/glslang/lib/Release/MachineIndependent.lib"
LibraryReleaseDir["glslang_SPIRV"] = "vendor/glslang/lib/Release/SPIRV.lib"
LibraryReleaseDir["glslang_OGLCompiler"] = "vendor/glslang/lib/Release/OGLCompiler.lib"
LibraryReleaseDir["glslang_OSDependent"] = "vendor/glslang/lib/Release/OSDependent.lib"
LibraryReleaseDir["glslang_GenericCodeGen"] = "vendor/glslang/lib/Release/GenericCodeGen.lib"
LibraryReleaseDir["SPIRV_Tools"] = "vendor/SPIRV-Tools/lib/Release/SPIRV-Tools.lib"
LibraryReleaseDir["SPIRV_Tools_opt"] = "vendor/SPIRV-Tools/lib/Release/SPIRV-Tools-opt.lib"


group "Dependencies"
    include "DebEngine/vendor/SDL2"
    include "DebEngine/vendor/imgui"
    include "DebEngine/vendor" -- SPIRV-Cross
    include "DebEngine/vendor/yaml"
group ""

--[[ --------- DebEngine project configuration --------- ]]
group "Core"
    project "DebEngine"
        location "DebEngine"
        kind "StaticLib"
        language "C++"
        cppdialect "C++17"
        staticruntime "on"
    
        targetdir ("bin/" .. outputdir .. "/%{prj.name}")
        objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
        
        pchheader "debpch.h"
        pchsource "%{prj.name}/src/debpch.cpp"
        
        files
        {
            "%{prj.name}/src/**.h",
            "%{prj.name}/src/**.cpp",
            "%{prj.name}/vendor/glm/glm/**.hpp",
            "%{prj.name}/vendor/glm/glm/**.inl",
        }

        defines
        {
            "_CRT_SECURE_NO_WARNINGS"
        }

        includedirs
        {
            "%{prj.name}/src",
            "%{prj.name}/vendor",
            "%{IncludeDir.spdlog}",
            "%{IncludeDir.SDL2}",
            "%{IncludeDir.glm}",
            "%{IncludeDir.ImGui}",
            "%{IncludeDir.ImGuizmo}",
            "%{IncludeDir.Vulkan}",
            "%{IncludeDir.shaderc}",
            "%{IncludeDir.SPIRV_Cross}",
            "%{IncludeDir.Assimp}",
            "%{IncludeDir.stb}",
            "%{IncludeDir.entt}",
            "%{IncludeDir.yaml}"
        }

        flags { "MultiProcessorCompile" }

        filter "system:windows"
            systemversion "latest"

            defines
            {
                "DEB_PLATFORM_WINDOWS",
                "DEB_BUILD_DLL"
            }

            postbuildcommands
            {
                ("{COPY} ../DebEngine/%{LibraryDir.SDL2} ../bin/" .. outputdir .. "/%{prj.name}")
            }

        filter "configurations:Debug"
            defines "DEB_DEBUG"
            runtime "Debug"
            symbols "on"

        links
        {
            "SDL2",
            "ImGui",
            "SPIRV-Cross",
            "yaml",
            "%{LibraryDir.Vulkan}",
            "%{LibraryDebugDir.shaderc}",
            "%{LibraryDebugDir.shaderc_util}",
            "%{LibraryDebugDir.glslang}",
            "%{LibraryDebugDir.glslang_MachineIndependent}",
            "%{LibraryDebugDir.glslang_SPIRV}",
            "%{LibraryDebugDir.glslang_OGLCompiler}",
            "%{LibraryDebugDir.glslang_OSDependent}",
            "%{LibraryDebugDir.glslang_GenericCodeGen}",
            "%{LibraryDebugDir.SPIRV_Tools}",
            "%{LibraryDebugDir.SPIRV_Tools_opt}",
        }

        filter "configurations:Release"
            defines "DEB_RELEASE"
            runtime "Release"
            optimize "on"

        links
        {
            "SDL2",
            "ImGui",
            "SPIRV-Cross",
            "yaml",
            "%{LibraryDir.Vulkan}",
            "%{LibraryReleaseDir.shaderc}",
            "%{LibraryReleaseDir.shaderc_util}",
            "%{LibraryReleaseDir.glslang}",
            "%{LibraryReleaseDir.glslang_MachineIndependent}",
            "%{LibraryReleaseDir.glslang_SPIRV}",
            "%{LibraryReleaseDir.glslang_OGLCompiler}",
            "%{LibraryReleaseDir.glslang_OSDependent}",
            "%{LibraryReleaseDir.glslang_GenericCodeGen}",
            "%{LibraryReleaseDir.SPIRV_Tools}",
            "%{LibraryReleaseDir.SPIRV_Tools_opt}"
        }
            
        filter "configurations:Dist"
            defines "DEB_DIST"
            runtime "Release"
            optimize "on"

        links
        {
            "SDL2",
            "ImGui",
            "SPIRV-Cross",
            "yaml",
            "%{LibraryDir.Vulkan}",
            "%{LibraryReleaseDir.shaderc}",
            "%{LibraryReleaseDir.shaderc_util}",
            "%{LibraryReleaseDir.glslang}",
            "%{LibraryReleaseDir.glslang_MachineIndependent}",
            "%{LibraryReleaseDir.glslang_SPIRV}",
            "%{LibraryReleaseDir.glslang_OGLCompiler}",
            "%{LibraryReleaseDir.glslang_OSDependent}",
            "%{LibraryReleaseDir.glslang_GenericCodeGen}",
            "%{LibraryReleaseDir.SPIRV_Tools}",
            "%{LibraryReleaseDir.SPIRV_Tools_opt}"
        }
group ""

--[[ --------- DebEditor project configuration --------- ]]
group "Tools"
    project "DebEditor"
        location "DebEditor"
        kind "StaticLib"
        language "C++"
        cppdialect "C++17"
        staticruntime "on"

        targetdir ("bin/" .. outputdir .. "/%{prj.name}")
        objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

        files
        {
            "%{prj.name}/src/**.h",
            "%{prj.name}/src/**.cpp",
        }

        includedirs
        {
            "%{prj.name}/src",
            "%{IncludeDir.spdlog}",
            "DebEngine/src",
            "DebEngine/vendor",
            "%{IncludeDir.glm}",
            "%{IncludeDir.entt}",
            "%{IncludeDir.ImGui}"
        }

        links
        {
            "DebEngine"
        }

        flags { "MultiProcessorCompile" }

        filter "system:windows"
            systemversion "latest"

            defines
            {
                "DEB_PLATFORM_WINDOWS",
            }

            --[[
            postbuildcommands
            {
                ("{COPY} ../DebEngine/%{LibraryDir.SDL2} ../bin/" .. outputdir .. "/%{prj.name}")
            }
            --]]

        filter "configurations:Debug"
            defines "DEB_DEBUG"
            runtime "Debug"
            symbols "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Debug/assimp-vc142-mtd.lib"
            }

            --[[
            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Debug/assimp-vc142-mtd.dll" "%{cfg.targetdir}"',
            }
            --]]

        filter "configurations:Release"
            defines "DEB_RELEASE"
            runtime "Release"
            optimize "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.lib"
            }

            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.dll" "%{cfg.targetdir}"',
            }
            
        filter "configurations:Dist"
            defines "DEB_DIST"
            runtime "Release"
            optimize "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.lib"
            }

            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.dll" "%{cfg.targetdir}"',
            }
group ""

group "Test"
    project "Sandbox"
        location "Sandbox"
        kind "ConsoleApp"
        language "C++"
        cppdialect "C++17"
        staticruntime "on"

        targetdir ("bin/" .. outputdir .. "/%{prj.name}")
        objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

        files
        {
            "%{prj.name}/src/**.h",
            "%{prj.name}/src/**.cpp",
        }

        includedirs
        {
            "%{IncludeDir.spdlog}",
            "DebEngine/src",
            "DebEngine/vendor",
            "DebEditor/src",
            "%{IncludeDir.glm}",
            "%{IncludeDir.entt}",
            "%{IncludeDir.ImGui}"
        }

        links
        {
            "DebEngine",
            "DebEditor"
        }

        flags { "MultiProcessorCompile" }

        filter "system:windows"
            systemversion "latest"

            defines
            {
                "DEB_PLATFORM_WINDOWS",
            }

            postbuildcommands
            {
                ("{COPY} ../DebEngine/%{LibraryDir.SDL2} ../bin/" .. outputdir .. "/%{prj.name}")
            }

        filter "configurations:Debug"
            defines "DEB_DEBUG"
            runtime "Debug"
            symbols "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Debug/assimp-vc142-mtd.lib"
            }

            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Debug/assimp-vc142-mtd.dll" "%{cfg.targetdir}"',
            }

        filter "configurations:Release"
            defines "DEB_RELEASE"
            runtime "Release"
            optimize "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.lib"
            }

            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.dll" "%{cfg.targetdir}"',
            }
            
        filter "configurations:Dist"
            defines "DEB_DIST"
            runtime "Release"
            optimize "on"

            links
            {
                "DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.lib"
            }

            postbuildcommands 
            {
                '{COPY} "../DebEngine/vendor/assimp/bin/Release/assimp-vc142-mt.dll" "%{cfg.targetdir}"',
            }
group ""