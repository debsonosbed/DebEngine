#pragma once

// For use by DebEngine apps

/////////////////////////////////////////////////
// Core /////////////////////////////////////////
/////////////////////////////////////////////////
#include "DebEngine/Core/Application.h"
#include "DebEngine/Core/Input/Input.h"
#include "DebEngine/Core/Input/Events/Event.h"
#include "DebEngine/Core/Input/Events/KeyEvent.h"
#include "DebEngine/Core/Input/Events/MouseEvent.h"
#include "DebEngine/Core/Input/Events/WindowEvent.h"
#include "DebEngine/Core/Log.h"
#include "DebEngine/Core/Time.h"
#include "DebEngine/Core/Profiler.h"
#include "DebEngine/Core/Utils.h"
#include "DebEngine/Core/Filesystem.h"
#include "DebEngine/Core/Layer.h"
#include "DebEngine/Core/LayerStack.h"
#include "DebEngine/Core/Statistics.h"
#include "DebEngine/Core/Core.h"
/////////////////////////////////////////////////


/////////////////////////////////////////////////
// Gameplay /////////////////////////////////////
/////////////////////////////////////////////////
#include "DebEngine/Gameplay/Scene.h"
#include "DebEngine/Gameplay/Components.h"
#include "DebEngine/Gameplay/Entity.h"
#include "DebEngine/Gameplay/ScriptableEntity.h"
/////////////////////////////////////////////////


/////////////////////////////////////////////////
// Graphics /////////////////////////////////////
/////////////////////////////////////////////////
#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Graphics/SceneRenderer.h"
#include "DebEngine/Graphics/RendererAPI.h"
#include "DebEngine/Graphics/VertexBuffer.h"
#include "DebEngine/Graphics/IndexBuffer.h"
#include "DebEngine/Graphics/RenderPass.h"
#include "DebEngine/Graphics/Framebuffer.h"
#include "DebEngine/Graphics/Pipeline.h"
#include "DebEngine/Graphics/Texture.h"
#include "DebEngine/Graphics/Material.h"
/////////////////////////////////////////////////
