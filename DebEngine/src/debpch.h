#pragma once

#ifdef DEB_PLATFORM_WINDOWS
	#include <Windows.h>
#endif

#include <memory>
#include <functional>
#include <algorithm>
#include <optional>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Log.h"
#include "DebEngine/Core/Input/Events/Event.h"
