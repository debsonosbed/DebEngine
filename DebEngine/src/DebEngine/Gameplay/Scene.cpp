#include "debpch.h"
#include "Scene.h"

#include "Entity.h"

#include "Components.h"
#include "DebEngine/Core/Utils.h"
#include "DebEngine/Core/Input/Events/Event.h"
#include "DebEngine/Core/Input/Events/WindowEvent.h"
#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Graphics/SceneRenderer.h"

#include "glm/glm.hpp"
#include <yaml-cpp/yaml.h>
#include "DebEngine/Vendor/yaml/YamlUtils.h"

#include <filesystem>

namespace DebEngine
{
	Scene::Scene()
	{
		m_Registry.on_construct<MeshComponent>().connect<&Scene::OnMeshComponentConstruct>(this);
		m_Registry.on_destroy<MeshComponent>().connect<&Scene::OnMeshComponentDestroy>(this);

#ifdef DEB_DIST
		Application::Get().GetWindow().TrapMouse(true);
#endif
	}

	Scene::~Scene()
	{
		m_Registry.clear();
	}

	Entity Scene::CreateEntity(const std::string& tag)
	{
		ScheduleRuntimeViewportUpdate();

		Entity entity = { m_Registry.create(), this };

		entity.AddComponent<TagComponent>(tag);
		entity.AddComponent<TransformComponent>();

		m_EntityCount++;

		return entity;
	}

	void Scene::DeleteEntity(Entity entity)
	{
		m_Registry.destroy(entity);

		ScheduleRuntimeViewportUpdate();

		m_EntityCount--;
	}

	void Scene::OnRuntimeStart()
	{
		m_SceneOptions.IsPlaying = true;

		m_Registry.view<NativeScriptComponent>().each([this](auto entity, auto& nsc)
			{
				if (!nsc.Instance->m_Entity)
				{
					nsc.Instance->m_Entity = { entity,  this };
					nsc.Instance->OnCreate();
				}
			});

		// There may be some script that will affect camera/entities position, update the view
		m_UpdateRuntimeViewport = true;
	}

	void Scene::OnRuntimeStop()
	{
		m_SceneOptions.IsPlaying = false;
	}

	void Scene::OnUpdate()
	{
#ifdef DEB_DIST
		if (Input::IsKeyPressed(KeyCode::F11))
		{
			Application::Get().GetWindow().TrapMouse(true);
		}
		if (Input::IsKeyPressed(KeyCode::ESCAPE))
		{
			Application::Get().GetWindow().TrapMouse(false);
		}
#endif

		// For demo purposes scene is playing once the Game Viewport has focus.
#ifndef DEB_DIST
		m_SceneOptions.IsPlaying = m_SceneOptions.DisplayRuntimeViewport;
#else
		m_SceneOptions.IsPlaying = true;
#endif

		if (m_SceneOptions.IsPlaying || m_SceneOptions.PlayScriptsInEditorMode)
		{
			m_Registry.view<NativeScriptComponent>().each([this](auto entity, auto& nsc)
				{
					// Only for testing purposes!
					if (!nsc.Instance->m_Entity)
					{
						nsc.Instance->m_Entity = { entity,  this };
						nsc.Instance->OnCreate();
					}
					else if(nsc.Instance->m_InitialOnCreateRun)
					{
						nsc.Instance->OnCreate();
						nsc.Instance->m_InitialOnCreateRun = false;
					}

					nsc.Instance->OnUpdate();
				});
		}
	}

	void Scene::OnRender()
	{
		SceneRenderer::BeginFrame();

		auto meshTransformGroup = m_Registry.group<MeshComponent>(entt::get<TransformComponent>);
#ifndef DEB_DIST
		if (m_SceneOptions.DisplayEditorViewport)
		{
			m_SceneOptions.EditorPass = true;


			// Render Editor
			{
				// Set current renderpass that will be used for rendering
				m_SceneOptions.CurrentRenderPass = SceneRenderer::GetRenderPassEditor();
				m_SceneOptions.Camera.Position = m_EditorCamera->GetPosition();

				SceneRenderer::BeginScene(this, { *m_EditorCamera, m_EditorCamera->GetViewMatrix() });
				{
					SceneRenderer::SubmitEnvironmentMap();

					if (m_SceneOptions.ShowGrid)
						SceneRenderer::SubmitGrid();

					SceneRenderer::SubmitAllSceneMeshes();
					//SceneRenderer::SubmitAllSceneMeshesNoBatch();

					Entity selectedEntity = Entity{ m_SelectedEntityHandle, this };
					if (selectedEntity && selectedEntity.HasComponent<MeshComponent>())
					{
						auto& mc = selectedEntity.GetComponent<MeshComponent>();
						if (mc.Mesh)
						{
							auto& tc = selectedEntity.GetComponent<TransformComponent>();
							SceneRenderer::SubmitMeshOutline(mc.Mesh, tc);
						}
					}
					// Render outline of selected
				}
				SceneRenderer::EndScene();
			}

			// Object picking scene
			// Order of rendering matters. Scene renderer position buffers are filled after main pass
			// So they can be reused in the object picking pass
			// TODO: Ideally it should be rendered only once - user clicks on the viewport, submit render and check content next frame
			if (m_EntityClickedState.EntitySelectDispatched)
			{
				m_SceneOptions.CurrentRenderPass = SceneRenderer::GetRenderPassObjectPicking();

				SceneRenderer::BeginScene(this, { *m_EditorCamera, m_EditorCamera->GetViewMatrix() });
				{
					SceneRenderer::SubmitAllSceneMeshesObjectPicking();
				}
				SceneRenderer::EndScene();
				m_EntityClickedState.EntitySelectDispatched = false;

				Renderer::SubmitPostFrame([this]()
					{
						auto selectedEntity = FindEntity(m_EntityClickedState.MousePosX, m_EntityClickedState.MousePosY);
						SetSelectedEntity(selectedEntity);
					});
			}
		}
#endif

		// Render runtime
		{
			auto& mainCameraEntity = GetMainCameraEntity();
			if (mainCameraEntity)
			{
#ifndef DEB_DIST
				if ((m_SceneOptions.DisplayRuntimeViewport && m_SceneOptions.IsPlaying) || m_UpdateRuntimeViewport)
#endif
				{
					m_SceneOptions.EditorPass = false;

					glm::mat4 cameraViewMatrix = glm::inverse(mainCameraEntity.GetComponent<TransformComponent>().GetTransform());
					m_MainCamera = (SceneCamera*)&mainCameraEntity.GetComponent<CameraComponent>();
					m_MainCamera->SetViewportSize(m_ViewportWidth, m_ViewportHeight);
					auto& ctc = mainCameraEntity.GetComponent<TransformComponent>();

					// Set current renderpass that will be used for rendering
					m_SceneOptions.CurrentRenderPass = SceneRenderer::GetRenderPassRuntime();
					m_SceneOptions.Camera.Position = ctc.Position;

					SceneRenderer::SubmitEnvironmentMap();

					SceneRenderer::BeginScene(this, { *m_MainCamera, cameraViewMatrix });

					SceneRenderer::SubmitAllSceneMeshes();

					SceneRenderer::EndScene();

					m_UpdateRuntimeViewport = false;
				}
			}
			else if ((m_SceneOptions.DisplayRuntimeViewport && m_SceneOptions.IsPlaying) || m_UpdateRuntimeViewport)
			{
				// Case when no main camera is added to the scene. 
				// In order to display black screen, render an empty scene, using dummy SceneCamera

				m_MainCamera = nullptr;

				m_SceneOptions.EditorPass = false;

				SceneCamera mainCamera;
				mainCamera.SetViewportSize(m_ViewportWidth, m_ViewportHeight);

				// Set current renderpass that will be used for rendering
				m_SceneOptions.CurrentRenderPass = SceneRenderer::GetRenderPassRuntime();
				m_SceneOptions.Camera.Position = glm::vec3(0.0f);

				SceneRenderer::SubmitEnvironmentMap();

				SceneRenderer::BeginScene(this, { mainCamera, glm::mat4(1.0f) });

				SceneRenderer::EndScene();

				m_UpdateRuntimeViewport = false;

				m_FirstFrame = false;
			}
		}

		SceneRenderer::EndFrame();

	}

	void Scene::OnEvent(Event& event)
	{
		// Viewport size is handed by imgui windows if not dist mode
#ifdef DEB_DIST
		EventDispatcher dispatcher(event);
		dispatcher.Dispatch<WindowResizedEvent>(BIND_EVENT_FN(Scene::OnWindowResize));
#endif
	}

	void Scene::SetViewportSize(u32 width, u32 height)
	{
		if (m_ViewportWidth != width || m_ViewportHeight != height)
		{
			m_ViewportWidth = width;
			m_ViewportHeight = height;
		}
	}

	void Scene::SelectEntityAt(u32 mousePosX, u32 mousePosY)
	{
		m_EntityClickedState = { mousePosX, mousePosY, true };
	}

	Entity Scene::FindEntity(u32 mousePosX, u32 mousePosY)
	{
		auto& colorPickingFramebuffer = SceneRenderer::GetRenderPassObjectPicking()->GetTargetFramebuffer();
		auto pixelValue = colorPickingFramebuffer->GetFramebufferColorTexture()->GetPixelValue(mousePosX, mousePosY);

		// Get id from picked up color
		s32 entityId = Utils::GetHexFromRGBA(pixelValue) - 1;

		return { entityId >= 0 ? (entt::entity)entityId : entt::null,  this };
	}

	void Scene::SetSelectedEntity(const Entity& entity)
	{
		m_SelectedEntityHandle = entity;
	}

	Entity Scene::GetSelectedEntity()
	{
		return { m_SelectedEntityHandle, this };
	}

	void Scene::SetEditorCamera(EditorCamera* editorCamera)
	{
		m_EditorCamera = editorCamera;
	}

	EditorCamera* Scene::GetEditorCamera()
	{
		return m_EditorCamera;
	}

	SceneCamera* Scene::GetMainCamera()
	{
		return m_MainCamera;
	}

	void Scene::ScheduleRuntimeViewportUpdate()
	{
		m_UpdateRuntimeViewport = true;
	}

	void Scene::LoadEnvironmentMap(const std::string& hdriTexturePath)
	{
		auto relativePath = std::filesystem::relative(hdriTexturePath).string();
		auto envMap = SceneRenderer::CreateEnvironmentMap(relativePath);

		//if (m_SceneOptions.EnvironmentMap.first) m_SceneOptions.EnvironmentMap.first->Destroy();
		//if (m_SceneOptions.EnvironmentMap.second) m_SceneOptions.EnvironmentMap.second->Destroy();

		m_SceneOptions.EnvironmentMap = envMap;
		m_SceneOptions.EnvProperties.Path = relativePath;

		SceneRenderer::SetEnvironmentMap(envMap);
	}

	EnvironmentProperties& Scene::GetEnvironmentProperties()
	{
		return m_SceneOptions.EnvProperties;
	}

	b8 Scene::LoadFromFile(const std::string& scenePath)
	{
		Destroy();

		std::ifstream stream(scenePath);
		std::stringstream strStream;
		strStream << stream.rdbuf();

		YAML::Node data = YAML::Load(strStream.str());
		if (!data["Scene"])
			return false;

		auto sceneNode = data["Scene"];
		if (sceneNode)
		{
			m_SceneOptions.SceneName = sceneNode["Name"].as<std::string>();
			m_SceneOptions.ShowGrid = sceneNode["ShowGrid"].as<b8>();
		}

		auto entitiesNode = data["Entities"];
		if (entitiesNode)
		{
			for (auto entityNode : entitiesNode)
			{
				Entity::Deserialize(entityNode, this);
			}
		}

		auto environmentNode = data["Environment"];
		if (environmentNode)
		{
			auto& envProperties = m_SceneOptions.EnvProperties;
			auto envMapPath = environmentNode["Path"].as<std::string>();
			if (envMapPath != envProperties.Path)
			{
				LoadEnvironmentMap(envMapPath);
			}
			else {
				envProperties.Path = envMapPath;
			}
			envProperties.LOD = environmentNode["LOD"].as<f32>();
			envProperties.Rotation = environmentNode["Rotation"].as<f32>();
		}

		auto dirLightNode = data["DirectionalLight"];
		if (dirLightNode)
		{
			auto& dirLight = m_SceneOptions.DirectionalLight;
			dirLight.Direction = dirLightNode["Direction"].as<glm::vec3>();
			dirLight.Radiance = dirLightNode["Radiance"].as<glm::vec3>();
			dirLight.Intensity = dirLightNode["Intensity"].as<f32>();
		}

		auto cameraNode = data["Camera"];
		if (cameraNode)
		{
			// TODO
		}

		OnRuntimeStart();

		return true;
	}

	void Scene::SaveToFile(const std::string& scenePath)
	{
		YAML::Emitter outStream;

		outStream << YAML::BeginMap;
		outStream << YAML::Key << "Scene";
		outStream << YAML::BeginMap;
		outStream << YAML::Key << "Name" << YAML::Value << m_SceneOptions.SceneName;
		outStream << YAML::Key << "ShowGrid" << YAML::Value << m_SceneOptions.ShowGrid;
		outStream << YAML::EndMap;
		{
			outStream << YAML::Key << "Entities";
			outStream << YAML::BeginSeq;
			{
				m_Registry.each([&](auto entityId)
					{
						Entity entity = { entityId, this };
						if (!entity) return;

						// Serialize entity
						entity.Serialize(outStream);
					});
			}
			outStream << YAML::EndSeq;

			outStream << YAML::Key << "Environment";
			outStream << YAML::BeginMap;
			outStream << YAML::Key << "Path" << YAML::Value << m_SceneOptions.EnvProperties.Path;
			outStream << YAML::Key << "LOD" << YAML::Value << m_SceneOptions.EnvProperties.LOD;
			outStream << YAML::Key << "Rotation" << YAML::Value << m_SceneOptions.EnvProperties.Rotation;
			outStream << YAML::EndMap;

			outStream << YAML::Key << "DirectionalLight";
			outStream << YAML::BeginMap;
			outStream << YAML::Key << "Direction" << YAML::Value << m_SceneOptions.DirectionalLight.Direction;
			outStream << YAML::Key << "Radiance" << YAML::Value << m_SceneOptions.DirectionalLight.Radiance;
			outStream << YAML::Key << "Intensity" << YAML::Value << m_SceneOptions.DirectionalLight.Intensity;
			outStream << YAML::EndMap;

			outStream << YAML::Key << "Camera";
			outStream << YAML::BeginMap;
			outStream << YAML::EndMap;
		}
		outStream << YAML::EndMap;

		// Save to file
		std::ofstream sceneFile(scenePath);
		sceneFile << outStream.c_str();
	}

	void Scene::LoadDefault()
	{
		Destroy();
		m_SceneOptions.SceneName = "MyNewScene";
		m_SceneOptions.ShowGrid = true;
		m_SceneOptions.DirectionalLight = {};
		auto oldEnvMapPath = m_SceneOptions.EnvProperties.Path;
		m_SceneOptions.EnvProperties = {};

		auto defaultEnvMapPath = std::filesystem::relative("assets/environment/default.hdr").string();
		if (oldEnvMapPath != defaultEnvMapPath) {
			LoadEnvironmentMap(defaultEnvMapPath);
		}
		else
		{
			m_SceneOptions.EnvProperties.Path = defaultEnvMapPath;
		}
	}

	const u32 Scene::GetEntitiesCount() const
	{
		return m_EntityCount;
	}

	SceneOptions& Scene::GetOptions()
	{
		return m_SceneOptions;
	}

	Entity Scene::GetMainCameraEntity()
	{
		auto entityView = m_Registry.view<CameraComponent>();
		for (const auto& entity : entityView)
		{
			auto& cameraComponent = entityView.get<CameraComponent>(entity);
			if (cameraComponent.IsMainCamera)
				return { entity, this };
		}

		return { };
	}

	b8 Scene::OnWindowResize(WindowResizedEvent& event)
	{
		SetViewportSize(event.GetWidth(), event.GetHeight());

		return false;
	}

	void Scene::OnMeshComponentConstruct(entt::registry& registry, entt::entity entityHandle)
	{
		Entity entity = { entityHandle, this };
		auto& mc = entity.GetComponent<MeshComponent>();
		if (!mc.Mesh) return;

		auto meshPath = mc.Mesh->GetPath();
		auto& entitiesContainer = m_EntityMeshPathGrouped[meshPath];
		entitiesContainer.push_back((u32)entityHandle);
		m_EntityMeshLookup.insert_or_assign((u32)entityHandle, mc.Mesh);

		// Allocate new scene renderer buffers. Each mesh (origin mesh loaded) needs to have their own data buffers
		if (SceneRenderer::GlobalData.PerMeshLoadedBuffers.find(meshPath) == SceneRenderer::GlobalData.PerMeshLoadedBuffers.end())
		{
			auto& sceneRendererBuffers = SceneRenderer::GlobalData.PerMeshLoadedBuffers[meshPath];
			SceneRenderer::AllocateNewSceneDataBuffers(sceneRendererBuffers);
		}
	}

	void Scene::OnMeshComponentDestroy(entt::registry& registry, entt::entity entityHandle)
	{
		Entity entity = { entityHandle, this };
		auto& mc = entity.GetComponent<MeshComponent>();
		if (!mc.Mesh) return;

		auto meshPath = mc.Mesh->GetPath();
		auto& entitiesContainer = m_EntityMeshPathGrouped.at(meshPath);
		auto entityPos = std::find(entitiesContainer.begin(), entitiesContainer.end(), (u32)entityHandle);
		if (entityPos != entitiesContainer.end())
		{
			entitiesContainer.erase(entityPos);
		}
		m_EntityMeshLookup.erase((u32)entityHandle);

		if (entitiesContainer.empty())
		{
			m_EntityMeshPathGrouped.erase(meshPath);
		}
	}

	void Scene::Destroy()
	{
		m_Registry.clear();
		m_EntityCount = 0;
		m_MainCamera = nullptr;
	}
}