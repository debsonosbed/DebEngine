#include "debpch.h"

#include "Entity.h"
#include "Components.h"

#include "DebEngine/Vendor/yaml/YamlUtils.h"

namespace DebEngine
{

	Entity::Entity(entt::entity entityHandle, Scene* scene)
		: m_EntityHandle(entityHandle), m_Scene(scene)
	{

	}

	Entity Entity::FindByTagName(const std::string& tag)
	{
		Entity entity = { };
		m_Scene->m_Registry.view<TagComponent>().each([this, tag, &entity](auto enttID, auto& tc)
			{
				if (tc.Tag == tag)
				{
					entity = { enttID, m_Scene };
					return;
				}
			});

		return entity;
	}

	bool Entity::Serialize(YAML::Emitter& outStream)
	{
		outStream << YAML::BeginMap;
		outStream << YAML::Key << "Entity" << YAML::Value << "entityid?";

		if (HasComponent<TagComponent>())
		{
			outStream << YAML::Key << TagComponent::GetName() << YAML::Value;
			outStream << YAML::BeginMap;

			const auto& tc = GetComponent<TagComponent>();
			outStream << YAML::Key << "Tag" << YAML::Value << tc.Tag;

			outStream << YAML::EndMap;
		}

		if (HasComponent<TransformComponent>())
		{
			outStream << YAML::Key << TransformComponent::GetName();
			outStream << YAML::BeginMap;

			const auto& tc = GetComponent<TransformComponent>();
			outStream << YAML::Key << "Position" << YAML::Value << tc.Position;
			outStream << YAML::Key << "Rotation" << YAML::Value << tc.Rotation;
			outStream << YAML::Key << "Scale" << YAML::Value << tc.Scale;

			outStream << YAML::EndMap;
		}

		if (HasComponent<CameraComponent>())
		{
			const auto& cc = GetComponent<CameraComponent>();
			outStream << YAML::Key << CameraComponent::GetName() << YAML::Value;
			outStream << YAML::BeginMap;
			// Serialize camera
			outStream << YAML::Key << "IsPrimary" << YAML::Value << cc.IsMainCamera;
			outStream << YAML::EndMap;
		}

		if (HasComponent<MeshComponent>())
		{
			outStream << YAML::Key << MeshComponent::GetName() << YAML::Value;
			outStream << YAML::BeginMap;

			const auto& mc = GetComponent<MeshComponent>();
			mc.Mesh->Serialize(outStream);

			outStream << YAML::EndMap;
		}

		if (HasComponent<SpriteRendererComponent>())
		{
			outStream << YAML::Key << SpriteRendererComponent::GetName() << YAML::Value;
			outStream << YAML::BeginMap;

			const auto& src = GetComponent<SpriteRendererComponent>();
			src.Material->Serialize(outStream);

			outStream << YAML::EndMap;
		}

		if (HasComponent<NativeScriptComponent>())
		{
			outStream << YAML::Key << NativeScriptComponent::GetName() << YAML::Value;
			outStream << YAML::BeginMap;

			auto& nsc = GetComponent<NativeScriptComponent>();
			outStream << YAML::Key << "ScriptName" << YAML::Value << nsc.GetScriptName(&nsc);

			outStream << YAML::EndMap;
		}

		outStream << YAML::EndMap;

		return true;
	}

	void Entity::Deserialize(YAML::Node& node, Scene* scene)
	{
		std::string tag = "";
		if (node[TagComponent::GetName()])
		{
			auto& componentNode = node[TagComponent::GetName()];
			tag = componentNode["Tag"].as<std::string>();
		}
		Entity entity = scene->CreateEntity(tag);

		if (node[TransformComponent::GetName()])
		{
			auto& componentNode = node[TransformComponent::GetName()];
			auto& tc = entity.GetComponent<TransformComponent>();
			tc.Position = componentNode["Position"].as<glm::vec3>();
			tc.Rotation = componentNode["Rotation"].as<glm::vec3>();
			tc.Scale = componentNode["Scale"].as<glm::vec3>();
		}

		if (node[CameraComponent::GetName()])
		{
			auto& componentNode = node[CameraComponent::GetName()];
			auto& cc = entity.AddComponent<CameraComponent>();
			cc.IsMainCamera = componentNode["IsPrimary"].as<b8>();
			// deserialize camera
		}

		if (node[MeshComponent::GetName()])
		{
			auto& componentNode = node[MeshComponent::GetName()];
			auto mesh = Mesh::Deserialize(componentNode);
			entity.AddComponent<MeshComponent>(mesh);
		}

		if (node[SpriteRendererComponent::GetName()])
		{
			auto& componentNode = node[SpriteRendererComponent::GetName()];
		}

		if (node[NativeScriptComponent::GetName()])
		{
			auto& componentNode = node[NativeScriptComponent::GetName()];
			auto scriptName = componentNode["ScriptName"].as<std::string>();
			if (!scriptName.empty())
			{
				entity.AddComponent<NativeScriptComponent>();
				ScriptRegistry::BindTo(scriptName, entity);
			}
		}
	}
}