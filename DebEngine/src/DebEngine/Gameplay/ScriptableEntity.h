#pragma once

#include "Entity.h"

#define ASSIGN_NAME(x)	static const char* GetScriptName() { return VAR_NAME(x); } \
						virtual const char* GetName() const override { return VAR_NAME(x); };

namespace DebEngine
{
	class ScriptableEntity
	{
		friend class Scene;
	public:
		virtual ~ScriptableEntity() = default;

		template<typename T>
		T& GetComponent()
		{
			return m_Entity.GetComponent<T>();
		}

		Entity FindByTagName(const std::string& tag)
		{
			return m_Entity.FindByTagName(tag);
		}
		
		Scene* GetScene() const
		{
			return m_Entity.m_Scene;
		}

		virtual const char* GetName() const = 0;
	protected:
		virtual void OnCreate() { }
		virtual void OnUpdate() { }
		virtual void OnDestroy() { }
	private:
		Entity m_Entity;
		b8 m_InitialOnCreateRun = true;
	};


	struct ScriptManager
	{
		void (*BindTo)(Entity entity);
	};

	class ScriptRegistry
	{
	public:
		static ScriptManager& Get(const std::string& scriptName)
		{
			DEB_CORE_ASSERT(m_ScriptRegistry.find(scriptName) != m_ScriptRegistry.end(), "Script does not exist in the registry!");
			return m_ScriptRegistry.at(scriptName);
		}

		static void BindTo(const std::string& scriptName, Entity entity)
		{
			DEB_CORE_ASSERT(m_ScriptRegistry.find(scriptName) != m_ScriptRegistry.end(), "Script does not exist in the registry!");
			m_ScriptRegistry.at(scriptName).BindTo(entity);
		}

		static const std::unordered_map<std::string, ScriptManager>& GetAll()
		{
			return m_ScriptRegistry;
		}

		template <typename T>
		static void Add()
		{
			m_ScriptRegistry.insert({ T::GetScriptName(),
				{ [](Entity entity)
					{
						entity.GetComponent<NativeScriptComponent>().Bind<T>();
					}
				} });
		}

	private:
		static std::unordered_map<std::string, ScriptManager> m_ScriptRegistry;
	};

}