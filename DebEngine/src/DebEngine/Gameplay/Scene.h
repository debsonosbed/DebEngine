#pragma once

#include "entt.hpp"

#include "SceneCamera.h"

#include "DebEngine/Editor/EditorCamera.h"
#include "DebEngine/Graphics/RenderPass.h"
#include "DebEngine/Graphics/Texture.h"
#include "DebEngine/Graphics/Mesh.h"

namespace DebEditor
{
	class SceneGraphPanel;
	class PropertiesPanel;
}

namespace DebEngine
{
	class Entity;
	struct TagComponent;
	struct TransformComponent;
	struct CameraComponent;
	struct MeshComponent;
	struct SpriteRendererComponent;
	struct PointLightComponent;
	struct NativeScriptComponent;

	struct EnvironmentProperties
	{
		f32 LOD = 1.0f;
		f32 Rotation = 90.0f;
		std::string Path;
	};

	struct CameraProperties
	{
		glm::vec3 Position;
	};

	struct DirectionalLightProperties
	{
		glm::vec3 Direction = { -1.0f, -0.9f, -0.6f };
		glm::vec3 Radiance = { 1.0f, 1.0f, 1.0f };
		f32 Intensity = 0.2f;
	};

	struct SceneOptions
	{
		std::string SceneName = "MyNewScene";
		std::shared_ptr<RenderPass> CurrentRenderPass;
		b8 DisplayEditorViewport = true;
		b8 DisplayRuntimeViewport = false;
		b8 ShowGrid = true;
		b8 ShowEnvironmentMap = false;
		b8 EditorPass = false;
		DirectionalLightProperties DirectionalLight;
		CameraProperties Camera;
		EnvironmentProperties EnvProperties;
		std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>> EnvironmentMap;
		b8 IsPlaying = false;
		b8 PlayScriptsInEditorMode = false;
	};

	enum class SceneState
	{

	};

	class Scene
	{
	public:
		friend class Entity;
		friend class SceneRenderer;

		// DebEditor friendship
		friend class DebEditor::SceneGraphPanel;
		friend class DebEditor::PropertiesPanel;

		Scene();
		~Scene();

		Entity CreateEntity(const std::string& tag = std::string());
		void DeleteEntity(Entity entity);

		void OnRuntimeStart();

		void OnRuntimeStop();

		/// <summary>
		/// Physics and script update
		/// </summary>
		void OnUpdate();

		void OnRender();

		void OnEvent(Event& event);

		void SetViewportSize(u32 width, u32 height);

		void SelectEntityAt(u32 mousePosX, u32 mousePosY);

		void SetSelectedEntity(const Entity& entity);
		Entity GetSelectedEntity();

		void SetEditorCamera(EditorCamera* editorCamera);
		EditorCamera* GetEditorCamera(); // Shouldnt return raw pointer, review
		SceneCamera* GetMainCamera(); // Shouldnt return raw pointer, review

		void ScheduleRuntimeViewportUpdate();

		void LoadEnvironmentMap(const std::string& hdriTexturePath);
		EnvironmentProperties& GetEnvironmentProperties();

		b8 LoadFromFile(const std::string& scenePath);
		void SaveToFile(const std::string& scenePath);

		void LoadDefault();

		const u32 GetEntitiesCount() const;

		SceneOptions& GetOptions();

	private:
		Entity GetMainCameraEntity();
		b8 OnWindowResize(WindowResizedEvent& event);
		void OnMeshComponentConstruct(entt::registry& registry, entt::entity entityHandle);
		void OnMeshComponentDestroy(entt::registry& registry, entt::entity entityHandle);
		void Destroy();
	private:
		struct EntityClickedState
		{
			u32 MousePosX = 0;
			u32 MousePosY = 0;
			b8 EntitySelectDispatched = false;
		} m_EntityClickedState;
		Entity FindEntity(u32 mousePosX, u32 mousePosY);

		entt::registry m_Registry;
		SceneOptions m_SceneOptions{};
		entt::entity m_SelectedEntityHandle = entt::null;
		entt::entity m_DirLightEntityHandle = entt::null;

		std::unordered_map<std::string, std::vector<u32>> m_EntityMeshPathGrouped;
		std::unordered_map<u32, std::shared_ptr<Mesh>> m_EntityMeshLookup;

		EditorCamera* m_EditorCamera = nullptr;
		SceneCamera* m_MainCamera = nullptr;
		// Rendering to runtime viewport if scene is no playing should be only a few amount of times
		// 1. On initialization
		// 2. On viewport resize
		// Runtime viewport does not have to be up to date with editor's viewport if scene is not playing - just show some preview of the scene.
		b8 m_UpdateRuntimeViewport = true;
		b8 m_InitialScriptRun = true;

		u32 m_EntityCount = 0;

		b8 m_FirstFrame = true;
		u32 m_ViewportWidth = 1280, m_ViewportHeight = 720;
	};
}

