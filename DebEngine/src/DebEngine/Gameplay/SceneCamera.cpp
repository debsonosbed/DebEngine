#include "debpch.h"
#include "SceneCamera.h"

#include "glm/gtc/quaternion.hpp"

namespace DebEngine
{
	SceneCamera::SceneCamera(ProjectionType projectionType) : m_ProjectionType(projectionType)
	{
		//SetCameraProjection();
	}

	void SceneCamera::SetPerspectiveFOV(f32 fov)
	{
		m_PerspectiveFOV = glm::radians(fov);
	}


	f32 SceneCamera::GetPerspectiveFOV() const
	{
		return glm::degrees(m_PerspectiveFOV);
	}

	void SceneCamera::SetPerspectiveNear(f32 perspectiveNear)
	{
		m_PerspectiveNear = perspectiveNear;
	}

	f32 SceneCamera::GetPerspectiveNear() const
	{
		return m_PerspectiveNear;
	}

	void SceneCamera::SetPerspectiveFar(f32 perspectiveFar)
	{
		m_PerspectiveFar = perspectiveFar;
	}

	f32 SceneCamera::GetPerspectiveFar() const
	{
		return m_PerspectiveFar;
	}

	void SceneCamera::SetViewportSize(u32 width, u32 height)
	{
		m_Width = width;
		m_Height = height;

		SetCameraProjection();
	}

	void SceneCamera::SetCameraProjection()
	{
		switch (m_ProjectionType)
		{
		case ProjectionType::Orthographic:
		{
			f32 aspect = m_Width / m_Height;
			f32 width = m_OrthographicSize * aspect;
			f32 height = m_OrthographicSize;

			m_ProjectionMatrix = glm::ortho(-width * 0.5f, width * 0.5f, -height * 0.5f, height * 0.5f);
			break;
		}
		case ProjectionType::Perspective:
		{
			m_ProjectionMatrix = glm::perspectiveFov(m_PerspectiveFOV, m_Width, m_Height, m_PerspectiveNear, m_PerspectiveFar);
			break;
		}
		}
	}

}