#pragma once

#include "Scene.h"

#include "entt.hpp"

namespace YAML
{
	class Emitter;
	class Node;
}

namespace DebEngine
{
	class Entity
	{
		friend class Scene;
		friend class SceneRenderer;
		friend class ScriptableEntity;
		//friend class ComponentsData;

		// DebEditor friendship
		friend class DebEditor::SceneGraphPanel;
		friend class DebEditor::PropertiesPanel;
	public:
		Entity() = default;

		// Only forward args to entity, do not unpack them here.
		template<typename T, typename... Args>
		T& AddComponent(Args&&... args)
		{
			DEB_CORE_ASSERT(HasComponent<T>() == false, "Enitity already has provided component!");

			return m_Scene->m_Registry.emplace<T>(m_EntityHandle, std::forward<Args>(args)...);;
		}

		template<typename T>
		T& GetComponent()
		{
			DEB_CORE_ASSERT(HasComponent<T>() == true, "Entity does not have provided component!");

			return m_Scene->m_Registry.get<T>(m_EntityHandle);
		}

		template<typename T>
		void RemoveComponent()
		{
			DEB_CORE_ASSERT(HasComponent<T>() == true, "Entity does not have provided component!");

			return m_Scene->m_Registry.remove<T>(m_EntityHandle);
		}

		template<typename T>
		b8 HasComponent()
		{
			return m_Scene->m_Registry.has<T>(m_EntityHandle);
		}

		Entity FindByTagName(const std::string& tag);

		/// <summary>
		/// A way to check if entity is valid
		/// </summary>
		operator b8() const {
			if (!m_Scene) return false;
			return m_Scene->m_Registry.valid(m_EntityHandle);
		}
		operator u32() const { return (u32)m_EntityHandle; }
		operator entt::entity() const { return m_EntityHandle; }

		bool operator==(const Entity& other) const
		{
			return m_EntityHandle == other.m_EntityHandle && m_Scene == other.m_Scene;
		}

		bool operator!=(const Entity& other) const
		{
			return !(*this == other);
		}

	protected:
		Scene* m_Scene;

	private:
		Entity(entt::entity entityHandle, Scene* scene);
		bool Serialize(YAML::Emitter& outStream);
		static void Deserialize(YAML::Node& node, Scene* scene);

	private:
		entt::entity m_EntityHandle{ entt::null };
	};
}