#pragma once

#include "DebEngine/Graphics/Camera.h"


namespace DebEngine
{
	class SceneCamera : public Camera
	{
	public:
		SceneCamera(ProjectionType projectionType = ProjectionType::Perspective);

		virtual ~SceneCamera() = default;

		void SetPerspectiveFOV(f32 fov);
		f32 GetPerspectiveFOV() const;

		void SetPerspectiveNear(f32 perspectiveNear);
		f32 GetPerspectiveNear() const;

		void SetPerspectiveFar(f32 perspectiveFar);
		f32 GetPerspectiveFar() const;


		void SetViewportSize(u32 width, u32 height);
	private:
		void SetCameraProjection();
	private:
		ProjectionType m_ProjectionType;
		f32 m_Width = 1280.f, m_Height = 720.f;

		f32 m_PerspectiveFOV = glm::radians(45.f);
		f32 m_PerspectiveNear = 0.1f, m_PerspectiveFar = 10000.f;

		f32 m_OrthographicSize = 10.0f;
		f32 m_OrthographicNear = -1.0f, m_OrthographicFar = 1.0f;
	};
}

