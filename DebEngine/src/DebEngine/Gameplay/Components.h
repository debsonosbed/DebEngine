#pragma once

#include "glm/glm.hpp"
#include <string>

#include "SceneCamera.h"
#include "ScriptableEntity.h"

#include "DebEngine/Graphics/Mesh.h"

#include "glm/gtx/quaternion.hpp"
#include <typeinfo>

namespace DebEngine
{
#define COMPONENT_SPECS(type, removable)	static std::string GetName() { return #type; } \
											static const b8 Removable = removable;

	struct TagComponent
	{
		TagComponent() = default;
		TagComponent(const TagComponent& other) = default;
		TagComponent(const std::string& tag) : Tag(tag) { }

		operator std::string& () { return Tag; }
		operator const std::string& () { return Tag; }

		std::string Tag;

		COMPONENT_SPECS(TagComponent, false)
	};

	struct TransformComponent
	{
		TransformComponent() = default;
		TransformComponent(const TransformComponent& other) = default;
		TransformComponent(const glm::vec3& position) : Position(position) { }

		glm::mat4 GetTransform() const
		{
			glm::mat4 rotation = glm::toMat4(glm::quat(Rotation));

			return glm::translate(glm::mat4(1.0f), Position) * rotation * glm::scale(glm::mat4(1.0f), Scale);
		}

		glm::vec3 Position{ 0.0f };
		glm::vec3 Rotation{ 0.0f };
		glm::vec3 Scale{ 1.f };

		COMPONENT_SPECS(TransformComponent, false)
	};

	struct CameraComponent
	{
		CameraComponent() = default;
		CameraComponent(const CameraComponent& other) = default;
		CameraComponent(const SceneCamera& camera) : Camera(camera) { }

		operator SceneCamera& () { return Camera; }
		operator const SceneCamera& () const { return Camera; }

		SceneCamera Camera;
		b8 IsMainCamera = true;

		COMPONENT_SPECS(CameraComponent, true)
	};

	struct MeshComponent
	{
		MeshComponent() = default;
		MeshComponent(const MeshComponent& other) = default;
		MeshComponent(const std::shared_ptr<DebEngine::Mesh>& mesh) : Mesh(mesh) { }
		MeshComponent(const std::string& path) : Mesh(DebEngine::Mesh::Create(path)) { }

		operator std::shared_ptr<DebEngine::Mesh>& () { return Mesh; }
		operator const std::shared_ptr<DebEngine::Mesh>& () const { return Mesh; }

		std::shared_ptr<DebEngine::Mesh> Mesh;

		COMPONENT_SPECS(MeshComponent, true)
	};

	struct SpriteRendererComponent
	{
		SpriteRendererComponent() = default;
		SpriteRendererComponent(const SpriteRendererComponent& other) = default;
		SpriteRendererComponent(const std::shared_ptr<DebEngine::Material>& material) : Material(material) { }

		std::shared_ptr<DebEngine::Material> Material;

		COMPONENT_SPECS(SpriteRendererComponent, true)
	};
	

	struct NativeScriptComponent
	{
		ScriptableEntity* Instance = nullptr;

		ScriptableEntity* (*InstantiateScript)();
		void (*DestroyScript)(NativeScriptComponent*);
		const char* (*GetScriptName)(NativeScriptComponent* nsc);


		template<typename T>
		void Bind()
		{
			InstantiateScript = []() { return static_cast<ScriptableEntity*>(new T()); };
			DestroyScript = [](NativeScriptComponent* nsc) { delete nsc->Instance; nsc->Instance = nullptr; };
			GetScriptName = [](NativeScriptComponent* nsc) { return nsc->Instance->GetName(); };

			// Ensure that once script has been bound, it's class is also instantiated.
			Instance = InstantiateScript();
		}

		COMPONENT_SPECS(NativeScriptComponent, true)
	};

	struct ComponentAccessManager
	{
		void (*AddTo)(Entity entity);
		void (*RemoveFrom)(Entity entity);
		b8(*ExistIn)(Entity entity);
		b8(*Removable)();
	};

	struct ComponentsData
	{
		std::unordered_map<std::string, ComponentAccessManager> Components =
		{
			{ TagComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<TagComponent>(); },
					[](Entity entity) { entity.RemoveComponent<TagComponent>();  },
					[](Entity entity) { return entity.HasComponent<TagComponent>(); },
					[]() { return TagComponent::Removable; }
				}
			},
			{ TransformComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<TransformComponent>(); },
					[](Entity entity) { entity.RemoveComponent<TransformComponent>();  },
					[](Entity entity) { return entity.HasComponent<TransformComponent>(); },
					[]() { return TransformComponent::Removable; }
				}
			},
			{ CameraComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<CameraComponent>(); },
					[](Entity entity) { entity.RemoveComponent<CameraComponent>();  },
					[](Entity entity) { return entity.HasComponent<CameraComponent>(); },
					[]() { return CameraComponent::Removable; }
				}
			},
			{ MeshComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<MeshComponent>(); },
					[](Entity entity) { entity.RemoveComponent<MeshComponent>();  },
					[](Entity entity) { return entity.HasComponent<MeshComponent>(); },
					[]() { return MeshComponent::Removable; }
				}
			},
			{ SpriteRendererComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<SpriteRendererComponent>(); },
					[](Entity entity) { entity.RemoveComponent<SpriteRendererComponent>();  },
					[](Entity entity) { return entity.HasComponent<SpriteRendererComponent>(); },
					[]() { return SpriteRendererComponent::Removable; }
				}
			},
			{ NativeScriptComponent::GetName(),
				{
					[](Entity entity) { entity.AddComponent<NativeScriptComponent>(); },
					[](Entity entity) { entity.RemoveComponent<NativeScriptComponent>();  },
					[](Entity entity) { return entity.HasComponent<NativeScriptComponent>(); },
					[]() { return NativeScriptComponent::Removable; }
				}
			},
		};
	};

	static const ComponentsData GlobalComponentsData;
}
