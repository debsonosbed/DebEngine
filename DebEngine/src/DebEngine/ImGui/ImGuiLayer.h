#pragma once

#include "DebEngine/Core/Layer.h"
#include "DebEngine/Graphics/CommandBuffer.h"

#include <imgui/imgui.h>

namespace DebEngine 
{

	class ImGuiLayer : public Layer
	{
	public:
		virtual void Begin() = 0;
		virtual void End() = 0;
		virtual void ImGuiRenderPass() = 0;

		static ImGuiLayer* Create();

		static ImFont* GetFont(const std::string& fontName);
		static void AddFont(const std::string& fontName, ImFont* font);
	private:
		static std::unordered_map<std::string, ImFont*> s_FontAtlas;
	};
}

