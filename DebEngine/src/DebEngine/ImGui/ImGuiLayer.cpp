#include "debpch.h"
#include "ImGuiLayer.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanImGuiLayer.h"

namespace DebEngine
{
	std::unordered_map<std::string, ImFont*> ImGuiLayer::s_FontAtlas;

	ImGuiLayer* ImGuiLayer::Create()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "ImGuiLayer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return new VulkanImGuiLayer();
		}

		DEB_CORE_ASSERT(false, "ImGuiLayer::Create -> Unknown RendererAPI!");
		return nullptr;
	}

	ImFont* ImGuiLayer::GetFont(const std::string& fontName)
	{
		DEB_CORE_ASSERT(s_FontAtlas.find(fontName) != s_FontAtlas.end());

		return s_FontAtlas.at(fontName);
	}

	void ImGuiLayer::AddFont(const std::string& fontName, ImFont* font)
	{
		DEB_CORE_ASSERT(font != nullptr);
		s_FontAtlas[fontName] = font;
	}
}