#pragma once

#include "DebEngine/Core/Filesystem.h"
#include "DebEngine/Graphics/Texture.h"
#include "DebEngine/Graphics/Material.h"
#include "DebEngine/Graphics/Pipeline.h"
#include "DebEngine/Graphics/IndexBuffer.h"

#include <glm/glm.hpp>

struct aiScene;
struct aiNode;
struct aiMaterial;
enum aiTextureType;

namespace YAML
{
	class Emitter;
	class Node;
}

namespace Assimp
{
	class Importer;
}

namespace DebEngine
{
	struct Vertex
	{
		glm::vec3 Position;
		glm::vec3 Normal;
		glm::vec2 TexCoord;
		glm::vec3 Tangent;
		glm::vec3 Binormal;
	};

	// Each vertex has 3 indices
	struct Index
	{
		glm::uvec3 Indices;
	};

	struct Submesh
	{
		std::string NodeName;
		std::string MeshName;

		u32 BaseVertex;
		u32 BaseIndex;
		u32 MaterialIndex;
		u32 IndexCount;

		glm::mat4 Transform;
		std::shared_ptr<VertexBuffer> SubmeshVertexBuffer;
		std::shared_ptr<IndexBuffer> SubmeshIndexBuffer;
	};

	struct MaterialPair
	{
		std::shared_ptr<Material> EditorMaterial;
		std::shared_ptr<Material> RuntimeMaterial;
	};

	class Mesh
	{
	public:
		Mesh() = default;
		Mesh(const std::string& path);

		const std::string& GetPath() const;
		std::shared_ptr<Material> GetMaterial() const;
		std::shared_ptr<VertexBuffer> GetVertexBuffer() const;
		std::shared_ptr<IndexBuffer> GetIndexBuffer() const;
		const std::vector<Submesh>& GetSubmeshes() const;
		std::shared_ptr<Material> GetSubmeshEditorMaterial(const Submesh& submesh) const;
		std::shared_ptr<Material> GetSubmeshRuntimeMaterial(const Submesh& submesh) const;

		b8 Serialize(YAML::Emitter& outStream);

		//////////////////////////////////////////////////////////////////////
		// Temporary API, only for engine's demo purposes
		// In later stages each submesh will be a child entity, where these properties will be accessed directly from that submesh's material
		glm::vec4 GetColor() const;
		void SetColor(const glm::vec4& color);
		f32 GetRoughness() const;
		void SetRoughness(f32 roughness);
		f32 GetMetalness() const;
		void SetMetalness(f32 metalness);
		//////////////////////////////////////////////////////////////////////

		static std::shared_ptr<Mesh> Deserialize(const YAML::Node& node);
		static std::shared_ptr<Mesh> Create(const std::string& path);
		static std::shared_ptr<Mesh> Copy(const std::shared_ptr<Mesh> mesh);
	private:
		void TraverseNodes(aiNode* node, const glm::mat4& parentTransform = glm::mat4(1.0f), u32 level = 0);
		std::shared_ptr<Texture2D> LoadTexture(aiTextureType textureType);
		void InitializeMaterialProperties(const MaterialPair& materialPair);
	private:
		std::string m_Path;

		std::unique_ptr<Assimp::Importer> m_AssimpImporter;
		const aiScene* m_AssimpScene;
		aiMaterial* m_CurrentAssimpMaterial = nullptr;

		u32 m_VerticesCount = 0;
		u32 m_IndicesCount = 0;
		std::shared_ptr<Pipeline> m_MeshEditorPipeline;
		std::shared_ptr<Pipeline> m_MeshRuntimePipeline;
		std::shared_ptr<Material> m_MeshMaterial;
		std::shared_ptr<VertexBuffer> m_MeshVertexBuffer;
		std::shared_ptr<IndexBuffer> m_MeshIndexBuffer;

		//////////////////////////////////////////////////////////////////////
		// Temporary, only for engine's demo purposes
		glm::vec4 m_Color;
		f32 m_Roughness;
		f32 m_Metalness = 0.6f;
		//////////////////////////////////////////////////////////////////////

		std::vector<Vertex> m_Vertices;
		std::vector<Index> m_Indices;

		std::vector<MaterialPair> m_Materials;
		std::vector<Submesh> m_Submeshes;
	};

	///////////////////////////////////////////////////////////////////////////////////////
	// MeshLibrary ////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	class MeshLibrary
	{
	public:
		static std::shared_ptr<Mesh> Get(const std::string& path);
		static void Add(const std::shared_ptr<Mesh> mesh);

	private:
		static std::unordered_map<std::string, std::shared_ptr<Mesh>> m_MeshMap;
	};
}

