#pragma once

#include "Framebuffer.h"
#include "RenderPass.h"

namespace DebEngine
{
	struct InheritanceInfo
	{
		std::shared_ptr<Framebuffer> Framebuffer = nullptr;
		std::shared_ptr<RenderPass> RenderPass = nullptr;
	};

	enum class CommandBufferType
	{
		Primary,
		Secondary,
		None
	};

	class CommandBuffer
	{
	public:
		CommandBuffer() = default;
		virtual ~CommandBuffer() = default;
		virtual void BeginRecording() = 0;
		virtual void EndRecording() = 0;
		virtual CommandBufferType GetType() const = 0;
		virtual void ExecuteCommands(std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers) = 0;
		virtual void SetTargetRenderPass(const std::shared_ptr<RenderPass>& renderPass);
		virtual void Flush(b8 endCommandBuffer = true) = 0;

		static std::shared_ptr<CommandBuffer> Create(void* commandBufferHandle);
		static std::shared_ptr<CommandBuffer> CreatePrimary();
		static std::shared_ptr<CommandBuffer> CreateSecondary();

	protected:
		std::shared_ptr<Framebuffer> m_Framenuffer;
		std::shared_ptr<RenderPass> m_RenderPass;
	};
}