#pragma once

#include "DebEngine/Graphics/Shader.h"
#include "DebEngine/Graphics/VertexBuffer.h"
#include "DebEngine/Graphics/RenderPass.h"

namespace DebEngine {

	enum class PipelineTopology {
		PointList,
		LineList,
		TriangleList,
		None
	};

	enum class PolygonMode {
		Fill,
		Line,
		None
	};

	enum class StencilOp
	{
		Keep,
		Zero,
		Replace,
		None
	};

	enum class CompareOp
	{
		Never,
		Less,
		LessOrEqual,
		Greater,
		NotEqual,
		GreaterOrEqual,
		Always,
		None,
	};

	enum class CullMode
	{
		FrontFace,
		BackFace,
		None
	};

	struct StencilOpState
	{
		DebEngine::CompareOp CompareOp = CompareOp::Always;
		StencilOp FailOp = StencilOp::Keep;
		StencilOp DepthFailOP = StencilOp::Keep;
		StencilOp PassOp = StencilOp::Replace;
		u32 CompareMask = 0xFF;
		u32 WriteMask = 0x00;
		u32 Reference = 1;
	};

	struct DepthStenctilState
	{
		StencilOpState Front;
		StencilOpState Back;
		CompareOp DepthCompareOp = CompareOp::LessOrEqual;
		b8 StencilTestEnabled = true;
		b8 DepthTestEnabled = true;
		b8 DepthWriteEnabled = true;
		b8 DepthBoundsTestEnabled = false;
	};

	struct PipelineSpecification
	{
		std::shared_ptr<DebEngine::Shader> Shader;
		std::vector<VertexBufferLayout> Layouts;
		std::shared_ptr<RenderPass> RenderPass;
		PipelineTopology PipelineTopology = PipelineTopology::TriangleList;
		PolygonMode PolygonMode = PolygonMode::Fill;
		u32 CullingEnabled = true;
		DepthStenctilState DepthStencilState{};
		DebEngine::CullMode CullMode = CullMode::None;
		b8 BlendEnabled = true;
	};

	class Pipeline
	{
	public:
		virtual ~Pipeline() = default;

		virtual void Bind() const = 0;
		virtual void Assemble() = 0;

		virtual void Push(const ShaderBufferMember& shaderBufferMember, Buffer& buffer) = 0;
		virtual void Push(const std::shared_ptr<ShaderBuffer> shaderBuffer, Buffer& buffer) = 0;

		virtual std::string GetName() const;

		virtual PipelineSpecification& GetSpecification() = 0;

		static std::shared_ptr<Pipeline> Create(const PipelineSpecification& pipelineSpecication, const std::string& name = "");
	protected:
		std::string m_Name;
	};

	////////////////////////////////////////////////////////////
	/// PipelineLibrary ////////////////////////////////////////
	////////////////////////////////////////////////////////////

	class PipelineLibrary
	{
	public:
		static void Add(const std::shared_ptr<Pipeline>& pipeline, const std::string& name);

		static std::shared_ptr<Pipeline>& Get(const std::string& name);
	private:
		static std::unordered_map<std::string, std::shared_ptr<Pipeline>> m_Pipelines;
	};
}

