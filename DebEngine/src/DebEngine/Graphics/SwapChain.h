#pragma once

#include "CommandBuffer.h"
#include "RenderPass.h"

struct SDL_Window;

namespace DebEngine
{
	class SwapChain
	{
	public:
		virtual ~SwapChain() = default;

		virtual void InitSurface(SDL_Window* windowHandle) = 0;
		virtual void Create(u32* width, u32* height, b8 vsync = false) = 0;
		virtual void BeginFrame() = 0;
		virtual void Present() = 0;
		virtual void OnResize(u32 width, u32 height) = 0;
		virtual u32 GetImageCount() const = 0;
		virtual u32 GetWidth() const = 0;
		virtual u32 GetHeight() const = 0;
		virtual void* GetSurfaceNative() const = 0;
		virtual std::shared_ptr<RenderPass>& GetRenderPass() = 0;

		virtual std::shared_ptr<CommandBuffer>& GetCurrnetDrawCommandBuffer() = 0;
		virtual std::shared_ptr<Framebuffer> GetFramebuffer(u32 index) const = 0;
		virtual std::shared_ptr<Framebuffer> GetCurrentFramebuffer() const = 0;
		virtual void CleanUp() = 0;

		static std::shared_ptr<SwapChain> Create();
	};
}

