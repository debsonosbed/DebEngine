#include "debpch.h"
#include "SwapChain.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanSwapChain.h"

namespace DebEngine
{
	std::shared_ptr<SwapChain> SwapChain::Create()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "SwapChain::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanSwapChain>();
		}

		DEB_CORE_ASSERT(false, "SwapChain::Create -> Unknown RendererAPI!");
		return nullptr;
	}

}