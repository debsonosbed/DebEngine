#pragma once

#include "DebEngine/Core/Core.h"
#include <glm/glm.hpp>

namespace DebEngine
{
	enum class ProjectionType
	{
		Orthographic,
		Perspective
	};

	class Camera
	{
	public:
		Camera() = default;
		Camera(const glm::mat4& projectionMatrix);
		virtual ~Camera() = default;

		virtual glm::mat4 GetProjectionMatrix() const;
		virtual void SetProjectionMatrix(const glm::mat4& projectionMatrix);

		f32 GetExposure() const;
		f32& GetExposure();
	protected:
		glm::mat4 m_ProjectionMatrix = glm::mat4(1.f);
		f32 m_Exposure = 0.8f;
	};
}

