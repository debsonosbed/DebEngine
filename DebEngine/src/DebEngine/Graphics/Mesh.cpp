#include "debpch.h"
#include "Mesh.h"

#include "Renderer.h"

#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/DefaultLogger.hpp>
#include <assimp/LogStream.hpp>

#include <filesystem>

#include <yaml-cpp/yaml.h>

#include "DebEngine/Vendor/yaml/YamlUtils.h"

namespace DebEngine
{
	glm::mat4 ConvertAssimpMat4(const aiMatrix4x4& matrix)
	{
		glm::mat4 result;
		// The a, b, c, d in assimp is the row ; The 1, 2, 3, 4 is the column
		result[0][0] = matrix.a1; result[1][0] = matrix.a2; result[2][0] = matrix.a3; result[3][0] = matrix.a4;
		result[0][1] = matrix.b1; result[1][1] = matrix.b2; result[2][1] = matrix.b3; result[3][1] = matrix.b4;
		result[0][2] = matrix.c1; result[1][2] = matrix.c2; result[2][2] = matrix.c3; result[3][2] = matrix.c4;
		result[0][3] = matrix.d1; result[1][3] = matrix.d2; result[2][3] = matrix.d3; result[3][3] = matrix.d4;
		return result;
	}

	static const uint32_t s_MeshImportFlags =
		aiProcess_CalcTangentSpace |        // Create binormals/tangents just in case
		aiProcess_Triangulate |             // Make sure we're triangles
		aiProcess_SortByPType |             // Split meshes by primitive type
		aiProcess_GenNormals |              // Make sure we have legit normals
		aiProcess_GenUVCoords |             // Convert UVs if required 
		aiProcess_OptimizeMeshes |          // Batch draws where possible
		aiProcess_ValidateDataStructure;    // Validation

	struct LogStream : public Assimp::LogStream
	{
		static void Initialize()
		{
			if (Assimp::DefaultLogger::isNullLogger())
			{
				Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
				Assimp::DefaultLogger::get()->attachStream(new LogStream, Assimp::Logger::Err | Assimp::Logger::Warn);
			}
		}

		virtual void write(const char* message) override
		{
			std::string msg(message);
			msg[msg.size() - 1] = '\0';
			DEB_CORE_ERROR("[Mesh -> Assimp]: {0}", msg);
		}
	};

	Mesh::Mesh(const std::string& meshPath)
	{
		LogStream::Initialize();

		m_Path = meshPath;

		DEB_CORE_LOG("Loading mesh at path: {0}", m_Path);

		m_AssimpImporter = std::make_unique<Assimp::Importer>();

		const aiScene* scene = m_AssimpImporter->ReadFile(m_Path, s_MeshImportFlags);
		if (scene == nullptr || !scene->HasMeshes())
			DEB_CORE_ERROR("Failed to load mesh file at path: {0}", m_Path);

		m_AssimpScene = scene;

		m_MeshEditorPipeline = PipelineLibrary::Get("PBREditor");
		m_MeshRuntimePipeline = PipelineLibrary::Get("PBRRuntime");
		m_MeshMaterial = Material::Create(m_MeshEditorPipeline);

		u32 vertexCount = 0;
		u32 indexCount = 0;

		m_Submeshes.reserve(m_AssimpScene->mNumMeshes);
		for (u32 m = 0; m < m_AssimpScene->mNumMeshes; m++)
		{
			auto mesh = scene->mMeshes[m];

			Submesh& submesh = m_Submeshes.emplace_back();
			submesh.BaseVertex = vertexCount;
			submesh.BaseIndex = indexCount;
			submesh.MaterialIndex = mesh->mMaterialIndex;
			submesh.IndexCount = mesh->mNumFaces * 3;
			submesh.MeshName = mesh->mName.C_Str();

			vertexCount += mesh->mNumVertices;
			indexCount += submesh.IndexCount;

			DEB_CORE_ASSERT(mesh->HasPositions(), "Meshes require positions.");
			DEB_CORE_ASSERT(mesh->HasNormals(), "Meshes require normals.");

			// Vertices
			for (u32 v = 0; v < mesh->mNumVertices; v++)
			{
				Vertex vertex;
				vertex.Position = { mesh->mVertices[v].x, mesh->mVertices[v].y, mesh->mVertices[v].z };
				vertex.Normal = { mesh->mNormals[v].x, mesh->mNormals[v].y, mesh->mNormals[v].z };

				if (mesh->HasTangentsAndBitangents())
				{
					vertex.Tangent = { mesh->mTangents[v].x, mesh->mTangents[v].y,mesh->mTangents[v].z };
					vertex.Binormal = { mesh->mBitangents[v].x, mesh->mBitangents[v].y, mesh->mBitangents[v].z };
				}

				if (mesh->HasTextureCoords(0))
					vertex.TexCoord = { mesh->mTextureCoords[0][v].x, mesh->mTextureCoords[0][v].y };

				m_Vertices.push_back(vertex);
			}

			// Indices
			for (u32 i = 0; i < mesh->mNumFaces; i++)
			{
				DEB_CORE_ASSERT(mesh->mFaces[i].mNumIndices == 3, "Face must have 3 indices.");
				Index index = { {mesh->mFaces[i].mIndices[0],mesh->mFaces[i].mIndices[1], mesh->mFaces[i].mIndices[2] } };
				m_Indices.push_back(index);
			}

			// Create vertex and index buffer for a submesh
			auto submeshVerticesContainer = std::vector(m_Vertices.begin() + submesh.BaseVertex, m_Vertices.end());
			Buffer verticesBuffer = { (u8*)submeshVerticesContainer.data(), u32(submeshVerticesContainer.size() * sizeof(Vertex)) };
			submesh.SubmeshVertexBuffer = VertexBuffer::Create(verticesBuffer);
			auto submeshIndicesContainer = std::vector(m_Indices.begin() + submesh.BaseIndex, m_Indices.end());
			Buffer indicesBuffer = { (u8*)submeshIndicesContainer.data(), u32(submeshIndicesContainer.size() * sizeof(Index)) };
			submesh.SubmeshIndexBuffer = IndexBuffer::Create(indicesBuffer, submeshIndicesContainer.size());
		}

		TraverseNodes(scene->mRootNode);

		if (m_AssimpScene->HasMaterials())
		{
			m_Materials.reserve(m_AssimpScene->mNumMaterials);

			for (u32 i = 0; i < m_AssimpScene->mNumMaterials; i++)
			{
				m_CurrentAssimpMaterial = m_AssimpScene->mMaterials[i];
				auto assimpMaterialName = m_CurrentAssimpMaterial->GetName();

				MaterialPair materialPair = { Material::Create(m_MeshEditorPipeline), Material::Create(m_MeshRuntimePipeline) };
				m_Materials.emplace_back(materialPair);
				InitializeMaterialProperties(materialPair);

				aiColor3D assimpColor;
				m_CurrentAssimpMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, assimpColor);
				m_Color = glm::vec4(assimpColor.r, assimpColor.g, assimpColor.b, 1.0);

				f32 shininess = 80.0f;
				m_CurrentAssimpMaterial->Get(AI_MATKEY_SHININESS, shininess);
				m_CurrentAssimpMaterial->Get(AI_MATKEY_REFLECTIVITY, m_Metalness);

				m_Roughness = 1.0f - glm::sqrt(shininess / 100.0f);

				aiString assimpTexturePath;
				// Assimp texture type order and DebEngine texture type order are the same
				// Once that changes(if) the implementation below needs to be updated
				for (u32 i = (u32)aiTextureType_DIFFUSE; i < (u32)aiTextureType::aiTextureType_UNKNOWN; i++)
				{
					auto textureType = (aiTextureType)i;
					auto texture = LoadTexture(textureType);

					if (texture)
					{
						materialPair.EditorMaterial->SetTexture(texture);
						materialPair.RuntimeMaterial->SetTexture(texture);
					}
					else
					{
						switch ((TextureType)textureType)
						{
						case TextureType::Albedo:
							materialPair.EditorMaterial->SetProperty(MaterialProperty::Color, m_Color);
							materialPair.RuntimeMaterial->SetProperty(MaterialProperty::Color, m_Color);
							break;
						case TextureType::Roughness:
							materialPair.EditorMaterial->SetProperty(MaterialProperty::Roughness, m_Roughness);
							materialPair.RuntimeMaterial->SetProperty(MaterialProperty::Roughness, m_Roughness);
							break;
						case TextureType::Metalness:
							materialPair.EditorMaterial->SetProperty(MaterialProperty::Metalness, m_Metalness);
							materialPair.RuntimeMaterial->SetProperty(MaterialProperty::Metalness, m_Metalness);
							break;
						}
					}
				}
			}
		}

		m_MeshVertexBuffer = VertexBuffer::Create(Buffer((u8*)m_Vertices.data(), m_Vertices.size() * sizeof(Vertex)));
		m_MeshIndexBuffer = IndexBuffer::Create(Buffer((u8*)m_Indices.data(), m_Indices.size() * sizeof(Index)), m_Indices.size());
	}

	const std::string& Mesh::GetPath() const
	{
		return m_Path;
	}

	std::shared_ptr<Material> Mesh::GetMaterial() const
	{
		return m_MeshMaterial;
	}

	std::shared_ptr<VertexBuffer> Mesh::GetVertexBuffer() const
	{
		return m_MeshVertexBuffer;
	}

	std::shared_ptr<IndexBuffer> Mesh::GetIndexBuffer() const
	{
		return m_MeshIndexBuffer;
	}

	const std::vector<Submesh>& Mesh::GetSubmeshes() const
	{
		return m_Submeshes;
	}

	std::shared_ptr<Material> Mesh::GetSubmeshEditorMaterial(const Submesh& submesh) const
	{
		DEB_CORE_ASSERT(submesh.MaterialIndex < m_Materials.size(), "Material for submesh does not exist!");
		return m_Materials[submesh.MaterialIndex].EditorMaterial;
	}

	std::shared_ptr<Material> Mesh::GetSubmeshRuntimeMaterial(const Submesh& submesh) const
	{
		DEB_CORE_ASSERT(submesh.MaterialIndex < m_Materials.size(), "Material for submesh does not exist!");
		return m_Materials[submesh.MaterialIndex].RuntimeMaterial;
	}

	b8 Mesh::Serialize(YAML::Emitter& outStream)
	{
		outStream << YAML::Key << "Mesh" << YAML::Value;
		outStream << YAML::BeginMap;
		{
			outStream << YAML::Key << "Path" << YAML::Value << m_Path;
			outStream << YAML::Key << "Color" << YAML::Value << m_Color;
			outStream << YAML::Key << "Roughness" << YAML::Value << m_Roughness;
			outStream << YAML::Key << "Metalness" << YAML::Value << m_Metalness;
		}
		outStream << YAML::EndMap;

		return true;
	}

	std::shared_ptr<Mesh> Mesh::Deserialize(const YAML::Node& node)
	{
		auto meshNode = node["Mesh"];
		if (meshNode)
		{
			auto meshPath = meshNode["Path"].as<std::string>();
			auto mesh = Mesh::Create(meshPath);
			auto meshColor = meshNode["Color"].as<glm::vec4>();
			mesh->SetColor(meshColor);
			auto meshRoughness = meshNode["Roughness"].as<f32>();
			mesh->SetRoughness(meshRoughness);
			auto meshMetalness = meshNode["Metalness"].as<f32>();
			mesh->SetMetalness(meshMetalness);

			return mesh;
		}

		return nullptr;
	}

	std::shared_ptr<Mesh> Mesh::Create(const std::string& path)
	{
		auto relativePath = std::filesystem::relative(path).string();
		auto mesh = MeshLibrary::Get(relativePath);
		if (mesh)
		{
			auto meshCopy = Mesh::Copy(mesh);
			return meshCopy;
		}

		mesh = std::make_shared<Mesh>(relativePath);
		MeshLibrary::Add(mesh);

		// New loaded mesh has to stay untouched - create copies of it only!
		return Mesh::Copy(mesh);
	}

	std::shared_ptr<Mesh> Mesh::Copy(const std::shared_ptr<Mesh> mesh)
	{
		auto meshCopy = std::make_shared<Mesh>();

		meshCopy->m_Path = mesh->m_Path;
		meshCopy->m_MeshVertexBuffer = mesh->m_MeshVertexBuffer;
		meshCopy->m_MeshIndexBuffer = mesh->m_MeshIndexBuffer;
		meshCopy->m_Submeshes = mesh->m_Submeshes;
		meshCopy->m_VerticesCount = mesh->m_VerticesCount;
		meshCopy->m_IndicesCount = mesh->m_IndicesCount;

		// Temporary stuff, for demo only//////
		meshCopy->m_Color = mesh->m_Color;
		meshCopy->m_Roughness = mesh->m_Roughness;
		meshCopy->m_Metalness = mesh->m_Metalness;
		///////////////////////////////////////

		meshCopy->m_Materials.reserve(mesh->m_Materials.size());
		for (auto& materialPair : mesh->m_Materials)
		{
			meshCopy->m_Materials.emplace_back(MaterialPair{ Material::Copy(materialPair.EditorMaterial), Material::Copy(materialPair.RuntimeMaterial) });
		}

		return meshCopy;
	}

	void Mesh::TraverseNodes(aiNode* node, const glm::mat4& parentTransform, u32 level)
	{
		glm::mat4 transform = parentTransform * ConvertAssimpMat4(node->mTransformation);
		for (uint32_t i = 0; i < node->mNumMeshes; i++)
		{
			uint32_t mesh = node->mMeshes[i];
			auto& submesh = m_Submeshes[mesh];
			submesh.NodeName = node->mName.C_Str();
			submesh.Transform = transform;
		}

		for (uint32_t i = 0; i < node->mNumChildren; i++)
			TraverseNodes(node->mChildren[i], transform, level + 1);
	}

	std::shared_ptr<Texture2D> Mesh::LoadTexture(aiTextureType textureType)
	{
		aiString assimpTexturePath;
		if (m_CurrentAssimpMaterial->GetTexture(textureType, 0, &assimpTexturePath) == AI_SUCCESS)
		{
			std::filesystem::path path = m_Path;
			auto parentPath = path.parent_path();
			parentPath /= std::string(assimpTexturePath.data);
			std::string texturePath = parentPath.string();

			LogCore::Message("Texture loaded at path: {0}", texturePath);

			auto texture = Texture2D::Create(texturePath, (TextureType)textureType);

			return texture;
		}

		return nullptr;
	}

	void Mesh::InitializeMaterialProperties(const MaterialPair& materialPair)
	{
		materialPair.EditorMaterial->Set("TextureToggles.hasAlbedo", 0);
		materialPair.EditorMaterial->Set("TextureToggles.hasNormals", 0);
		materialPair.EditorMaterial->Set("TextureToggles.hasRoughness", 0);
		materialPair.EditorMaterial->Set("TextureToggles.hasMetalness", 0);

		materialPair.RuntimeMaterial->Set("TextureToggles.hasAlbedo", 0);
		materialPair.RuntimeMaterial->Set("TextureToggles.hasNormals", 0);
		materialPair.RuntimeMaterial->Set("TextureToggles.hasRoughness", 0);
		materialPair.RuntimeMaterial->Set("TextureToggles.hasMetalness", 0);
	}

	glm::vec4 Mesh::GetColor() const
	{
		return m_Color;
	}

	void Mesh::SetColor(const glm::vec4& color)
	{
		m_Color = color;
		for (const auto& submesh : m_Submeshes)
		{
			GetSubmeshEditorMaterial(submesh)->SetProperty(MaterialProperty::Color, m_Color);
			GetSubmeshRuntimeMaterial(submesh)->SetProperty(MaterialProperty::Color, m_Color);
		}
	}

	f32 Mesh::GetRoughness() const
	{
		return m_Roughness;
	}

	void Mesh::SetRoughness(f32 roughness)
	{
		m_Roughness = roughness;
		for (const auto& submesh : m_Submeshes)
		{
			GetSubmeshEditorMaterial(submesh)->SetProperty(MaterialProperty::Roughness, m_Roughness);
			GetSubmeshRuntimeMaterial(submesh)->SetProperty(MaterialProperty::Roughness, m_Roughness);
		}
	}

	f32 Mesh::GetMetalness() const
	{
		return m_Metalness;
	}

	void Mesh::SetMetalness(f32 metalness)
	{
		m_Metalness = metalness;
		for (const auto& submesh : m_Submeshes)
		{
			GetSubmeshEditorMaterial(submesh)->SetProperty(MaterialProperty::Metalness, m_Metalness);
			GetSubmeshRuntimeMaterial(submesh)->SetProperty(MaterialProperty::Metalness, m_Metalness);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// MeshLibrary ////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	std::unordered_map<std::string, std::shared_ptr<DebEngine::Mesh>> MeshLibrary::m_MeshMap;

	std::shared_ptr<DebEngine::Mesh> MeshLibrary::Get(const std::string& path)
	{
		if (m_MeshMap.find(path) != m_MeshMap.end())
		{
			return m_MeshMap[path];
		}

		return nullptr;
	}

	void MeshLibrary::Add(const std::shared_ptr<Mesh> mesh)
	{
		m_MeshMap.insert_or_assign(mesh->GetPath(), mesh);
	}
}