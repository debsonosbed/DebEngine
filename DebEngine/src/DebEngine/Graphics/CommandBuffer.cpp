#include "debpch.h"
#include "CommandBuffer.h"

#include "RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanCommandBuffer.h"

namespace DebEngine
{
	void CommandBuffer::SetTargetRenderPass(const std::shared_ptr<RenderPass>& renderPass)
	{
		m_RenderPass = renderPass;
	}

	std::shared_ptr<CommandBuffer> CommandBuffer::Create(void* commandBufferHandle)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanCommandBuffer>(commandBufferHandle); //  should Vulkan RenderPass impl
		}

		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Unknown RendererAPI!");
		return nullptr;
	}

	std::shared_ptr<CommandBuffer> CommandBuffer::CreatePrimary()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanCommandBuffer>(CommandBufferType::Primary); //  should Vulkan RenderPass impl
		}

		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Unknown RendererAPI!");
		return nullptr;
	}

	std::shared_ptr<CommandBuffer> CommandBuffer::CreateSecondary()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanCommandBuffer>(CommandBufferType::Secondary); //  should Vulkan RenderPass impl
		}

		DEB_CORE_ASSERT(false, "CommandBuffer::Create -> Unknown RendererAPI!");
		return nullptr;
	}

}