#include "debpch.h"
#include "Shader.h"

#include "RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanShader.h"

#include <yaml-cpp/yaml.h>
#include <yaml-cpp/binary.h>

namespace DebEngine
{

	std::shared_ptr<Shader> Shader::Create(const std::string& filePath)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Shader::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanShader>(filePath); //  should Vulkan Shader impl
		}

		DEB_CORE_ASSERT(false, "Shader::Create -> Unknown RendererAPI!");
		return nullptr;
	}

	std::shared_ptr<DebEngine::Shader> Shader::CreateFromString(const std::string& source)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Shader::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanShader>(source); // should Vulkan Shader impl
		}

		DEB_CORE_ASSERT(false, "Shader::CreateFromString -> Unknown RendererAPI!");
		return nullptr;
	}

	////////////////////////////////////////////////////////////
	/// ShaderLibrary //////////////////////////////////////////
	////////////////////////////////////////////////////////////

	std::unordered_map<std::string, std::shared_ptr<DebEngine::Shader>> ShaderLibrary::m_Shaders;

	void ShaderLibrary::Add(const std::shared_ptr<Shader>& shader)
	{
		auto& name = shader->GetName();
		DEB_CORE_ASSERT(m_Shaders.find(name) == m_Shaders.end());
		m_Shaders[name] = shader;
	}

	std::shared_ptr<Shader>& ShaderLibrary::Load(const std::string& path)
	{
		auto shader = Shader::Create(path);
		auto& name = shader->GetName();
		DEB_CORE_ASSERT(m_Shaders.find(name) == m_Shaders.end());
		m_Shaders[name] = shader;

		return m_Shaders[name];
	}

	std::shared_ptr<Shader>& ShaderLibrary::Load(const std::string& name, const std::string& path)
	{
		DEB_CORE_ASSERT(m_Shaders.find(name) == m_Shaders.end());
		auto shader = Shader::Create(path);
		m_Shaders[name] = shader;

		return m_Shaders[name];
	}

	std::shared_ptr<DebEngine::Shader>& ShaderLibrary::Get(const std::string& name)
	{
		DEB_CORE_ASSERT(m_Shaders.find(name) != m_Shaders.end());
		return m_Shaders.at(name);
	}

	////////////////////////////////////////////////////////////
	/// ShaderDataDeclaration //////////////////////////////////
	////////////////////////////////////////////////////////////

	ShaderDataDeclaration::ShaderDataDeclaration(ShaderBufferType shaderDataSourceType, const std::string& name)
		: m_ShaderBufferType(shaderDataSourceType), m_Name(name)
	{

	}

	const ShaderBufferType ShaderDataDeclaration::GetShaderBufferType() const
	{
		return m_ShaderBufferType;
	}

	const std::string& ShaderDataDeclaration::GetName() const
	{
		return m_Name;
	}

	Buffer& ShaderDataDeclaration::GetBuffer()
	{
		return m_Buffer;
	}

	const std::shared_ptr<Texture2D>& ShaderDataDeclaration::GetTexture() const
	{
		return m_Texture;
	}

	void ShaderDataDeclaration::SetTexture(const std::shared_ptr<Texture2D>& texture)
	{
		m_Texture = texture;
	}

	const std::shared_ptr<TextureCube>& ShaderDataDeclaration::GetTextureCube() const
	{
		return m_TextureCube;
	}

	void ShaderDataDeclaration::SetTextureCube(const std::shared_ptr<TextureCube>& texture)
	{
		m_TextureCube = texture;
	}

	b8 ShaderDataDeclaration::HasTextureCube() const
	{
		return m_TextureCube != nullptr;
	}

	b8 ShaderDataDeclaration::IsUploaded() const
	{
		return m_Uploaded;
	}

	void ShaderDataDeclaration::SetUploaded(b8 value)
	{
		m_Uploaded = value;
	}

	void ShaderDataDeclaration::SetInitializeImageSampler(b8 val)
	{
		m_InitializeImageSampler = val;
	}

	b8 ShaderDataDeclaration::IsInitializeImageSampler() const
	{
		return m_InitializeImageSampler;
	}

	b8 ShaderDataDeclaration::Serialize(YAML::Emitter& outStream)
	{
		outStream << YAML::Key << "ShaderDataDeclaration" << YAML::Value;
		outStream << YAML::BeginMap;
		{
			outStream << YAML::Key << "Name" << YAML::Value << m_Name;
			outStream << YAML::Key << "ShaderBufferType" << YAML::Value << (u32)m_ShaderBufferType;
			YAML::Binary bufferData(m_Buffer.Data, m_Buffer.Size);
			outStream << YAML::Key << "Buffer" << YAML::Value << bufferData;
		}
		outStream << YAML::EndMap;

		return true;
	}

	ShaderDataDeclaration ShaderDataDeclaration::Deserialize(const YAML::Node& node)
	{
		auto shaderDataDeclNode = node["ShaderDataDeclaration"];
		auto bufferType = (ShaderBufferType)shaderDataDeclNode["ShaderBufferType"].as<u32>();
		auto name = shaderDataDeclNode["Name"].as<std::string>();
		auto shaderDataDecl = ShaderDataDeclaration(bufferType, name);
		YAML::Binary binary = shaderDataDeclNode["Buffer"].as<YAML::Binary>();
		shaderDataDecl.m_Buffer.Allocate(binary.size());
		shaderDataDecl.m_Buffer.Write((u8*)binary.data(), binary.size());

		return shaderDataDecl;
	}

	ShaderDataDeclaration ShaderDataDeclaration::Copy(ShaderDataDeclaration& shaderDataDeclaration)
	{
		ShaderDataDeclaration shaderDataDeclarationCopy{};

		shaderDataDeclarationCopy.m_ShaderBufferType = shaderDataDeclaration.m_ShaderBufferType;
		shaderDataDeclarationCopy.m_Name = shaderDataDeclaration.m_Name;
		shaderDataDeclarationCopy.m_Texture = shaderDataDeclaration.m_Texture; // Will that work?
		shaderDataDeclarationCopy.m_Buffer = Buffer::Copy(shaderDataDeclaration.GetBuffer());

		return shaderDataDeclarationCopy;
	}

	std::shared_ptr<ShaderBuffer> ShaderBuffer::Create()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "ShaderBuffer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanShaderBuffer>();
		}

		DEB_CORE_ASSERT(false, "ShaderBuffer::Create -> Unknown RendererAPI!");
		return nullptr;

	}
}