#include "debpch.h"
#include "Camera.h"

namespace DebEngine
{

	Camera::Camera(const glm::mat4& projectionMatrix) : m_ProjectionMatrix(projectionMatrix)
	{

	}

	glm::mat4 Camera::GetProjectionMatrix() const
	{
		return m_ProjectionMatrix;
	}

	void Camera::SetProjectionMatrix(const glm::mat4& projectionMatrix)
	{
		m_ProjectionMatrix = projectionMatrix;
	}

	f32 Camera::GetExposure() const
	{
		return m_Exposure;
	}

	f32& Camera::GetExposure()
	{
		return m_Exposure;
	}

}
