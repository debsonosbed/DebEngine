#pragma once

namespace DebEngine
{
	class Viewport
	{
	public:
		virtual ~Viewport() = default;

		virtual void SetViewport(u32 width, u32 height) = 0;

		static std::shared_ptr<Viewport> Create(u32 x, u32 y, u32 width, u32 height);
	};
}

