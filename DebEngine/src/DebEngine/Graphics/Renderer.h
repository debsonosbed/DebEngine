#pragma once

#include "Shader.h"
#include "RendererAPI.h"
#include "RendererContext.h"
#include "Pipeline.h"
#include "Framebuffer.h"
#include "RenderPass.h"
#include "Material.h"
#include "Camera.h"
#include "Mesh.h"

#include "DebEngine/Gameplay/Components.h"
#include "DebEngine/Editor/EditorCamera.h"
#include "DebEngine/Gameplay/SceneCamera.h"

#include "DebEngine/Core/Application.h"
#include "DebEngine/Graphics/RenderCommandQueue.h"

namespace DebEngine
{
	class Renderer
	{
	public:
		struct QuadData
		{
			std::shared_ptr<DebEngine::Material> Material = nullptr;
			glm::mat4 Transform = glm::mat4(1.0);
			std::shared_ptr<VertexBuffer> InstancedDataBuffer = nullptr;
		};

		struct MeshData
		{
			std::shared_ptr<DebEngine::Mesh> Mesh;
			DebEngine::TransformComponent TransformComponent;
			glm::vec4 Color;
		};
	public:
		static void Init();
		static void Shutdown();

		static void OnWindowResize(u32 width, u32 height);

		static void BeginFrame();
		static void EndFrame();

		static void BeginRecording(const std::shared_ptr<CommandBuffer>& commandBuffer);
		static void EndRecording(const std::shared_ptr<CommandBuffer>& commandBuffer);

		static void BeginRenderPass(const std::shared_ptr<RenderPass>& renderPass, b8 isSubpass = false);
		static void EndRenderPass();

		/*static ImTextureID GetEditorImGuiTextureID();
		static ImTextureID GetRuntimeImGuiTextureID();*/

		static std::shared_ptr<RenderPass> GetRenderPassSwapChain();
		/*static std::shared_ptr<RenderPass> GetRenderPassScene();
		static std::shared_ptr<RenderPass> GetRenderPassRuntime();*/
		static std::shared_ptr<Texture2D> GetDummyTexture();
		static std::shared_ptr<TextureCube> GetDummyTextureCube();
		static std::shared_ptr<VertexBuffer> GetDummyVertexBuffer();

		static const std::shared_ptr<CommandBuffer>& GetCurrentCommandBuffer();
		static const std::shared_ptr<CommandBuffer>& GetSwapchainCommandBuffer();

		static std::shared_ptr<CommandBuffer> CreateSecondaryCommandBuffer();

		template<typename FuncT>
		static void Submit(FuncT&& func)
		{
			auto renderCommand = [](void* ptr) {
				auto funcPtr = (FuncT*)ptr;
				(*funcPtr)();

				funcPtr->~FuncT();
			};
			auto storageBuffer = GetRenderCommandQueue().Allocate(renderCommand, sizeof(func));
			new (storageBuffer) FuncT(std::forward<FuncT>(func));
		}

		template<typename FuncT>
		static void SubmitPostFrame(FuncT&& func)
		{
			auto renderCommand = [](void* ptr) {
				auto funcPtr = (FuncT*)ptr;
				(*funcPtr)();

				funcPtr->~FuncT();
			};
			auto storageBuffer = GetCleanUpRenderCommandQueue().Allocate(renderCommand, sizeof(func));
			new (storageBuffer) FuncT(std::forward<FuncT>(func));
		}

		static void SubmitQuad(const QuadData& quadData);
		static void SubmitFullscrenQuad(const std::shared_ptr<Material>& material);

		static void SubmitMeshes(const std::vector<MeshData>& meshDataContainer);
		static void SubmitMeshesObjectPicking(const std::vector<MeshData>& meshDataContainer);

		static void WaitAndRender();
		static void WaitPostFrame();
		static u32 GetRenderCommandCount();

		static RendererAPI& GetAPI();
		static RendererAPIType GetCurrentAPI() { return RendererAPI::CurrentAPIType(); }
		static std::shared_ptr<RendererContext> GetContext();

	private:
		static std::unique_ptr<RendererAPI> s_RendererAPI;
		static RenderCommandQueue& GetRenderCommandQueue();
		static RenderCommandQueue& GetCleanUpRenderCommandQueue();
	};
}