#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Graphics/CommandBuffer.h"

namespace DebEngine
{

	enum class RendererAPIType : u8 {
		None = 0,
		Vulkan = 1
	};

	struct RenderAPICapabilities
	{
		std::string Vendor = "Test";
		std::string Renderer = "TestRenderer";
		std::string Version = "1.0";
	};

	class RendererAPI
	{
	public:
		static RenderAPICapabilities& GetCapabilities()
		{
			static RenderAPICapabilities renderAPICapabilities;
			return renderAPICapabilities;
		}
	public:
		virtual ~RendererAPI() = default;

		virtual void Init() = 0;

		virtual void Clear() = 0;
		// Keep viewport flipped - Vulkan specific
		// https://www.saschawillems.de/blog/2019/03/29/flipping-the-vulkan-viewport/
		virtual void SetViewport(u32 x, u32 y, u32 width, u32 height, b8 flipViewport = true) = 0;
		virtual void ExecuteCommands(const std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers) = 0;
		virtual void Draw(u32 vertexCount, u32 instancesCount = 1, u32 firstVertex = 0, u32 firstInstance = 0) = 0;
		virtual void DrawIndexed(u32 indexCount, u32 instancesCount = 1, u32 baseIndex = 0, u32 baseVertex = 0) = 0;
		virtual void SetLineThickness(f32 thickness) = 0;

		static void	SetCurrentAPIType(RendererAPIType api) { s_RendererAPIType = api; }
		static RendererAPIType CurrentAPIType() { return s_RendererAPIType; }
	private:
		static RendererAPIType s_RendererAPIType;
	};
}

