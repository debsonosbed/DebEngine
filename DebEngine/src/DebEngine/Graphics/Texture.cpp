#include "debpch.h"
#include "Texture.h"

#include "RendererAPI.h"

#include "DebEngine/Core/Pool.h"
#include "DebEngine/Platform/Vulkan/VulkanTexture.h"

#include <yaml-cpp/yaml.h>

namespace DebEngine
{
	TextureType Texture::GetTextureType() const
	{
		return m_TextureType;
	}

	const std::string& Texture::GetPath() const
	{
		return m_Path;
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// Texture2D //////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	bool Texture2D::Serialize(YAML::Emitter& outStream)
	{
		outStream << YAML::Key << "Texture" << YAML::Value;
		outStream << YAML::BeginMap;
		{
			outStream << YAML::Key << "Type";
			outStream << YAML::Value << (u32)m_TextureType;
			outStream << YAML::Key << "Path";
			outStream << YAML::Value << m_Path;
		}
		outStream << YAML::EndMap;

		return true;
	}

	std::shared_ptr<Texture2D> Texture2D::Deserialize(const YAML::Node& node)
	{
		auto textureNode = node["Texture"];
		if (textureNode)
		{
			auto& texturePath = textureNode["Path"].as<std::string>();
			auto& texture = Texture2D::Create(texturePath);

			return texture;
		}

		return nullptr;
	}

	std::shared_ptr<Texture2D> Texture2D::Create(const u32 width, const u32 height, ColorFormat colorFormat)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::Texture2D -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanTexture2D>(width, height, colorFormat);
		}

		DEB_CORE_ASSERT(false, "Texture2D -> Unknown RendererAPI!");
		return nullptr;
	}


	std::shared_ptr<Texture2D> Texture2D::Create(const std::string& path, TextureType textureType)
	{
		auto texture = TextureLibrary::Get(path);
		if (texture) return std::dynamic_pointer_cast<Texture2D>(texture);

		std::shared_ptr<Texture2D> instance = nullptr;
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::Texture2D -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	instance = std::make_shared<VulkanTexture2D>(path, textureType);
		}

		return instance;
	}

	std::shared_ptr<Texture2D> Texture2D::Create(const std::shared_ptr<Framebuffer>& framebuffer, TextureType textureType)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::Texture2D -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanTexture2D>(framebuffer, textureType);
		}

		DEB_CORE_ASSERT(false, "Texture2D -> Unknown RendererAPI!");
		return nullptr;
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// TextureCube ////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	std::shared_ptr<TextureCube> TextureCube::Create(u32 width, u32 height)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::TextureCube -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanTextureCube>(width, height);
		}

		DEB_CORE_ASSERT(false, "Texture2D -> Unknown RendererAPI!");
		return nullptr;
	}

	std::shared_ptr<TextureCube> TextureCube::Create(const std::string& path)
	{
		auto texture = TextureLibrary::Get(path);
		if (texture) return std::dynamic_pointer_cast<TextureCube>(texture);

		std::shared_ptr<TextureCube> instance = nullptr;
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::TextureCube -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	instance =  std::make_shared<VulkanTextureCube>(path);
		}

		return instance;
	}

	std::shared_ptr<TextureCube> TextureCube::Create(const std::vector<std::string>& facesPaths)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::TextureCube -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanTextureCube>(facesPaths);
		}

		DEB_CORE_ASSERT(false, "Texture2D -> Unknown RendererAPI!");
		return nullptr;
	}

	u32 Texture::CalculateMipMapCount(u32 width, u32 height)
	{
		u32 levels = 1;
		while ((width | height) >> levels)
			levels++;

		return levels;
	}

	///////////////////////////////////////////////////////////////////////////////////////
	// TextureLibrary /////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	std::unordered_map<std::string, std::shared_ptr<Texture>> TextureLibrary::m_TextureMap;

	std::shared_ptr<Texture> TextureLibrary::Get(const std::string& path)
	{
		if (m_TextureMap.find(path) != m_TextureMap.end())
		{
			return m_TextureMap[path];
		}

		return nullptr;
	}

	void TextureLibrary::Add(const std::shared_ptr<Texture> texture)
	{
		m_TextureMap.insert_or_assign(texture->GetPath(), texture);
	}
}