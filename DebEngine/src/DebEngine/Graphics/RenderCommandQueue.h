#pragma once

// Queue used to store, allocate memory and execute render functions

#include "debpch.h"

namespace DebEngine
{
	class RenderCommandQueue
	{
	public:
		typedef void(*RenderCommandFn)(void*);

		RenderCommandQueue();
		~RenderCommandQueue();

		void* Allocate(RenderCommandFn func, u32 size);
		u32 GetSize() const;

		void Execute();
	private:
		u8* m_CommandBuffer;
		u8* m_CommandBufferPtr;
		u32 m_CommandCount = 0;
	};
}

