
#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Buffer.h"
#include "DebEngine/Graphics/Framebuffer.h"

namespace YAML
{
	class Emitter;
	class Node;
}

namespace DebEngine
{
	typedef void* ImTextureID;

	enum class TextureType
	{
		Albedo = 1,
		Specular,
		Ambient,
		Emissive,
		Height,
		Normals,
		Roughness,
		Opacity,
		Displacement,
		LightMap,
		Reflection,
		// PBR
		BaseColor,
		NormalCamera,
		EmissionColor,
		Metalness,
		DiffuseRoughness,
		AmbientOcclusion,
		FramebufferColor,
		FramebufferDepth,
		CubeMap,
		IrradianceMap,
		BRDFLUT,
		// Scene global data model
		None,
	};

	class Texture
	{
	public:
		virtual ~Texture() = default;

		virtual void Bind() = 0;
		virtual u32 GetWidth() const = 0;
		virtual u32 GetHeight() const = 0;
		virtual u32 GetMipLevelCount() const = 0;
		virtual Buffer& GetImageBuffer(u32 x = 0, u32 y = 0, u32 width = 0, u32 height = 0) = 0;
		TextureType GetTextureType() const;
		virtual const std::string& GetPath() const;
		virtual b8 Compare(const std::shared_ptr<Texture> texture) const = 0;

		u32 CalculateMipMapCount(u32 width, u32 height);

		virtual bool Serialize(YAML::Emitter& outStream) = 0;

		virtual void Resize(u32 width, u32 height) = 0;
		virtual void Destroy() = 0;
	protected:
		std::string m_Path;
		TextureType m_TextureType;
	};

	class Texture2D : public Texture
	{
	public:
		Texture2D() = default;
		virtual ~Texture2D() = default;

		virtual void Resize(u32 width, u32 height) = 0;
		virtual ImTextureID GetImGuiTexture() = 0;
		virtual b8 IsLoaded() const = 0;

		virtual bool Serialize(YAML::Emitter& outStream) override;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>Pixel value in RGBA color space</returns>
		virtual glm::uvec4 GetPixelValue(u32 x, u32 y) = 0;

		virtual void ChangeLayoutWrite() = 0; // TEmp
		virtual void ChangeLayoutRead() = 0; // TEmp
		virtual void CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel) = 0;

		static std::shared_ptr<Texture2D> Deserialize(const YAML::Node& node);
		static std::shared_ptr<Texture2D> Create(const u32 width, const u32 height, ColorFormat colorFormat = ColorFormat::BGRA8);
		static std::shared_ptr<Texture2D> Create(const std::string& path, TextureType textureType = TextureType::Albedo);
		static std::shared_ptr<Texture2D> Create(const std::shared_ptr<Framebuffer>& framebuffer, TextureType textureType = TextureType::Albedo);
	protected:
		std::shared_ptr<Framebuffer> m_Framebuffer;
	};

	class TextureCube : public Texture
	{
	public:
		TextureCube() = default;
		virtual ~TextureCube() = default;

		virtual void ChangeLayoutWrite() = 0; // TEmp
		virtual void ChangeLayoutRead() = 0; // TEmp
		virtual void CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel) = 0;

		static std::shared_ptr<TextureCube> Create(u32 width, u32 height); // one face dimensions
		static std::shared_ptr<TextureCube> Create(const std::string& path);
		static std::shared_ptr<TextureCube> Create(const std::vector<std::string>& facesPaths);
	};

	///////////////////////////////////////////////////////////////////////////////////////
	// TextureLibrary /////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	class TextureLibrary
	{
	public:
		static std::shared_ptr<Texture> Get(const std::string& path);
		static void Add(const std::shared_ptr<Texture> texture);

	private:
		static std::unordered_map<std::string, std::shared_ptr<Texture>> m_TextureMap;
	};
}
