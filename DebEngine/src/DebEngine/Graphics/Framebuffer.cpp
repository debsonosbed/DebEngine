#include "debpch.h"
#include "Framebuffer.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanFramebuffer.h"

namespace DebEngine
{

	u32 Framebuffer::GetSize() const
	{
		return m_Size;
	}

	const FramebufferSpecification& Framebuffer::GetSpecification() const
	{
		return m_FramebufferSpecification;
	}

	std::shared_ptr<Framebuffer> Framebuffer::Create(const FramebufferSpecification& framebufferSpecification)
	{
		std::shared_ptr<Framebuffer> result = nullptr;

		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Framebuffer::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	result = std::make_shared<VulkanFramebuffer>(framebufferSpecification); break; //  should Vulkan Framebuffer impl
		}

		if(!framebufferSpecification.SwapChainTarget)
			FramebufferPool::GetGlobal()->Add(result);

		return result;
	}

	std::shared_ptr<FramebufferPool> FramebufferPool::s_Instance = std::make_shared<FramebufferPool>();

	FramebufferPool::FramebufferPool(u32 maxFramebuffers /*= 32*/)
	{

	}

	FramebufferPool::~FramebufferPool()
	{

	}

	std::weak_ptr<Framebuffer> FramebufferPool::AllocateBuffer()
	{
		return std::weak_ptr<Framebuffer>();
	}

	void FramebufferPool::Add(const std::shared_ptr<Framebuffer>& framebuffer)
	{
		m_Pool.push_back(framebuffer);
	}

	std::shared_ptr<FramebufferPool>& FramebufferPool::GetGlobal()
	{
		return s_Instance;
	}
}
