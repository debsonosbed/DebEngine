#pragma once

namespace DebEngine
{
	enum class MSAA
	{
		X1,
		X2,
		X4,
		X8,
		X16
	};

	enum class ColorFormat
	{
		None,
		RGBA8,
		BGRA8,
		RGBA16F,
		RGBA32F
	};

	enum class DepthFormat
	{
		None,
		D16,
		D32F,
		D32F8S,
	};

	enum class Layout
	{
		Present,
		ReadOnly,
		ColorAttachment,
		None
	};
}