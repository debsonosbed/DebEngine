#pragma once
#include "debpch.h"

#include "Texture.h"

namespace DebEngine
{
	struct ObjectDataModel
	{
		glm::mat4 Transform;
		glm::vec4 Color;

		u32 HasAlbedo;
		u32 HasNormals;
		u32 HasRoughness;
		u32 HasMetalness;
	};

	enum class ObjectProperty
	{
		Transform,
		Color,
	};

	struct PerObjectSamplersModel
	{
		TextureType Slot1 = TextureType::Albedo;
		TextureType Slot2 = TextureType::Normals;
		TextureType Slot3 = TextureType::Roughness;
		TextureType Slot4 = TextureType::Metalness;
	};
}