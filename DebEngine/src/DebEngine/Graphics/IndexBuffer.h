#pragma once

namespace DebEngine
{
	class IndexBuffer
	{
	public:
		virtual ~IndexBuffer() = default;

		virtual void SetData(void* buffer, u32 size, u32 offset = 0) = 0;
		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;
		virtual u32 GetCount() const = 0;
		virtual u32 GetSize() const = 0;

		static std::shared_ptr<IndexBuffer> Create(Buffer& buffer, u32 indicesCount);
	};
}