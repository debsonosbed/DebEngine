#include "debpch.h"
#include "RenderPass.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanRenderPass.h"

namespace DebEngine
{
	void RenderPass::SetTargetFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer)
	{
		m_TargetFramebuffer = framebuffer;
	}

	std::shared_ptr<Framebuffer> RenderPass::GetTargetFramebuffer() const
	{
		return m_TargetFramebuffer;
	}

	std::shared_ptr<DebEngine::RenderPass> RenderPass::Create(const RenderPassSpecification& renderPassSpecification)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "RenderPass::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanRenderPass>(renderPassSpecification); //  should Vulkan RenderPass impl
		}

		DEB_CORE_ASSERT(false, "RenderPass::Create -> Unknown RendererAPI!");
		return nullptr;
	}
}
