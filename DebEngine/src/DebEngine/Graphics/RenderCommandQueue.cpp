#include "debpch.h"
#include "RenderCommandQueue.h"

#include "DebEngine/Core/Profiler.h"

namespace DebEngine
{
	RenderCommandQueue::RenderCommandQueue()
	{
		u32 size = 10 * 1024 * 1024; // Default 10 MB buffer;
		m_CommandBuffer = new u8[size];
		m_CommandBufferPtr = m_CommandBuffer;
		memset(m_CommandBuffer, 0, size);
	}

	RenderCommandQueue::~RenderCommandQueue()
	{
		delete[] m_CommandBuffer;
	}

	void* RenderCommandQueue::Allocate(RenderCommandFn func, u32 size)
	{
		*(RenderCommandFn*)m_CommandBufferPtr = func;
		m_CommandBufferPtr += sizeof(RenderCommandFn);

		*(u32*)m_CommandBufferPtr = size;
		m_CommandBufferPtr += sizeof(u32);

		void* memory = m_CommandBufferPtr;
		m_CommandBufferPtr += size;

		m_CommandCount++;

		return memory;
	}

	u32 RenderCommandQueue::GetSize() const
	{
		return m_CommandCount;
	}

	void RenderCommandQueue::Execute()
	{
		u8* buffer = m_CommandBuffer;

		for (u32 i = 0; i < m_CommandCount; i++)
		{
			RenderCommandFn function = *(RenderCommandFn*)buffer;
			buffer += sizeof(RenderCommandFn);

			u32 size = *(u32*)buffer;
			buffer += sizeof(u32);
			function(buffer);
			buffer += size;
		}

		m_CommandBufferPtr = m_CommandBuffer;
		m_CommandCount = 0;
	}
}