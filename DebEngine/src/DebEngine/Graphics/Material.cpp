#include "debpch.h"
#include "Material.h"

#include "Renderer.h"

#include "DebEngine/Platform/Vulkan/Vulkan.h"

#include "DebEngine/Platform/Vulkan/VulkanMaterial.h"

#include "DebEngine/Vendor/yaml/YamlUtils.h"

namespace DebEngine
{

	Material::Material(const std::shared_ptr<Pipeline>& pipeline)
	{
		m_Name = "Material";
	}

	Material::~Material()
	{
		for (auto& [name, shaderDataDecl] : m_ShaderDataDeclaration)
		{
			shaderDataDecl.GetBuffer().Clear();
		}
	}

	std::unordered_map<std::string, ShaderDataDeclaration>& Material::GetShaderDataDeclarations()
	{
		return m_ShaderDataDeclaration;
	}

	void Material::SetPipeline(const std::shared_ptr<Pipeline>& pipeline)
	{
		m_Pipeline = pipeline;

		// Since a new pipeline will be used, all the shader data needs to be set once again
		for (auto& [name, shaderDataDecl] : m_ShaderDataDeclaration)
		{
			shaderDataDecl.SetUploaded(false);
		}
	}

	std::shared_ptr<Pipeline> Material::GetPipeline()
	{
		return m_Pipeline;
	}

	std::shared_ptr<Material> Material::Create(const std::shared_ptr<Pipeline>& pipeline, b8 isPrimary)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Material::Create -> Invalid RendererAPI (none)"); break;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanMaterial>(pipeline, isPrimary);
		}

		return nullptr;
	}

	std::shared_ptr<Material> Material::Copy(std::shared_ptr<Material> material, b8 isPrimary)
	{
		auto materialNew = Material::Create(material->GetPipeline(), isPrimary);

		for (auto& [name, dataDecl] : material->m_ShaderDataDeclaration)
		{
			materialNew->m_ShaderDataDeclaration.insert({ name, ShaderDataDeclaration::Copy(dataDecl) });
		}

		materialNew->m_PropertiesContainer = material->m_PropertiesContainer;
		materialNew->m_TextureSlots = material->m_TextureSlots;

		return materialNew;
	}

	void Material::SetTexture(std::shared_ptr<Texture2D> texture)
	{
		auto textureType = texture->GetTextureType();
		switch (textureType)
		{
		case TextureType::Albedo:
			Set("TextureToggles.hasAlbedo", 1);
			Set("u_TextureAlbedo", texture);
			break;;
		case TextureType::Specular:
			Set("TextureToggles.hasSpecular", 1);
			Set("u_TextureSpecular", texture);
			break;
		case TextureType::Ambient:
			Set("TextureToggles.hasAmbient", 1);
			Set("u_TextureAmbient", texture);
			break;
		case TextureType::Emissive:
			Set("TextureToggles.hasEmissive", 1);
			Set("u_TextureEmissive", texture);
			break;
		case TextureType::Height:
			Set("TextureToggles.hasHeight", 1);
			Set("u_TextureHeight", texture);
			break;
		case TextureType::Normals:
			Set("TextureToggles.hasNormals", 1);
			Set("u_TextureNormals", texture);
			break;
		case TextureType::Roughness:
			Set("TextureToggles.hasRoughness", 1);
			Set("u_TextureRoughness", texture);
			break;
		case TextureType::Opacity:
			Set("TextureToggles.hasOpacity", 1);
			Set("u_TextureOpacity", texture);
			break;
		case TextureType::Displacement:
			Set("TextureToggles.hasDisplacement", 1);
			Set("u_TextureDisplacement", texture);
			break;
		case TextureType::LightMap:
			Set("TextureToggles.hasLightMap", 1);
			Set("u_TextureLightMap", texture);
			break;
		case TextureType::Reflection:
			Set("TextureToggles.hasReflection", 1);
			Set("u_TextureReflection", texture);
			break;
		case TextureType::BaseColor:
			Set("TextureToggles.hasBaseColor", 1);
			Set("u_TextureBaseColor", texture);
			break;
		case TextureType::NormalCamera:
			Set("TextureToggles.hasNormalCamera", 1);
			break;
		case TextureType::EmissionColor:
			Set("TextureToggles.hasEmissionColor", 1);
			break;
		case TextureType::Metalness:
			Set("TextureToggles.hasMetalness", 1);
			Set("u_TextureMetalness", texture);
			break;
		case TextureType::DiffuseRoughness:
			Set("TextureToggles.hasDiffuseRoughness", 1);
			Set("u_TextureDiffuseRoughness", texture);
			break;
		case TextureType::AmbientOcclusion:
			Set("TextureToggles.hasAmbientOcclusion", 1);
			break;
		}

		m_TextureSlots[textureType] = texture;
	}

	std::shared_ptr<Texture2D> Material::GetTexture(TextureType textureType) const
	{
		DEB_CORE_ASSERT(m_TextureSlots.find(textureType) != m_TextureSlots.end(), "Texture does not exists!");
		return m_TextureSlots.at(textureType);
	}

	void Material::SetProperty(MaterialProperty matProp, s32 val)
	{
		m_PropertiesContainer.insert_or_assign(matProp, PropertyValue(val));
	}

	void Material::SetProperty(MaterialProperty matProp, f32 val)
	{
		switch (matProp)
		{
		case MaterialProperty::Roughness:
			Set("Material.Roughness", val);
			break;
		case MaterialProperty::Metalness:
			Set("Material.Metalness", val);
			break;
		}

		m_PropertiesContainer.insert_or_assign(matProp, PropertyValue(val));
	}

	void Material::SetProperty(MaterialProperty matProp, const glm::vec3& val)
	{
		m_PropertiesContainer.insert_or_assign(matProp, PropertyValue(val));
	}

	void Material::SetProperty(MaterialProperty matProp, const glm::vec4& val)
	{
		switch (matProp)
		{
		case MaterialProperty::Color:
			Set("Material.Color", val);
			break;
		}

		m_PropertiesContainer.insert_or_assign(matProp, PropertyValue(val));
	}

	const std::string& Material::GetName() const
	{
		return m_Name;
	}

	bool Material::Serialize(YAML::Emitter& outStream)
	{
		/*outStream << YAML::Key << "Material";
		outStream << YAML::BeginMap;
		outStream << YAML::Key << "PipelineName" << YAML::Value << m_Pipeline->GetName(); // only preloaded pieplines can be serialized for now

		outStream << YAML::Key << "Textures";
		{
			outStream << YAML::BeginSeq;
			for (const auto& [slot, texture] : m_TextureSlots)
			{
				texture->Serialize(outStream);
			}
			outStream << YAML::EndSeq;
		}
		outStream << YAML::Key << "MaterialProperties";
		{
			outStream << YAML::BeginMap;
			outStream << YAML::BeginSeq;
			for (auto& [type, var] : m_PropertiesContainer)
			{
				outStream << YAML::BeginMap;
				switch (type)
				{
				case MaterialProperty::Color:
					outStream << YAML::Key << (u32)type << YAML::Value << GetProperty<glm::vec4>(type);
					break;
				case MaterialProperty::Metalness:
					outStream << YAML::Key << (u32)type << YAML::Value << GetProperty<f32>(type);
					break;
				case MaterialProperty::Roughness:
					outStream << YAML::Key << (u32)type << YAML::Value << GetProperty<f32>(type);
					break;
				}
				outStream << YAML::EndMap;
			}
			outStream << YAML::EndSeq;
			outStream << YAML::EndMap;
		}
		outStream << YAML::EndMap;*/

		return true;
	}

	std::shared_ptr<DebEngine::Material> Material::Deserialize(const YAML::Node& node)
	{
		/*auto materialNode = node["Material"];
		if (materialNode)
		{
			auto pipelineName = materialNode["PipelineName"].as<std::string>();
			auto pipeline = PipelineLibrary::Get(pipelineName);
			auto material = Material::Create(pipeline);

			if (materialNode["Textures"])
			{
				for (const auto& textureNode : materialNode["Textures"])
				{
					auto texture = Texture2D::Deserialize(textureNode);

					material->m_TextureSlots.clear();
					material->m_TextureSlots[texture->GetTextureType()] = texture;
				}
			}

			if (materialNode["MaterialProperties"])
			{
				for (const auto& shaderDataDeclNode : shaderDataDeclMapNode["ShaderDataDeclarations"])
				{
					auto shaderDataDecl = ShaderDataDeclaration::Deserialize(shaderDataDeclNode);
					if (shaderDataDecl.GetBuffer().Size == 16)
					{
						glm::vec4 color = *(glm::vec4*)shaderDataDecl.GetBuffer().Data;
					}
					material->m_ShaderDataDeclaration[bindIndex].insert({ shaderDataDecl.GetName(), shaderDataDecl });
				}

			}

			return material;
		}*/

		return nullptr;
	}

	ShaderBufferType Material::FindShaderBufferType(const std::string& name)
	{
		auto& shaderBuffers = m_Shader->GetShaderBuffers();

		std::string parentName = GetParentName(name);
		DEB_CORE_ASSERT(shaderBuffers.find(parentName) != shaderBuffers.end());

		return shaderBuffers.at(parentName)->Type;
	}

	std::string Material::GetParentName(const std::string& name)
	{
		std::string parentName = name;
		u32 dotPosition = name.find('.');
		if (dotPosition != -1)
			parentName = name.substr(0, dotPosition);

		return parentName;
	}

	std::string Material::GetChildName(const std::string& name)
	{
		std::string childName;
		u32 dotPosition = name.find('.');
		if (dotPosition != -1)
			childName = name.substr(dotPosition + 1, name.length());

		return childName;
	}
}
