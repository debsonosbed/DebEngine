#pragma once

#include "Framebuffer.h"
#include "Defs.h"

namespace DebEngine
{
	struct RenderPassSpecification
	{
		DebEngine::ColorFormat ColorFormat = ColorFormat::BGRA8;
		DebEngine::DepthFormat DepthFormat = DepthFormat::D32F8S;
		b8 HasColorAttachment = true;
		b8 HasDepthAttachment = true;
		b8 IsPrimary = false;
		MSAA MSAAType = MSAA::X1;
		glm::ivec4 ClearColor = glm::ivec4(0, 0, 0, 255); // in RGB
		Layout FinalLayout = Layout::ReadOnly;
		b8 ClearOnStart = true;
	};

	class RenderPass
	{
	public:
		virtual ~RenderPass() = default;

		virtual const RenderPassSpecification& GetSpecification() const = 0;
		void SetTargetFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer);
		std::shared_ptr<Framebuffer> GetTargetFramebuffer() const;

		virtual void BeginRenderPass(const std::shared_ptr<CommandBuffer>& cmdBuffer, b8 isSubpass = false) = 0;
		virtual void EndRenderPass(const std::shared_ptr<CommandBuffer>& cmdBuffer) = 0;
		virtual const u32 GetColorFormat() const = 0;

		static std::shared_ptr<RenderPass> Create(const RenderPassSpecification& renderPassSpecification);
	protected:
		std::shared_ptr<Framebuffer> m_TargetFramebuffer;
	};
}

