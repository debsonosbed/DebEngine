#pragma once

#include "Shader.h"
#include "Pipeline.h"
#include "Texture.h"
#include <variant>

#include <glm/glm.hpp>


namespace DebEngine
{
	enum class MaterialProperty
	{
		Color,
		Roughness,
		Metalness
	};

	typedef std::variant<s32, f32, u32, glm::vec3, glm::vec4> PropertyValue;

	class Material
	{
		friend class MaterialPool;
		friend class Mesh;
	public:
		Material() = default;
		Material(const std::shared_ptr<Pipeline>& pipeline);
		~Material();

		virtual void Bind() = 0;

		template <typename T>
		void Set(const std::string& name, const T& value)
		{
			//DEB_CORE_LOG("Bind index: {0}", m_BindIndex);
			if (m_ShaderDataDeclaration.find(name) == m_ShaderDataDeclaration.end())
			{
				// If shader data is not declared, declare it
				auto shaderBufferType = FindShaderBufferType(name);
				ShaderDataDeclaration shaderDataDeclaration(shaderBufferType, name);
				m_ShaderDataDeclaration[name] = shaderDataDeclaration;
			}
			auto& uniform = m_ShaderDataDeclaration.at(name);
			uniform.SetUploaded(false);

			auto& buffer = uniform.GetBuffer();

			u32 dataSize = sizeof(value);

			// Case when different data submitted under the same name
			if (buffer.GetSize() != dataSize)
			{
				buffer.Clear();
				buffer.Allocate(dataSize);
			}

			buffer.Write((u8*)&value, dataSize);
		}

		template <>
		void Set<std::shared_ptr<Texture2D>>(const std::string& name, const std::shared_ptr<Texture2D>& texture)
		{
			if (m_ShaderDataDeclaration.find(name) == m_ShaderDataDeclaration.end())
			{
				// If shader data is not declared, declare it
				auto shaderDataSourceType = FindShaderBufferType(name);
				ShaderDataDeclaration shaderDataDeclaration(shaderDataSourceType, name);
				m_ShaderDataDeclaration[name] = shaderDataDeclaration;
			}

			auto& shaderDataDecl = m_ShaderDataDeclaration.at(name);
			shaderDataDecl.SetTexture(texture);
			shaderDataDecl.SetUploaded(false);
		}

		template <>
		void Set<std::shared_ptr<TextureCube>>(const std::string& name, const std::shared_ptr<TextureCube>& texture)
		{
			if (m_ShaderDataDeclaration.find(name) == m_ShaderDataDeclaration.end())
			{
				// If shader data is not declared, declare it
				auto shaderDataSourceType = FindShaderBufferType(name);
				ShaderDataDeclaration shaderDataDeclaration(shaderDataSourceType, name);
				m_ShaderDataDeclaration[name] = shaderDataDeclaration;
			}

			auto& shaderDataDecl = m_ShaderDataDeclaration.at(name);
			shaderDataDecl.SetTextureCube(texture);
			shaderDataDecl.SetUploaded(false);
		}

		std::unordered_map<std::string, ShaderDataDeclaration>& GetShaderDataDeclarations();

		void SetPipeline(const std::shared_ptr<Pipeline>& pipeline);
		std::shared_ptr<Pipeline> GetPipeline();


		void SetTexture(std::shared_ptr<Texture2D> texture);
		std::shared_ptr<Texture2D> GetTexture(TextureType textureType) const;

		void SetProperty(MaterialProperty matProp, s32 val);
		void SetProperty(MaterialProperty matProp, f32 val);
		void SetProperty(MaterialProperty matProp, const glm::vec3& val);
		void SetProperty(MaterialProperty matProp, const glm::vec4& val);

		template <typename T>
		T GetProperty(MaterialProperty matProp)
		{
			if (m_PropertiesContainer.find(matProp) != m_PropertiesContainer.end())
			{
				auto& var = m_PropertiesContainer.at(matProp);
				if (var.index() != std::variant_npos)
					return std::get<T>(var);
			}
			else
			{
				//LogCore::Warn("Material Property not present on that material!");
			}

			return T();
		}

		const std::string& GetName() const;

		bool Serialize(YAML::Emitter& outStream);
		static std::shared_ptr<Material> Deserialize(const YAML::Node& node);
		static std::shared_ptr<Material> Create(const std::shared_ptr <Pipeline>& piepline, b8 isPrimary = true);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="material"></param>
		/// <param name="isPrimary">By default value set to false, since that's the expected behaviour - copying a material from a primary one</param>
		/// <returns></returns>
		static std::shared_ptr<Material> Copy(std::shared_ptr<Material> material, b8 isPrimary = false);
	protected:
		ShaderBufferType FindShaderBufferType(const std::string& name);

		std::string GetParentName(const std::string& name);
		std::string GetChildName(const std::string& name);
	protected:
		std::string m_Name;

		std::shared_ptr<Pipeline> m_Pipeline;
		std::shared_ptr<Shader> m_Shader;
		std::unordered_map<std::string, ShaderDataDeclaration> m_ShaderDataDeclaration;
		std::unordered_map<MaterialProperty, PropertyValue> m_PropertiesContainer;

		// Vulkan specific
		// Identifies if current material is part of the first mesh instance
		// Its set to true always when mesh is loaded
		// Other copies of the mesh have their own materials, which are not primary.
		b8 m_IsPrimary = false;
	private:
		std::unordered_map<TextureType, std::shared_ptr<Texture2D>> m_TextureSlots;
	};
}
