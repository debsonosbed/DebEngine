#include "debpch.h"
#include "VertexBuffer.h"

#include "RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanVertexBuffer.h"

namespace DebEngine {

	std::shared_ptr<VertexBuffer> VertexBuffer::Create(Buffer& buffer)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::VertexBuffer -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanVertexBuffer>(buffer); // should return Vulkan vertex buffer impl
		}

		DEB_CORE_ASSERT(false, "DebEngine::VertexBuffer -> Unknown RendererAPI!");
		return nullptr;
	}

	std::shared_ptr<DebEngine::VertexBuffer> VertexBuffer::CreateDynamic(u32 baseSize)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::VertexBuffer -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanVertexBuffer>(baseSize); // should return Vulkan vertex buffer impl
		}

		DEB_CORE_ASSERT(false, "DebEngine::VertexBuffer -> Unknown RendererAPI!");
		return nullptr;
	}

	const u32 VertexBufferLayout::GetElementCount() const
	{
		return m_Elements.size();
	}

	const b8 VertexBufferLayout::IsInstanceLayout() const
	{
		return m_InstanceLayout;
	}

	std::vector<VertexBufferElement>::iterator VertexBufferLayout::begin()
	{
		return m_Elements.begin();
	}

	std::vector<VertexBufferElement>::const_iterator VertexBufferLayout::begin() const
	{
		return m_Elements.begin();
	}

	std::vector<VertexBufferElement>::iterator VertexBufferLayout::end()
	{
		return m_Elements.end();
	}

	std::vector<VertexBufferElement>::const_iterator VertexBufferLayout::end() const
	{
		return m_Elements.end();
	}

	void VertexBufferLayout::CalculateOffsetAndStride()
	{
		u32 offset = 0;
		m_Stride = 0;
		
		for (auto& element : m_Elements)
		{
			element.Offset = offset;
			offset += element.Size;
			m_Stride += element.Size;
		}
	}
}