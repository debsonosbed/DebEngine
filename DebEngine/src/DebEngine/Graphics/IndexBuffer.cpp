#include "debpch.h"
#include "IndexBuffer.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanIndexBuffer.h"

namespace DebEngine
{
	std::shared_ptr<IndexBuffer> IndexBuffer::Create(Buffer& buffer, u32 indicesCount)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "DebEngine::IndexBuffer -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanIndexBuffer>(buffer, indicesCount); // should return Vulkan index buffer impl
		}

		DEB_CORE_ASSERT(false, "DebEngine::IndexBuffer -> Unknown RendererAPI!");
		return nullptr;
	}
}