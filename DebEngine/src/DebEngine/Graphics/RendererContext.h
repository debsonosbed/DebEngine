#pragma once

#include "DebEngine/Graphics/SwapChain.h"

struct SDL_Window;

namespace DebEngine
{
	class RendererContext
	{
	public:
		RendererContext() = default;
		virtual ~RendererContext() = default;

		virtual void Create() = 0;
		virtual void BeginFrame() = 0;
		virtual void SwapBuffers() = 0;
		virtual void OnResize(u32 width, u32 height) = 0;
		virtual std::shared_ptr<SwapChain> GetSwapChain() const = 0;
		virtual void Shutdown() = 0;

		static std::shared_ptr<RendererContext> Create(SDL_Window* windowHandle);
	};
}