#pragma once


#include <glm/glm.hpp>
#include "DebEngine/Core/Buffer.h"


namespace DebEngine 
{

	class RenderPass;
	class CommandBuffer;
	class Texture2D;

	struct FramebufferSpecification
	{
		f32 Scale = 1.f;
		u32 Width = 0;
		u32 Height = 0;
		glm::vec4 ClearColor;
		u32 Samples = 1;
		std::shared_ptr<RenderPass> RenderPass;
		b8 UpdateOnResize = true;

		b8 SwapChainTarget = false;
	};

	class Framebuffer
	{
	public:
		virtual ~Framebuffer() = default;

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual void SetupViewport(const std::shared_ptr<CommandBuffer>& drawCommandBuffer, b8 flip = false) = 0;
		virtual void Resize(u32 width, u32 height, b8 forceRecreate = false) = 0;
		virtual void AddResizeCallback(const std::function<void(std::shared_ptr<Framebuffer>)>& resizeCallback) = 0;
		virtual u32 GetWidth() const = 0;
		virtual u32 GetHeight() const = 0;

		virtual void InitializeLayout() = 0;
		virtual void ChangeLayoutWrite() = 0; // TEmp
		virtual void ChangeLayoutRead() = 0; // TEmp

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Size of a framebuffer in bytes</returns>
		virtual u32 GetSize() const;
		virtual const std::shared_ptr<Texture2D> GetFramebufferColorTexture() const = 0;
		virtual const std::shared_ptr<Texture2D> GetFramebufferDepthTexture() const = 0;
		//virtual Buffer& GetColorScreenshotBuffer() = 0;

		virtual std::shared_ptr<RenderPass> GetRenderPass() const = 0;
		virtual const FramebufferSpecification& GetSpecification() const;

		static std::shared_ptr<Framebuffer> Create(const FramebufferSpecification& framebufferSpecification);
	protected:
		u32 m_Size = 0;
		FramebufferSpecification m_FramebufferSpecification;
	};

	class FramebufferPool final
	{
	public:
		FramebufferPool(u32 maxFramebuffers = 32);
		~FramebufferPool();

		std::weak_ptr<Framebuffer> AllocateBuffer();
		void Add(const std::shared_ptr<Framebuffer>& framebuffer);

		std::vector<std::shared_ptr<Framebuffer>>& GetAll() { return m_Pool; }

		static std::shared_ptr<FramebufferPool>& GetGlobal();
	private:
		std::vector<std::shared_ptr<Framebuffer>> m_Pool;

		static std::shared_ptr<FramebufferPool> s_Instance;
	};
}

