#include "debpch.h"
#include "SceneRenderer.h"

#include "Renderer.h"
#include "Material.h"

#include "DebEngine/Gameplay/Entity.h"
#include "entt.hpp"

#include "DebEngine/Core/Thread.h"
#include "DebEngine/Core/Utils.h"
#include "DebEngine/Core/Statistics.h"
#include "DebEngine/Core/Profiler.h"

#include <glm/gtc/matrix_transform.hpp>

#define INITIAL_SCENE_MESHE_BUFFER_SIZE 10 * 10 * 1024 // 10 KB initial
// Longest preprocessor def I've ever defined
// Minimum count of entities in the scene to enable building data buffers by multiple threads rather than the main thread.
#define SCENE_ENTITIES_DATA_BUFFER_BUILDING_MULTITHREADED_ENABLED_MIN_COUNT 1000
#define SCENE_BUFFERS_BUILDING_THREADS_COUNT 4

namespace DebEngine
{
	struct RenderPassData
	{
		std::shared_ptr<DebEngine::RenderPass> RenderPass;
		std::shared_ptr<DebEngine::Framebuffer> Framebuffer;
		std::shared_ptr<DebEngine::Pipeline> Pipeline;
		std::shared_ptr<DebEngine::Material> Material;
		std::vector<std::shared_ptr<DebEngine::Material>> Materials;
		std::shared_ptr<DebEngine::Texture2D> Texture;
		std::shared_ptr<DebEngine::TextureCube> TextureCube;
		std::shared_ptr<DebEngine::CommandBuffer> CommandBuffer;
	};

	struct MeshInstancedPos
	{
		glm::mat4 Transform;
	};

	struct MeshInstancedDataObjectPicking
	{
		glm::vec4 Color = glm::vec4(1.0f);
	};

	std::unordered_map<std::string, std::shared_ptr<DebEngine::Texture2D>> SceneRenderer::m_EnvironmentPropertiesTextures;
	std::unordered_map<std::string, std::shared_ptr<DebEngine::TextureCube>> SceneRenderer::m_EnvironmentPropertiesTexturesCube;

	struct SceneRendererData
	{
		Scene* ActiveScene;
		DebEngine::SceneRendererCamera SceneRendererCamera;

		RenderPassData Outline;
		f32 OutlineThickness = 1.05f;
		glm::vec3 OutlineColor = { 1.f, 0.66f, 0.f };

		std::vector<Renderer::QuadData> QuadsData;
		std::vector<Renderer::MeshData> MeshesContainer;
		std::vector<Renderer::MeshData> MeshesOutlineContainer;
		std::vector<Renderer::MeshData> ObjectPickingMeshesContainer;

		RenderPassData PBREditor;
		RenderPassData PBRRuntime;

		RenderPassData Grid;
		f32 GridSize = 1600.0f;
		f32 GridResolution = 0.025f;
		glm::mat4 GridTransform = glm::mat4(1.0f);

		RenderPassData ObjectPicking;

		// Main rendering
		RenderPassData Editor;
		RenderPassData Runtime;
		RenderPassData CompositeEditor;
		std::shared_ptr<Texture2D> CurrentEditorFramebufferTexture;
		RenderPassData CompositeRuntime;
		std::shared_ptr<Texture2D> m_CurrentRuntimeFramebufferTexture;

		ImTextureID m_EditorViewportTextureId;
		ImTextureID m_RuntimeViewportTextureId;

		std::pair<f32, f32> EditorViewportDim = { 1280.0f, 720.0f };
		std::pair<f32, f32> RuntimeViewportDim = { 1280.0f, 720.0f };

		b8 m_UpdateEditorFramebufferSize = false;
		b8 m_UpdateRuntimeFramebufferSize = false;

		// outline
		std::shared_ptr<Material> m_OutlinePassMaterial;
		std::shared_ptr<Pipeline> m_OutlinePipeline;

		// Environment
		RenderPassData EnvEditor;
		RenderPassData EnvRuntime;
		std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>> EnvMap;
		std::shared_ptr<Mesh> CubeMesh;
		std::shared_ptr<Texture2D> m_HdirTexture;
		RenderPassData CubeMap;
		RenderPassData IrradianceMap;
		RenderPassData PrefilteredCubeMap;
		RenderPassData BRDFLUT;

		b8 AllSceneMeshesSubmitted = false;
		b8 AllSceneMeshesSubmittedNoBatch = false;
		b8 AllSceneMeshesObjectPickingSubmitted = false;
		b8 GridSubmitted = false;
		b8 EnvMapSubmitted = false;

		std::shared_ptr<VertexBuffer> MeshInstancedDataAttribBuffer;
		Buffer MeshInstancedDataBuffer;

		ThreadPool SceneBufferBuildersThreadPool;
		std::mutex SceneBuffersMutex;
	};

	static SceneRendererData s_SceneRenderData;

	GlobalSceneRendererData SceneRenderer::GlobalData;

	void SceneRenderer::Init()
	{
		auto shaderPlainColor = ShaderLibrary::Load("assets/shaders/PlainColor.glsl");
		{
			RenderPassSpecification renderPassSpecification = {};
			renderPassSpecification.ClearColor = { 0, 0, 0, 0 };
			s_SceneRenderData.ObjectPicking.RenderPass = RenderPass::Create(renderPassSpecification);

			FramebufferSpecification framebufferSpecification = {};
			framebufferSpecification.Width = s_SceneRenderData.EditorViewportDim.first;
			framebufferSpecification.Height = s_SceneRenderData.EditorViewportDim.second;
			framebufferSpecification.RenderPass = s_SceneRenderData.ObjectPicking.RenderPass;
			s_SceneRenderData.ObjectPicking.Framebuffer = Framebuffer::Create(framebufferSpecification);

			s_SceneRenderData.ObjectPicking.RenderPass->SetTargetFramebuffer(s_SceneRenderData.ObjectPicking.Framebuffer);


			// Plain color

			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.ObjectPicking.RenderPass;
			pipelineSpecification.Shader = shaderPlainColor;
			pipelineSpecification.BlendEnabled = false;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceTransform1"},
						{ ShaderDataType::Float4, "a_InstanceTransform2"},
						{ ShaderDataType::Float4, "a_InstanceTransform3"},
						{ ShaderDataType::Float4, "a_InstanceTransform4"},
					},
					true // specifies that it is instance layout
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceColor"},
					},
					true
				}
			};
			//pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
			//pipelineSpecification.DepthStencilState.DepthWriteEnabled = false;

			s_SceneRenderData.ObjectPicking.Pipeline = Pipeline::Create(pipelineSpecification, "PlainColor");
			s_SceneRenderData.ObjectPicking.Material = Material::Create(s_SceneRenderData.ObjectPicking.Pipeline);
		}

		// Composite initialization
		auto shaderComposite = ShaderLibrary::Load("assets/shaders/Composite.glsl");

		// Composite editor pass
		{
			RenderPassSpecification renderPassSpecification = {};
			//renderPassSpecification.MSAAType = MSAA::X8;
			s_SceneRenderData.CompositeEditor.RenderPass = RenderPass::Create(renderPassSpecification);

			FramebufferSpecification framebufferSpecification = {};
			framebufferSpecification.Width = s_SceneRenderData.EditorViewportDim.first;
			framebufferSpecification.Height = s_SceneRenderData.EditorViewportDim.second;
			framebufferSpecification.RenderPass = s_SceneRenderData.CompositeEditor.RenderPass;
			s_SceneRenderData.CompositeEditor.Framebuffer = Framebuffer::Create(framebufferSpecification);

			s_SceneRenderData.CompositeEditor.RenderPass->SetTargetFramebuffer(s_SceneRenderData.CompositeEditor.Framebuffer);

			// Pipeline composite
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.CompositeEditor.RenderPass;
			pipelineSpecification.Shader = shaderComposite;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float2, "a_TexCoord" }
					}
				},
			};
			s_SceneRenderData.CompositeEditor.Pipeline = Pipeline::Create(pipelineSpecification, "CompositeEditor");

			s_SceneRenderData.CompositeEditor.Material = Material::Create(s_SceneRenderData.CompositeEditor.Pipeline);
		}

#ifndef DEB_DIST
		// Composite runtime pass
		{
			RenderPassSpecification renderPassSpecification = {};
			//renderPassSpecification.MSAAType = MSAA::X8;
			s_SceneRenderData.CompositeRuntime.RenderPass = RenderPass::Create(renderPassSpecification);

			FramebufferSpecification framebufferSpecification = {};
			framebufferSpecification.Width = s_SceneRenderData.RuntimeViewportDim.first;
			framebufferSpecification.Height = s_SceneRenderData.RuntimeViewportDim.second;
			framebufferSpecification.RenderPass = s_SceneRenderData.CompositeRuntime.RenderPass;
			s_SceneRenderData.CompositeRuntime.Framebuffer = Framebuffer::Create(framebufferSpecification);

			s_SceneRenderData.CompositeRuntime.RenderPass->SetTargetFramebuffer(s_SceneRenderData.CompositeRuntime.Framebuffer);


			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.CompositeRuntime.RenderPass;
			pipelineSpecification.Shader = shaderComposite;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float2, "a_TexCoord" }
					}
				},
			};
			s_SceneRenderData.CompositeRuntime.Pipeline = Pipeline::Create(pipelineSpecification, "CompositeRuntime");

			s_SceneRenderData.CompositeRuntime.Material = Material::Create(s_SceneRenderData.CompositeRuntime.Pipeline);
		}
#else
		// Composite runtime pass
		{
			s_SceneRenderData.CompositeRuntime.RenderPass = Renderer::GetRenderPassSwapChain();

			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.CompositeRuntime.RenderPass;
			pipelineSpecification.Shader = shaderComposite;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float2, "a_TexCoord" }
					}
				},
			};
			s_SceneRenderData.CompositeRuntime.Pipeline = Pipeline::Create(pipelineSpecification, "CompositeRuntime");

			s_SceneRenderData.CompositeRuntime.Material = Material::Create(s_SceneRenderData.CompositeRuntime.Pipeline);
		}
#endif

		// Init off screen rendering data
		// scene
		{
			RenderPassSpecification renderPassSpec{};
			//renderPassSpec.MSAAType = MSAA::X8;
			s_SceneRenderData.Editor.RenderPass = RenderPass::Create(renderPassSpec);

			FramebufferSpecification framebufferSpecification{};
			framebufferSpecification.RenderPass = s_SceneRenderData.Editor.RenderPass;
			framebufferSpecification.Width = s_SceneRenderData.EditorViewportDim.first;
			framebufferSpecification.Height = s_SceneRenderData.EditorViewportDim.second;
			s_SceneRenderData.Editor.Framebuffer = Framebuffer::Create(framebufferSpecification);

			s_SceneRenderData.Editor.RenderPass->SetTargetFramebuffer(s_SceneRenderData.Editor.Framebuffer);
		}

		// runtime
		{
			RenderPassSpecification renderPassSpec{};
			//renderPassSpec.MSAAType = MSAA::X8;
			s_SceneRenderData.Runtime.RenderPass = RenderPass::Create(renderPassSpec);

			FramebufferSpecification framebufferSpecification{};
			framebufferSpecification.RenderPass = s_SceneRenderData.Runtime.RenderPass;
#ifndef DEB_DIST
			framebufferSpecification.Width = s_SceneRenderData.RuntimeViewportDim.first;
			framebufferSpecification.Height = s_SceneRenderData.RuntimeViewportDim.second;
#endif
			s_SceneRenderData.Runtime.Framebuffer = Framebuffer::Create(framebufferSpecification);

#ifdef DEB_DIST
			s_SceneRenderData.Runtime.Framebuffer->AddResizeCallback([](std::shared_ptr<Framebuffer> framebuffer)
				{
					u32 width = framebuffer->GetWidth();
					u32 height = framebuffer->GetHeight();

					auto& framebufferTexture = framebuffer->GetFramebufferColorTexture();
					s_SceneRenderData.CompositeRuntime.Material->Set("u_Texture", framebufferTexture);
				});
#endif

			s_SceneRenderData.Runtime.RenderPass->SetTargetFramebuffer(s_SceneRenderData.Runtime.Framebuffer);
		}

		// PBR 
		auto shaderPBR = ShaderLibrary::Load("assets/shaders/DebEnginePBR.glsl");

		// PBR Editor
		{
			s_SceneRenderData.PBREditor.RenderPass = s_SceneRenderData.Editor.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.PBREditor.RenderPass;
			pipelineSpecification.Shader = shaderPBR;
			pipelineSpecification.BlendEnabled = true;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceTransform1"},
						{ ShaderDataType::Float4, "a_InstanceTransform2"},
						{ ShaderDataType::Float4, "a_InstanceTransform3"},
						{ ShaderDataType::Float4, "a_InstanceTransform4"},
					},
					true // specifies that it is instance layout
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceColor"},
						{ ShaderDataType::Float, "a_InstanceRoughness"},
						{ ShaderDataType::Float, "a_InstanceMetalness"},
					},
					true
				}
			};
			pipelineSpecification.DepthStencilState.Front.WriteMask = 0xFF;
			pipelineSpecification.DepthStencilState.Back = pipelineSpecification.DepthStencilState.Front;

			s_SceneRenderData.PBREditor.Pipeline = Pipeline::Create(pipelineSpecification, "PBREditor");
		}

		// PBR Runtime
		{
			s_SceneRenderData.PBRRuntime.RenderPass = s_SceneRenderData.Runtime.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.PBRRuntime.RenderPass;
			pipelineSpecification.Shader = shaderPBR;
			pipelineSpecification.BlendEnabled = true;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceTransform1"},
						{ ShaderDataType::Float4, "a_InstanceTransform2"},
						{ ShaderDataType::Float4, "a_InstanceTransform3"},
						{ ShaderDataType::Float4, "a_InstanceTransform4"},
					},
					true // specifies that it is instance layout
				},
				{
					{
						{ ShaderDataType::Float4, "a_InstanceColor"},
						{ ShaderDataType::Float, "a_InstanceRoughness"},
						{ ShaderDataType::Float, "a_InstanceMetalness"},
					},
					true
				}
			};

			s_SceneRenderData.PBRRuntime.Pipeline = Pipeline::Create(pipelineSpecification, "PBRRuntime");
		}


		// Grid
		auto shaderGrid = ShaderLibrary::Load("assets/shaders/Grid.glsl");

		{
			s_SceneRenderData.Grid.RenderPass = s_SceneRenderData.Editor.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
			pipelineSpecification.DepthStencilState.DepthWriteEnabled = false;
			pipelineSpecification.DepthStencilState.StencilTestEnabled = false;
			pipelineSpecification.DepthStencilState.Back.WriteMask = 0x00;
			pipelineSpecification.DepthStencilState.Front = pipelineSpecification.DepthStencilState.Back;
			pipelineSpecification.RenderPass = s_SceneRenderData.Grid.RenderPass;
			pipelineSpecification.Shader = shaderGrid;
			pipelineSpecification.Layouts = {
			{
				{ ShaderDataType::Float3, "a_Position" },
				{ ShaderDataType::Float2, "a_TexCoord" },
			}
			};
			pipelineSpecification.Shader = shaderGrid;

			s_SceneRenderData.Grid.Pipeline = Pipeline::Create(pipelineSpecification, "Grid");
			s_SceneRenderData.Grid.Material = Material::Create(s_SceneRenderData.Grid.Pipeline);

			f32 offsetY = -1.0f; // Because the quad size is 2x2
			auto& gridTransform = s_SceneRenderData.GridTransform;
			gridTransform = glm::translate(gridTransform, glm::vec3(2.0f, offsetY, 0.0f));
			gridTransform = glm::rotate(gridTransform, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
			gridTransform = glm::translate(gridTransform, glm::vec3(-2.0f, offsetY, 0.0f));;
			gridTransform = glm::scale(gridTransform, glm::vec3(s_SceneRenderData.GridSize, s_SceneRenderData.GridSize, 1.0f));
		}

		// Outline
		auto shaderOutline = ShaderLibrary::Load("assets/shaders/Outline.glsl");

		// Outline
		{
			s_SceneRenderData.Outline.RenderPass = s_SceneRenderData.Editor.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.Outline.RenderPass;
			pipelineSpecification.Shader = shaderOutline;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};

			pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
			pipelineSpecification.DepthStencilState.Back.CompareOp = CompareOp::NotEqual;
			pipelineSpecification.DepthStencilState.Back.WriteMask = 0x00;
			pipelineSpecification.DepthStencilState.Front = pipelineSpecification.DepthStencilState.Back;

			s_SceneRenderData.Outline.Pipeline = Pipeline::Create(pipelineSpecification, "Outline");

			s_SceneRenderData.Outline.Material = Material::Create(s_SceneRenderData.Outline.Pipeline);
		}

		// Environment
		auto shaderEnvironment = ShaderLibrary::Load("assets/shaders/Environment.glsl");

		// Environment Editor
		{
			s_SceneRenderData.EnvEditor.RenderPass = s_SceneRenderData.Editor.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.EnvEditor.RenderPass;
			pipelineSpecification.Shader = shaderEnvironment;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};
			pipelineSpecification.DepthStencilState.StencilTestEnabled = false;
			pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
			pipelineSpecification.DepthStencilState.DepthWriteEnabled = false;
			pipelineSpecification.DepthStencilState.Back.WriteMask = 0x00;
			pipelineSpecification.DepthStencilState.Front = pipelineSpecification.DepthStencilState.Back;
			pipelineSpecification.CullMode = CullMode::FrontFace;

			s_SceneRenderData.EnvEditor.Pipeline = Pipeline::Create(pipelineSpecification, "EnvironmentEditor");

			s_SceneRenderData.EnvEditor.Material = Material::Create(s_SceneRenderData.EnvEditor.Pipeline);
		}

		// Environment Runtime
		{
			s_SceneRenderData.EnvRuntime.RenderPass = s_SceneRenderData.Runtime.RenderPass;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.EnvRuntime.RenderPass;
			pipelineSpecification.Shader = shaderEnvironment;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};
			pipelineSpecification.DepthStencilState.StencilTestEnabled = false;
			pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
			pipelineSpecification.DepthStencilState.DepthWriteEnabled = false;
			pipelineSpecification.CullMode = CullMode::FrontFace;

			s_SceneRenderData.EnvRuntime.Pipeline = Pipeline::Create(pipelineSpecification, "EnvironmentRuntime");

			s_SceneRenderData.EnvRuntime.Material = Material::Create(s_SceneRenderData.EnvRuntime.Pipeline);
		}

		Renderer::Submit([]
			{
				s_SceneRenderData.CubeMesh = Mesh::Create("assets/models/cube.fbx");
			});

		// Cube Map
		{
			s_SceneRenderData.CubeMap.CommandBuffer = CommandBuffer::CreatePrimary();

			RenderPassSpecification renderPassSpec{};
			renderPassSpec.ColorFormat = ColorFormat::RGBA16F;
			renderPassSpec.FinalLayout = Layout::ColorAttachment;
			renderPassSpec.HasDepthAttachment = false;
			s_SceneRenderData.CubeMap.RenderPass = RenderPass::Create(renderPassSpec);

			FramebufferSpecification framebufferSpecification{};
			framebufferSpecification.RenderPass = s_SceneRenderData.CubeMap.RenderPass;
			framebufferSpecification.Width = 2048;
			framebufferSpecification.Height = 2048;
			s_SceneRenderData.CubeMap.Framebuffer = Framebuffer::Create(framebufferSpecification);

			s_SceneRenderData.CubeMap.RenderPass->SetTargetFramebuffer(s_SceneRenderData.CubeMap.Framebuffer);

			auto& shaderCubeMap = Shader::Create("assets/shaders/Skybox.glsl");

			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.CubeMap.RenderPass;
			pipelineSpecification.Shader = shaderCubeMap;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};

			s_SceneRenderData.CubeMap.Pipeline = Pipeline::Create(pipelineSpecification, "CubeMap");
		}

		// Irradiance map
		{
			s_SceneRenderData.IrradianceMap.CommandBuffer = CommandBuffer::CreatePrimary();

			auto& shaderIrradianceMap = Shader::Create("assets/shaders/IrradianceMap.glsl");

			s_SceneRenderData.IrradianceMap.RenderPass = s_SceneRenderData.CubeMap.RenderPass;
			s_SceneRenderData.IrradianceMap.Framebuffer = s_SceneRenderData.CubeMap.Framebuffer;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.IrradianceMap.RenderPass;
			pipelineSpecification.Shader = shaderIrradianceMap;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};

			s_SceneRenderData.IrradianceMap.Pipeline = Pipeline::Create(pipelineSpecification, "IrradianceMap");
		}

		// Prefiltered cube map
		{
			s_SceneRenderData.PrefilteredCubeMap.CommandBuffer = CommandBuffer::CreatePrimary();

			auto& shaderPrefilteredCubeMap = Shader::Create("assets/shaders/PrefilterCubeMap.glsl");

			s_SceneRenderData.PrefilteredCubeMap.RenderPass = s_SceneRenderData.CubeMap.RenderPass;
			s_SceneRenderData.PrefilteredCubeMap.Framebuffer = s_SceneRenderData.CubeMap.Framebuffer;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.PrefilteredCubeMap.RenderPass;
			pipelineSpecification.Shader = shaderPrefilteredCubeMap;
			pipelineSpecification.Layouts = {
				{
					{
						{ ShaderDataType::Float3, "a_Position" },
						{ ShaderDataType::Float3, "a_Normal" },
						{ ShaderDataType::Float2, "a_TexCoord" },
						{ ShaderDataType::Float3, "a_Tangent" },
						{ ShaderDataType::Float3, "a_Binormal" }
					}
				},
			};

			s_SceneRenderData.PrefilteredCubeMap.Pipeline = Pipeline::Create(pipelineSpecification, "PrefilteredCubeMap");
		}

		// BRDFLUT		
		{
			s_SceneRenderData.BRDFLUT.CommandBuffer = CommandBuffer::CreatePrimary();

			auto& shaderBRDFLUT = Shader::Create("assets/shaders/BRDFLUT.glsl");

			s_SceneRenderData.BRDFLUT.RenderPass = s_SceneRenderData.CubeMap.RenderPass;
			s_SceneRenderData.BRDFLUT.Framebuffer = s_SceneRenderData.CubeMap.Framebuffer;
			PipelineSpecification pipelineSpecification = {};
			pipelineSpecification.RenderPass = s_SceneRenderData.BRDFLUT.RenderPass;
			pipelineSpecification.Shader = shaderBRDFLUT;

			s_SceneRenderData.BRDFLUT.Pipeline = Pipeline::Create(pipelineSpecification, "BRDFLUT");

			Profiler::StartMeasurementRT("Generating BRDFLUT texture");
			s_SceneRenderData.BRDFLUT.Texture = GenerateBRDFLUT();
			Profiler::EndMeasurementRT();
		}


		// Scene renderer buffers data
		{
			s_SceneRenderData.SceneBufferBuildersThreadPool.SetThreadCount(SCENE_BUFFERS_BUILDING_THREADS_COUNT);
		}

		Renderer::Submit([]()
			{
				auto& framebufferTexture = s_SceneRenderData.Editor.Framebuffer->GetFramebufferColorTexture();
				s_SceneRenderData.m_EditorViewportTextureId = framebufferTexture->GetImGuiTexture();
			});

		Renderer::Submit([]()
			{
				auto& framebufferTexture = s_SceneRenderData.Runtime.Framebuffer->GetFramebufferColorTexture();
				s_SceneRenderData.m_RuntimeViewportTextureId = framebufferTexture->GetImGuiTexture();
			});

		Renderer::Submit([]()
			{
				// Default outline settings
				s_SceneRenderData.Outline.Material->Set("Settings.OutlineThickness", 1.05f);
				s_SceneRenderData.Outline.Material->Set("Material.Color", glm::vec4(1.f, 0.66f, 0.f, 1.f));
			});

		Renderer::Submit([]()
			{
				s_SceneRenderData.Grid.Material->Set("Material.Scale", s_SceneRenderData.GridSize);
				s_SceneRenderData.Grid.Material->Set("Material.Resolution", s_SceneRenderData.GridResolution);
			});

		Renderer::Submit([]()
			{
				f32 lod = 1.0f;
				s_SceneRenderData.EnvEditor.Material->Set("pc_Properties.cubeMapLOD", lod);
				s_SceneRenderData.EnvRuntime.Material->Set("pc_Properties.cubeMapLOD", lod);
			});
	}

	void SceneRenderer::BeginFrame()
	{
		Renderer::BeginFrame();

		if (s_SceneRenderData.m_UpdateEditorFramebufferSize)
		{
			UpdateEditorFramebuffersSize();

			s_SceneRenderData.m_UpdateEditorFramebufferSize = false;
		}

		if (s_SceneRenderData.m_UpdateRuntimeFramebufferSize)
		{
			UpdateRuntimeFramebuffersSize();

			s_SceneRenderData.m_UpdateRuntimeFramebufferSize = false;
		}
	}

	void SceneRenderer::EndFrame()
	{
#ifndef DEB_DIST
		auto& sceneOptions = s_SceneRenderData.ActiveScene->GetOptions();
		if (sceneOptions.DisplayEditorViewport)
		{
			CompositePassEditor();
		}
#endif

#ifndef DEB_DIST
		if (sceneOptions.DisplayRuntimeViewport)
#endif
		{
#ifndef DEB_DIST
			Statistics::SetRuntimeViewportDimensions(s_SceneRenderData.EditorViewportDim.first, s_SceneRenderData.EditorViewportDim.second);
			Statistics::SetRuntimeFramebufferSize(s_SceneRenderData.Runtime.Framebuffer->GetSize());
#endif
			CompositePassRuntime();
		}

		Renderer::EndFrame();
	}

	void SceneRenderer::BeginScene(Scene* scene, const SceneRendererCamera& sceneRendererCamera)
	{
		s_SceneRenderData.ActiveScene = scene;
		s_SceneRenderData.SceneRendererCamera = sceneRendererCamera;

		auto& renderPass = scene->GetOptions().CurrentRenderPass;
		Renderer::BeginRenderPass(renderPass);

		auto& frameBuffer = renderPass->GetTargetFramebuffer();

		u32 width = frameBuffer->GetWidth();
		u32 height = frameBuffer->GetHeight();

		Renderer::GetAPI().SetViewport(0, 0, width, height);
	}

	void SceneRenderer::EndScene()
	{
		// Perform batch render only if it the editor's scene viewport is enabled or if game viewport enabled and main camera is present 
		auto& sceneOptions = s_SceneRenderData.ActiveScene->GetOptions();
		if (!sceneOptions.DisplayRuntimeViewport || (sceneOptions.DisplayRuntimeViewport && s_SceneRenderData.ActiveScene->GetMainCamera()))
		{
			BatchRender();
		}
		Renderer::EndRenderPass();
	}

	/*void SceneRenderer::SubmitQuad(const std::shared_ptr<Material>& material, const TransformComponent& tc)
	{
		s_SceneRenderData.QuadsData.push_back({ material, tc });
	}*/

	void SceneRenderer::SubmitMesh(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc)
	{
		s_SceneRenderData.MeshesContainer.push_back({ mesh, tc });
	}

	void SceneRenderer::SubmitMeshOutline(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc)
	{
		s_SceneRenderData.MeshesOutlineContainer.push_back({ mesh, tc });
	}

	void SceneRenderer::SubmitMeshPlain(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc, const glm::vec4& color)
	{
		s_SceneRenderData.ObjectPickingMeshesContainer.push_back({ mesh, tc, color });
	}

	void SceneRenderer::SetEnvironmentMap(const std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>>& environmentMap)
	{
		s_SceneRenderData.EnvMap = environmentMap;

		auto& [radianceMap, irradianceMap] = s_SceneRenderData.EnvMap;
		s_SceneRenderData.EnvEditor.Material->Set("u_TextureCubeMap", radianceMap);
		s_SceneRenderData.EnvRuntime.Material->Set("u_TextureCubeMap", radianceMap);
	}

	std::shared_ptr<RenderPass> SceneRenderer::GetRenderPassObjectPicking()
	{
		return s_SceneRenderData.ObjectPicking.RenderPass;
	}

	std::shared_ptr<RenderPass> SceneRenderer::GetRenderPassEditor()
	{
		return s_SceneRenderData.Editor.RenderPass;
	}

	std::shared_ptr<RenderPass> SceneRenderer::GetRenderPassRuntime()
	{
		return s_SceneRenderData.Runtime.RenderPass;
	}

	void SceneRenderer::SetEditorViewportSize(u32 width, u32 height)
	{
		if (s_SceneRenderData.EditorViewportDim.first != width || s_SceneRenderData.EditorViewportDim.second != height)
		{
			s_SceneRenderData.m_UpdateEditorFramebufferSize = true;
		}

		s_SceneRenderData.EditorViewportDim.first = width;
		s_SceneRenderData.EditorViewportDim.second = height;
	}

	void SceneRenderer::SetRuntimeViewportSize(u32 width, u32 height)
	{
		if (s_SceneRenderData.RuntimeViewportDim.first != width || s_SceneRenderData.RuntimeViewportDim.second != height)
		{
			s_SceneRenderData.m_UpdateRuntimeFramebufferSize = true;
		}

		s_SceneRenderData.RuntimeViewportDim.first = width;
		s_SceneRenderData.RuntimeViewportDim.second = height;
	}

	void SceneRenderer::BatchRender()
	{
		const glm::mat4 projection = s_SceneRenderData.SceneRendererCamera.Camera.GetProjectionMatrix();
		const glm::mat4 viewProjection = projection * s_SceneRenderData.SceneRendererCamera.View;

		const auto& sceneOptions = s_SceneRenderData.ActiveScene->GetOptions();
		const auto& envMapProperties = sceneOptions.EnvProperties;

		if (s_SceneRenderData.EnvMapSubmitted)
		{
			auto& envCube = s_SceneRenderData.CubeMesh;

			std::shared_ptr<Material> envMaterial = nullptr;
			if (sceneOptions.EditorPass)
			{
				envMaterial = s_SceneRenderData.EnvEditor.Material;
				s_SceneRenderData.EnvEditor.Pipeline->Bind();
			}
			else
			{
				envMaterial = s_SceneRenderData.EnvRuntime.Material;
				s_SceneRenderData.EnvRuntime.Pipeline->Bind();
			}

			envMaterial->Set("pc_Properties.cubeMapLOD", envMapProperties.LOD);

			envCube->GetVertexBuffer()->Bind();
			envCube->GetIndexBuffer()->Bind();

			glm::mat4 view = s_SceneRenderData.SceneRendererCamera.View;
			view[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

			auto& submeshes = envCube->GetSubmeshes();
			for (const auto& submesh : submeshes)
			{
				envMaterial->Set("pc_Transform.ViewProjection", projection * view);
				envMaterial->Bind();

				Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1, submesh.BaseIndex, submesh.BaseVertex);
			}

			s_SceneRenderData.EnvMapSubmitted = false;
		}

		if (s_SceneRenderData.GridSubmitted)
		{
			s_SceneRenderData.Grid.Pipeline->Bind();
			s_SceneRenderData.Grid.Material->Set("pc_Transform.ViewProjection", viewProjection);


			Renderer::SubmitQuad({ s_SceneRenderData.Grid.Material, s_SceneRenderData.GridTransform });
			s_SceneRenderData.GridSubmitted = false;
		}

		/*for (const auto& quadData : s_SceneRenderData.m_QuadsData)
		{
			quadData.Material->Set("ViewProjection", viewProjection);

			Renderer::SubmitQuad(quadData);
		}
		s_SceneRenderData.m_QuadsData.clear();*/



		if (s_SceneRenderData.AllSceneMeshesSubmitted)
		{
			RenderAllSceneMeshes();
			s_SceneRenderData.AllSceneMeshesSubmitted = false;
		}

		if (s_SceneRenderData.AllSceneMeshesSubmittedNoBatch)
		{
			RenderAllSceneMeshesNoBatch();
			s_SceneRenderData.AllSceneMeshesSubmittedNoBatch = false;
		}

		if (s_SceneRenderData.AllSceneMeshesObjectPickingSubmitted)
		{
			RenderAllSceneMeshesObjectPicking();
			s_SceneRenderData.AllSceneMeshesObjectPickingSubmitted = false;
		}

		if (s_SceneRenderData.MeshesOutlineContainer.size() > 0)
		{
			s_SceneRenderData.Outline.Pipeline->Bind();
			for (const auto& meshData : s_SceneRenderData.MeshesOutlineContainer)
			{
				meshData.Mesh->GetVertexBuffer()->Bind();
				meshData.Mesh->GetIndexBuffer()->Bind();
				auto& material = s_SceneRenderData.Outline.Material;

				material->Set("pc_Transform.ViewProjection", viewProjection);

				auto& submemeshes = meshData.Mesh->GetSubmeshes();
				for (const auto& submesh : submemeshes)
				{
					material->Set("pc_Transform.Transform", meshData.TransformComponent.GetTransform() * submesh.Transform);
					material->Bind();

					Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1, submesh.BaseIndex, submesh.BaseVertex);
				}
			}

			s_SceneRenderData.MeshesOutlineContainer.clear();
		}
	}

	void SceneRenderer::AllocateNewSceneDataBuffers(InstancedDataBuffers& instancedDataNuffers)
	{
		instancedDataNuffers.MeshInstancedPosAttribBuffer = VertexBuffer::CreateDynamic(INITIAL_SCENE_MESHE_BUFFER_SIZE);
		instancedDataNuffers.MeshInstancedPosBuffer = { nullptr, INITIAL_SCENE_MESHE_BUFFER_SIZE };
		instancedDataNuffers.MeshInstancedDataAttribBuffer = VertexBuffer::CreateDynamic(INITIAL_SCENE_MESHE_BUFFER_SIZE);
		instancedDataNuffers.MeshInstancedDataBuffer = { nullptr, INITIAL_SCENE_MESHE_BUFFER_SIZE };

		instancedDataNuffers.MeshInstancedDataAttribBufferObjectPicking = VertexBuffer::CreateDynamic(INITIAL_SCENE_MESHE_BUFFER_SIZE);
		instancedDataNuffers.MeshInstancedDataBufferObjectPicking = { nullptr, INITIAL_SCENE_MESHE_BUFFER_SIZE };
	}

	void SceneRenderer::DestroySceneDataBuffers(InstancedDataBuffers& instancedDataBuffers)
	{
		instancedDataBuffers.MeshInstancedPosAttribBuffer->Destroy();
		instancedDataBuffers.MeshInstancedPosBuffer.Clear();
		instancedDataBuffers.MeshInstancedDataAttribBuffer->Destroy();
		instancedDataBuffers.MeshInstancedDataBuffer.Clear();

		instancedDataBuffers.MeshInstancedDataAttribBufferObjectPicking->Destroy();
		instancedDataBuffers.MeshInstancedDataBufferObjectPicking.Clear();
	}

	std::shared_ptr<TextureCube> SceneRenderer::GenerateCubeMap(const std::shared_ptr<Texture2D> hdriTexture)
	{
		u32 faceSize = 2048;

		auto cubeMapEmpty = TextureCube::Create(faceSize, faceSize);

		std::vector<glm::mat4> matrices = {
			// POSITIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
		};

		u32 mipMapCount = cubeMapEmpty->GetMipLevelCount();
		u32 facesCount = 6;
		s_SceneRenderData.CubeMap.Materials.reserve(facesCount);
		for (u32 face = 0; face < facesCount; face++)
		{
			auto& material = s_SceneRenderData.CubeMap.Materials.emplace_back(Material::Create(s_SceneRenderData.CubeMap.Pipeline));
			material->Set("u_Texture", hdriTexture);
		}

		Renderer::BeginRecording(s_SceneRenderData.CubeMap.CommandBuffer);
		{
			auto& envCube = s_SceneRenderData.CubeMesh;
			envCube->GetVertexBuffer()->Bind();
			envCube->GetIndexBuffer()->Bind();
			s_SceneRenderData.CubeMap.Pipeline->Bind();
			cubeMapEmpty->ChangeLayoutWrite();
			for (u32 mipMapLevel = 0; mipMapLevel < mipMapCount; mipMapLevel++)
			{
				for (u32 face = 0; face < facesCount; face++)
				{
					u32 viewportSize = faceSize * std::pow(0.5f, mipMapLevel);
					Renderer::GetAPI().SetViewport(0, 0, viewportSize, viewportSize, false);

					Renderer::BeginRenderPass(s_SceneRenderData.CubeMap.RenderPass);
					{
						auto& envMaterial = s_SceneRenderData.CubeMap.Materials[face];

						auto& submeshes = envCube->GetSubmeshes();
						for (const auto& submesh : submeshes)
						{
							glm::mat4 mat = glm::perspective(glm::pi<f32>() / 2.0f, 1.0f, 0.1f, 512.0f) * matrices[face];
							envMaterial->Set("pc_Transform.ViewProjection", mat);
							envMaterial->Bind();

							Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1, submesh.BaseIndex, submesh.BaseVertex);
						}
					}
					Renderer::EndRenderPass();

					// Copy offscreen's color attachment image to the cube map texture
					cubeMapEmpty->CopyImageFromFramebuffer(s_SceneRenderData.CubeMap.Framebuffer, viewportSize, viewportSize, face, mipMapLevel);
				}
			}
		}
		cubeMapEmpty->ChangeLayoutRead();
		Renderer::EndRecording(s_SceneRenderData.CubeMap.CommandBuffer);
		s_SceneRenderData.CubeMap.CommandBuffer->Flush(false);

		Renderer::Submit([]
			{
				// Clean cube map rendering data here...
				s_SceneRenderData.CubeMap.Materials.clear();
			});

		return cubeMapEmpty;
	}

	std::shared_ptr<TextureCube> SceneRenderer::GenerateIrradianceMap(const std::shared_ptr<TextureCube> cubeMap)
	{
		u32 faceSize = 32;

		auto irradianceMapEmpty = TextureCube::Create(faceSize, faceSize);

		std::vector<glm::mat4> matrices = {
			// POSITIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
		};

		u32 mipMapCount = irradianceMapEmpty->GetMipLevelCount();
		u32 facesCount = 6;
		s_SceneRenderData.IrradianceMap.Materials.reserve(facesCount);
		for (u32 face = 0; face < facesCount; face++)
		{
			auto& material = s_SceneRenderData.IrradianceMap.Materials.emplace_back(Material::Create(s_SceneRenderData.IrradianceMap.Pipeline));
			material->Set("u_TextureCubeMap", cubeMap);
		}

		Renderer::BeginRecording(s_SceneRenderData.IrradianceMap.CommandBuffer);
		{
			auto& envCube = s_SceneRenderData.CubeMesh;
			envCube->GetVertexBuffer()->Bind();
			envCube->GetIndexBuffer()->Bind();
			s_SceneRenderData.IrradianceMap.Pipeline->Bind();
			irradianceMapEmpty->ChangeLayoutWrite();
			for (u32 mipMapLevel = 0; mipMapLevel < mipMapCount; mipMapLevel++)
			{
				for (u32 face = 0; face < facesCount; face++)
				{
					u32 viewportSize = faceSize * std::pow(0.5f, mipMapLevel);
					Renderer::GetAPI().SetViewport(0, 0, viewportSize, viewportSize, false);

					Renderer::BeginRenderPass(s_SceneRenderData.IrradianceMap.RenderPass);
					{
						auto& envMaterial = s_SceneRenderData.IrradianceMap.Materials[face];

						auto& submeshes = envCube->GetSubmeshes();
						for (const auto& submesh : submeshes)
						{
							glm::mat4 mat = glm::perspective(glm::pi<f32>() / 2.0f, 1.0f, 0.1f, 512.0f) * matrices[face];
							envMaterial->Set("pc_Transform.ViewProjection", mat);
							envMaterial->Bind();

							Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1, submesh.BaseIndex, submesh.BaseVertex);
						}
					}
					Renderer::EndRenderPass();

					// Copy offscreen's color attachment image to the cube map texture
					irradianceMapEmpty->CopyImageFromFramebuffer(s_SceneRenderData.IrradianceMap.Framebuffer, viewportSize, viewportSize, face, mipMapLevel);
				}
			}
		}
		irradianceMapEmpty->ChangeLayoutRead();
		Renderer::EndRecording(s_SceneRenderData.IrradianceMap.CommandBuffer);
		s_SceneRenderData.IrradianceMap.CommandBuffer->Flush(false);

		Renderer::Submit([]
			{
				// Clean cube map rendering data here...
				s_SceneRenderData.IrradianceMap.Materials.clear();
			});

		return irradianceMapEmpty;
	}

	std::shared_ptr<TextureCube> SceneRenderer::GeneratePrefilteredCubeMap(const std::shared_ptr<TextureCube> cubeMap)
	{
		u32 faceSize = 2048;

		auto prefilteredCubeMapEmpty = TextureCube::Create(faceSize, faceSize);

		std::vector<glm::mat4> matrices = {
			// POSITIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_X
			glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Y
			glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// POSITIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
			// NEGATIVE_Z
			glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
		};

		u32 mipMapCount = prefilteredCubeMapEmpty->GetMipLevelCount();
		u32 facesCount = 6;
		s_SceneRenderData.PrefilteredCubeMap.Materials.reserve(facesCount);
		for (u32 face = 0; face < facesCount; face++)
		{
			auto& material = s_SceneRenderData.PrefilteredCubeMap.Materials.emplace_back(Material::Create(s_SceneRenderData.PrefilteredCubeMap.Pipeline));
			material->Set("u_TextureCubeMap", cubeMap);
			material->Set("pc_Properties.samplesCount", 32);
		}

		Renderer::BeginRecording(s_SceneRenderData.PrefilteredCubeMap.CommandBuffer);
		{
			auto& envCube = s_SceneRenderData.CubeMesh;
			envCube->GetVertexBuffer()->Bind();
			envCube->GetIndexBuffer()->Bind();
			s_SceneRenderData.PrefilteredCubeMap.Pipeline->Bind();
			prefilteredCubeMapEmpty->ChangeLayoutWrite();
			for (u32 mipMapLevel = 0; mipMapLevel < mipMapCount; mipMapLevel++)
			{
				for (u32 face = 0; face < facesCount; face++)
				{
					u32 viewportSize = faceSize * std::pow(0.5f, mipMapLevel);
					Renderer::GetAPI().SetViewport(0, 0, viewportSize, viewportSize, false);

					Renderer::BeginRenderPass(s_SceneRenderData.PrefilteredCubeMap.RenderPass);
					{
						auto& envMaterial = s_SceneRenderData.PrefilteredCubeMap.Materials[face];
						f32 roughness = (f32)mipMapLevel / (f32)(mipMapCount - 1);
						envMaterial->Set("pc_Properties.roughness", roughness);

						auto& submeshes = envCube->GetSubmeshes();
						for (const auto& submesh : submeshes)
						{
							glm::mat4 mat = glm::perspective(glm::pi<f32>() / 2.0f, 1.0f, 0.1f, 512.0f) * matrices[face];
							envMaterial->Set("pc_Transform.ViewProjection", mat);
							envMaterial->Bind();

							Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1, submesh.BaseIndex, submesh.BaseVertex);
						}
					}
					Renderer::EndRenderPass();

					// Copy offscreen's color attachment image to the cube map texture
					prefilteredCubeMapEmpty->CopyImageFromFramebuffer(s_SceneRenderData.PrefilteredCubeMap.Framebuffer, viewportSize, viewportSize, face, mipMapLevel);
				}
			}
		}
		prefilteredCubeMapEmpty->ChangeLayoutRead();
		Renderer::EndRecording(s_SceneRenderData.PrefilteredCubeMap.CommandBuffer);
		s_SceneRenderData.PrefilteredCubeMap.CommandBuffer->Flush(false);

		Renderer::Submit([]
			{
				// Clean cube map rendering data here...
				s_SceneRenderData.PrefilteredCubeMap.Materials.clear();
			});

		return prefilteredCubeMapEmpty;
	}

	std::shared_ptr<Texture2D> SceneRenderer::GenerateBRDFLUT()
	{
		u32 size = 1024;

		auto BRDFLUTTextureEmpty = Texture2D::Create(size, size, ColorFormat::RGBA16F);

		Renderer::BeginRecording(s_SceneRenderData.BRDFLUT.CommandBuffer);
		{
			BRDFLUTTextureEmpty->ChangeLayoutWrite();
			Renderer::GetAPI().SetViewport(0, 0, size, size, false);

			Renderer::BeginRenderPass(s_SceneRenderData.BRDFLUT.RenderPass);
			{
				s_SceneRenderData.BRDFLUT.Pipeline->Bind();

				Renderer::GetAPI().Draw(3);
			}
			Renderer::EndRenderPass();

			// Copy offscreen's color attachment image to the cube map texture
			BRDFLUTTextureEmpty->CopyImageFromFramebuffer(s_SceneRenderData.BRDFLUT.Framebuffer, size, size, 0, 0);
			BRDFLUTTextureEmpty->ChangeLayoutRead();
		}
		Renderer::EndRecording(s_SceneRenderData.BRDFLUT.CommandBuffer);
		s_SceneRenderData.BRDFLUT.CommandBuffer->Flush(false);

		return BRDFLUTTextureEmpty;
	}

	void SceneRenderer::UpdateEditorFramebuffersSize()
	{
#ifndef DEB_DIST
		u32 width = s_SceneRenderData.EditorViewportDim.first;
		u32 height = s_SceneRenderData.EditorViewportDim.second;

		if (!s_SceneRenderData.ActiveScene->GetOptions().DisplayRuntimeViewport)
		{
			UpdateRuntimeFramebuffersSize();
			// Resize camera's projection matrix to the actual viewport size
			s_SceneRenderData.ActiveScene->SetViewportSize(width, height);
		}

		Renderer::Submit([width, height]()
			{
				s_SceneRenderData.Editor.Framebuffer->Resize(width, height);
				s_SceneRenderData.CompositeEditor.Framebuffer->Resize(width, height);
				s_SceneRenderData.ObjectPicking.Framebuffer->Resize(width, height);

				auto& framebufferTextureComposite = s_SceneRenderData.CompositeEditor.Framebuffer->GetFramebufferColorTexture();
				s_SceneRenderData.m_EditorViewportTextureId = framebufferTextureComposite->GetImGuiTexture();

				s_SceneRenderData.ActiveScene->ScheduleRuntimeViewportUpdate();
			});
		auto& framebufferTexture = s_SceneRenderData.Editor.Framebuffer->GetFramebufferColorTexture();
		s_SceneRenderData.CompositeEditor.Material->Set("u_Texture", framebufferTexture);
#endif
	}

	void SceneRenderer::UpdateRuntimeFramebuffersSize()
	{
#ifndef DEB_DIST
		u32 width = s_SceneRenderData.RuntimeViewportDim.first;
		u32 height = s_SceneRenderData.RuntimeViewportDim.second;

		if (!s_SceneRenderData.ActiveScene->GetOptions().DisplayEditorViewport)
		{
			UpdateEditorFramebuffersSize();
		}

		s_SceneRenderData.ActiveScene->SetViewportSize(width, height);

		Renderer::Submit([width, height]()
			{
				s_SceneRenderData.Runtime.Framebuffer->Resize(width, height);
				s_SceneRenderData.CompositeRuntime.Framebuffer->Resize(width, height);

				auto& framebufferTextureComposite = s_SceneRenderData.CompositeRuntime.Framebuffer->GetFramebufferColorTexture();
				s_SceneRenderData.m_RuntimeViewportTextureId = framebufferTextureComposite->GetImGuiTexture();

				s_SceneRenderData.ActiveScene->ScheduleRuntimeViewportUpdate();
			});
		auto& framebufferTexture = s_SceneRenderData.Runtime.Framebuffer->GetFramebufferColorTexture();
		s_SceneRenderData.CompositeRuntime.Material->Set("u_Texture", framebufferTexture);

		s_SceneRenderData.ActiveScene->ScheduleRuntimeViewportUpdate();
#endif
	}

	void SceneRenderer::CompositePassEditor()
	{
		Renderer::BeginRenderPass(s_SceneRenderData.CompositeEditor.RenderPass);

		auto& frameBuffer = s_SceneRenderData.CompositeEditor.RenderPass->GetTargetFramebuffer();
		u32 width = frameBuffer->GetWidth();
		u32 height = frameBuffer->GetHeight();
		Renderer::GetAPI().SetViewport(0, 0, width, height);

		s_SceneRenderData.CompositeEditor.Pipeline->Bind();
		auto& framebufferTexture = s_SceneRenderData.Editor.Framebuffer->GetFramebufferColorTexture();
		if (framebufferTexture != s_SceneRenderData.CurrentEditorFramebufferTexture)
		{
			s_SceneRenderData.CurrentEditorFramebufferTexture = framebufferTexture;
			s_SceneRenderData.CompositeEditor.Material->Set("u_Texture", framebufferTexture);
		}
		s_SceneRenderData.CompositeEditor.Material->Set("pc_Properties.Exposure", s_SceneRenderData.ActiveScene->GetEditorCamera()->GetExposure());

		Renderer::SubmitFullscrenQuad(s_SceneRenderData.CompositeEditor.Material);

		Renderer::EndRenderPass();
	}

	void SceneRenderer::CompositePassRuntime()
	{
#ifndef DEB_DIST
		Renderer::BeginRenderPass(s_SceneRenderData.CompositeRuntime.RenderPass);

		auto& frameBuffer = s_SceneRenderData.CompositeRuntime.RenderPass->GetTargetFramebuffer();
#else
		Renderer::BeginRenderPass(Renderer::GetRenderPassSwapChain());

		auto& frameBuffer = Renderer::GetRenderPassSwapChain()->GetTargetFramebuffer();
#endif
		u32 width = frameBuffer->GetWidth();
		u32 height = frameBuffer->GetHeight();
		Renderer::GetAPI().SetViewport(0, 0, width, height);

		s_SceneRenderData.CompositeRuntime.Pipeline->Bind();
		auto& framebufferTexture = s_SceneRenderData.Runtime.Framebuffer->GetFramebufferColorTexture();
		if (framebufferTexture != s_SceneRenderData.m_CurrentRuntimeFramebufferTexture)
		{
			s_SceneRenderData.m_CurrentRuntimeFramebufferTexture = framebufferTexture;
			s_SceneRenderData.CompositeRuntime.Material->Set("u_Texture", framebufferTexture);
		}

		if (s_SceneRenderData.ActiveScene && s_SceneRenderData.ActiveScene->GetMainCamera())
			s_SceneRenderData.CompositeRuntime.Material->Set("pc_Properties.Exposure", s_SceneRenderData.ActiveScene->GetMainCamera()->GetExposure());

		Renderer::SubmitFullscrenQuad(s_SceneRenderData.CompositeRuntime.Material);

		Renderer::EndRenderPass();
	}

	ImTextureID SceneRenderer::GetEditorImGuiTextureID()
	{
		return s_SceneRenderData.m_EditorViewportTextureId;
	}

	ImTextureID SceneRenderer::GetRuntimeImGuiTextureID()
	{
		return s_SceneRenderData.m_RuntimeViewportTextureId;
	}

	std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>> SceneRenderer::CreateEnvironmentMap(const std::string& path)
	{
		// Clear all textures if loaded
		s_SceneRenderData.m_HdirTexture = Texture2D::Create(path);

		Profiler::StartMeasurementRT("Generating Cube Map from HDRi");
		s_SceneRenderData.CubeMap.TextureCube = GenerateCubeMap(s_SceneRenderData.m_HdirTexture);
		Profiler::EndMeasurementRT();

		Profiler::StartMeasurementRT("Generating Prefiltered Cube Map from Cube Map");
		auto prefilteredCubeMap = GeneratePrefilteredCubeMap(s_SceneRenderData.CubeMap.TextureCube);
		Profiler::EndMeasurementRT();

		Profiler::StartMeasurementRT("Generating Irradiacne Map from Cube Map");
		auto irradianceMap = GenerateIrradianceMap(s_SceneRenderData.CubeMap.TextureCube);
		Profiler::EndMeasurementRT();

		s_SceneRenderData.m_HdirTexture->Destroy();
		s_SceneRenderData.CubeMap.TextureCube->Destroy();

		return { prefilteredCubeMap, irradianceMap };
	}

	std::shared_ptr<DebEngine::TextureCube> SceneRenderer::CreateEnvironmentMap(const std::vector<std::string>& facesPaths)
	{
		auto cubeMap = TextureCube::Create(facesPaths);

		return cubeMap;
	}

	void SceneRenderer::SubmitAllSceneMeshes()
	{
		s_SceneRenderData.AllSceneMeshesSubmitted = true;
	}

	void SceneRenderer::SubmitAllSceneMeshesNoBatch()
	{
		s_SceneRenderData.AllSceneMeshesSubmittedNoBatch = true;
	}

	void SceneRenderer::SubmitAllSceneMeshesObjectPicking()
	{
		s_SceneRenderData.AllSceneMeshesObjectPickingSubmitted = true;
	}

	void SceneRenderer::SubmitGrid()
	{
		s_SceneRenderData.GridSubmitted = true;
	}

	void SceneRenderer::SubmitEnvironmentMap()
	{
		s_SceneRenderData.EnvMapSubmitted = true;
	}

	void SceneRenderer::RenderAllSceneMeshes()
	{
		auto scene = s_SceneRenderData.ActiveScene;
		auto& sceneOptions = scene->GetOptions();

		const glm::mat4 projection = s_SceneRenderData.SceneRendererCamera.Camera.GetProjectionMatrix();
		const glm::mat4 viewProjection = projection * s_SceneRenderData.SceneRendererCamera.View;

		const auto& directionalLight = sceneOptions.DirectionalLight;
		const auto& camera = sceneOptions.Camera;
		const auto& envMapProperties = sceneOptions.EnvProperties;
		auto& [radianceMap, irradianceMap] = s_SceneRenderData.EnvMap;

		if (sceneOptions.EditorPass)
			s_SceneRenderData.PBREditor.Pipeline->Bind();
		else
			s_SceneRenderData.PBRRuntime.Pipeline->Bind();
		for (const auto& [path, entityContainer] : scene->m_EntityMeshPathGrouped)
		{
			if (entityContainer.empty())
			{
				auto meshPath = path;
				auto& sceneRendererBuffers = SceneRenderer::GlobalData.PerMeshLoadedBuffers[meshPath];
				DestroySceneDataBuffers(sceneRendererBuffers);
				Renderer::Submit([meshPath]()
					{
						// Remove the entry once buffers are cleared (
						SceneRenderer::GlobalData.PerMeshLoadedBuffers.erase(meshPath);
					});

				continue;
			}


			// Build instanced meshes attrib buffer first
			const auto& meshOrigin = MeshLibrary::Get(path);
			const auto& submeshesOrigin = meshOrigin->GetSubmeshes();

			auto& buffers = GlobalData.PerMeshLoadedBuffers.at(path);
			auto& posAttribBuffer = buffers.MeshInstancedPosAttribBuffer;
			auto& posBuffer = buffers.MeshInstancedPosBuffer;
			auto& dataAttribBuffer = buffers.MeshInstancedDataAttribBuffer;
			auto& dataBuffer = buffers.MeshInstancedDataBuffer;

			// All submeshes data size
			u32 currentPosDataSize = u32(entityContainer.size() * submeshesOrigin.size()) * sizeof(MeshInstancedPos);
			u32 currentDataSize = u32(entityContainer.size() * submeshesOrigin.size()) * sizeof(MeshInstancedData);

			// Check if buffers have approperiate size, if not resize them
			if (currentPosDataSize > posBuffer.Size)
			{
				RecreateSceneBuffer(posBuffer, currentPosDataSize);
			}

			if (currentDataSize > dataBuffer.Size)
			{
				RecreateSceneBuffer(dataBuffer, currentDataSize);
			}

			if (entityContainer.size() > SCENE_ENTITIES_DATA_BUFFER_BUILDING_MULTITHREADED_ENABLED_MIN_COUNT)
			{
				// Split the work between threads
				auto& threadPool = s_SceneRenderData.SceneBufferBuildersThreadPool;
				u32 entitiesCount = entityContainer.size();
				u32 threadCount = threadPool.Threads.size();
				u32 threadJobCount = entitiesCount / threadCount;

				auto& entityContainerRef = entityContainer;
				for (u32 threadIndex = 0; threadIndex < threadCount; threadIndex++)
				{
					auto& currentThread = threadPool.Threads[threadIndex];


					auto threadJob = [threadIndex, threadJobCount, &entityContainerRef, scene, &sceneOptions, &posBuffer, &dataBuffer, entitiesCount, threadCount]()
					{
						u32 startIndex = threadIndex * threadJobCount;
						u32 endIndex = (threadIndex + 1) * threadJobCount;

						// if number of entities isn't dividable by the thread count, make sure that the leftover jobs are executed on the last thread
						if (threadIndex == threadCount - 1)
						{
							if (threadJobCount * threadIndex < entitiesCount)
							{
								endIndex += entitiesCount - threadJobCount * threadCount;
							}
						}

						u32 meshIndex = startIndex;
						for (u32 i = startIndex; i < endIndex; i++)
						{
							const auto entityHandle = entityContainerRef[i];
							Entity entity = { (entt::entity)entityHandle, scene };
							const auto& tc = entity.GetComponent<TransformComponent>();
							// Should rather calculate transform inside the shader (around 1ms on frame performance gain)
							auto meshTransform = tc.GetTransform();
							const auto& mesh = scene->m_EntityMeshLookup[entityHandle];
							const auto& submeshes = mesh->GetSubmeshes();
							u32 submeshIndex = 0;
							for (const auto& submesh : submeshes)
							{
								std::shared_ptr<Material> material = nullptr;
								if (sceneOptions.EditorPass)
									material = mesh->GetSubmeshEditorMaterial(submesh);
								else
									material = mesh->GetSubmeshRuntimeMaterial(submesh);

								// Write mesh position data to a buffer
								{
									auto transform = meshTransform * submesh.Transform;
									MeshInstancedPos meshInstancedPos = { transform };
									u32 dataSize = sizeof(MeshInstancedPos);
									u32 offset = (meshIndex * (u32)submeshes.size() + submeshIndex) * dataSize;
									DEB_CORE_ASSERT(offset < posBuffer.Size, "Invalid offset!");
									posBuffer.Write((u8*)&meshInstancedPos, dataSize, offset);
								}

								// Write mesh specific data to a buffer
								{
									auto meshInstancedData = GetMeshData(material);
									u32 dataSize = sizeof(MeshInstancedData);
									u32 offset = meshIndex * (u32)submeshes.size() * dataSize + submeshIndex * dataSize;
									DEB_CORE_ASSERT(offset < dataBuffer.Size, "Invalid offset!");
									dataBuffer.Write((u8*)&meshInstancedData, dataSize, offset);
								}

								submeshIndex++;
							}
							meshIndex++;
						}
					};
					currentThread->AddJob(threadJob);
				}

				threadPool.ExecuteThreads();
			}
			else
			{
				u32 meshIndex = 0;
				for (const auto& entityHandle : entityContainer)
				{
					Entity entity = { (entt::entity)entityHandle, scene };
					const auto& tc = entity.GetComponent<TransformComponent>();
					// Should rather calculate transform inside the shader (around 1ms on frame performance gain)
					auto meshTransform = tc.GetTransform();
					const auto& mesh = scene->m_EntityMeshLookup[entityHandle];
					const auto& submeshes = mesh->GetSubmeshes();
					u32 submeshIndex = 0;
					for (const auto& submesh : submeshes)
					{
						std::shared_ptr<Material> material = nullptr;
						if (sceneOptions.EditorPass)
							material = mesh->GetSubmeshEditorMaterial(submesh);
						else
							material = mesh->GetSubmeshRuntimeMaterial(submesh);

						// Write mesh position data to a buffer
						{
							auto transform = meshTransform * submesh.Transform;
							MeshInstancedPos meshInstancedPos = { transform };
							u32 dataSize = sizeof(MeshInstancedPos);
							u32 offset = meshIndex * (u32)submeshesOrigin.size() * dataSize + submeshIndex * dataSize;
							DEB_CORE_ASSERT(offset < posBuffer.Size, "Invalid offset!");
							posBuffer.Write((u8*)&meshInstancedPos, dataSize, offset);
						}

						// Write mesh specific data to a buffer
						{
							//auto propVal = std::get<glm::vec4>(material->GetProperty(MaterialProperty::Color));
							auto meshInstancedData = GetMeshData(material);
							u32 dataSize = sizeof(MeshInstancedData);
							u32 offset = meshIndex * (u32)submeshesOrigin.size() * dataSize + submeshIndex * dataSize;
							DEB_CORE_ASSERT(offset < dataBuffer.Size, "Invalid offset!");
							dataBuffer.Write((u8*)&meshInstancedData, dataSize, offset);
						}

						submeshIndex++;
					}
					meshIndex++;
				}
			}

			// Update instacned data buffer
			{
				Buffer buffer = { posBuffer.Data, u32(entityContainer.size() * sizeof(MeshInstancedPos)) };
				posAttribBuffer->UpdateBuffer(buffer);
			}

			{
				Buffer buffer = { dataBuffer.Data, u32(entityContainer.size() * sizeof(MeshInstancedData)) };
				dataAttribBuffer->UpdateBuffer(buffer);
			}

			std::shared_ptr<Material> materialOrigin = nullptr;
			for (const auto& submesh : submeshesOrigin)
			{
				if (sceneOptions.EditorPass)
					materialOrigin = meshOrigin->GetSubmeshEditorMaterial(submesh);
				else
					materialOrigin = meshOrigin->GetSubmeshRuntimeMaterial(submesh);

				// Update submesh material data
				if (radianceMap) materialOrigin->Set("u_TextureRadiance", radianceMap);
				if (irradianceMap) materialOrigin->Set("u_TextureIrradiance", irradianceMap);
				if (s_SceneRenderData.BRDFLUT.Texture) materialOrigin->Set("u_TextureBRDF", s_SceneRenderData.BRDFLUT.Texture);
				materialOrigin->Set("pc_Transform.ViewProjection", viewProjection);
				//material->Set("pc_Transform.Transform", meshData.TransformComponent.GetTransform() * submesh.Transform);
				//material->Set("pc_Transform.Transform", glm::mat4(1.0f));
				materialOrigin->Set("Camera.Position", camera.Position);
				materialOrigin->Set("DirectionalLight.Direction", directionalLight.Direction);
				materialOrigin->Set("DirectionalLight.Radiance", directionalLight.Radiance);
				materialOrigin->Set("DirectionalLight.Intensity", directionalLight.Intensity);
				materialOrigin->Set("Environment.EnvMapRotation", envMapProperties.Rotation);
				materialOrigin->Bind();

				// Bind submesh vertex buffer
				// Bind submesh index buffer
				// Bind instanced buffer
				submesh.SubmeshVertexBuffer->Bind();
				submesh.SubmeshIndexBuffer->Bind();
				posAttribBuffer->Bind(1);
				dataAttribBuffer->Bind(2);

				// Render submesh instanced..
				Renderer::GetAPI().DrawIndexed(submesh.IndexCount, entityContainer.size());
			}
		}
	}

	void SceneRenderer::RenderAllSceneMeshesNoBatch()
	{
		auto scene = s_SceneRenderData.ActiveScene;
		auto& sceneOptions = scene->GetOptions();

		const glm::mat4 projection = s_SceneRenderData.SceneRendererCamera.Camera.GetProjectionMatrix();
		const glm::mat4 viewProjection = projection * s_SceneRenderData.SceneRendererCamera.View;

		const auto& directionalLight = sceneOptions.DirectionalLight;
		const auto& camera = sceneOptions.Camera;
		const auto& envMapProperties = sceneOptions.EnvProperties;
		auto& [radianceMap, irradianceMap] = s_SceneRenderData.EnvMap;

		if (sceneOptions.EditorPass)
			s_SceneRenderData.PBREditor.Pipeline->Bind();
		else
			s_SceneRenderData.PBRRuntime.Pipeline->Bind();
		for (const auto& [path, entityContainer] : scene->m_EntityMeshPathGrouped)
		{
			if (entityContainer.empty())
			{
				auto meshPath = path;
				auto& sceneRendererBuffers = SceneRenderer::GlobalData.PerMeshLoadedBuffers[meshPath];
				DestroySceneDataBuffers(sceneRendererBuffers);
				Renderer::Submit([meshPath]()
					{
						// Remove the entry once buffers are cleared (
						SceneRenderer::GlobalData.PerMeshLoadedBuffers.erase(meshPath);
					});

				continue;
			}


			// Build instanced meshes attrib buffer first
			const auto& meshOrigin = MeshLibrary::Get(path);
			const auto& submeshesOrigin = meshOrigin->GetSubmeshes();

			auto& buffers = GlobalData.PerMeshLoadedBuffers.at(path);
			auto& posAttribBuffer = buffers.MeshInstancedPosAttribBuffer;
			auto& posBuffer = buffers.MeshInstancedPosBuffer;
			auto& dataAttribBuffer = buffers.MeshInstancedDataAttribBuffer;
			auto& dataBuffer = buffers.MeshInstancedDataBuffer;

			// All submeshes data size
			u32 currentPosDataSize = u32(entityContainer.size() * submeshesOrigin.size()) * sizeof(MeshInstancedPos);
			u32 currentDataSize = u32(entityContainer.size() * submeshesOrigin.size()) * sizeof(MeshInstancedData);

			// Check if buffers have approperiate size, if not resize them
			if (currentPosDataSize > posBuffer.Size)
			{
				RecreateSceneBuffer(posBuffer, currentPosDataSize);
			}

			if (currentDataSize > dataBuffer.Size)
			{
				RecreateSceneBuffer(dataBuffer, currentDataSize);
			}


			for (const auto& entityHandle : entityContainer)
			{
				Entity entity = { (entt::entity)entityHandle, scene };
				const auto& tc = entity.GetComponent<TransformComponent>();
				// Should rather calculate transform inside the shader (around 1ms on frame performance gain)
				auto meshTransform = tc.GetTransform();
				const auto& mesh = scene->m_EntityMeshLookup[entityHandle];
				const auto& submeshes = mesh->GetSubmeshes();
				for (const auto& submesh : submeshes)
				{
					std::shared_ptr<Material> material = nullptr;
					if (sceneOptions.EditorPass)
						material = mesh->GetSubmeshEditorMaterial(submesh);
					else
						material = mesh->GetSubmeshRuntimeMaterial(submesh);

					if (radianceMap) material->Set("u_TextureRadiance", radianceMap);
					if (irradianceMap) material->Set("u_TextureIrradiance", irradianceMap);
					if (s_SceneRenderData.BRDFLUT.Texture) material->Set("u_TextureBRDF", s_SceneRenderData.BRDFLUT.Texture);
					material->Set("pc_Transform.ViewProjection", viewProjection);
					//material->Set("pc_Transform.Transform", meshData.TransformComponent.GetTransform() * submesh.Transform);
					//material->Set("pc_Transform.Transform", glm::mat4(1.0f));
					material->Set("Camera.Position", camera.Position);
					material->Set("DirectionalLight.Direction", directionalLight.Direction);
					material->Set("DirectionalLight.Radiance", directionalLight.Radiance);
					material->Set("DirectionalLight.Intensity", directionalLight.Intensity);
					material->Set("Environment.EnvMapRotation", envMapProperties.Rotation);
					material->Bind();

					submesh.SubmeshVertexBuffer->Bind();
					submesh.SubmeshIndexBuffer->Bind();
					posAttribBuffer->Bind(1);
					dataAttribBuffer->Bind(2);

					//auto transform = meshTransform * submesh.Transform;

					Renderer::GetAPI().DrawIndexed(submesh.IndexCount, 1);
				}
			}
		}
	}

	void SceneRenderer::RenderAllSceneMeshesObjectPicking()
	{
		auto scene = s_SceneRenderData.ActiveScene;
		auto& sceneOptions = scene->GetOptions();

		const glm::mat4 projection = s_SceneRenderData.SceneRendererCamera.Camera.GetProjectionMatrix();
		const glm::mat4 viewProjection = projection * s_SceneRenderData.SceneRendererCamera.View;

		s_SceneRenderData.ObjectPicking.Pipeline->Bind();
		for (const auto& [path, entityContainer] : scene->m_EntityMeshPathGrouped)
		{
			// Build instanced meshes attrib buffer first
			const auto& meshOrigin = MeshLibrary::Get(path);
			const auto& submeshesOrigin = meshOrigin->GetSubmeshes();

			auto& buffers = GlobalData.PerMeshLoadedBuffers.at(path);
			auto& posAttribBuffer = buffers.MeshInstancedPosAttribBuffer;
			auto& dataAttribBuffer = buffers.MeshInstancedDataAttribBufferObjectPicking;
			auto& dataBuffer = buffers.MeshInstancedDataBufferObjectPicking;

			// All submeshes data size
			u32 currentDataSize = entityContainer.size() * submeshesOrigin.size() * sizeof(MeshInstancedDataObjectPicking);

			// Check if buffers have approperiate size, if not resize them
			if (currentDataSize > dataBuffer.Size)
			{
				RecreateSceneBuffer(dataBuffer, currentDataSize);
			}

			if (entityContainer.size() > SCENE_ENTITIES_DATA_BUFFER_BUILDING_MULTITHREADED_ENABLED_MIN_COUNT)
			{
				// Split the work between threads
				auto& threadPool = s_SceneRenderData.SceneBufferBuildersThreadPool;
				u32 entitiesCount = entityContainer.size();
				u32 threadCount = threadPool.Threads.size();
				u32 threadJobCount = (u32)entityContainer.size() / threadCount;

				auto& entityContainerRef = entityContainer;
				for (u32 threadIndex = 0; threadIndex < threadCount; threadIndex++)
				{
					auto& currentThread = threadPool.Threads[threadIndex];
					auto threadJob = [threadIndex, threadJobCount, &entityContainerRef, scene, &sceneOptions, &dataBuffer, threadCount, entitiesCount]()
					{
						u32 startIndex = threadIndex * threadJobCount;
						u32 endIndex = (threadIndex + 1) * threadJobCount;

						// if number of entities isn't dividable by the thread count, make sure that the leftover jobs are executed on the last thread
						if (threadIndex == threadCount - 1)
						{
							if (threadJobCount * threadIndex < entitiesCount)
							{
								endIndex += entitiesCount - threadJobCount * threadCount;
							}
						}

						u32 meshIndex = startIndex;
						for (u32 i = startIndex; i < endIndex; i++)
						{
							const auto entityHandle = entityContainerRef[i];
							u32 id = (u32)entityHandle + 1;
							auto colorRGBA = Utils::GetRGBAFromHex(id);
							// Save colorRGBA
							auto colorNormalized = Utils::FromRGBA(colorRGBA);

							const auto& mesh = scene->m_EntityMeshLookup[entityHandle];
							const auto& submeshes = mesh->GetSubmeshes();
							u32 submeshIndex = 0;
							for (const auto& submesh : submeshes)
							{
								// Write object colors to a buffer (for object picking references)
								{
									//auto propVal = std::get<glm::vec4>(material->GetProperty(MaterialProperty::Color));
									MeshInstancedDataObjectPicking meshInstancedData = { colorNormalized };
									u32 dataSize = sizeof(MeshInstancedDataObjectPicking);
									u32 offset = (meshIndex * (u32)submeshes.size() + submeshIndex) * dataSize;
									DEB_CORE_ASSERT(offset < dataBuffer.Size, "Invalid offset!");
									dataBuffer.Write((u8*)&meshInstancedData, dataSize, offset);
								}

								submeshIndex++;
							}
							meshIndex++;
						}
					};
					currentThread->AddJob(threadJob);
				}

				threadPool.ExecuteThreads();
			}
			else
			{
				u32 meshIndex = 0;
				for (const auto& entityHandle : entityContainer)
				{
					u32 id = (u32)entityHandle + 1;
					auto colorRGBA = Utils::GetRGBAFromHex(id);
					// Save colorRGBA
					auto colorNormalized = Utils::FromRGBA(colorRGBA);

					const auto& mesh = scene->m_EntityMeshLookup[entityHandle];
					const auto& submeshes = mesh->GetSubmeshes();
					u32 submeshIndex = 0;
					for (const auto& submesh : submeshes)
					{
						// Write mesh specific data to a buffer
						{
							//auto propVal = std::get<glm::vec4>(material->GetProperty(MaterialProperty::Color));
							MeshInstancedDataObjectPicking meshInstancedData = { colorNormalized };
							u32 dataSize = sizeof(MeshInstancedDataObjectPicking);
							u32 offset = meshIndex * (u32)submeshesOrigin.size() * dataSize + submeshIndex * dataSize;
							DEB_CORE_ASSERT(offset < dataBuffer.Size, "Invalid offset!");
							dataBuffer.Write((u8*)&meshInstancedData, dataSize, offset);
						}

						submeshIndex++;
					}
					meshIndex++;
				}
			}

			{
				Buffer buffer = { dataBuffer.Data, u32(entityContainer.size() * sizeof(MeshInstancedDataObjectPicking)) };
				dataAttribBuffer->UpdateBuffer(buffer);
			}

			for (const auto& submesh : submeshesOrigin)
			{
				auto& material = s_SceneRenderData.ObjectPicking.Material;

				material->Set("pc_Transform.ViewProjection", viewProjection);
				material->Bind();

				// Bind submesh vertex buffer
				// Bind submesh index buffer
				// Bind instanced buffer
				submesh.SubmeshVertexBuffer->Bind();
				submesh.SubmeshIndexBuffer->Bind();
				posAttribBuffer->Bind(1);
				dataAttribBuffer->Bind(2);

				// Render submesh instanced..
				Renderer::GetAPI().DrawIndexed(submesh.IndexCount, entityContainer.size());
			}
		}
	}

	void SceneRenderer::RecreateSceneBuffer(Buffer& buffer, u32 newSize)
	{
		// Allocate a bigger buffer than needed. 
		u32 initialSize = INITIAL_SCENE_MESHE_BUFFER_SIZE;
		u32 calculatedSize = std::ceil((f32)newSize / (f32)initialSize) * initialSize;
		buffer.Allocate(calculatedSize);
	}

	SceneRenderer::MeshInstancedData SceneRenderer::GetMeshData(const std::shared_ptr<Material>& material)
	{
		MeshInstancedData mid = {};
		mid.Color = material->GetProperty<glm::vec4>(MaterialProperty::Color);
		mid.Roughness = material->GetProperty<f32>(MaterialProperty::Roughness);
		mid.Metalness = material->GetProperty<f32>(MaterialProperty::Metalness);

		return mid;
	}
}