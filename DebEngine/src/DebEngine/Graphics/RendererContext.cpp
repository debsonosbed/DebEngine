#include "debpch.h"
#include "RendererContext.h"

#include "RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanContext.h"

namespace DebEngine
{
	std::shared_ptr<DebEngine::RendererContext> RendererContext::Create(SDL_Window* windowHandle)
	{
		switch (RendererAPI::CurrentAPIType())
		{
			case RendererAPIType::None:		DEB_CORE_ASSERT(false, "RendererContext-> Invalid RendererAPI (none)"); return nullptr;
			case RendererAPIType::Vulkan:	return std::make_shared<VulkanContext>(windowHandle);
		}

		DEB_CORE_ASSERT(false, "RendererContext -> Unknown RendererAPI!")
		return nullptr;
	}
}