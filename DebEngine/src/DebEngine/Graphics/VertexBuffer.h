#pragma once

#include "DebEngine/Core/Buffer.h"

namespace DebEngine
{
	enum class ShaderDataType
	{
		None = 0,
		Bool,
		Int,
		Int2,
		Int3,
		Int4,
		Float,
		Float2,
		Float3,
		Float4,
		Mat3,
		Mat4
	};

	static u32 ShaderDataTypeSize(ShaderDataType type)
	{
		switch (type)
		{
		case ShaderDataType::Bool:		return 1;
		case ShaderDataType::Int:		return 4;
		case ShaderDataType::Int2:		return 4 * 2;
		case ShaderDataType::Int3:		return 4 * 3;
		case ShaderDataType::Int4:		return 4 * 4;
		case ShaderDataType::Float:		return 4;
		case ShaderDataType::Float2:	return 4 * 2;
		case ShaderDataType::Float3:	return 4 * 3;
		case ShaderDataType::Float4:	return 4 * 4;
		case ShaderDataType::Mat3:		return 4 * 3 * 3;
		case ShaderDataType::Mat4:		return 4 * 4 * 4;
		}

		DEB_CORE_ASSERT(false, "DebEngine::Buffer -> Invalid ShaderDataType!");
		return 0;
	}

	struct VertexBufferElement
	{
		ShaderDataType Type;
		std::string Name;
		u32 Size;
		u32 Offset;
		b8 Normalized;

		VertexBufferElement() = default;
		VertexBufferElement(ShaderDataType type, const std::string& name, b8 normalized = false)
			: Type(type), Name(name), Size(ShaderDataTypeSize(type)), Offset(0), Normalized(normalized) { }

		u32 GetComponentCount() const
		{
			switch (Type)
			{
			case ShaderDataType::Bool:		return 1;
			case ShaderDataType::Int:		return 1;
			case ShaderDataType::Int2:		return 2;
			case ShaderDataType::Int3:		return 3;
			case ShaderDataType::Int4:		return 4;
			case ShaderDataType::Float:		return 1;
			case ShaderDataType::Float2:	return 2;
			case ShaderDataType::Float3:	return 3;
			case ShaderDataType::Float4:	return 4;
			case ShaderDataType::Mat3:		return 3 * 3;
			case ShaderDataType::Mat4:		return 4 * 4;
			}

			DEB_CORE_ASSERT(false, "DebEngine::Buffer -> Invalid ShaderDataType!");
			return 0;
		}
	};

	class VertexBufferLayout
	{
	public:
		VertexBufferLayout() = default;
		VertexBufferLayout(const std::initializer_list<VertexBufferElement>& elements, b8 instanceLayout = false)
			: m_Elements(elements), m_InstanceLayout(instanceLayout)
		{
			CalculateOffsetAndStride();
		}

		const u32 GetStride() const { return m_Stride; }
		const std::vector<VertexBufferElement>& GetElements() const { return m_Elements; }
		const u32 GetElementCount() const;
		const b8 IsInstanceLayout() const;

		std::vector<VertexBufferElement>::iterator begin();
		std::vector<VertexBufferElement>::iterator end();
		std::vector<VertexBufferElement>::const_iterator begin() const;
		std::vector<VertexBufferElement>::const_iterator end() const;
	private:
		void CalculateOffsetAndStride();
	private:
		b8 m_InstanceLayout;
		u32	m_Stride = 0;
		std::vector<VertexBufferElement> m_Elements;
	};

	enum class VertexBufferUsage
	{
		None = 0, Static = 1, Dynamic = 2
	};

	class VertexBuffer {
	public:
		virtual ~VertexBuffer() = default;

		virtual void UpdateBuffer(Buffer& buffer, u32 offset = 0) = 0;
		virtual void Bind(u32 bindingPoint = 0) const = 0;
		virtual void Unbind() const = 0;
		virtual void SetLayout(const VertexBufferLayout& layout) = 0;
		virtual const VertexBufferLayout& GetLayout() const = 0;
		virtual Buffer& GetBuffer() = 0;
		virtual void Destroy() = 0;

		static std::shared_ptr<VertexBuffer> Create(Buffer& buffer);
		static std::shared_ptr<VertexBuffer> CreateDynamic(u32 baseSize);
	};
}

