#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Buffer.h"
#include "DebEngine/Graphics/Texture.h"

#include <glm/glm.hpp>

namespace YAML
{
	class Emitter;
	class Node;
}

namespace DebEngine {

	enum class ShaderUniformType
	{
		None = 0,
		Bool,
		Int,
		Float,
		Vec2,
		Vec3,
		Vec4,
		Mat3,
		Mat4
	};

	enum ShaderType
	{
		ShaderTypeNone,
		ShaderTypeVertex,
		ShaderTypeFragment
	};

	enum class ShaderBufferType
	{
		StorageBuffers,
		UniformBufferConstant,
		PushConstant,
		Sampler,
		ImageSampler,
		ImageSamplerCube,
		CombinedImageSampler,
		CombinedImageSamplerCube,
		None
	};

	struct ShaderBuffer;

	struct ShaderBufferMember
	{
	public:
		std::string Name;
		ShaderUniformType UniformType = ShaderUniformType::None;
		u32 Size = 0;
		u32 Offset = 0;
		ShaderType ShaderType = ShaderType::ShaderTypeNone;

		std::shared_ptr<ShaderBuffer> Parent = nullptr;
	};

	struct PushConstantUniform
	{
		const ShaderBufferMember& GetMember(const std::string& name) const
		{
			DEB_CORE_ASSERT(Uniforms.find(name) != Uniforms.end());
			return Uniforms.at(name);
		}

		std::string Name;
		u32 Size = 0;
		u32 Offset = 0;
		ShaderType ShaderType = ShaderType::ShaderTypeNone;
		std::unordered_map<std::string, ShaderBufferMember> Uniforms;
	};

	struct ShaderBuffer
	{
		virtual ~ShaderBuffer() = default;

		const ShaderBufferMember& GetMember(const std::string& name) const
		{
			DEB_CORE_ASSERT(Uniforms.find(name) != Uniforms.end());
			return Uniforms.at(name);
		}

		ShaderBufferType Type = ShaderBufferType::None;
		u32 Size = 0;
		u32 Offset = 0; // Applicable to push constants
		u32 BindingPoint = 0;
		std::string Name;
		b8 IsBound = false;
		ShaderType ShaderType = ShaderType::ShaderTypeNone;
		std::unordered_map<std::string, ShaderBufferMember> Uniforms;

		static std::shared_ptr<ShaderBuffer> Create();
	};

	struct ImageSampler
	{
		u32 BindinPoint = 0;
		std::string Name;
		ShaderType ShaderType = ShaderType::ShaderTypeNone;
	};

	class ShaderDataDeclaration
	{
	public:
		ShaderDataDeclaration() = default;
		ShaderDataDeclaration(ShaderBufferType shaderBufferType, const std::string& name);

		const ShaderBufferType GetShaderBufferType() const;
		const std::string& GetName() const;
		Buffer& GetBuffer();
		const std::shared_ptr<Texture2D>& GetTexture() const;
		void SetTexture(const std::shared_ptr<Texture2D>& texture);
		const std::shared_ptr<TextureCube>& GetTextureCube() const;
		void SetTextureCube(const std::shared_ptr<TextureCube>& texture);
		b8 HasTextureCube() const;
		b8 IsUploaded() const;
		void SetUploaded(b8 value);
		void SetInitializeImageSampler(b8 val);
		b8 IsInitializeImageSampler() const;

		b8 Serialize(YAML::Emitter& outStream);

		static ShaderDataDeclaration Deserialize(const YAML::Node& node);
		static ShaderDataDeclaration Copy(ShaderDataDeclaration& shaderDataDeclaration);

	private:
		std::shared_ptr<Texture2D> m_Texture;
		std::shared_ptr<TextureCube> m_TextureCube;
		ShaderBufferType m_ShaderBufferType;
		std::string m_Name;	// Full name of the accessed uniform buffer/push constant
		Buffer m_Buffer;
		b8 m_Uploaded = false;
		b8 m_InitializeImageSampler = false; // Use for Image Samplers initialization only
	};


	class Shader
	{
	public:
		virtual ~Shader() = default;

		virtual void Reload() = 0;
		virtual void Bind() = 0;

		virtual const std::string& GetName() const = 0;
		virtual const std::unordered_map<std::string, std::shared_ptr<ShaderBuffer>>& GetShaderBuffers() const = 0;

		static std::shared_ptr<Shader> Create(const std::string& filePath);
		static std::shared_ptr<Shader> CreateFromString(const std::string& source);
	private:
		u32 m_RendererID;
	};

	////////////////////////////////////////////////////////////
	/// ShaderLibrary //////////////////////////////////////////
	////////////////////////////////////////////////////////////

	class ShaderLibrary
	{
	public:
		static void Add(const std::shared_ptr<Shader>& shader);
		static std::shared_ptr<Shader>& Load(const std::string& path);
		static std::shared_ptr<Shader>& Load(const std::string& name, const std::string& path);

		static std::shared_ptr<Shader>& Get(const std::string& name);
	private:
		static std::unordered_map<std::string, std::shared_ptr<Shader>> m_Shaders;
	};

}

