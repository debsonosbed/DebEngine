#include "debpch.h"
#include "Renderer.h"

#include "Material.h"
#include "Viewport.h"

#include "DebEngine/Graphics/VertexBuffer.h"
#include "DebEngine/Graphics/IndexBuffer.h"
#include "DebEngine/Platform/Vulkan/VulkanRendererAPI.h"
#include "DebEngine/Core/Statistics.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <imgui/imgui.h>
#include "backends/imgui_impl_vulkan.h"

namespace DebEngine
{
	RendererAPIType RendererAPI::s_RendererAPIType = RendererAPIType::Vulkan;

	std::unique_ptr<RendererAPI> Renderer::s_RendererAPI = nullptr;

	struct Quad
	{
		std::shared_ptr<Material> Material;
		glm::mat4 Transform;
	};

	struct MeshInstance
	{
		std::shared_ptr<Mesh> Mesh;
		glm::mat4 Transform;
		std::shared_ptr<VertexBuffer> InstancedDataBuffer = nullptr;
	};

	struct MeshInstancedData
	{
		glm::mat4 Transform;
		glm::vec4 Color = glm::vec4(1.0f);
	};

	struct RendererData
	{
		std::shared_ptr<Viewport> m_Viewport;
		std::shared_ptr<Viewport> m_ImGuiViewport;

		RenderCommandQueue m_CommandQueue;
		RenderCommandQueue m_CleanUpRenderQueue;

		std::shared_ptr<Framebuffer> m_DefaultFramebuffer;
		std::shared_ptr<RenderPass> m_DefaultRenderPass;
		std::shared_ptr<RenderPass> m_ActiveRenderPass = nullptr;
		std::vector<std::shared_ptr<CommandBuffer>> m_ActiveCommandBufferStack;

		std::vector<std::shared_ptr<CommandBuffer>> m_CommandBufferStack;

		std::shared_ptr<CommandBuffer> m_MainCommandBuffer;

		std::shared_ptr<VertexBuffer> m_FullscreenQuadVertexBuffer;
		std::shared_ptr<IndexBuffer> m_FullscreenQuadIndexBuffer;
		std::shared_ptr<Pipeline> m_FullscreenQuadPipeline;

		std::shared_ptr<Material> m_OutlineMaterial;

		f32 m_OutlineThickens = 1.05f;
		glm::vec4 m_OutlineColor = glm::vec4(1.f, 0.66f, 0.f, 1.f);

		std::vector<Quad> m_Quads;
		std::vector<MeshInstance> m_ModelInstances;
		std::shared_ptr<VertexBuffer> m_DummyVertexBuffer;

		// Offscreen rendering
		std::shared_ptr<CommandBuffer> m_ImGuiCommandBuffer;

		std::shared_ptr<Texture2D> m_DummyTexture;
		std::shared_ptr<TextureCube> m_DummyTextureCube;

		// Batch rendering
		std::unordered_map<std::string, Renderer::MeshData> MeshDataGrouped;
		std::shared_ptr<VertexBuffer> MeshInstancedDataBuffer;
		std::shared_ptr<VertexBuffer> MeshInstancedObjectPickingDataBuffer;
		Buffer MeshDataBuffer;
	};

	static RendererData s_RenderData;

	void Renderer::Init()
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Renderer::Create Renderer API -> Invalid RendererAPI (none)"); break;
		case RendererAPIType::Vulkan:	s_RendererAPI = std::make_unique<VulkanRendererAPI>(); break;
		}

		DEB_CORE_ASSERT(s_RendererAPI != nullptr, "Renderer::Create Renderer API -> Invalid RendererAPI (none)");

		auto shaderSimple = ShaderLibrary::Load("assets/shaders/SimpleShader.glsl");
		auto shaderTexture = ShaderLibrary::Load("assets/shaders/Texture.glsl");

		s_RenderData.m_DefaultRenderPass = GetContext()->GetSwapChain()->GetRenderPass();

		PipelineSpecification pipelineSpecification{};
		pipelineSpecification.RenderPass = s_RenderData.m_DefaultRenderPass;

		pipelineSpecification.Layouts = {
			{
				{ ShaderDataType::Float3, "a_Position" },
				{ ShaderDataType::Float2, "a_TexCoord" }
			}
		};
		pipelineSpecification.DepthStencilState.DepthTestEnabled = false;
		pipelineSpecification.DepthStencilState.DepthWriteEnabled = false;
		pipelineSpecification.DepthStencilState.StencilTestEnabled = false;

		pipelineSpecification.Shader = shaderTexture;
		pipelineSpecification.CullingEnabled = false;

		pipelineSpecification.CullingEnabled = true;
		pipelineSpecification.Shader = shaderSimple;
		//const auto pipelineDefault = Pipeline::Create(pipelineSpecification, "Default");

		pipelineSpecification.PolygonMode = PolygonMode::Line;
		//const auto pipelineWireframe = Pipeline::Create(pipelineSpecification, "Wireframe");

		pipelineSpecification.PolygonMode = PolygonMode::Fill;
		pipelineSpecification.PipelineTopology = PipelineTopology::LineList;


		// Create fullscreen quad
		struct QuadVertex
		{
			glm::vec3 Position;
			glm::vec2 TexCoord;
		};

		std::vector<QuadVertex> quadData = {
			{{ -1.0f, -1.0f, 0.1f }, { 0, 0 }},
			{{  1.0f, -1.0f, 0.1f }, { 1, 0 }},
			{{  1.0f,  1.0f, 0.1f }, { 1, 1 }},
			{{ -1.0f,  1.0f, 0.1f }, { 0, 1 }},
		};

		s_RenderData.m_FullscreenQuadVertexBuffer = VertexBuffer::Create(Buffer((u8*)quadData.data(), quadData.size() * sizeof(QuadVertex)));
		std::vector<u32> indices = { 0, 1, 2, 2, 3, 0 };
		s_RenderData.m_FullscreenQuadIndexBuffer = IndexBuffer::Create(Buffer((u8*)indices.data(), indices.size() * sizeof(u32)), indices.size());

		s_RenderData.m_Viewport = Viewport::Create(0, 0, 1280, 720);
		s_RenderData.m_ImGuiViewport = Viewport::Create(0, 0, 1280, 720);

		s_RenderData.m_DummyTexture = Texture2D::Create(1, 1);
		s_RenderData.m_DummyTextureCube = TextureCube::Create(1, 1);

		std::vector<u32> dummyData = { 0 };
		s_RenderData.m_DummyVertexBuffer = VertexBuffer::Create(Buffer((u8*)dummyData.data(), dummyData.size() * sizeof(u32)));
	}

	void Renderer::Shutdown()
	{

	}

	void Renderer::OnWindowResize(u32 width, u32 height)
	{

	}

	void Renderer::BeginFrame()
	{
		auto context = Renderer::GetContext();
		auto swapChain = context->GetSwapChain();
		Renderer::Submit([swapChain]()
			{
				s_RenderData.m_ActiveCommandBufferStack.push_back(swapChain->GetCurrnetDrawCommandBuffer());
				Renderer::GetCurrentCommandBuffer()->BeginRecording();
			}
		);
	}

	void Renderer::EndFrame()
	{
		auto context = Renderer::GetContext();

#
		Renderer::BeginRenderPass(Renderer::GetRenderPassSwapChain(), true);
		{
			Application::Get().GetImGuiLayer().ImGuiRenderPass();

			Renderer::GetAPI().ExecuteCommands(s_RenderData.m_CommandBufferStack);
		}
		Renderer::EndRenderPass();


		Renderer::Submit([context]()
			{
				Renderer::GetCurrentCommandBuffer()->EndRecording();
				s_RenderData.m_ActiveCommandBufferStack.pop_back();

				s_RenderData.m_ActiveRenderPass = nullptr;
			}
		);
	}

	void Renderer::BeginRecording(const std::shared_ptr<CommandBuffer>& commandBuffer)
	{
		Renderer::Submit([commandBuffer]()
			{
				s_RenderData.m_ActiveCommandBufferStack.push_back(commandBuffer);
				Renderer::GetCurrentCommandBuffer()->BeginRecording();
			}
		);
	}

	void Renderer::EndRecording(const std::shared_ptr<CommandBuffer>& commandBuffer)
	{
		Renderer::Submit([commandBuffer]()
			{
				Renderer::GetCurrentCommandBuffer()->EndRecording();
				s_RenderData.m_ActiveCommandBufferStack.pop_back();
			}
		);
	}

	void Renderer::BeginRenderPass(const std::shared_ptr<RenderPass>& renderPass, b8 isSubpass)
	{
		Renderer::Submit([renderPass, isSubpass]()
			{
				s_RenderData.m_ActiveRenderPass = renderPass;

				s_RenderData.m_ActiveRenderPass->BeginRenderPass(Renderer::GetCurrentCommandBuffer(), isSubpass);
			}
		);
	}

	void Renderer::EndRenderPass()
	{
		Renderer::Submit([]()
			{
				s_RenderData.m_ActiveRenderPass->EndRenderPass(Renderer::GetCurrentCommandBuffer());
			});
	}

	std::shared_ptr<RenderPass> Renderer::GetRenderPassSwapChain()
	{
		return s_RenderData.m_DefaultRenderPass;
	}

	std::shared_ptr<Texture2D> Renderer::GetDummyTexture()
	{
		return s_RenderData.m_DummyTexture;
	}

	std::shared_ptr<TextureCube> Renderer::GetDummyTextureCube()
	{
		return s_RenderData.m_DummyTextureCube;
	}

	std::shared_ptr<VertexBuffer> Renderer::GetDummyVertexBuffer()
	{
		return s_RenderData.m_DummyVertexBuffer;
	}

	const std::shared_ptr<CommandBuffer>& Renderer::GetCurrentCommandBuffer()
	{
		return s_RenderData.m_ActiveCommandBufferStack.back();
	}

	const std::shared_ptr<CommandBuffer>& Renderer::GetSwapchainCommandBuffer()
	{
		return s_RenderData.m_ActiveCommandBufferStack.front();
	}

	std::shared_ptr<CommandBuffer> Renderer::CreateSecondaryCommandBuffer()
	{
		auto cmdBuffer = CommandBuffer::CreateSecondary();
		s_RenderData.m_CommandBufferStack.push_back(cmdBuffer);
		return cmdBuffer;
	}

	void Renderer::SubmitQuad(const QuadData& quadData)
	{
		s_RenderData.m_FullscreenQuadVertexBuffer->Bind();
		s_RenderData.m_FullscreenQuadIndexBuffer->Bind();

		quadData.Material->Set("pc_Transform.Transform", quadData.Transform);
		//material->Set("pc_Transform.ViewProjection", s_RenderData.m_ViewProjectionMatrix);
		quadData.Material->Bind();

		Renderer::GetAPI().DrawIndexed(s_RenderData.m_FullscreenQuadIndexBuffer->GetCount());

		//s_RenderData.m_Quads.push_back({ material, glm::scale(transform, glm::vec3(0.5f)) });
	}

	void Renderer::SubmitFullscrenQuad(const std::shared_ptr<Material>& material)
	{
		s_RenderData.m_FullscreenQuadVertexBuffer->Bind();
		s_RenderData.m_FullscreenQuadIndexBuffer->Bind();

		material->Bind();

		Renderer::GetAPI().DrawIndexed(s_RenderData.m_FullscreenQuadIndexBuffer->GetCount());
	}

	void Renderer::SubmitMeshes(const std::vector<MeshData>& meshDataContainer)
	{
		// Build data buffers
		if (meshDataContainer.size() > 0)
		{
			auto& baseMeshData = meshDataContainer[0];
			auto& baseMeshSubmeshes = baseMeshData.Mesh->GetSubmeshes();
			std::vector<MeshInstancedData> meshInstancedDataContainer(baseMeshSubmeshes.size() * meshDataContainer.size());

			u32 meshIndex = 0;
			for (const auto& meshData : meshDataContainer)
			{
				auto& submeshes = meshData.Mesh->GetSubmeshes();
				u32 submeshIndex = 0;
				for (const auto& submesh : submeshes)
				{
					// Each Mesh can have multiple submeshes
					// Each submesh can have a different transform
					// Get transform of each submesh
					// Material properties should be accessed here since there is one material per submesh
					auto transform = meshData.TransformComponent.GetTransform() * submesh.Transform;
					MeshInstancedData meshInstancedData = { transform };
					meshInstancedDataContainer[meshIndex * submeshes.size() + submeshIndex] = meshInstancedData;
					submeshIndex++;
				}
				meshIndex++;
			}

			// update instanced data buffer
			Buffer instancedDataBuffer = { (u8*)meshInstancedDataContainer.data(), u32(meshInstancedDataContainer.size() * sizeof(MeshInstancedData)) };
			s_RenderData.MeshInstancedDataBuffer->UpdateBuffer(instancedDataBuffer);

			u32 submeshIndex = 0;
			for (const auto& submesh : baseMeshSubmeshes)
			{
				// Bind submesh vertices
				// Bind submesh indices
				// Bind submesh instanced data buffer
				submesh.SubmeshVertexBuffer->Bind();
				submesh.SubmeshIndexBuffer->Bind();
				s_RenderData.MeshInstancedDataBuffer->Bind(1);

				Renderer::GetAPI().DrawIndexed(submesh.IndexCount, meshDataContainer.size());
				// draw submesh
			}
		}
	}

	void Renderer::SubmitMeshesObjectPicking(const std::vector<MeshData>& meshDataContainer)
	{
		// All meshes here are submited with ONE COLOR
		// No need to care about submeshes properties other than transform
		// Build data buffers
		if (meshDataContainer.size() > 0)
		{
			auto& baseMeshData = meshDataContainer[0];
			auto& baseMeshSubmeshes = baseMeshData.Mesh->GetSubmeshes();
			std::vector<MeshInstancedData> meshInstancedDataContainer(baseMeshSubmeshes.size() * meshDataContainer.size());

			u32 meshIndex = 0;
			for (const auto& meshData : meshDataContainer)
			{
				auto& submeshes = meshData.Mesh->GetSubmeshes();
				u32 submeshIndex = 0;
				for (const auto& submesh : submeshes)
				{
					// Each Mesh can have multiple submeshes
					// Each submesh can have a different transform
					// Get transform of each submesh
					auto transform = meshData.TransformComponent.GetTransform() * submesh.Transform;
					meshInstancedDataContainer[meshIndex * submeshes.size() + submeshIndex] = { transform, meshData.Color };;
					submeshIndex++;
				}
				meshIndex++;
			}

			// update instanced data buffer
			Buffer instancedDataBuffer = { (u8*)meshInstancedDataContainer.data(), u32(meshInstancedDataContainer.size() * sizeof(MeshInstancedData)) };
			s_RenderData.MeshInstancedObjectPickingDataBuffer->UpdateBuffer(instancedDataBuffer);

			u32 submeshIndex = 0;
			for (const auto& submesh : baseMeshSubmeshes)
			{
				// Bind submesh vertices
				// Bind submesh indices
				// Bind submesh instanced data buffer
				submesh.SubmeshVertexBuffer->Bind();
				submesh.SubmeshIndexBuffer->Bind();
				s_RenderData.MeshInstancedObjectPickingDataBuffer->Bind(1);

				Renderer::GetAPI().DrawIndexed(submesh.IndexCount, meshDataContainer.size());
				// draw submesh
			}
		}
	}

	void Renderer::WaitAndRender()
	{
		s_RenderData.m_CommandQueue.Execute();
	}

	void Renderer::WaitPostFrame()
	{
		s_RenderData.m_CleanUpRenderQueue.Execute();
	}

	u32 Renderer::GetRenderCommandCount()
	{
		return s_RenderData.m_CommandQueue.GetSize();
	}

	RendererAPI& Renderer::GetAPI()
	{
		return *s_RendererAPI;
	}

	std::shared_ptr<RendererContext> Renderer::GetContext()
	{
		return Application::Get().GetWindow().GetRendererContext();
	}

	RenderCommandQueue& Renderer::GetRenderCommandQueue()
	{
		return s_RenderData.m_CommandQueue;
	}

	RenderCommandQueue& Renderer::GetCleanUpRenderCommandQueue()
	{
		return s_RenderData.m_CleanUpRenderQueue;
	}
}