#pragma once

#include "Renderer.h"
#include "Camera.h"
#include "Mesh.h"

#include "DebEngine/Gameplay/Scene.h"

namespace DebEngine
{
	struct SceneRendererCamera
	{
		DebEngine::Camera Camera;
		glm::mat4 View;
	};

	struct InstancedDataBuffers
	{
		std::shared_ptr<VertexBuffer> MeshInstancedPosAttribBuffer;
		Buffer MeshInstancedPosBuffer;
		std::shared_ptr<VertexBuffer> MeshInstancedDataAttribBuffer;
		Buffer MeshInstancedDataBuffer;

		// Object picking
		std::shared_ptr<VertexBuffer> MeshInstancedDataAttribBufferObjectPicking;
		Buffer MeshInstancedDataBufferObjectPicking;
	};

	struct GlobalSceneRendererData
	{
		std::unordered_map<std::string, InstancedDataBuffers> PerMeshLoadedBuffers;
	};

	class SceneRenderer
	{
	public:
		static void Init();

		static void BeginFrame();
		static void EndFrame();

		static void BeginScene(Scene* scene, const SceneRendererCamera& sceneRendererCamera);
		static void EndScene();

		//static void SubmitQuad(const std::shared_ptr<Material>& material, const TransformComponent& tc);
		static void SubmitMesh(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc);
		static void SubmitMeshOutline(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc);
		static void SubmitMeshPlain(const std::shared_ptr<Mesh>& mesh, const TransformComponent& tc, const glm::vec4& color = glm::vec4(1.0f));
		static void SetEnvironmentMap(const std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>>& environmentMap);

		static std::shared_ptr<RenderPass> GetRenderPassObjectPicking();

		static std::shared_ptr<RenderPass> GetRenderPassEditor();
		static std::shared_ptr<RenderPass> GetRenderPassRuntime();

		//static std::shared_ptr<Material>& GetSceneShader();

		static void SetEditorViewportSize(u32 width, u32 height);
		static void SetRuntimeViewportSize(u32 width, u32 height);
		static ImTextureID GetEditorImGuiTextureID();
		static ImTextureID GetRuntimeImGuiTextureID();

		static std::pair<std::shared_ptr<TextureCube>, std::shared_ptr<TextureCube>> CreateEnvironmentMap(const std::string& path);
		static std::shared_ptr<TextureCube> CreateEnvironmentMap(const std::vector<std::string>& facesPaths);

		static void SubmitAllSceneMeshes();
		static void SubmitAllSceneMeshesNoBatch(); // performance testing purposes
		static void SubmitAllSceneMeshesObjectPicking();
		static void SubmitGrid();
		static void SubmitEnvironmentMap();

		static void BatchRender();

		static void AllocateNewSceneDataBuffers(InstancedDataBuffers& instancedDataNuffers);
		static void DestroySceneDataBuffers(InstancedDataBuffers& instancedDataNuffers);

		static GlobalSceneRendererData GlobalData;
	private:
		struct MeshInstancedData;

		static std::shared_ptr<TextureCube> GenerateCubeMap(const std::shared_ptr<Texture2D> hdriTexture);
		static std::shared_ptr<TextureCube> GenerateIrradianceMap(const std::shared_ptr<TextureCube> cubeMap);
		static std::shared_ptr<TextureCube> GeneratePrefilteredCubeMap(const std::shared_ptr<TextureCube> cubeMap);
		static std::shared_ptr<Texture2D> GenerateBRDFLUT();
		static void UpdateEditorFramebuffersSize();
		static void UpdateRuntimeFramebuffersSize();
		static void CompositePassEditor();
		static void CompositePassRuntime();
		static void RenderAllSceneMeshes();
		static void RenderAllSceneMeshesNoBatch(); // performance testing purposes
		static void RenderAllSceneMeshesObjectPicking();

		static void RecreateSceneBuffer(Buffer& buffer, u32 newSize);
		static MeshInstancedData GetMeshData(const std::shared_ptr<Material>& material);

	private:
		struct MeshInstancedData
		{
			glm::vec4 Color = glm::vec4(1.0f);
			f32 Roughness = 0.0f;
			f32 Metalness = 0.0f;
		};

		static std::unordered_map<std::string, Buffer> m_EnvironmentPropertiesTypes;
		static std::unordered_map<std::string, std::shared_ptr<Texture2D>> m_EnvironmentPropertiesTextures;
		static std::unordered_map<std::string, std::shared_ptr<TextureCube>> m_EnvironmentPropertiesTexturesCube;
	};
}
