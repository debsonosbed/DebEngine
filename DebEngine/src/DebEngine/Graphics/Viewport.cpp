#include "debpch.h"
#include "Viewport.h"

#include "RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanViewport.h"

namespace DebEngine
{
	std::shared_ptr<Viewport> Viewport::Create(u32 x, u32 y, u32 width, u32 height)
	{
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Framebuffer::Create -> Invalid RendererAPI (none)"); break;
		case RendererAPIType::Vulkan:	return std::make_shared<VulkanViewport>(x, y, width, height);
		}

		return nullptr;
	}
}