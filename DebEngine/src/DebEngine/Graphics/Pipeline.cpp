#include "debpch.h"
#include "Pipeline.h"

#include "DebEngine/Graphics/RendererAPI.h"

#include "DebEngine/Platform/Vulkan/VulkanPipeline.h"

namespace DebEngine
{
	std::string Pipeline::GetName() const
	{
		return m_Name;
	}

	std::shared_ptr<Pipeline> Pipeline::Create(const PipelineSpecification& pipelineSpecication, const std::string& name)
	{
		std::shared_ptr<Pipeline> instance = nullptr;
		switch (RendererAPI::CurrentAPIType())
		{
		case RendererAPIType::None:		DEB_CORE_ASSERT(false, "Pipeline::Create -> Invalid RendererAPI (none)"); return nullptr;
		case RendererAPIType::Vulkan:	instance =  std::make_shared<VulkanPipeline>(pipelineSpecication, name); // should return Vulkan pipeline impl
		}

		PipelineLibrary::Add(instance, name);
		
		return instance;
	}

	////////////////////////////////////////////////////////////
	/// PipelineLibrary ////////////////////////////////////////
	////////////////////////////////////////////////////////////

	std::unordered_map<std::string, std::shared_ptr<DebEngine::Pipeline>> PipelineLibrary::m_Pipelines;

	void PipelineLibrary::Add(const std::shared_ptr<Pipeline>& pipeline, const std::string& name)
	{
		DEB_CORE_ASSERT(m_Pipelines.find(name) == m_Pipelines.end());
		m_Pipelines[name] = pipeline;
	}

	std::shared_ptr<Pipeline>& PipelineLibrary::Get(const std::string& name) 
	{
		DEB_CORE_ASSERT(m_Pipelines.find(name) != m_Pipelines.end());
		return m_Pipelines.at(name);
	}
}