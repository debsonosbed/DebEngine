#pragma once

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>

namespace DebEngine
{
	class Thread
	{

	public:
		Thread();
		~Thread();

		// Add a new job to the thread's queue
		void AddJob(std::function<void()> function);

		// Wait until all work items have been finished
		void Execute();
	private:
		// Loop through all remaining jobs
		void QueueLoop();
	private:
		bool destroying = false;
		std::thread worker;
		std::queue<std::function<void()>> jobQueue;
		std::mutex queueMutex;
		std::condition_variable condition;
	};

	///////////////////////////////////////////////////////////////
	// Thread Pool ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	class ThreadPool
	{
	public:
		// Sets the number of threads to be allocated in this pool
		void SetThreadCount(u32 count);

		// Wait until all threads have finished their work items
		void ExecuteThreads();
	public:
		std::vector<std::unique_ptr<Thread>> Threads;
	};
}