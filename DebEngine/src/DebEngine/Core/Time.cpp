#include "debpch.h"
#include "Time.h"

#include <SDL.h>

namespace DebEngine
{
	f64 Time::m_DeltaTime = 0.f;

	f64 Time::GetTime()
	{
		return (f64)SDL_GetTicks() / 1000.0;
	}

	u64 Time::GetTimeMilliseconds()
	{
		return SDL_GetTicks();
	}

	const f64 Time::DeltaTime()
	{
		return m_DeltaTime / 1000.0; // In seconds
	}
}