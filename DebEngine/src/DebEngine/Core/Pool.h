#pragma once
#include "debpch.h"

namespace DebEngine
{
	class Pool
	{
	public:
		Pool() = default;

		Pool(u32 size)
			: m_Size(size)
		{
			m_Pool.resize(size);
			for (s64 i = size; i >= 0; i--)
			{
				m_Pool.emplace_back(i);
			}
		}

		u32 Get()
		{
			DEB_CORE_ASSERT(m_Pool.size() > 0, "Pool empty");
			u32 val = m_Pool.back();
			m_Pool.pop_back();
			return val;
		}

		void Return(u32 val)
		{
			m_Pool.push_back(val);
		}

		void Rebuild()
		{
			m_Pool.clear();
			Pool(m_Size);
		}
	private:
		std::vector<u32> m_Pool;
		u32 m_Size = 0;
	};
}