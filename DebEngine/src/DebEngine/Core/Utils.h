#pragma once

#include <glm/glm.hpp>

namespace DebEngine
{
	class Utils
	{
	public:
		static const glm::ivec4 ToRGBA(f32 r, f32 g, f32 b, f32 a);
		static const glm::ivec4 ToRGBA(const glm::vec4& color);
		static const glm::vec4 FromRGBA(u8 r, u8 g, u8 b, u8 a);
		static const glm::vec4 FromRGBA(const glm::ivec4& color);
		static const glm::ivec4 GetRGBAFromHex(u32 hexValue);
		static const glm::ivec4 GetBGRAFromHex(u32 hexValue);
		static const u32 GetHexFromRGBA(const glm::ivec4& color);
	};
}

