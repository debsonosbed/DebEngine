#include "debpch.h"
#include "Log.h"

#include "spdlog/sinks/stdout_color_sinks.h"

namespace DebEngine
{
	std::shared_ptr<spdlog::logger> Logger::s_CoreLogger;
	std::shared_ptr<spdlog::logger> Logger::s_ClientLogger;
	std::shared_ptr<spdlog::logger> Logger::s_EditorCoreLogger;
	std::shared_ptr<spdlog::logger> Logger::s_EditorClientLogger;

	std::ostringstream Logger::s_LoggerStream;

	std::shared_ptr<spdlog::sinks::ostream_sink_mt> Logger::s_EditorConsoleSink;

	std::vector<std::pair<DebEngine::LogType, std::string>> Logger::m_LogLinesContainer;

	u32 Logger::m_CurrentEditorLogStreamSize = 0;

	void Logger::Init()
	{
		auto formattingPattern = "%^[%T] %n: %v%$";
		auto editorFormattingPattern = "[%T] %n: %v%L";
		auto coreTitle = "[DebEngine - Core]";
		auto clientTitle = "[DebEngine - Client]";

		spdlog::set_pattern(formattingPattern);

		s_CoreLogger = spdlog::stdout_color_mt(coreTitle);
		s_CoreLogger->set_level(spdlog::level::trace);

		s_ClientLogger = spdlog::stdout_color_mt(clientTitle);
		s_ClientLogger->set_level(spdlog::level::trace);

		s_EditorConsoleSink = std::make_shared<spdlog::sinks::ostream_sink_mt>(s_LoggerStream);

		s_EditorCoreLogger = std::make_shared<spdlog::logger>(coreTitle, s_EditorConsoleSink);
		s_EditorCoreLogger->set_pattern(editorFormattingPattern);
		s_EditorCoreLogger->set_level(spdlog::level::trace);

		s_EditorClientLogger = std::make_shared<spdlog::logger>(clientTitle, s_EditorConsoleSink);
		s_EditorClientLogger->set_pattern(editorFormattingPattern);
		s_EditorClientLogger->set_level(spdlog::level::trace);
	}

	std::shared_ptr<spdlog::logger>& Logger::GetCoreLogger()
	{
		return s_CoreLogger;
	}

	std::shared_ptr<spdlog::logger>& Logger::GetEditorCoreLogger()
	{
		return s_EditorCoreLogger;
	}

	std::shared_ptr<spdlog::logger>& Logger::GetClientLogger()
	{
		return s_ClientLogger;
	}

	std::shared_ptr<spdlog::logger>& Logger::GetEditorClientLogger()
	{
		return s_EditorClientLogger;
	}

	std::ostringstream& Logger::GetEditorLogStream()
	{
		return s_LoggerStream;
	}

	std::vector<std::pair<LogType, std::string>>& Logger::GetLogLines()
	{
		auto& logStream = s_LoggerStream;
		logStream.seekp(0, std::ios::end);
		u32 logStreamSize = logStream.tellp();

		if (logStreamSize != m_CurrentEditorLogStreamSize)
		{
			auto& logStreamStr = logStream.str();
			std::istringstream iss(logStreamStr);
			iss.seekg(m_CurrentEditorLogStreamSize);
			std::string logLine;
			while (std::getline(iss, logLine))
			{
				auto logType = GetLogType(logLine);
				logLine[logLine.size() - 2] = logLine[logLine.size() - 1]; // Get rid of the log type character
				logLine.resize(logLine.size() - 1);
				m_LogLinesContainer.push_back({ logType, logLine });
			}

			m_CurrentEditorLogStreamSize = logStreamSize;
		}

		return m_LogLinesContainer;
	}

	LogType Logger::GetLogType(const std::string& logLine)
	{
		auto logTypeChar = logLine[logLine.size() - 2];
		switch (logTypeChar)
		{
		case 'T':
			return LogType::Trace;
		case 'I':
			return LogType::Info;
		case 'W':
			return LogType::Warn;
		case 'E':
			return LogType::Error;
		case 'C':
			return LogType::Critical;
		}


		return LogType::Info;
	}
}