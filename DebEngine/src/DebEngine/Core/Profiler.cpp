#include "debpch.h"
#include "Profiler.h"

#include "DebEngine/Graphics/Renderer.h";

#include "Log.h"

namespace DebEngine
{
	std::vector<Profiler::ProfilerParams> Profiler::m_ProfilerStack;

	void Profiler::StartMeasurement(const std::string& name)
	{
		Profiler::ProfilerParams profilerParams = {};
		profilerParams.MeasurementName = name;
		profilerParams.StartTime = std::chrono::high_resolution_clock::now();
		m_ProfilerStack.push_back(profilerParams);
	}

	void Profiler::StartMeasurementRT(const std::string& name)
	{
		Renderer::Submit([name]()
			{
				Profiler::StartMeasurement(name);
			});
	}

	void Profiler::EndMeasurement()
	{
		auto endTime = std::chrono::high_resolution_clock::now();
		auto& profilerParams = m_ProfilerStack.back();
		auto duration = std::chrono::duration<double, std::milli>(endTime - profilerParams.StartTime).count();
		LogCore::Message("Measurement [{:s}] - Duration - {:.2f}ms", profilerParams.MeasurementName, duration);
		m_ProfilerStack.pop_back();
	}

	void Profiler::EndMeasurementRT()
	{
		Renderer::Submit([]()
			{
				Profiler::EndMeasurement();
			});
	}

}