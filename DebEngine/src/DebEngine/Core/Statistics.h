#pragma once

namespace DebEngine
{
	class Statistics
	{
		friend class Application;
		friend class Renderer;
		friend class SceneRenderer;
	public:
		static f64 GetFPS();
		static f64 GetFrameTime();
		static f64 GetMainThreadFrameTime();
		static f64 GetRenderThreadFrameTime();

		static std::pair<u32, u32> GetGameViewportDimensions();
		static u32 GetGameViewportSize();

	private:
		static void Update();
		static void SetRuntimeViewportDimensions(u32 width, u32 height);
		static void SetRuntimeFramebufferSize(u32 size);

		static void SetMainThreadFrameTime(f64 frameTime);
		static void SetRenderThreadFrameTime(f64 frameTime);
	private:
		static f64 m_FramesPerSecond;
		static f64 m_UpdateInterval;

		static f64 m_AverageFrameTime;
		static u32 m_FrameCount;
		static f64 m_FrameTimeElapsedAcumulator;

		static f64 m_MainThreadFrameTimeElapsedAcumulator;
		static f64 m_MainThreadFrameTime;
		static f64 m_AverageMainThreadFrameTime;

		static f64 m_RenderThreadFrameTimeElapsedAcumulator;
		static f64 m_RenderThreadFrameTime;
		static f64 m_AverageRenderThreadFrameTime;

		static u32 m_GameViewportWidth;
		static u32 m_GameViewportHeight;
		static u32 m_GameFramebufferSize;
	};
}

