#include "debpch.h"
#include "Filesystem.h"

namespace DebEngine
{
	Path::Path(const std::string& path) : m_PathLiteral(path)
	{

	}

	const std::string& Path::Literal() const
	{
		return m_PathLiteral;
	}

	std::ostream& operator<<(std::ostream& out, const Path& path)
	{
		out << path.m_PathLiteral;
		return out;
	}

	Path& Path::operator=(const std::string& pathLiteral)
	{
		m_PathLiteral = pathLiteral;
		return *this;
	}

	Path& File::GetPath()
	{
		return m_Path;
	}

	Directory& File::GetDirectory()
	{
		Directory dir;
		return dir;
	}

	std::vector<Path> Directory::GetFiles()
	{
		std::vector<Path> paths;
		return paths;
	}

	std::string Directory::operator=(const Directory& dir)
	{
		// Should return directory
		return "";
	}

	std::string Filesystem::OpenFileDialog(const std::string& initialDir, const char* filter)
	{
		return s_Instance->OpenFileDialogImpl(initialDir, filter);
	}
}