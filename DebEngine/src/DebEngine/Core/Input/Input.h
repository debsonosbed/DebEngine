#pragma once

#include "DebEngine/Core/Core.h"
#include "KeyCodes.h"
#include <glm/glm.hpp>

namespace DebEngine
{
	class Input
	{
		friend class WindowsWindow;
	public:
		static b8 IsKeyDown(KeyCode keyCode) { return s_Instance->IsKeyDownImpl(keyCode); }
		static b8 IsKeyPressed(KeyCode keyCode) { return s_Instance->IsKeyPressedImpl(keyCode); }
		static b8 IsMouseButtonDown(KeyCode keyCode) { return s_Instance->IsMouseButtonDownImpl(keyCode); }
		static b8 IsMouseButtonPressed(KeyCode keyCode) { return s_Instance->IsMouseButtonPressedImpl(keyCode); }
		static glm::ivec2 GetMousePosition() { return s_Instance->GetMousePositionImpl(); }
		static glm::ivec2 GetGlobalMousePosition() { return s_Instance->GetGlobalMousePositionImpl(); }
		static glm::ivec2 GetGlobalMousePositionDelta() { return s_Instance->GetGlobalMousePositionDeltaImpl(); }
		static s32 GetMouseX() { return s_Instance->GetMouseXImpl(); }
		static s32 GetMouseY() { return s_Instance->GetMouseYImpl(); }
		static glm::ivec2 GetMousePositionDelta() { return s_Instance->GetMousePositionDeltaImpl(); }
		static glm::ivec2 GetMouseScrollDelta() { return s_Instance->GetMouseScrollDeltaImpl(); }

	protected:
		static void UpdateKeyboardState() { s_Instance->UpdateKeyboardStateImpl(); }
		static void UpdateMouseState() { s_Instance->UpdateMouseStateImpl(); }
		static void UpdateMouseScrollValue(u32 x, u32 y) { s_Instance->UpdateMouseScrollValueImpl(x, y); }
		static void UpdateMousePositionDelta(u32 x, u32 y) { s_Instance->UpdateMousePositionDeltaImpl(x, y); }

	protected:
		// Implementation will vary depending on the platform
		virtual b8 IsKeyDownImpl(KeyCode keyCode) = 0;
		virtual b8 IsKeyPressedImpl(KeyCode keyCode) = 0;
		virtual b8 IsMouseButtonDownImpl(KeyCode keyCode) = 0;
		virtual b8 IsMouseButtonPressedImpl(KeyCode keyCode) = 0;
		virtual glm::ivec2 GetMousePositionImpl() const = 0;
		virtual glm::ivec2 GetGlobalMousePositionImpl() const = 0;
		virtual glm::ivec2 GetGlobalMousePositionDeltaImpl() const = 0;
		virtual s32 GetMouseXImpl() const = 0;
		virtual s32 GetMouseYImpl() const = 0;
		virtual glm::ivec2 GetMousePositionDeltaImpl() const = 0;
		virtual glm::ivec2 GetMouseScrollDeltaImpl() const = 0;
		virtual void UpdateKeyboardStateImpl() = 0;
		virtual void UpdateMouseStateImpl() = 0;
		virtual void UpdateMouseScrollValueImpl(u32 x, u32 y) = 0;
		virtual void UpdateMousePositionDeltaImpl(u32 x, u32 y) = 0;

	private:
		static Input* s_Instance;
	};
}

