#pragma once

#include "Event.h"

#include <sstream>

namespace DebEngine
{
	class WindowResizedEvent : public Event
	{
	public:
		WindowResizedEvent(u32 width, u32 height) : m_Width(width), m_Height(height) { }

		u32 GetWidth() const { return m_Width; }
		u32 GetHeight() const { return m_Height; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "WindowResizeEvent: (" << m_Width << ", " << m_Height << ")";
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowResized);
		EVENT_CLASS_CATEGORY(EventCategoryWindow);
	private:
		u32 m_Width, m_Height;
	};

	class WindowClosedEvent : public Event
	{
	public:
		WindowClosedEvent() { }

		EVENT_CLASS_TYPE(WindowClosed);
		EVENT_CLASS_CATEGORY(EventCategoryWindow);
	};

	class WindowMinimizedEvent : public Event
	{
	public:
		WindowMinimizedEvent() { }

		EVENT_CLASS_TYPE(WindowMinimized);
		EVENT_CLASS_CATEGORY(EventCategoryWindow);
	};

	class WindowRestoredEvent : public Event
	{
	public:
		WindowRestoredEvent() { }

		EVENT_CLASS_TYPE(WindowRestored);
		EVENT_CLASS_CATEGORY(EventCategoryWindow);
	};
}
