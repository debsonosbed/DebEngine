#pragma once

#include "debpch.h"
#include "DebEngine/Core/Input/KeyCodes.h"

namespace DebEngine {

	enum class EventType
	{
		None = 0,
		WindowClosed, WindowResized, WindowMinimized, WindowRestored,
		KeyPressed, KeyReleased,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	enum EventCategory
	{
		None = 0,
		EventCategoryWindow			= BIT(0),
		EventCategoryInput			= BIT(1),
		EventCategoryKeyboard		= BIT(2),
		EventCategoryMouse			= BIT(3),
		EventCategoryMouseButton	= BIT(4)
	};

#define EVENT_CLASS_TYPE(type) static EventType GetStaticType() { return EventType::##type; }\
		virtual EventType GetEventType() const override { return GetStaticType(); }\
		virtual const char* GetName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual s32 GetCategoryFlags() const override { return category; }

	class Event 
	{
	public:
		virtual EventType GetEventType() const = 0;
		virtual const char* GetName() const = 0;
		virtual s32 GetCategoryFlags() const = 0;
		virtual std::string ToString() const { return GetName(); }

		b8 IsInCategory(EventCategory category)
		{
			return GetCategoryFlags() & category;
		}

	public:
		b8 Handled = false;
	};

	class EventDispatcher
	{
		template<typename T>
		using EventFn = std::function<b8(T&)>;

	public:
		EventDispatcher(Event& event) : m_Event(event) { }

		template<typename T>
		b8 Dispatch(EventFn<T> callback)
		{
			if (m_Event.GetEventType() == T::GetStaticType())
			{
				m_Event.Handled = callback(*(T*)&m_Event);
				return true;
			}

			return false;
		}

	private:
		Event& m_Event;
	};

	inline std::ostream& operator<<(std::ostream& os, const Event& e)
	{
		return os << e.ToString();
	}
}
