#pragma once

#include "Event.h"

#include <sstream>

namespace DebEngine
{
	class MouseMovedEvent : public Event
	{
	public:
		MouseMovedEvent(f32 x, f32 y) : m_MouseX(x), m_MouseY(y) { }

		f32 GetX() const { return m_MouseX; }
		f32 GetY() const { return m_MouseY; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "MouseMovedEvent: " << " (" << m_MouseX << ", " << m_MouseY << ")";
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseMoved);
		EVENT_CLASS_CATEGORY(EventCategory::EventCategoryMouse | EventCategory::EventCategoryInput);
	private:
		f32 m_MouseX, m_MouseY;
	};


	class MouseScrollEvent : public Event
	{
	public:
		MouseScrollEvent(f32 x, f32 y) : m_OffsetX(x), m_OffsetY(y) { }

		f32 GetOffsetX() const { return m_OffsetX; }
		f32 GetOffsetY() const { return m_OffsetY; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "MouseScrolledEvent: (" << m_OffsetX << ", " << m_OffsetY << ")";
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseScrolled);
		EVENT_CLASS_CATEGORY(EventCategory::EventCategoryMouse | EventCategory::EventCategoryInput);
	private:
		f32 m_OffsetX, m_OffsetY;
	};

	class MouseButtonEvent : public Event
	{
	public:
		s32 GetMouseButton() const { return m_Button; }

		EVENT_CLASS_CATEGORY(EventCategory::EventCategoryMouse | EventCategory::EventCategoryInput);
	protected:
		MouseButtonEvent(s32 button) : m_Button(button) { }

		s32 m_Button;
	};

	class MouseButtonPressedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonPressedEvent(s32 button) : MouseButtonEvent(button) { }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "MouseButtonPressed: " << m_Button;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseButtonPressed);
	};


	class MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(s32 button) : MouseButtonEvent(button) { }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "MouseButtonReleased: " << m_Button;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseButtonReleased);
	};
}
