#include "debpch.h"
#include "Core.h"

#define DEBENGINE_BUILD_ID "v0.1a"

namespace DebEngine 
{
	void InitializeCore()
	{
		DebEngine::Logger::Init();

		DEB_CORE_TRACE("DebEngine {0}", DEBENGINE_BUILD_ID);
		DEB_CORE_TRACE("Initializing...");
	}

	void ShutdownCore()
	{
		DEB_CORE_TRACE("DebEngine is Shutting down...");
	}
}