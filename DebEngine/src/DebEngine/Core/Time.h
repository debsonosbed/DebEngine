#pragma once

#include "Application.h"

namespace DebEngine
{
	class Time
	{
		friend class Application;
	public:
		/// <summary>
		/// 
		/// </summary>
		/// <returns>Time since application started in seconds</returns>
		static f64 GetTime();

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Time since application started in milliseconds</returns>
		static u64 GetTimeMilliseconds();

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Time that took to draw previous frame</returns>
		static const f64 DeltaTime();
	private:
		static f64 m_DeltaTime;
	};
}

