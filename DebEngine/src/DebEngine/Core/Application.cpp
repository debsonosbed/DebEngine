
#include "debpch.h"
#include "Application.h"

#include <imgui/imgui.h>

#include "DebEngine/Core/Input/Events/WindowEvent.h"
#include "DebEngine/Core/Log.h"
#include "DebEngine/Core/Input/Input.h"
#include "DebEngine/Core/Time.h"
#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Graphics/SceneRenderer.h"
#include "Statistics.h"
#include "Profiler.h"

#include <SDL.h>

namespace DebEngine
{
	Application* Application::s_Instance = nullptr;

	Application::Application(const ApplicationSettings& settings)
	{
		DEB_CORE_ASSERT(!s_Instance, "Application already exists!");
		s_Instance = this;

		Profiler::StartMeasurement("Game Engine load");

		m_Window = Window::Create(WindowSettings({ settings.Name, settings.WindowWidth, settings.WindowHeight }));
		m_Window->SetEventCallback(BIND_EVENT_FN(Application::OnEvent));
		m_Window->SetVSync(true);

		m_ImGuiLayer = ImGuiLayer::Create();
		PushOverlay(m_ImGuiLayer);

		Renderer::Init();
		SceneRenderer::Init();

		Renderer::WaitAndRender();

		Profiler::EndMeasurement();
	}

	Application::~Application()
	{
	}

	void Application::Run()
	{
		OnInit();


		m_RenderThread = std::make_unique<Thread>();

		while (m_Running)
		{
			m_Window->ProcessEvents();

			if (!m_Minimized)
			{
				auto instance = this;
				//Renderer::Submit([instance] { instance->RenderImGui(); });

				for (auto layer : m_LayerStack)
					layer->OnUpdate();

				f64 mainThreadDeltaFrameTime = GetDeltaFrameTime();
				Statistics::SetMainThreadFrameTime(mainThreadDeltaFrameTime);

				// Temporary Editor's renering is done on main thread
				// Reason: Issues with system file dialogs and secondary thread exectuion
				RenderImGui();
				// Integrate thread to the render command queue
				m_RenderThread->AddJob([this] { RenderThreadFunc(); });
				m_RenderThread->Execute();
				// Update of ImGui window has to be done on main thread
				// Investigate what causes the issue
				m_ImGuiLayer->End();

				f64 renderThreadDeltaFrameTime = GetDeltaFrameTime();
				Statistics::SetRenderThreadFrameTime(renderThreadDeltaFrameTime);

				Time::m_DeltaTime = mainThreadDeltaFrameTime + renderThreadDeltaFrameTime;
			}

			Statistics::Update();
		}

		OnShutdown();
	}

	void Application::OnEvent(Event& event)
	{
		EventDispatcher eventDispatcher(event);
		eventDispatcher.Dispatch<WindowClosedEvent>(BIND_EVENT_FN(Application::OnWindowClosed));
		eventDispatcher.Dispatch<WindowResizedEvent>(BIND_EVENT_FN(Application::OnWindowResized));
		eventDispatcher.Dispatch<WindowMinimizedEvent>(BIND_EVENT_FN(Application::OnWindowMinimized));
		eventDispatcher.Dispatch<WindowRestoredEvent>(BIND_EVENT_FN(Application::OnWindowRestored));

		for (auto it = m_LayerStack.end(); it != m_LayerStack.begin(); )
		{
			(*--it)->OnEvent(event);
			if (event.Handled)
				break;
		}
	}

	void Application::PushLayer(Layer* layer)
	{
		m_LayerStack.PushLayer(layer);
	}

	void Application::PushOverlay(Layer* overlay)
	{
		m_LayerStack.PushOverlay(overlay);
	}

	void Application::RenderImGui()
	{
		m_ImGuiLayer->Begin();

		//Profiler::StartMeasurement("ImGui");
		for (auto layer : m_LayerStack)
			layer->OnImGuiRender();
		//Profiler::EndMeasurement();
	}

	u64 Application::GetTime()
	{
		return SDL_GetTicks();
	}

	Window& Application::GetWindow()
	{
		return *m_Window;
	}

	ImGuiLayer& Application::GetImGuiLayer()
	{
		return *m_ImGuiLayer;
	}

	const std::string Application::GetConfigurationName()
	{
#if defined(DEB_DEBUG)
		return "Debug";
#elif defined(DEB_RELEASE)
		return "Release";
#elif defined(DEB_DIST)
		return "Distribution";
#else
#error Undefined configuration!
#endif
	}

	const std::string Application::GetPlatformName()
	{
#if defined(DEB_PLATFORM_WINDOWS)
		return "Windows x64";
#else
#error Undefined platform!
#endif
	}

	b8 Application::OnWindowClosed(WindowClosedEvent& event)
	{
		m_Running = false;
		return true;
	}

	b8 Application::OnWindowResized(WindowResizedEvent& event)
	{
		u32 width = event.GetWidth();
		u32 height = event.GetHeight();

		m_Window->GetRendererContext()->OnResize(width, height);
		DEB_CORE_LOG("Width: {0}  Height: {1}", width, height);

		auto& framebuffers = FramebufferPool::GetGlobal()->GetAll();
		for (const auto& framebuffer : framebuffers)
		{
			const auto& framebufferSpec = framebuffer->GetSpecification();
			if (framebufferSpec.Width == 0)
			{
				width *= framebufferSpec.Scale;
				height *= framebufferSpec.Scale;
				// Resize framebuffers after the whole frame has been rendered
				// to avoid destroying framebuffers that were submitted with a draw calls
				Renderer::SubmitPostFrame([framebuffer, width, height]()
					{
						framebuffer->Resize(width, height);
					});
			}
		}

		return true;
	}

	b8 Application::OnWindowMinimized(WindowMinimizedEvent& event)
	{
		DEB_CORE_LOG("minimized");
		m_Minimized = true;

		return true;
	}

	b8 Application::OnWindowRestored(WindowRestoredEvent& event)
	{
		DEB_CORE_LOG("restored");
		m_Minimized = false;

		return true;
	}

	f64 Application::GetDeltaFrameTime()
	{
		f64 deltaFrameTime = 0.0;
		u64 time = SDL_GetPerformanceCounter();
		if (!m_isFirstFrame)
		{
			deltaFrameTime = ((time - m_LastFrameTime) * 1000 / (f64)SDL_GetPerformanceFrequency());
			m_LastFrameTime = time;
		}
		else
		{
			m_LastFrameTime = time;
			m_isFirstFrame = false;
		}

		return deltaFrameTime;
	}

	void Application::RenderThreadFunc()
	{
		m_Window->GetRendererContext()->BeginFrame();

		//Profiler::StartMeasurement("Wait And Render");
		Renderer::WaitAndRender();
		//Profiler::EndMeasurement();

		m_Window->SwapBuffers();

		Renderer::WaitPostFrame();

	}
}