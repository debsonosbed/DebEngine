#include "debpch.h"
#include "Thread.h"

namespace DebEngine
{
	Thread::Thread()
	{
		worker = std::thread(&Thread::QueueLoop, this);
	}

	Thread::~Thread()
	{
		if (worker.joinable())
		{
			Execute();
			queueMutex.lock();
			destroying = true;
			condition.notify_one();
			queueMutex.unlock();
			worker.join();
		}
	}

	void Thread::AddJob(std::function<void()> function)
	{
		std::lock_guard<std::mutex> lock(queueMutex);
		jobQueue.push(std::move(function));
		condition.notify_one();
	}

	void Thread::Execute()
	{
		std::unique_lock<std::mutex> lock(queueMutex);
		condition.wait(lock, [this]() { return jobQueue.empty(); });
	}

	void Thread::QueueLoop()
	{
		while (true)
		{
			std::function<void()> job;
			{
				std::unique_lock<std::mutex> lock(queueMutex);
				condition.wait(lock, [this] { return !jobQueue.empty() || destroying; });
				if (destroying)
				{
					break;
				}
				job = jobQueue.front();
			}

			job();

			{
				std::lock_guard<std::mutex> lock(queueMutex);
				jobQueue.pop();
				condition.notify_one();
			}
		}
	}

	///////////////////////////////////////////////////////////////
	// Thread Pool ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	void ThreadPool::SetThreadCount(u32 count)
	{
		Threads.clear();
		for (u32 i = 0; i < count; i++)
		{
			Threads.push_back(std::make_unique<Thread>());
		}
	}

	void ThreadPool::ExecuteThreads()
	{
		for (const auto& thread : Threads)
		{
			thread->Execute();
		}
	}
}