#include "debpch.h"
#include "Layer.h"

namespace DebEngine
{
	Layer::Layer(const std::string& layerName) : m_LayerName(layerName) { }

	const std::string& Layer::GetName() const
	{
		 return m_LayerName; 
	}
}
