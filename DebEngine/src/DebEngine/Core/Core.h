#pragma once

#include <memory>

#define DEBENGINE_BUILD_ID "v0.1a"

namespace DebEngine
{
	void InitializeCore();
	void ShutdownCore();
}

#ifdef DEB_PLATFORM_WINDOWS
#else
#error DebEngine only supports Windows platform (for now)!
#endif

#define BYTES_TO_MB(x) ((f32)x / 1048576.f)
#define DEB_EXPAND_VARGS(x) x
#define BIT(x) (1 << x)
#define VAR_NAME(x) (#x)
#define BIND_EVENT_FN(fn) std::bind(&##fn, this, std::placeholders::_1)

#include "Assert.h"
#include "Types.h"
#include "Time.h"