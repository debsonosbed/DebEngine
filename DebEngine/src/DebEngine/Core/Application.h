#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/LayerStack.h"
#include "DebEngine/Core/Window.h"
#include "DebEngine/Core/Input/Events/WindowEvent.h"
#include "DebEngine/ImGui/ImGuiLayer.h"
#include "DebEngine/Core/Thread.h"

namespace DebEngine
{
	struct ApplicationSettings
	{
		std::string Name;
		u32 WindowWidth, WindowHeight;
	};

	class Application
	{
	public:
		Application(const ApplicationSettings& settings = { "DebEngine", 1280, 720 });
		virtual ~Application();

		virtual void Run();
		virtual void OnInit() { }
		virtual void OnEvent(Event& event);
		virtual void OnShutdown() { }

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);
		void RenderImGui();

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Time in milliseconds since start of the program.</returns>
		u64 GetTime();

		Window& GetWindow();
		ImGuiLayer& GetImGuiLayer();

		inline static Application& Get() { return *s_Instance; }
		static const std::string GetConfigurationName();
		static const std::string GetPlatformName();
	private:
		b8 OnWindowClosed(WindowClosedEvent& event);
		b8 OnWindowResized(WindowResizedEvent& event);
		b8 OnWindowMinimized(WindowMinimizedEvent& event);
		b8 OnWindowRestored(WindowRestoredEvent& event);

		f64 GetDeltaFrameTime();
		void RenderThreadFunc();
	private:
		std::unique_ptr<Window> m_Window;
		b8 m_Running = true;
		b8 m_Minimized = false;
		LayerStack m_LayerStack;
		ImGuiLayer* m_ImGuiLayer;
		u64 m_LastFrameTime = 0.f;
		b8 m_isFirstFrame = true;
		f64 m_MainThreadFrameTime = 0.0;
		f64 m_RenderThreadFrameTime = 0.0;

		std::unique_ptr<Thread> m_RenderThread;

		static Application* s_Instance;
	};

	// To be defined in Client
	std::unique_ptr<Application> CreateApplication();
}
