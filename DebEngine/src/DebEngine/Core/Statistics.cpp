#include "debpch.h"
#include "Statistics.h"

#include "Time.h"

namespace DebEngine
{
	f64 Statistics::m_FramesPerSecond = 0.0;
	f64 Statistics::m_AverageFrameTime = 0.0;
	f64 Statistics::m_UpdateInterval = 1000.0; // in ms
	u32 Statistics::m_FrameCount = 0;
	f64 Statistics::m_FrameTimeElapsedAcumulator = 0.0;

	f64 Statistics::m_MainThreadFrameTimeElapsedAcumulator = 0.0;
	f64 Statistics::m_MainThreadFrameTime = 0.0;
	f64 Statistics::m_AverageMainThreadFrameTime = 0.0;

	f64 Statistics::m_RenderThreadFrameTimeElapsedAcumulator = 0.0;
	f64 Statistics::m_RenderThreadFrameTime = 0.0;
	f64 Statistics::m_AverageRenderThreadFrameTime = 0.0;

	u32 Statistics::m_GameViewportHeight = 0;
	u32 Statistics::m_GameViewportWidth = 0;
	u32 Statistics::m_GameFramebufferSize = 0;

	f64 Statistics::GetFPS()
	{
		return m_FramesPerSecond;
	}

	f64 Statistics::GetFrameTime()
	{
		return m_AverageFrameTime;
	}

	f64 Statistics::GetMainThreadFrameTime()
	{
		return m_AverageMainThreadFrameTime;
	}

	f64 Statistics::GetRenderThreadFrameTime()
	{
		return m_AverageRenderThreadFrameTime;
	}

	std::pair<u32, u32> Statistics::GetGameViewportDimensions()
	{
		return { m_GameViewportWidth, m_GameViewportHeight };
	}

	u32 Statistics::GetGameViewportSize()
	{
		return m_GameFramebufferSize;
	}

	void Statistics::Update()
	{
		m_FrameCount++;
		m_FrameTimeElapsedAcumulator += Time::DeltaTime() * 1000.0;
		m_MainThreadFrameTimeElapsedAcumulator += m_MainThreadFrameTime;
		m_RenderThreadFrameTimeElapsedAcumulator += m_RenderThreadFrameTime;
		if (m_FrameTimeElapsedAcumulator > m_UpdateInterval)
		{
			m_AverageFrameTime = m_FrameTimeElapsedAcumulator / (f64)m_FrameCount;
			m_AverageMainThreadFrameTime = m_MainThreadFrameTimeElapsedAcumulator / (f64)m_FrameCount;
			m_AverageRenderThreadFrameTime = m_RenderThreadFrameTimeElapsedAcumulator / (f64)m_FrameCount;

			m_FramesPerSecond = (1000.0 * (f64)m_FrameCount) / m_FrameTimeElapsedAcumulator;

			m_FrameCount = 0;
			m_FrameTimeElapsedAcumulator = 0.0;
			m_MainThreadFrameTimeElapsedAcumulator = 0.0;
			m_RenderThreadFrameTimeElapsedAcumulator = 0.0;
		}
	}

	void Statistics::SetRuntimeViewportDimensions(u32 width, u32 height)
	{
		m_GameViewportWidth = width;
		m_GameViewportHeight = height;
	}

	void Statistics::SetRuntimeFramebufferSize(u32 size)
	{
		m_GameFramebufferSize = size;
	}

	void Statistics::SetMainThreadFrameTime(f64 frameTime)
	{
		m_MainThreadFrameTime = frameTime;
	}

	void Statistics::SetRenderThreadFrameTime(f64 frameTime)
	{
		m_RenderThreadFrameTime = frameTime;
	}
}