#include "debpch.h"
#include "Utils.h"
#include <glm/vec4.hpp>

namespace DebEngine
{
	const glm::ivec4 Utils::ToRGBA(f32 r, f32 g, f32 b, f32 a)
	{
		return glm::ivec4{ r * 255.f, g * 255.f, b * 255.f, a * 255.f };
	}

	const glm::ivec4 Utils::ToRGBA(const glm::vec4& color)
	{
		return color * 255.f;
	}

	const glm::vec4 Utils::FromRGBA(u8 r, u8 g, u8 b, u8 a)
	{
		return glm::vec4{ r / 255.f, g / 255.f, b / 255.f, a / 255.f };
	}

	const glm::vec4 Utils::FromRGBA(const glm::ivec4& color)
	{
		return (glm::vec4)color / 255.f;
	}

	const glm::ivec4 Utils::GetRGBAFromHex(u32 hexValue)
	{
		return glm::ivec4(hexValue & 0xFF, (hexValue >> 8) & 0xFF, (hexValue >> 16) & 0xFF, (hexValue >> 24) & 0xFF);
	}

	const glm::ivec4 Utils::GetBGRAFromHex(u32 hexValue)
	{
		return glm::ivec4((hexValue >> 16) & 0xFF, (hexValue >> 8) & 0xFF, hexValue & 0xFF, (hexValue >> 24) & 0xFF);
	}

	const u32 Utils::GetHexFromRGBA(const glm::ivec4& color)
	{
		u8 colors[4] = { color.r, color.g, color.b, color.a };

		return *(u32*)colors;
	}
}