#pragma once

// Macros for assert
// Support for assert with/without assert message

//#ifdef DEB_DEBUG
#if 1
	#define DEB_ENABLE_ASSERTS
#endif

#ifdef DEB_ENABLE_ASSERTS
	#define DEB_ASSERT_NO_MESSAGE(condition) { if(!(condition)) { DEB_ERROR("Assertion Failed"); __debugbreak(); } }
	#define DEB_ASSERT_MESSAGE(condition, ...) { if(!(condition)) { DEB_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }

	#define DEB_ASSERT_RESOLVE(arg1, arg2, macro, ...) macro
	#define DEB_GET_ASSERT_MACRO(...) DEB_EXPAND_VARGS(DEB_ASSERT_RESOLVE(__VA_ARGS__, DEB_ASSERT_MESSAGE, DEB_ASSERT_NO_MESSAGE))

	#define DEB_ASSERT(...) DEB_EXPAND_VARGS( DEB_GET_ASSERT_MACRO(__VA_ARGS__)(__VA_ARGS__) )
	#define DEB_CORE_ASSERT(...) DEB_EXPAND_VARGS( DEB_GET_ASSERT_MACRO(__VA_ARGS__)(__VA_ARGS__) )
#else
	#define DEB_ASSERT(x, ...)
	#define DEB_CORE_ASSERT(x, ...)
#endif