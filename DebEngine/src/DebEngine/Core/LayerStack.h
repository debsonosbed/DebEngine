#pragma once

#include "Core.h"
#include "Layer.h"

#include <vector>

namespace DebEngine {

	typedef std::vector<Layer*> Layers;

	class  LayerStack
	{
	public:
		LayerStack() = default;
		~LayerStack();

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);
		void PopLayer(Layer* layer);
		void PopOverlay(Layer* overlay);

		Layers::iterator begin() { return m_Layers.begin(); }
		Layers::iterator end() { return m_Layers.end(); }
		Layers::reverse_iterator rbegin() { return m_Layers.rbegin(); }
		Layers::reverse_iterator rend() { return m_Layers.rend(); }

		Layers::const_iterator begin() const { return m_Layers.begin(); }
		Layers::const_iterator end() const { return m_Layers.end(); }
		Layers::const_reverse_iterator rbegin() const { return m_Layers.rbegin(); }
		Layers::const_reverse_iterator rend() const { return m_Layers.rend(); }
	private:
		Layers m_Layers;
		u32 m_LayerInsertIndex = 0;
	};
}

