#pragma once

#include <chrono>

namespace DebEngine
{
	class Profiler
	{
	public:
		static void StartMeasurement(const std::string& name);

		/// <summary>
		/// Start measurement on Render thread
		/// </summary>
		/// <param name="name"></param>
		static void StartMeasurementRT(const std::string& name);

		static void EndMeasurement();

		/// <summary>
		/// End measurement on render thread
		/// </summary>
		static void EndMeasurementRT();

	private:
		struct ProfilerParams
		{
			std::string MeasurementName;
			std::chrono::steady_clock::time_point StartTime;
		};

		static std::vector<ProfilerParams> m_ProfilerStack;
	};
}

