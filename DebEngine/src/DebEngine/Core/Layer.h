#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Input/Events/Event.h"

namespace DebEngine
{
	class Layer
	{
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer() = default;

		virtual void OnAttach() = 0;
		virtual void OnDetach() = 0;
		virtual void OnUpdate() = 0;
		virtual void OnImGuiRender() = 0;
		virtual void OnEvent(Event& event) = 0;

		const std::string& GetName() const;
	private:
		std::string m_LayerName;
	};
}
