#pragma once

#include <iostream>
#include <vector>
#include <memory>

namespace DebEngine
{
	class Path
	{
	public:
		Path() = default;
		Path(const std::string& path);

		// Immutable by default
		const std::string& Literal() const;

		Path& operator=(const std::string& pathLiteral);
		friend std::ostream& operator<<(std::ostream& out, const Path& path);
	private:
		std::string m_PathLiteral;
	};

	class File
	{
		friend class Directory;
	public:
		File() = default;

		Path& GetPath();
		Directory& GetDirectory();

	private:
		Path m_Path;
	};

	class Directory
	{
	public:
		Directory() = default;

		std::vector<Path> GetFiles();

		std::string operator=(const Directory& dir);
	private:
		Path m_DirectoryPath;
	};

	class Filesystem
	{
	public:
		static std::string OpenFileDialog(const std::string& initialDir = std::string(), const char* = nullptr);

	private:
		virtual std::string OpenFileDialogImpl(const std::string& initialDir, const char* filter) = 0;

		static std::unique_ptr<Filesystem> s_Instance;
	};
}
