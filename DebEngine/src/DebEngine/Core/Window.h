#pragma once

#include <functional>

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Input/Events/Event.h"
#include "DebEngine/Graphics/RendererContext.h"

namespace DebEngine
{
	struct WindowSettings
	{
		std::string Title;
		u32 Width;
		u32 Height;

		WindowSettings(const std::string& title = "DebEngine", u32 width = 1280, u32 height = 720) :
			Title(title), Width(width), Height(height) { }
	};

	class Window
	{
	public:
		using EventCallbackFn = std::function<void(Event&)>;

		virtual ~Window() = default;

		virtual void ProcessEvents() = 0;
		virtual void SwapBuffers() = 0;

		virtual u32 GetWidth() const = 0;
		virtual u32 GetHeight() const = 0;
		virtual u32 GetAspectRatio() const = 0;
		virtual void SetMouseVisible(b8 t_IsVisible) = 0;
		virtual b8 GetMouseVisible() const = 0;
		virtual void TrapMouse(b8 enabled) = 0;
		virtual void SetVSync(b8 enabled) = 0;
		virtual b8 IsVSync() const = 0;
		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void* GetNativeWindow() const = 0;

		virtual std::shared_ptr<RendererContext> GetRendererContext() = 0;

		static std::unique_ptr<Window> Create(const WindowSettings& props = WindowSettings());
	};
}
