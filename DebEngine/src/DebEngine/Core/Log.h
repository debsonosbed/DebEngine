#pragma once

#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include <spdlog/logger.h>

#include "spdlog/sinks/ostream_sink.h"

#define CONSOLE_LOGS_ENABLED true

namespace DebEngine
{
	enum class LogType
	{
		Trace,
		Info,
		Warn,
		Error,
		Critical
	};

	class Logger
	{
	public:
		static void Init();

		static std::shared_ptr<spdlog::logger>& GetCoreLogger();
		static std::shared_ptr<spdlog::logger>& GetClientLogger();
		static std::shared_ptr<spdlog::logger>& GetEditorCoreLogger();
		static std::shared_ptr<spdlog::logger>& GetEditorClientLogger();
		static std::ostringstream& GetEditorLogStream();

		static std::vector<std::pair<LogType, std::string>>& GetLogLines();

	private:
		static LogType GetLogType(const std::string& logLine);
	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
		static std::shared_ptr<spdlog::logger> s_EditorCoreLogger;
		static std::shared_ptr<spdlog::logger> s_EditorClientLogger;

		static std::ostringstream s_LoggerStream;

		static std::shared_ptr<spdlog::sinks::ostream_sink_mt> s_EditorConsoleSink;
		static std::vector<std::pair<LogType, std::string>> m_LogLinesContainer;

		static u32 m_CurrentEditorLogStreamSize;
	};

	class Log
	{
	public:
		template<typename... Args>
		static void Message(Args&& ...args)
		{
			if(CONSOLE_LOGS_ENABLED) Logger::GetClientLogger()->trace(std::forward<Args>(args)...);
			Logger::GetEditorClientLogger()->trace(std::forward<Args>(args)...);
		}

		template<typename... Args>
		static void Warn(Args&& ...args)
		{
			if (CONSOLE_LOGS_ENABLED) Logger::GetClientLogger()->warn(std::forward<Args>(args)...);
			Logger::GetEditorClientLogger()->warn(std::forward<Args>(args)...);
		}

		template<typename... Args>
		static void Error(Args&& ...args)
		{
			if (CONSOLE_LOGS_ENABLED) Logger::GetClientLogger()->error(std::forward<Args>(args)...);
			Logger::GetEditorClientLogger()->error(std::forward<Args>(args)...);
		}
	};

	class LogCore
	{
	public:
		template<typename... Args>
		static void Message(Args&& ...args)
		{
			if (CONSOLE_LOGS_ENABLED) Logger::GetCoreLogger()->trace(std::forward<Args>(args)...);
			Logger::GetEditorCoreLogger()->trace(std::forward<Args>(args)...);
		}

		template<typename... Args>
		static void Warn(Args&& ...args)
		{
			if (CONSOLE_LOGS_ENABLED) Logger::GetCoreLogger()->warn(std::forward<Args>(args)...);
			Logger::GetEditorCoreLogger()->warn(std::forward<Args>(args)...);
		}

		template<typename... Args>
		static void Error(Args&& ...args)
		{
			if (CONSOLE_LOGS_ENABLED) Logger::GetCoreLogger()->error(std::forward<Args>(args)...);
			Logger::GetEditorCoreLogger()->error(std::forward<Args>(args)...);
		}
	};
}


// DebEngine - Core console logging macros
#define DEB_CORE_TRACE(...)		::DebEngine::Logger::GetCoreLogger()->trace(__VA_ARGS__); ::DebEngine::Logger::GetEditorCoreLogger()->trace(__VA_ARGS__)
#define DEB_CORE_LOG(...)		::DebEngine::Logger::GetCoreLogger()->info(__VA_ARGS__); ::DebEngine::Logger::GetEditorCoreLogger()->info(__VA_ARGS__)
#define DEB_CORE_WARN(...)		::DebEngine::Logger::GetCoreLogger()->warn(__VA_ARGS__); ::DebEngine::Logger::GetEditorCoreLogger()->warn(__VA_ARGS__)
#define DEB_CORE_ERROR(...)		::DebEngine::Logger::GetCoreLogger()->error(__VA_ARGS__); ::DebEngine::Logger::GetEditorCoreLogger()->error(__VA_ARGS__)
#define DEB_CORE_CRITICAL(...)	::DebEngine::Logger::GetCoreLogger()->critical(__VA_ARGS__); ::DebEngine::Logger::GetEditorCoreLogger()->critical(__VA_ARGS__)

// DebEngine - Client console logging macros
#define DEB_TRACE(...)			::DebEngine::Logger::GetClientLogger()->trace(__VA_ARGS__); ::DebEngine::Logger::GetEditorClientLogger()->trace(__VA_ARGS__)
#define DEB_LOG(...)			::DebEngine::Logger::GetClientLogger()->info(__VA_ARGS__); ::DebEngine::Logger::GetEditorClientLogger()->info(__VA_ARGS__)
#define DEB_WARN(...)			::DebEngine::Logger::GetClientLogger()->warn(__VA_ARGS__); ::DebEngine::Logger::GetEditorClientLogger()->warn(__VA_ARGS__)
#define DEB_ERROR(...)			::DebEngine::Logger::GetClientLogger()->error(__VA_ARGS__); ::DebEngine::Logger::GetEditorClientLogger()->error(__VA_ARGS__)
#define DEB_CRITICAL(...)		::DebEngine::Logger::GetClientLogger()->critical(__VA_ARGS__); ::DebEngine::Logger::GetEditorClientLogger()->critical(__VA_ARGS__)
