#include "debpch.h"
#include "EditorCamera.h"

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Input/Input.h"
#include "DebEngine/Core/Time.h"

#include <glm/gtc/matrix_transform.hpp>

namespace DebEngine
{
	EditorCamera::EditorCamera(const glm::mat4& projectionMatrix)
		: Camera(projectionMatrix)
	{
		m_CameraTarget = glm::vec3(0.f);

		glm::vec3 position = { 0, 0, 3 };
		m_Distance = glm::distance(position, m_CameraTarget);

		m_Yaw = 0.f;
		m_Pitch = 0.f;

		UpdateCameraView();
	}

	void EditorCamera::OnUpdate()
	{
		if (Input::IsKeyDown(KeyCode::LCTRL))
		{
			const auto deltaMousePosition = (glm::vec2)Input::GetMousePositionDelta();

			if (Input::IsMouseButtonDown(KeyCode::MouseMiddle))
				MousePan(deltaMousePosition);
			else if (Input::IsMouseButtonDown(KeyCode::MouseLeft))
				MouseRotate(deltaMousePosition);
			else if (Input::IsMouseButtonDown(KeyCode::MouseRight))
				MouseZoom(deltaMousePosition.y);
		}

		UpdateCameraView();
	}

	b8 EditorCamera::OnMouseScroll(MouseScrollEvent& e)
	{
		f32 delta = e.GetOffsetY() * 20.0f;
		MouseZoom(delta);
		UpdateCameraView();
		return false;
	}

	void EditorCamera::OnEvent(Event& event)
	{
		EventDispatcher dispatcher(event);
		dispatcher.Dispatch<MouseScrollEvent>(BIND_EVENT_FN(EditorCamera::OnMouseScroll));
	}

	void EditorCamera::SetViewportSize(u32 width, u32 height)
	{
		m_ViewportWidth = width;
		m_ViewportHeight = height;
	}

	const glm::mat4& EditorCamera::GetViewMatrix() const
	{
		return m_ViewMatrix;
	}

	glm::mat4 EditorCamera::GetViewProjectionMatrix() const
	{
		return m_ProjectionMatrix * m_ViewMatrix;
	}

	glm::vec3 EditorCamera::Up()
	{
		return glm::rotate(GetOrientation(), glm::vec3(0.f, 1.f, 0.f));
	}

	glm::vec3 EditorCamera::Right()
	{
		return glm::rotate(GetOrientation(), glm::vec3(1.f, 0.f, 0.f));
	}

	glm::vec3 EditorCamera::Front()
	{
		return glm::rotate(GetOrientation(), glm::vec3(0.f, 0.f, -1.f));
	}

	void EditorCamera::SetPosition(const glm::vec3& position)
	{
		m_Position = position;
	}

	const glm::vec3& EditorCamera::GetPosition() const
	{
		return m_Position;
	}

	glm::quat EditorCamera::GetOrientation() const
	{
		return glm::quat(glm::vec3(-m_Pitch, -m_Yaw, 0.f));
	}

	f32 EditorCamera::GetPitch() const
	{
		return m_Pitch;
	}

	f32 EditorCamera::GetYaw() const
	{
		return m_Yaw;
	}

	glm::mat4 EditorCamera::GetProjectionMatrix() const
	{
		return m_ProjectionMatrix;
	}

	void EditorCamera::SetProjectionMatrix(const glm::mat4& projectionMatrix)
	{
		m_ProjectionMatrix = projectionMatrix;
	}

	void EditorCamera::UpdateCameraView()
	{
		m_Position = CalculatePosition();

		glm::quat orientation = GetOrientation();
		m_ViewMatrix = glm::translate(glm::mat4(1.0f), m_Position) * glm::toMat4(orientation);
		m_ViewMatrix = glm::inverse(m_ViewMatrix);
	}

	void EditorCamera::MousePan(const glm::vec2& delta)
	{
		auto speed = GetPanSpeed();
		m_CameraTarget += -Right() * delta.x * speed.x * m_Distance;
		m_CameraTarget += Up() * delta.y * speed.y * m_Distance;
	}

	void EditorCamera::MouseZoom(f32 delta)
	{
		m_Distance -= delta * GetZoomSpeed();
		if (m_Distance < 1.0f)
		{
			m_CameraTarget += Front();
			m_Distance = 1.0f;
		}
	}

	glm::vec3 EditorCamera::CalculatePosition()
	{
		return m_CameraTarget - Front() * m_Distance;
	}

	glm::vec2 EditorCamera::GetPanSpeed()
	{
		f32 x = std::min(m_ViewportWidth / 1000.0f, 2.4f); // max = 2.4f
		f32 xFactor = 0.0366f * (x * x) - 0.1778f * x + 0.3021f;

		f32 y = std::min(m_ViewportHeight / 1000.0f, 2.4f); // max = 2.4f
		f32 yFactor = 0.0366f * (y * y) - 0.1778f * y + 0.3021f;

		return { xFactor * Time::DeltaTime(), yFactor * Time::DeltaTime() };
	}

	f32 EditorCamera::GetRotationSpeed()
	{
		return 1.0f * Time::DeltaTime();
	}

	f32 EditorCamera::GetZoomSpeed() const
	{
		f32 distance = m_Distance * 0.2f;
		distance = std::max(distance, 0.0f);
		f32 speed = distance * distance;
		speed = std::min(speed, 100.0f); // max speed = 100
		return speed * Time::DeltaTime() * 5.0f;
	}

	void EditorCamera::MouseRotate(const glm::vec2& delta)
	{
		f32 yawSign = Up().y > 0 ? 1.f : -1.f;
		m_Yaw += yawSign * delta.x * GetRotationSpeed();
		m_Pitch += delta.y * GetRotationSpeed();
	}
}