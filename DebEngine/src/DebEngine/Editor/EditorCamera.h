#pragma once

#include "DebEngine/Graphics/Camera.h"
#include "DebEngine/Core/Input/Input.h"
#include "DebEngine/Core/Input/Events/MouseEvent.h"
#include <glm/gtx/quaternion.hpp>

namespace DebEngine
{
	class EditorCamera : public Camera
	{
	public:
		EditorCamera() = default;
		EditorCamera(const glm::mat4& projectionMatrix);

		void OnUpdate();
		void OnEvent(Event& event);

		void SetViewportSize(u32 width, u32 height);
		virtual const glm::mat4& GetViewMatrix() const;
		virtual glm::mat4 GetProjectionMatrix() const override;
		virtual void SetProjectionMatrix(const glm::mat4& projectionMatrix) override;
		virtual glm::mat4 GetViewProjectionMatrix() const;
		glm::vec3 Up();
		glm::vec3 Right();
		glm::vec3 Front();
		void SetPosition(const glm::vec3& position);
		const glm::vec3& GetPosition() const;
		glm::quat GetOrientation() const;
		f32 GetPitch() const;
		f32 GetYaw() const;
	private:
		void UpdateCameraView();
		b8 OnMouseScroll(MouseScrollEvent& e);

		void MousePan(const glm::vec2& delta);
		void MouseZoom(f32 delta);

		glm::vec3 CalculatePosition();

		glm::vec2 GetPanSpeed();
		f32 GetRotationSpeed();
		f32 GetZoomSpeed() const;

		void MouseRotate(const glm::vec2& delta);
	private:
		glm::mat4 m_ViewMatrix = glm::mat4(1.f);
		glm::vec3 m_Position = glm::vec3(0.f);
		glm::vec3 m_CameraTarget;
		f32 m_ViewportWidth = 1280.0f;
		f32 m_ViewportHeight = 720.0f;
		f32 m_Distance = 0.0f;

		f32 m_Yaw = 0.f;
		f32 m_Pitch = 0.f;
	};
}
