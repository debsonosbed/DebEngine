#include "debpch.h"
#include "YamlUtils.h"

namespace YAML
{
	Emitter& operator<<(Emitter& outStream, const glm::vec3 vec)
	{
		outStream << YAML::Flow;
		outStream << YAML::BeginSeq;
		outStream << vec.x << vec.y << vec.z;
		outStream << YAML::EndSeq;

		return outStream;
	}

	Emitter& operator<<(Emitter& outStream, const glm::vec4 vec)
	{
		outStream << YAML::Flow;
		outStream << YAML::BeginSeq;
		outStream << vec.x << vec.y << vec.z << vec.w;
		outStream << YAML::EndSeq;

		return outStream;
	}
}