#pragma once

#ifdef DEB_PLATFORM_WINDOWS
extern std::unique_ptr<DebEngine::Application> DebEngine::CreateApplication();

#ifdef DEB_DIST
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, char*, int nShowCmd)
#else
int main(int argc, char** argv)
#endif
{
	DebEngine::InitializeCore();
	auto app = DebEngine::CreateApplication();
	app->Run();
	DebEngine::ShutdownCore();
}
#endif
