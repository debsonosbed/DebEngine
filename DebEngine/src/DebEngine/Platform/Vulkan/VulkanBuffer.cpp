#include "debpch.h"
#include "VulkanBuffer.h"

#include "VulkanContext.h"

namespace DebEngine
{
	void VulkanBuffer::Allocate(VkBufferUsageFlags usageFlags, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceSize size, void* data)
	{
		auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();

		vDevice = vulkanLogicalDevice;

		// Create the buffer handle
		VkBufferCreateInfo bufferCreateInfo{};
		bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferCreateInfo.usage = usageFlags;
		bufferCreateInfo.size = size;

		VK_CHECK_RESULT(vkCreateBuffer(vulkanLogicalDevice, &bufferCreateInfo, nullptr, &vBuffer));

		// Create the memory backing up the buffer handle
		VulkanMemoryAllocator allocator("UniformBuffer");

		VkMemoryRequirements memReqs;
		vkGetBufferMemoryRequirements(vulkanLogicalDevice, vBuffer, &memReqs);

		allocator.Allocate(memReqs, &vMemory, memoryPropertyFlags);

		Alignment = memReqs.alignment;
		Size = size;
		UsageFlags = usageFlags;
		MemoryPropertyFlags = memoryPropertyFlags;

		// If a pointer to the buffer data has been passed, map the buffer and copy over the data
		if (data != nullptr)
		{
			VK_CHECK_RESULT(map());
			memcpy(Mapped, data, size);
			if ((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == 0)
				flush();

			unmap();
		}

		// Initialize a default descriptor that covers the whole buffer size
		setupDescriptor(size);
		// Attach the memory to the buffer object
		bind();
	}

	VkResult VulkanBuffer::map(VkDeviceSize size, VkDeviceSize offset)
	{
		return vkMapMemory(vDevice, vMemory, offset, size, 0, &Mapped);
	}

	/**
	* Unmap a mapped memory range
	*
	* @note Does not return a result as vkUnmapMemory can't fail
	*/
	void VulkanBuffer::unmap()
	{
		if (Mapped)
		{
			vkUnmapMemory(vDevice, vMemory);
			Mapped = nullptr;
		}
	}

	/**
	* Attach the allocated memory block to the buffer
	*
	* @param offset (Optional) Byte offset (from the beginning) for the memory region to bind
	*
	* @return VkResult of the bindBufferMemory call
	*/
	VkResult VulkanBuffer::bind(VkDeviceSize offset)
	{
		return vkBindBufferMemory(vDevice, vBuffer, vMemory, offset);
	}

	/**
	* Setup the default descriptor for this buffer
	*
	* @param size (Optional) Size of the memory range of the descriptor
	* @param offset (Optional) Byte offset from beginning
	*
	*/
	void VulkanBuffer::setupDescriptor(VkDeviceSize size, VkDeviceSize offset)
	{
		vDescriptor.offset = offset;
		vDescriptor.buffer = vBuffer;
		vDescriptor.range = size;
	}

	/**
	* Copies the specified data to the mapped buffer
	*
	* @param data Pointer to the data to copy
	* @param size Size of the data to copy in machine units
	*
	*/
	void VulkanBuffer::copyTo(void* data, VkDeviceSize size)
	{
		assert(Mapped);
		memcpy(Mapped, data, size);
	}

	void VulkanBuffer::copyTo(Buffer& buffer, u32 offset)
	{
		assert(Mapped);
		memcpy(Mapped, buffer.Data, buffer.Size);
	}

	/**
	* Flush a memory range of the buffer to make it visible to the device
	*
	* @note Only required for non-coherent memory
	*
	* @param size (Optional) Size of the memory range to flush. Pass VK_WHOLE_SIZE to flush the complete buffer range.
	* @param offset (Optional) Byte offset from beginning
	*
	* @return VkResult of the flush call
	*/
	VkResult VulkanBuffer::flush(VkDeviceSize size, VkDeviceSize offset)
	{
		VkMappedMemoryRange mappedRange = {};
		mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		mappedRange.memory = vMemory;
		mappedRange.offset = offset;
		mappedRange.size = size;
		return vkFlushMappedMemoryRanges(vDevice, 1, &mappedRange);
	}

	/**
	* Invalidate a memory range of the buffer to make it visible to the host
	*
	* @note Only required for non-coherent memory
	*
	* @param size (Optional) Size of the memory range to invalidate. Pass VK_WHOLE_SIZE to invalidate the complete buffer range.
	* @param offset (Optional) Byte offset from beginning
	*
	* @return VkResult of the invalidate call
	*/
	VkResult VulkanBuffer::invalidate(VkDeviceSize size, VkDeviceSize offset)
	{
		VkMappedMemoryRange mappedRange = {};
		mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		mappedRange.memory = vMemory;
		mappedRange.offset = offset;
		mappedRange.size = size;
		return vkInvalidateMappedMemoryRanges(vDevice, 1, &mappedRange);
	}

	/**
	* Release all Vulkan resources held by this buffer
	*/
	void VulkanBuffer::destroy()
	{
		if (vBuffer)
		{
			vkDestroyBuffer(vDevice, vBuffer, nullptr);
		}
		if (vMemory)
		{
			vkFreeMemory(vDevice, vMemory, nullptr);
		}
	}
}