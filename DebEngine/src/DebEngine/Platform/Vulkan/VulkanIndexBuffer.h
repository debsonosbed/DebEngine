#pragma once

#include "DebEngine/Graphics/IndexBuffer.h"

#include "DebEngine/Core/Buffer.h"

#include "VulkanMemoryAllocator.h"

namespace DebEngine
{
	class VulkanIndexBuffer : public IndexBuffer
	{
	public:
		VulkanIndexBuffer(Buffer& buffer, u32 indicesCount);

		virtual void SetData(void* buffer, u32 size, u32 offset = 0) override;
		virtual void Bind() const override;
		virtual void Unbind() const override;
		virtual u32 GetCount() const override;
		virtual u32 GetSize() const override;

		VulkanBuffer GetVulkanBuffer() const;
	private:
		void AllocateBuffer(u8* data, u32 size);
	private:
		Buffer m_LocalData;
		u32 m_IndicesCount = 0;

		VulkanBuffer m_VulkanBuffer;
	};
}

