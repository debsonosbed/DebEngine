#pragma once

#include "DebEngine/Graphics/VertexBuffer.h"

#include "DebEngine/Core/Buffer.h"
#include "VulkanBuffer.h"

#include "Vulkan.h"

namespace DebEngine
{
	class VulkanVertexBuffer : public VertexBuffer
	{
	public:
		VulkanVertexBuffer(Buffer& buffer);
		VulkanVertexBuffer(u32 baseSize);
		virtual ~VulkanVertexBuffer();

		// Applicable only to dynamic usage buffers
		virtual void UpdateBuffer(Buffer& buffer, u32 offset = 0) override;
		virtual void Bind(u32 bindingPoint = 0) const override;
		virtual void Unbind() const override;
		virtual const VertexBufferLayout& GetLayout() const override;
		virtual void SetLayout(const VertexBufferLayout& layout) override;
		virtual Buffer& GetBuffer() override;
		virtual void Destroy() override;

		VulkanBuffer GetVulkanBuffer() const;
	private:
		void Resize(u8* data, u32 size);
	private:
		Buffer m_LocalData;
		u32 m_BaseSize = 0;

		VulkanBuffer m_VulkanBuffer;
	};
}

