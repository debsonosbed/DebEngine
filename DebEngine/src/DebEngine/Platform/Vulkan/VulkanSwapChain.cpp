#include "debpch.h"
#include "VulkanSwapChain.h"

//#include <vulkan/vulkan_win32.h>
#include <SDL_vulkan.h>

#include "VulkanRenderPass.h"
#include "VulkanFramebuffer.h"
#include "VulkanUtils.h"

namespace DebEngine
{

	VulkanSwapChain::VulkanSwapChain(VkInstance instance, const std::shared_ptr<VulkanDevice>& logicalDevice)
		: m_Instance(instance), m_LogicalDevice(logicalDevice)
	{
		m_MemoryAllocator = std::make_unique<VulkanMemoryAllocator>(m_LogicalDevice, "SwapChainMemoryAllocator");
	}

	void VulkanSwapChain::InitSurface(SDL_Window* windowHandle)
	{
		VkPhysicalDevice physicalDevice = m_LogicalDevice->GetPhysicalDevice()->Get();

		DEB_CORE_ASSERT(SDL_Vulkan_CreateSurface(windowHandle, m_Instance, &m_Surface) == SDL_TRUE, "VulkanSwapChain::InitSurface -> Could not create Vulkan surface!");

		// Get available queue family properties
		u32 queueFamilyCount;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
		DEB_CORE_ASSERT(queueFamilyCount >= 1, "");

		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProperties.data());

		// Iterate over each queue to learn whether it supports presenting:
		// Find a queue with present support
		// Will be used to present the swap chain images to the windowing system

		std::vector<VkBool32> supportsPresent(queueFamilyCount);
		for (u32 i = 0; i < queueFamilyCount; i++)
		{
			vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, m_Surface, &supportsPresent[i]);
		}

		// Search for a graphics and a present queue in the array of queue
		// families, try to find one that supports both (best performance)
		std::optional<u32> graphicsQueueNodeIndex;
		std::optional<u32> presentQueueNodeIndex;
		for (u32 i = 0; i < queueFamilyCount; i++)
		{
			if ((queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
			{
				if (!graphicsQueueNodeIndex.has_value())
					graphicsQueueNodeIndex = i;

				if (supportsPresent[i] == VK_TRUE)
				{
					graphicsQueueNodeIndex = i;
					presentQueueNodeIndex = i;
					break;
				}
			}
		}

		if (!presentQueueNodeIndex.has_value())
		{
			// If there is no queue that supports both(present and graphics)
			// try to find a separate present queue
			for (u32 i = 0; i < queueFamilyCount; i++)
			{
				if (supportsPresent[i] == VK_TRUE)
				{
					presentQueueNodeIndex = i;
					break;
				}
			}
		}

		DEB_CORE_ASSERT(graphicsQueueNodeIndex.has_value(), "VulkanSwapChain::InitSurface -> Could not find graphics queue!");
		DEB_CORE_ASSERT(presentQueueNodeIndex.has_value(), "VulkanSwapChain::InitSurface -> Could not find present queue!");
		DEB_CORE_LOG("VulkanSwapChain::InitSurface -> Presentation queues are supported!");

		m_QueueNodeIndex = graphicsQueueNodeIndex.value();

		FindImageFormatAndColorSpace();
	}

	void VulkanSwapChain::Create(u32* width, u32* height, b8 vsync /*= false*/)
	{
		VkDevice logicalDevice = m_LogicalDevice->Get();
		VkPhysicalDevice physicalDevice = m_LogicalDevice->GetPhysicalDevice()->Get();

		VkSwapchainKHR oldSwapChain = m_VulkanSwapChain;


		// Get physical device surface properties and formats
		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		VK_CHECK_RESULT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, m_Surface, &surfaceCapabilities));

		// Get available present modes
		u32 presentModeCount;
		VK_CHECK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, m_Surface, &presentModeCount, nullptr));
		DEB_CORE_ASSERT(presentModeCount > 0, "VulkanSwapChain::Create -> Mo present modes available!");

		std::vector<VkPresentModeKHR> presentModes(presentModeCount);
		VK_CHECK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, m_Surface, &presentModeCount, presentModes.data()));

		VkExtent2D swapchainExtent{};
		// If width(and height) equals the special value 0xFFFFFFFF, the size of the surface will be set by the swapchain
		if (surfaceCapabilities.currentExtent.width == (u32)-1)
		{
			// If the surface size is undefined, the size is set to the size of images requested.
			swapchainExtent.width = *width;
			swapchainExtent.height = *height;
		}
		else
		{
			// If the surface size is defined, the swap chain size must match
			// Resolution of swap chain images is almost always the same as resolution of the window
			swapchainExtent = surfaceCapabilities.currentExtent;
			*width = surfaceCapabilities.currentExtent.width;
			*height = surfaceCapabilities.currentExtent.height;
		}

		m_Width = *width;
		m_Height = *height;

		// Select a present mode for the swapchain

		// The VK_PRESENT_MODE_FIFO_KHR mode must always be present as per spec
		// This mode waits for the vertical blank("v-sync")
		VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

		// If v-sync is not requested, try to find a mailbox mode
		// It's the lowest latency non-tearing present mode available
		if (!vsync)
		{
			for (u32 i = 0; i < presentModeCount; i++)
			{
				if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
				{
					swapchainPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
					break;
				}

				// Choose IMMEDIATE present mode when MAILBOX mode not available
				if ((swapchainPresentMode != VK_PRESENT_MODE_MAILBOX_KHR) && (presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR))
				{
					swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
				}
			}
		}

		// Determine the number of images
		// Simply sticking to this minimum means that we may sometimes have
		// to wait on the driver to complete internal operations before we can acquire
		// another image to render to. Therefore it is recommended to request at least one
		// more image than the minimum.
		// Make sure to not exceed the maximum number of images
		u32 desiredNumberOfSwapchainImages = surfaceCapabilities.minImageCount + 1;
		if ((surfaceCapabilities.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surfaceCapabilities.maxImageCount))
		{
			desiredNumberOfSwapchainImages = surfaceCapabilities.maxImageCount;
		}

		// Find the transformation of the surface
		VkSurfaceTransformFlagsKHR preTransform;
		if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
		{
			preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		}
		else
		{
			preTransform = surfaceCapabilities.currentTransform;
		}

		// Find a supported composite alpha format (not all devices support alpha opaque)
		VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		// Select the first composite alpha format available
		std::vector<VkCompositeAlphaFlagBitsKHR> compositeAlphaFlags = {
			VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
		};

		for (const auto& compositeAlphaFlag : compositeAlphaFlags)
		{
			if (surfaceCapabilities.supportedCompositeAlpha & compositeAlphaFlag)
			{
				compositeAlpha = compositeAlphaFlag;
				break;
			}
		}

		VkSwapchainCreateInfoKHR swapchainCreateInfo{};
		swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapchainCreateInfo.surface = m_Surface;
		swapchainCreateInfo.minImageCount = desiredNumberOfSwapchainImages;
		swapchainCreateInfo.imageFormat = m_VulkanColorFormat;
		swapchainCreateInfo.imageColorSpace = m_ColorSpace;
		swapchainCreateInfo.imageExtent = { swapchainExtent.width, swapchainExtent.height };
		swapchainCreateInfo.imageArrayLayers = 1;
		// Use VK_IMAGE_USAGE_TRANSFER_DST_BIT when post-processing needed
		swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapchainCreateInfo.queueFamilyIndexCount = 0;
		swapchainCreateInfo.pQueueFamilyIndices = nullptr;
		swapchainCreateInfo.preTransform = (VkSurfaceTransformFlagBitsKHR)preTransform;
		swapchainCreateInfo.compositeAlpha = compositeAlpha;
		swapchainCreateInfo.presentMode = swapchainPresentMode;
		swapchainCreateInfo.oldSwapchain = oldSwapChain;
		swapchainCreateInfo.pNext = nullptr;
		// Setting clipped to VK_TRUE allows the implementation to discard rendering outside of the surface area
		swapchainCreateInfo.clipped = VK_TRUE;

		// Enable transfer source on swap chain images if supported
		if (surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT) {
			swapchainCreateInfo.imageUsage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		}

		// Enable transfer destination on swap chain images if supported
		if (surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT) {
			swapchainCreateInfo.imageUsage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		}

		VK_CHECK_RESULT(vkCreateSwapchainKHR(logicalDevice, &swapchainCreateInfo, nullptr, &m_VulkanSwapChain));

		// If an existing swap chain is re-created, destroy the old swap chain
		// This also cleans up all the presentable images
		if (oldSwapChain != VK_NULL_HANDLE)
		{
			for (u32 i = 0; i < m_ImageCount; i++)
			{
				vkDestroyImageView(logicalDevice, m_Buffers[i].ImageView, nullptr);
			}
			vkDestroySwapchainKHR(logicalDevice, oldSwapChain, nullptr);
		}
		VK_CHECK_RESULT(vkGetSwapchainImagesKHR(logicalDevice, m_VulkanSwapChain, &m_ImageCount, nullptr));

		// Get the swap chain images
		m_Images.resize(m_ImageCount);
		VK_CHECK_RESULT(vkGetSwapchainImagesKHR(logicalDevice, m_VulkanSwapChain, &m_ImageCount, m_Images.data()));

		// Create an image view for every image
		// Get the swap chain buffers containing the image and image view
		m_Buffers.resize(m_ImageCount);
		for (u32 i = 0; i < m_ImageCount; i++)
		{
			VkImageViewCreateInfo colorAttachmentView{};
			colorAttachmentView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			colorAttachmentView.image = m_Images[i];
			colorAttachmentView.viewType = VK_IMAGE_VIEW_TYPE_2D;
			colorAttachmentView.format = m_VulkanColorFormat;
			colorAttachmentView.flags = 0;
			colorAttachmentView.pNext = nullptr;
			colorAttachmentView.components = {
				VK_COMPONENT_SWIZZLE_R,
				VK_COMPONENT_SWIZZLE_G,
				VK_COMPONENT_SWIZZLE_B,
				VK_COMPONENT_SWIZZLE_A
			};
			colorAttachmentView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			colorAttachmentView.subresourceRange.baseMipLevel = 0;
			colorAttachmentView.subresourceRange.levelCount = 1;
			colorAttachmentView.subresourceRange.baseArrayLayer = 0;
			colorAttachmentView.subresourceRange.layerCount = 1;

			m_Buffers[i].Image = m_Images[i];

			VK_CHECK_RESULT(vkCreateImageView(logicalDevice, &colorAttachmentView, nullptr, &m_Buffers[i].ImageView));
		}


		CreateDrawCommandBuffers();

		// Synchronization objects
		VkSemaphoreCreateInfo semaphoreCreateInfo{};
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		// Create a semaphore used to synchronize image presentation
		// Ensures that the image is displayed before we start submitting new commands to the queue
		VK_CHECK_RESULT(vkCreateSemaphore(m_LogicalDevice->Get(), &semaphoreCreateInfo, nullptr, &m_Semaphores.PresentComplete));
		// Create a semaphore used to synchronize command submission
		// Ensures that the image is not presented until all commands have been submitted and executed
		VK_CHECK_RESULT(vkCreateSemaphore(m_LogicalDevice->Get(), &semaphoreCreateInfo, nullptr, &m_Semaphores.RenderComplete));

		VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		m_SubmitInfo = {};
		m_SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		m_SubmitInfo.pWaitDstStageMask = &pipelineStageFlags;
		m_SubmitInfo.waitSemaphoreCount = 1;
		m_SubmitInfo.pWaitSemaphores = &m_Semaphores.PresentComplete;
		m_SubmitInfo.signalSemaphoreCount = 1;
		m_SubmitInfo.pSignalSemaphores = &m_Semaphores.RenderComplete;

		// Wait fences to sync command buffer access
		VkFenceCreateInfo fenceCreateInfo{};
		fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
		m_WaitFences.resize(m_VulkanDrawCommandBuffers.size());
		for (auto& fence : m_WaitFences)
			VK_CHECK_RESULT(vkCreateFence(m_LogicalDevice->Get(), &fenceCreateInfo, nullptr, &fence));

		CreateDepthStencil();

		if (m_RenderPass == nullptr)
		{
			VulkanRenderPassSpecification renderPassSpec{};
			renderPassSpec.VulkanLogicalDevice = logicalDevice;
			renderPassSpec.ColorFormat = VulkanColorFormatToDeb(m_VulkanColorFormat);
			renderPassSpec.DepthFormat = VulkanDepthFormatToDeb(m_VulkanDepthFormat);
			renderPassSpec.FinalLayout = Layout::Present;
			renderPassSpec.IsPrimary = true;
			renderPassSpec.ClearOnStart = false;
			//renderPassSpec.MSAAType = MSAA::X8;
			m_RenderPass = VulkanRenderPass::Create(renderPassSpec);

			m_VulkanRenderPass = std::dynamic_pointer_cast<VulkanRenderPass>(m_RenderPass)->GetVulkanRenderPass();
		}


		CreateFramebuffers();
	}

	void VulkanSwapChain::BeginFrame()
	{
		VK_CHECK_RESULT(AcquireNextImage(m_Semaphores.PresentComplete, &m_CurrentBufferIndex));
		// Since swap chain's render pass has more than one frame buffer, the render's pass target
		// framebuffer needs to be updated every frame
		m_RenderPass->SetTargetFramebuffer(m_FrameBuffers[m_CurrentBufferIndex]);
	}

	void VulkanSwapChain::Present()
	{
		const u64 DEFAULT_FENCE_TIMEOUT = 100000000000;

		// Use a fence to wait until the command buffer has finished execution before using it again
		VK_CHECK_RESULT(vkWaitForFences(m_LogicalDevice->Get(), 1, &m_WaitFences[m_CurrentBufferIndex], VK_TRUE, UINT64_MAX));
		VK_CHECK_RESULT(vkResetFences(m_LogicalDevice->Get(), 1, &m_WaitFences[m_CurrentBufferIndex]));

		// Pipeline stage at which the queue submission will wait (via pWaitSemaphores)
		VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		// The submit info structure specifies a command buffer queue submission batch
		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.pWaitDstStageMask = &waitStageMask;               // Pointer to the list of pipeline stages that the semaphore waits will occur at
		submitInfo.pWaitSemaphores = &m_Semaphores.PresentComplete;      // Semaphore(s) to wait upon before the submitted command buffer starts executing
		submitInfo.waitSemaphoreCount = 1;                           // One wait semaphore
		submitInfo.pSignalSemaphores = &m_Semaphores.RenderComplete;     // Semaphore(s) to be signaled when command buffers have completed
		submitInfo.signalSemaphoreCount = 1;                         // One signal semaphore
		submitInfo.pCommandBuffers = &m_VulkanDrawCommandBuffers[m_CurrentBufferIndex]; // Command buffers(s) to execute in this batch (submission)
		submitInfo.commandBufferCount = 1;                           // One command buffer

		// Submit to the graphics queue passing a wait fence
		VK_CHECK_RESULT(vkQueueSubmit(m_LogicalDevice->GetQueue(), 1, &submitInfo, m_WaitFences[m_CurrentBufferIndex]));

		// Present the current buffer to the swap chain
		// Pass the semaphore signaled by the command buffer submission from the submit info as the wait semaphore for swap chain presentation
		// This ensures that the image is not presented to the windowing system until all commands have been submitted
		VkResult present = QueuePresent(m_LogicalDevice->GetQueue(), m_CurrentBufferIndex, m_Semaphores.RenderComplete);
		if (present != VK_SUCCESS || present == VK_SUBOPTIMAL_KHR)
		{
			if (present == VK_ERROR_OUT_OF_DATE_KHR)
			{
				OnResize(m_Width, m_Height);
				return;
			}
			else
			{
				VK_CHECK_RESULT(present);
			}
		}

		VK_CHECK_RESULT(vkWaitForFences(m_LogicalDevice->Get(), 1, &m_WaitFences[m_CurrentBufferIndex], VK_TRUE, DEFAULT_FENCE_TIMEOUT));
	}

	void VulkanSwapChain::OnResize(u32 width, u32 height)
	{
		DEB_CORE_WARN("VulkanContext::OnResize -> Resized to ({0}, {1})", width, height);
		auto logicalDevice = m_LogicalDevice->Get();

		vkDeviceWaitIdle(logicalDevice);

		vkDestroyImageView(logicalDevice, m_DepthStencil.ImageView, nullptr);
		vkDestroyImage(logicalDevice, m_DepthStencil.Image, nullptr);

		// TODO: Seems that render pass doesnt have to be destroyed on resize - double check that
		//vkDestroyRenderPass(logicalDevice, m_VulkanRenderPass, nullptr);
		for (const auto& fence : m_WaitFences)
			vkDestroyFence(logicalDevice, fence, nullptr);
		vkDestroySemaphore(logicalDevice, m_Semaphores.PresentComplete, nullptr);
		vkDestroySemaphore(logicalDevice, m_Semaphores.RenderComplete, nullptr);

		vkFreeMemory(logicalDevice, m_DepthStencil.DeviceMemory, nullptr);

		for (const auto& framebuffer : m_VulkanFrameBuffers)
			vkDestroyFramebuffer(logicalDevice, framebuffer, nullptr);

		// Command buffers need to be recreated as they may store
		// references to the recreated frame buffers!
		vkFreeCommandBuffers(logicalDevice, m_CommandPool, static_cast<u32>(m_VulkanDrawCommandBuffers.size()), m_VulkanDrawCommandBuffers.data());
		vkDestroyCommandPool(logicalDevice, m_CommandPool, nullptr);

		vkDeviceWaitIdle(logicalDevice);

		// And now recreate the swap chain with new size
		Create(&width, &height);
	}

	u32 VulkanSwapChain::GetImageCount() const
	{
		return m_ImageCount;
	}

	std::shared_ptr<Framebuffer> VulkanSwapChain::GetFramebuffer(u32 index) const
	{
		DEB_CORE_ASSERT(index < m_ImageCount);
		return m_FrameBuffers[index];
	}

	std::shared_ptr<Framebuffer> VulkanSwapChain::GetCurrentFramebuffer() const
	{
		return GetFramebuffer(m_CurrentBufferIndex);
	}

	void VulkanSwapChain::CleanUp()
	{
		VkDevice logicalDevice = m_LogicalDevice->Get();

		vkDeviceWaitIdle(logicalDevice);

		for (const auto& framebuffer : m_VulkanFrameBuffers)
			vkDestroyFramebuffer(logicalDevice, framebuffer, nullptr);

		vkFreeCommandBuffers(logicalDevice, m_CommandPool, static_cast<u32>(m_VulkanDrawCommandBuffers.size()), m_VulkanDrawCommandBuffers.data());

		if (m_Surface)
		{
			for (const auto& fence : m_WaitFences)
				vkDestroyFence(logicalDevice, fence, nullptr);

			vkDestroyRenderPass(logicalDevice, m_VulkanRenderPass, nullptr);
			vkDestroyCommandPool(logicalDevice, m_CommandPool, nullptr);
			vkDestroySemaphore(logicalDevice, m_Semaphores.PresentComplete, nullptr);
			vkDestroySemaphore(logicalDevice, m_Semaphores.RenderComplete, nullptr);
			vkDestroySwapchainKHR(logicalDevice, m_VulkanSwapChain, nullptr);
			vkDestroySurfaceKHR(m_Instance, m_Surface, nullptr);
		}

		if (m_VulkanSwapChain)
		{
			for (u32 i = 0; i < m_ImageCount; i++)
			{
				vkDestroyImageView(logicalDevice, m_Buffers[i].ImageView, nullptr);
			}
		}

		vkDestroyImage(logicalDevice, m_DepthStencil.Image, nullptr);
		vkDestroyImageView(logicalDevice, m_DepthStencil.ImageView, nullptr);
		vkFreeMemory(logicalDevice, m_DepthStencil.DeviceMemory, nullptr);

		m_Surface = VK_NULL_HANDLE;
		m_VulkanSwapChain = VK_NULL_HANDLE;

		vkDeviceWaitIdle(logicalDevice);
	}

	VkSwapchainKHR VulkanSwapChain::GetVulkanSwapChain() const
	{
		return m_VulkanSwapChain;
	}

	VkResult VulkanSwapChain::AcquireNextImage(VkSemaphore presentCompleteSemaphore, u32* imageIndex)
	{
		// By setting timeout to UINT64_MAX we will always wait until the next image has been acquired or an actual error is thrown
		// With that we don't have to handle VK_NOT_READY
		return vkAcquireNextImageKHR(m_LogicalDevice->Get(), m_VulkanSwapChain, UINT64_MAX, presentCompleteSemaphore, (VkFence)nullptr, imageIndex);
	}

	VkResult VulkanSwapChain::QueuePresent(VkQueue queue, u32 imageIndex, VkSemaphore waitSemaphore /*= VK_NULL_HANDLE*/)
	{
		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.pNext = nullptr;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &m_VulkanSwapChain;
		presentInfo.pImageIndices = &imageIndex;
		// Check if a wait semaphore has been specified to wait for before presenting the image
		if (waitSemaphore != VK_NULL_HANDLE)
		{
			presentInfo.pWaitSemaphores = &waitSemaphore;
			presentInfo.waitSemaphoreCount = 1;
		}
		return vkQueuePresentKHR(queue, &presentInfo);
	}

	void VulkanSwapChain::CreateDrawCommandBuffers()
	{
		// Create one command buffer for each swap chain image and reuse for rendering
		m_VulkanDrawCommandBuffers.resize(m_ImageCount);

		VkCommandPoolCreateInfo commandPoolCreateInfo{};
		commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		commandPoolCreateInfo.queueFamilyIndex = m_QueueNodeIndex;
		commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		VK_CHECK_RESULT(vkCreateCommandPool(m_LogicalDevice->Get(), &commandPoolCreateInfo, nullptr, &m_CommandPool));

		VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
		commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		commandBufferAllocateInfo.commandPool = m_CommandPool;
		commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		commandBufferAllocateInfo.commandBufferCount = static_cast<u32>(m_VulkanDrawCommandBuffers.size());
		VK_CHECK_RESULT(vkAllocateCommandBuffers(m_LogicalDevice->Get(), &commandBufferAllocateInfo, m_VulkanDrawCommandBuffers.data()));

		m_DrawCommandBuffers.clear();
		m_DrawCommandBuffers.reserve(m_VulkanDrawCommandBuffers.size());
		for (const auto& vulkanCommandBuffer : m_VulkanDrawCommandBuffers)
			m_DrawCommandBuffers.emplace_back(CommandBuffer::Create(static_cast<void*>(vulkanCommandBuffer)));
	}

	void VulkanSwapChain::CreateDepthStencil()
	{
		// All depth formats may be optional, so we need to find a suitable depth format to use
		std::vector<DepthFormat> depthFormats = {
			DepthFormat::D32F8S,
			DepthFormat::D32F,
			DepthFormat::D16
		};

		for (const auto& format : depthFormats)
		{
			VkFormat vulkanFormat = DebDepthFormatToVulkan(format);
			VkFormatProperties formatProperties;
			vkGetPhysicalDeviceFormatProperties(m_LogicalDevice->GetPhysicalDevice()->Get(), vulkanFormat, &formatProperties);
			// Format must support depth stencil attachment for optimal tiling
			if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
			{
				m_VulkanDepthFormat = vulkanFormat;
				break;
			}
		}


		DEB_CORE_ASSERT(m_VulkanDepthFormat, "VulkanContext::CreateDepthStencil -> Could not find a matching depth format");


		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = m_VulkanDepthFormat;
		imageCreateInfo.extent = { m_Width, m_Height, 1 };
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;


		VkDevice logicalDevice = m_LogicalDevice->Get();
		VK_CHECK_RESULT(vkCreateImage(logicalDevice, &imageCreateInfo, nullptr, &m_DepthStencil.Image));
		VkMemoryRequirements memoryRequirements{};
		vkGetImageMemoryRequirements(logicalDevice, m_DepthStencil.Image, &memoryRequirements);
		m_MemoryAllocator->Allocate(memoryRequirements, &m_DepthStencil.DeviceMemory);
		VK_CHECK_RESULT(vkBindImageMemory(logicalDevice, m_DepthStencil.Image, m_DepthStencil.DeviceMemory, 0));

		VkImageViewCreateInfo imageViewCreateInfo{};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.image = m_DepthStencil.Image;
		imageViewCreateInfo.format = m_VulkanDepthFormat;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		// Stencil aspect should only be set on depth + stencil formats (VK_FORMAT_D16_UNORM_S8_UINT..VK_FORMAT_D32_SFLOAT_S8_UINT
		if (m_VulkanDepthFormat >= VK_FORMAT_D16_UNORM_S8_UINT) {
			imageViewCreateInfo.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}

		VK_CHECK_RESULT(vkCreateImageView(logicalDevice, &imageViewCreateInfo, nullptr, &m_DepthStencil.ImageView));
	}

	void VulkanSwapChain::CreateFramebuffers()
	{
		std::vector<VkImageView> imageViewAttachments(2);
		imageViewAttachments[1] = m_DepthStencil.ImageView;

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = m_VulkanRenderPass;
		framebufferInfo.pAttachments = imageViewAttachments.data();
		framebufferInfo.attachmentCount = static_cast<u32>(imageViewAttachments.size());
		framebufferInfo.width = m_Width;
		framebufferInfo.height = m_Height;
		framebufferInfo.layers = 1;


		// Create frame buffers for every swap chain image (actually triple buffering)
		m_VulkanFrameBuffers.resize(m_ImageCount);
		m_FrameBuffers.resize(m_ImageCount);
		VulkanFramebufferSpecification framebufferSpec{};
		framebufferSpec.Width = m_Width;
		framebufferSpec.Height = m_Height;
		framebufferSpec.SwapChainTarget = true;
		framebufferSpec.RenderPass = m_RenderPass;
		for (u32 i = 0; i < m_VulkanFrameBuffers.size(); i++)
		{
			imageViewAttachments[0] = m_Buffers[i].ImageView;
			VK_CHECK_RESULT(vkCreateFramebuffer(m_LogicalDevice->Get(), &framebufferInfo, nullptr, &m_VulkanFrameBuffers[i]));

			{
				framebufferSpec.Framebuffer = m_VulkanFrameBuffers[i];
				m_FrameBuffers[i] = VulkanFramebuffer::Create(framebufferSpec);
			}
		}

		// Set target framebuffer for the Render Pass
		m_RenderPass->SetTargetFramebuffer(m_FrameBuffers[m_CurrentBufferIndex]);
	}

	std::pair<VkFormat, VkColorSpaceKHR> VulkanSwapChain::FindImageFormatAndColorSpace()
	{
		VkPhysicalDevice physicalDevice = m_LogicalDevice->GetPhysicalDevice()->Get();

		// Get list of supported surface formats
		u32 formatCount;
		VK_CHECK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_Surface, &formatCount, nullptr));
		DEB_CORE_ASSERT(formatCount > 0, "");

		std::vector<VkSurfaceFormatKHR> surfaceFormats(formatCount);
		VK_CHECK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_Surface, &formatCount, surfaceFormats.data()));

		// If the surface format list only includes one entry with VK_FORMAT_UNDEFINED
		// then there is no preferred format, so we assume VK_FORMAT_B8G8R8A8_UNORM
		if ((formatCount == 1) && (surfaceFormats.front().format == VK_FORMAT_UNDEFINED))
		{
			m_VulkanColorFormat = VK_FORMAT_B8G8R8A8_UNORM;
			m_ColorSpace = surfaceFormats.front().colorSpace;
		}
		else
		{
			// Iterate over the list of all available surface formats 
			// and check for the presence of VK_FORMAT_B8G8R8A8_UNFORM
			b8 desiredColorFormatFound = false;
			for (auto&& surfaceFormat : surfaceFormats)
			{
				if (surfaceFormat.format == VK_FORMAT_B8G8R8A8_UNORM)
				{
					m_VulkanColorFormat = surfaceFormat.format;
					m_ColorSpace = surfaceFormat.colorSpace;
					desiredColorFormatFound = true;
					break;
				}
			}

			// In case VK_FORMAT_B8G8R8A8_UNFORM is not available, then select
			// the first available color format
			if (!desiredColorFormatFound)
			{
				m_VulkanColorFormat = surfaceFormats.front().format;
				m_ColorSpace = surfaceFormats.front().colorSpace;
			}
		}

		return { m_VulkanColorFormat, m_ColorSpace };
	}

	u32 VulkanSwapChain::GetWidth() const
	{
		return m_Width;
	}

	u32 VulkanSwapChain::GetHeight() const
	{
		return m_Height;
	}

	void* VulkanSwapChain::GetSurfaceNative() const
	{
		return (void*)m_Surface;
	}

	std::shared_ptr<RenderPass>& VulkanSwapChain::GetRenderPass()
	{
		return m_RenderPass;
	}

	VkRenderPass VulkanSwapChain::GetVulkanRenderPass() const
	{
		return m_VulkanRenderPass;
	}

	VkFramebuffer VulkanSwapChain::GetVulkanFramebuffer(u32 index) const
	{
		DEB_CORE_ASSERT(index < m_ImageCount, "");
		return m_VulkanFrameBuffers[index];
	}

	VkCommandBuffer VulkanSwapChain::GetVulkanDrawCommandBuffer(u32 index) const
	{
		DEB_CORE_ASSERT(index < m_ImageCount, "");
		return m_VulkanDrawCommandBuffers[index];
	}

	VkFramebuffer VulkanSwapChain::GetVulkanCurrentFramebuffer() const
	{
		return GetVulkanFramebuffer(m_CurrentBufferIndex);
	}

	VkCommandBuffer VulkanSwapChain::GetVulkanCurrentDrawCommandBuffer() const
	{
		return GetVulkanDrawCommandBuffer(m_CurrentBufferIndex);
	}

	std::shared_ptr<CommandBuffer>& VulkanSwapChain::GetCurrnetDrawCommandBuffer()
	{
		return m_DrawCommandBuffers[m_CurrentBufferIndex];
	}
}