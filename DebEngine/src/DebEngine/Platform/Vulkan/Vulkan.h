#pragma once

#include <vulkan/vulkan.h>

#define VK_CHECK_RESULT(f)                                                                    \
{                                                                                             \
	VkResult res = (f);                                                                       \
	if (res != VK_SUCCESS)                                                                    \
	{                                                                                         \
		DEB_CORE_ERROR("VkResult is: {0}\nFile: {1}\nLine: {2}\n", res, __FILE__, __LINE__);  \
		DEB_CORE_ASSERT(res == VK_SUCCESS);                                               \
	}                                                                                         \
}