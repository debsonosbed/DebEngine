#include "debpch.h"
#include "VulkanIndexBuffer.h"

#include "DebEngine/Graphics/Renderer.h"

#include "VulkanContext.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanCommandBuffer.h"

namespace DebEngine
{
	VulkanIndexBuffer::VulkanIndexBuffer(Buffer& buffer, u32 indicesCount)
		: m_LocalData(Buffer::Copy(buffer)), m_IndicesCount(indicesCount)
	{
		AllocateBuffer(m_LocalData.Data, m_LocalData.Size);
	}

	void VulkanIndexBuffer::SetData(void* buffer, u32 size, u32 offset /*= 0*/)
	{

	}

	void VulkanIndexBuffer::Bind() const
	{
		auto instance = this;
		Renderer::Submit([this]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				vkCmdBindIndexBuffer(drawCommandBuffer, m_VulkanBuffer.vBuffer, 0, VK_INDEX_TYPE_UINT32);
			});
	}

	void VulkanIndexBuffer::Unbind() const
	{

	}

	u32 VulkanIndexBuffer::GetCount() const
	{
		return m_IndicesCount;
	}

	u32 VulkanIndexBuffer::GetSize() const
	{
		return m_LocalData.Size;
	}

	VulkanBuffer VulkanIndexBuffer::GetVulkanBuffer() const
	{
		return m_VulkanBuffer;
	}

	void VulkanIndexBuffer::AllocateBuffer(u8* data, u32 size)
	{
		Renderer::Submit([this, data, size]() mutable
			{
				// TODO: Use staging
				auto device = VulkanContext::GetCurrentDevice();

				m_VulkanBuffer.Allocate(
					VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					size,
					data);
			});
	}
}
