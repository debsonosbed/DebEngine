#include "debpch.h"
#include "VulkanCommandBuffer.h"

#include "DebEngine/Graphics/Renderer.h"

#include "VulkanSwapChain.h"
#include "VulkanContext.h"
#include "VulkanFramebuffer.h"
#include "VulkanRenderPass.h"

namespace DebEngine
{
	VulkanCommandBuffer::VulkanCommandBuffer(void* commandBufferHandle)
		: m_VulkanCommandBuffer(static_cast<VkCommandBuffer>(commandBufferHandle)),
		m_CommandBufferType(CommandBufferType::Primary)
	{

	}

	VulkanCommandBuffer::VulkanCommandBuffer(CommandBufferType commandBufferType)
		: m_VulkanCommandBuffer(VK_NULL_HANDLE), m_CommandBufferType(commandBufferType)
	{
		auto instance = this;
		switch (commandBufferType)
		{
		case CommandBufferType::Primary:
		{

			Renderer::Submit([instance]()
				{
					auto vulkanLogicalDevice = std::dynamic_pointer_cast<VulkanContext>(Renderer::GetContext())->GetCurrentDevice();
					instance->m_VulkanCommandBuffer = vulkanLogicalDevice->CreatePrimaryCommandBuffer(false);
				});
			break;
		}
		case CommandBufferType::Secondary:
		{
			Renderer::Submit([instance]()
				{
					auto vulkanLogicalDevice = std::dynamic_pointer_cast<VulkanContext>(Renderer::GetContext())->GetCurrentDevice();
					instance->m_VulkanCommandBuffer = vulkanLogicalDevice->CreateSecondaryCommandBuffer();
				});
			break;
		}
		}
	}

	void VulkanCommandBuffer::BeginRecording()
	{
		VkCommandBufferBeginInfo cmdBufInfo{};
		cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		switch (m_CommandBufferType)
		{
		case CommandBufferType::Primary:
		{

			break;
		}
		case CommandBufferType::Secondary:
		{
			auto& renderPassSwapchain = Renderer::GetRenderPassSwapChain();
			auto vulkanRenderPass = std::dynamic_pointer_cast<VulkanRenderPass>(renderPassSwapchain)->GetVulkanRenderPass();
			auto vulkanFramebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(renderPassSwapchain->GetTargetFramebuffer())->GetVulkanFramebuffer();
			VkCommandBufferInheritanceInfo inheritanceInfo{};
			inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
			inheritanceInfo.renderPass = vulkanRenderPass;
			inheritanceInfo.framebuffer = VK_NULL_HANDLE;

			cmdBufInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
			cmdBufInfo.pInheritanceInfo = &inheritanceInfo;
			break;
		}
		}

		vkBeginCommandBuffer(m_VulkanCommandBuffer, &cmdBufInfo);
	}

	CommandBufferType VulkanCommandBuffer::GetType() const
	{
		return m_CommandBufferType;
	}

	void VulkanCommandBuffer::Flush(b8 endCommandBuffer)
	{
		auto instance = this;
		Renderer::Submit([instance, endCommandBuffer]()
			{
				auto vulkanLogicalDevice = std::dynamic_pointer_cast<VulkanContext>(Renderer::GetContext())->GetCurrentDevice();
				vulkanLogicalDevice->FlushCommandBuffer(instance->m_VulkanCommandBuffer, endCommandBuffer);
			});
	}

	void VulkanCommandBuffer::EndRecording()
	{
		vkEndCommandBuffer(m_VulkanCommandBuffer);
	}

	VkCommandBuffer& VulkanCommandBuffer::GetVulkanCommandBuffer()
	{
		return m_VulkanCommandBuffer;
	}

	void VulkanCommandBuffer::ExecuteCommands(std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers)
	{
		auto instance = this;
		/*Renderer::Submit([instance, commandBuffers]() mutable
			{*/
		std::vector<VkCommandBuffer> vulkanCommandsBuffers;
		vulkanCommandsBuffers.reserve(commandBuffers.size());
		for (const auto& commandBuffer : commandBuffers)
		{
			vulkanCommandsBuffers.emplace_back(std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer());
		}

		vkCmdExecuteCommands(instance->m_VulkanCommandBuffer, vulkanCommandsBuffers.size(), vulkanCommandsBuffers.data());

		commandBuffers.clear();
		//});
	}
}