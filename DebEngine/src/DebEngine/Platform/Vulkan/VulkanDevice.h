#pragma once

#include "Vulkan.h"
#include <unordered_set>
#include "VulkanBuffer.h"

namespace DebEngine
{
	//////////////////////////////////////////////////////////
	//////////////////// Physical Device /////////////////////
	//////////////////////////////////////////////////////////

	class VulkanPhysicalDevice
	{
		friend class VulkanDevice;
	public:
		struct QueueFamilyIndices
		{
			u32 Graphics = -1;
			u32 Compute = -1;
			u32 Transfer = -1;
		};
	public:
		VulkanPhysicalDevice();
		~VulkanPhysicalDevice();

		b8 IsExtensionSupported(const std::string& extensionName) const;
		u32 GetMemoryTypeIndex(u32 typeBits, VkMemoryPropertyFlags properties);
		inline VkPhysicalDevice Get() const { return m_PhysicalDevice; }
		const QueueFamilyIndices GetQueueFamilyIndices() const { return m_QueueFamilyIndices; }
		VkFormat GetVulkanDepthFormat() const;
		VkPhysicalDeviceProperties GetDeviceProperties() const;
		static std::shared_ptr<VulkanPhysicalDevice> Select();
	private:
		VkFormat FindDepthFormat() const;
		QueueFamilyIndices GetQueueFamilyIndices(s32 flags);
	private:
		QueueFamilyIndices m_QueueFamilyIndices;

		VkPhysicalDevice m_PhysicalDevice;
		VkPhysicalDeviceProperties m_Properties;
		VkPhysicalDeviceFeatures m_Features;
		VkPhysicalDeviceMemoryProperties m_MemoryProperties;

		VkFormat m_DepthFormat = VK_FORMAT_UNDEFINED;

		const f32 m_DefaultQueuePriority = 1.f;

		std::vector<VkQueueFamilyProperties> m_QueueFamilyProperties;
		std::unordered_set<std::string> m_SupportedExtensions;
		std::vector<VkDeviceQueueCreateInfo> m_QueueCreateInfos;
	};

	//////////////////////////////////////////////////////////
	//////////////////// Logical Device //////////////////////
	//////////////////////////////////////////////////////////

	class VulkanDevice
	{
	public:
		VulkanDevice() = default;
		VulkanDevice(const std::shared_ptr<VulkanPhysicalDevice>& physicalDevice, VkPhysicalDeviceFeatures enabledFeatures);
		~VulkanDevice();

		VkCommandBuffer CreatePrimaryCommandBuffer(bool begin);
		VkCommandBuffer CreateSecondaryCommandBuffer();
		void FlushCommandBuffer(VkCommandBuffer& commandBuffe, b8 endCommandBuffer = true);
		VkQueue GetQueue() const;
		const std::shared_ptr<VulkanPhysicalDevice> GetPhysicalDevice() const;
		VkDevice Get() const;
	private:
		b8 m_EnableDebugMarkers = false;

		VkDevice m_LogicalDevice = nullptr;
		VkPhysicalDeviceFeatures m_EnabledFeatures;
		VkCommandPool m_CommandPool;
		VkQueue m_Queue;

		std::shared_ptr<VulkanPhysicalDevice> m_PhysicalDevice;
	};
}