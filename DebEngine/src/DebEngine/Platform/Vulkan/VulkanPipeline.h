#include "DebEngine/Graphics/Pipeline.h"

#include "Vulkan.h"

namespace DebEngine
{
	class VulkanPipeline : public Pipeline
	{
	public:
		VulkanPipeline() = default;
		VulkanPipeline(const PipelineSpecification& pipelineSpecification, const std::string& name);
		virtual ~VulkanPipeline() = default;

		virtual void Bind() const override;
		virtual void Assemble() override;
		virtual void Push(const ShaderBufferMember& shaderBufferMember, Buffer& buffer) override;
		virtual void Push(const std::shared_ptr<ShaderBuffer> shaderBuffer, Buffer& buffer) override;

		virtual PipelineSpecification& GetSpecification() override;

		VkPipeline GetVulkanPipeline() const;
		VkPipelineLayout GetVulkanPipelineLayout() const;
	private:
		VkPrimitiveTopology GetToplogy() const;
		VkPolygonMode GetPolygonMode() const;
		VkPipelineDepthStencilStateCreateInfo BuildDepthStenilState() const;

		VkCompareOp GetCompareOp(CompareOp compareOp) const;
		VkStencilOp GetStencilOp(StencilOp stencilOp) const;
		VkCullModeFlagBits GetCullMode(CullMode cullMode) const;
	private:
		PipelineSpecification m_PipelineSpecification;

		VkPipelineLayout m_PipelineLayout;
		VkPipeline m_VulkanPipeline;
	};
}