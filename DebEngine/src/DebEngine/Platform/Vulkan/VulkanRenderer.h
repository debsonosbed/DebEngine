#pragma once

#include "Vulkan.h"

#include "DebEngine/Graphics/Framebuffer.h"
#include "DebEngine/Graphics/Pipeline.h"
#include "DebEngine/Graphics/VertexBuffer.h"
#include "DebEngine/Graphics/IndexBuffer.h"

// Only a test class for Vulkan implementations, will be removed in the future once renderer is working properly
namespace DebEngine
{
	class VulkanRenderer
	{
	public:
		static void Init();
		static void Shutdown();
		static void Draw();

		static void OnWindowResize(u32 width, u32 height);

		static void BeginFrame();
		static void EndFrame();

		static void SubmitQuad();

		static void BeginRenderPass(const std::shared_ptr<RenderPass>& renderPass);
		static void EndRenderPass();

	private:
		static void CompositeRenderPass(VkCommandBufferInheritanceInfo& inheritanceInfo);
		static void RenderQuad(const std::shared_ptr<Pipeline>& pipeline, glm::mat4 transform);
	private:

	};
}

