#include "debpch.h"
#include "VulkanVertexBuffer.h"

#include "DebEngine/Graphics/Renderer.h"

#include "VulkanContext.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanCommandBuffer.h"

namespace DebEngine
{
	VulkanVertexBuffer::VulkanVertexBuffer(Buffer& buffer)
		: m_LocalData(Buffer::Copy(buffer))
	{
		Resize(m_LocalData.Data, m_LocalData.Size);
	}

	VulkanVertexBuffer::VulkanVertexBuffer(u32 baseSize)
		: m_BaseSize(baseSize)
	{
		Resize(nullptr, baseSize);
	}

	VulkanVertexBuffer::~VulkanVertexBuffer()
	{
		m_VulkanBuffer.destroy();
	}

	void VulkanVertexBuffer::Bind(u32 bindingPoint) const
	{
		Renderer::Submit([this, bindingPoint]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				std::vector<VkDeviceSize> offsets = { 0 };
				vkCmdBindVertexBuffers(drawCommandBuffer, bindingPoint, 1, &m_VulkanBuffer.vBuffer, offsets.data());
			});
	}

	void VulkanVertexBuffer::Unbind() const
	{

	}

	const VertexBufferLayout& VulkanVertexBuffer::GetLayout() const
	{
		return {};
	}

	void VulkanVertexBuffer::SetLayout(const VertexBufferLayout& layout)
	{

	}

	void VulkanVertexBuffer::UpdateBuffer(Buffer& buffer, u32 offset)
	{
		if (buffer.Size > m_VulkanBuffer.Size)
		{
			// Allocate a bigger buffer than needed. 
			u32 calculatedSize = std::ceil((f32)buffer.Size / (f32)m_BaseSize) * m_BaseSize;
			Resize(nullptr, calculatedSize);
		}

		// Careful! `buffer` parameter has to be valid at least till frame finish.
		Renderer::Submit([this, buffer, offset]() mutable
			{
				m_VulkanBuffer.vDescriptor.range = buffer.Size;

				m_VulkanBuffer.map(buffer.Size, offset);
				m_VulkanBuffer.copyTo(buffer, offset);
				m_VulkanBuffer.unmap();
			});
	}

	Buffer& VulkanVertexBuffer::GetBuffer()
	{
		return m_LocalData;
	}

	void VulkanVertexBuffer::Destroy()
	{
		Renderer::Submit([this]
			{
				m_VulkanBuffer.destroy();
			});
	}

	VulkanBuffer VulkanVertexBuffer::GetVulkanBuffer() const
	{
		return m_VulkanBuffer;
	}

	void VulkanVertexBuffer::Resize(u8* data, u32 size)
	{
		Renderer::Submit([this, data, size]() mutable
			{
				// TODO: Use staging
				auto device = VulkanContext::GetCurrentDevice();

				m_VulkanBuffer.Allocate(
					VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					size,
					data);
			});
	}
}
