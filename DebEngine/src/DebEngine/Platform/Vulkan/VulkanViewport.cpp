#include "debpch.h"
#include "VulkanViewport.h"

#include "DebEngine/Graphics/Renderer.h"

namespace DebEngine
{

	VulkanViewport::VulkanViewport(u32 x, u32 y, u32 width, u32 height)
		: m_Width(width), m_Height(height), m_PosX(x), m_PosY(y)
	{

	}

	void VulkanViewport::SetViewport(u32 width, u32 height)
	{
		m_Width = width;
		m_Height = height;

		Renderer::GetAPI().SetViewport(m_PosX, m_PosY, m_Width, m_Height);
	}
}
