#pragma once

#include <SDL.h>

#include "DebEngine/Graphics/RendererContext.h"
#include "DebEngine/Graphics/Renderer.h"

#include "Vulkan.h"
#include "VulkanDevice.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanSwapChain.h"

namespace DebEngine
{
	class VulkanContext : public RendererContext
	{
	public:
		VulkanContext() = default;
		VulkanContext(SDL_Window* windowHandle);
		~VulkanContext();

		virtual void Create() override;
		virtual void BeginFrame() override;
		virtual void SwapBuffers() override;
		virtual void OnResize(u32 width, u32 height) override;
		virtual std::shared_ptr<SwapChain> GetSwapChain() const override { return m_SwapChain; }
		virtual void Shutdown() override;

		std::shared_ptr<VulkanDevice> GetDevice() const { return m_LogicalDevice; }

		static VkInstance GetInstance() { return s_VulkanInstance; }
		static std::shared_ptr<VulkanContext> Get() { return std::dynamic_pointer_cast<VulkanContext>(Renderer::GetContext()); }
		static std::shared_ptr<VulkanDevice> GetCurrentDevice() { return Get()->GetDevice(); }
	private:
		SDL_Window* m_WindowHandle;

		std::shared_ptr<VulkanPhysicalDevice> m_PhysicalDevice;
		std::shared_ptr<VulkanDevice> m_LogicalDevice;
		std::unique_ptr<VulkanMemoryAllocator> m_MemoryAllocator;
		//std::shared_ptr<SwapChain> m_SwapChain;
		std::shared_ptr<VulkanSwapChain> m_SwapChain;

		VkPipelineCache m_VulkanPipelineCache;
		VkDebugReportCallbackEXT m_DebugReportCallback = VK_NULL_HANDLE;

		static VkInstance s_VulkanInstance;
	};
}
