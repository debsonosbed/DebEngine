#pragma once

#include "Vulkan.h"
#include "DebEngine/Graphics/Shader.h"

#include <glm/glm.hpp>

namespace DebEngine
{
	struct VulkanBufferDescription
	{
		VkDeviceMemory Memory;
		VkBuffer Buffer;
		VkDescriptorBufferInfo Descriptor;
	};

	struct VulkanShaderBuffer : public ShaderBuffer
	{
		virtual ~VulkanShaderBuffer() = default;

		// Each shader buffer can have multiple bind indexes
		// Bind index is determined by counting executions of Material's `Bind` method.
		std::unordered_map<u32, std::vector<VulkanBufferDescription>> VulkanBufferDescriptions;
	};

	class VulkanShader : public Shader
	{
	public:
		struct PushConstantRange
		{
			std::string Name;
			u32 Offset = 0;
			u32 Size = 0;
			VkShaderStageFlagBits ShaderStage = VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
		};

	public:
		VulkanShader(const std::string& path);
		virtual ~VulkanShader();

		void Reload() override;
		void Bind() override;
		const std::string& GetName() const override { return m_Name; }

		const std::vector<VkPipelineShaderStageCreateInfo>& GetShaderStages() const;

		virtual const std::unordered_map<std::string, std::shared_ptr<ShaderBuffer>>& GetShaderBuffers() const override;

		const std::vector<PushConstantRange>& GetPushConstantRanges() const;
		std::shared_ptr<ShaderBuffer> GetUniformBuffer(const std::string& name);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="descriptorSet"></param>
		/// <returns>False if descriptor pool hasnt been created. Cases when no uniforms present in the shader</returns>
		b8 CreateDescriptorSet(VkDescriptorSet& descriptorSet);

		VkWriteDescriptorSet& GetDescriptorSet(const std::string& name);
		VkDescriptorSetLayout GetDescriptorSetLayout() const;
		const std::vector<VkDescriptorSetLayout>& GetDescriptorSetsLayouts() const;

		static VkShaderStageFlagBits ShaderTypeToShaderStage(ShaderType shaderType);
		static ShaderType ShaderStageToShaderType(VkShaderStageFlagBits shaderStage);
	private:
		std::unordered_map<VkShaderStageFlagBits, std::string> ExtractShaderByType(const std::string& source);
		void Compile(std::vector<std::vector<u32>>& outputBinary);
		void Load(VkPipelineShaderStageCreateInfo& shaderStageCreateInfo, ShaderType shaderType, const std::vector<u32> shaderData);
		void ExtractShaderLayout(VkShaderStageFlagBits shaderStage, const std::vector<u32>& shaderData);
		void CreateDescriptors();
		void Clean();
	private:
		std::vector<VkPipelineShaderStageCreateInfo> m_ShaderStages;
		std::unordered_map<VkShaderStageFlagBits, std::string> m_ShaderSource;
		std::vector<VkShaderModule> m_ShaderModules;

		std::string m_Name;
		std::string m_ShaderPath;
		ShaderType m_ShaderType;

		std::unordered_map<std::string, u32> m_UniformLocations;
		std::vector<PushConstantRange> m_PushContantRanges;
		std::unordered_map<std::string, VkWriteDescriptorSet> m_WriteDescriptorSets;

		std::unordered_map<std::string, std::shared_ptr<ShaderBuffer>> m_ShaderBuffers;

		std::vector<VkDescriptorSetLayout> m_DescriptorSetsLayouts;
		VkDescriptorSetLayout m_DescriptorSetLayout;
		VkDescriptorPool m_DescriptorPool = VK_NULL_HANDLE;

		u32 m_UBOCount = 0;
		u32 m_SamplersCount = 0;
	};
}

