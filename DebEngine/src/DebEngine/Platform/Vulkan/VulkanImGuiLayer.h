#pragma once

#include "DebEngine/ImGui/ImGuiLayer.h"

#include "backends/imgui_impl_vulkan.h"

#include "Vulkan.h"

namespace DebEngine
{
	class VulkanImGuiLayer : public ImGuiLayer
	{
	public:
		VulkanImGuiLayer();
		VulkanImGuiLayer(const std::string& name);
		virtual ~VulkanImGuiLayer() = default;

		virtual void Begin() override;
		virtual void End() override;
		virtual void ImGuiRenderPass() override;

		virtual void OnAttach() override;
		virtual void OnUpdate() override;
		virtual void OnEvent(Event& event) override;
		virtual void OnDetach() override;
		virtual void OnImGuiRender() override;

	private:
		VkDescriptorPool m_DescriptorPool;
		std::shared_ptr<CommandBuffer> m_CommandBuffer;
	};
}

