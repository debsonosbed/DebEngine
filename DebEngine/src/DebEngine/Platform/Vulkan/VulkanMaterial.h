#pragma once

#include "DebEngine/Graphics/Material.h"

#include "Vulkan.h"
#include "VulkanShader.h"
#include "VulkanBuffer.h"
#include "VulkanPipeline.h"

namespace DebEngine
{
	class VulkanMaterial : public Material
	{
	public:
		VulkanMaterial(const std::shared_ptr<Pipeline>& pipeline, b8 isMeshPrimary);

		virtual void Bind() override;
	private:
		VkDescriptorSet m_VulkanDescriptorSet = VK_NULL_HANDLE;
		std::shared_ptr<VulkanShader> m_VulkanShader;
		std::shared_ptr<VulkanPipeline> m_VulkanPipeline;

		std::unordered_map<std::string, VkWriteDescriptorSet> m_WDSMap;
		std::unordered_map<std::string, VulkanBuffer> m_VulkanBuffers;

		VkDescriptorImageInfo m_DummyImageInfo;
	};
}

