#pragma once

#include "Vulkan.h"

namespace DebEngine
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// VulkanBuffer ////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////

	struct VulkanBuffer
	{
		void Allocate(VkBufferUsageFlags usageFlags, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceSize size, void* data = nullptr);
		VkResult map(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
		void unmap();
		VkResult bind(VkDeviceSize offset = 0);
		void setupDescriptor(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
		void copyTo(void* data, VkDeviceSize size);
		void copyTo(Buffer& buffer, u32 offset = 0);
		VkResult flush(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
		VkResult invalidate(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);
		void destroy();

		VkDevice vDevice;
		VkBuffer vBuffer = VK_NULL_HANDLE;
		VkDeviceMemory vMemory = VK_NULL_HANDLE;
		VkDescriptorBufferInfo vDescriptor;
		VkDeviceSize Size = 0;
		VkDeviceSize Alignment = 0;
		void* Mapped = nullptr;
		/** @brief Usage flags to be filled by external source at buffer creation (to query at some later point) */
		VkBufferUsageFlags UsageFlags;
		/** @brief Memory property flags to be filled by external source at buffer creation (to query at some later point) */
		VkMemoryPropertyFlags MemoryPropertyFlags;
	};
}

