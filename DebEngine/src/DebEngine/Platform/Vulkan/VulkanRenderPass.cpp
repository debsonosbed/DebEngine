#include "debpch.h"
#include "VulkanRenderPass.h"

#include "DebEngine/Core/Utils.h"
#include "VulkanContext.h"
#include "VulkanFramebuffer.h"
#include "VulkanCommandBuffer.h"

#include "VulkanUtils.h"

namespace DebEngine
{
	VulkanRenderPass::VulkanRenderPass(const RenderPassSpecification& specification)
		: m_Specification(specification)
	{
		AssembleRenderQueue();
	}

	VulkanRenderPass::VulkanRenderPass(const VulkanRenderPassSpecification& vulkanSpecification)
		: m_Specification(vulkanSpecification), m_VulkanSpecification(vulkanSpecification)
	{
		if (m_VulkanSpecification.VulkanLogicalDevice)
			Assemble();
		else
			AssembleRenderQueue();
	}

	VulkanRenderPass::~VulkanRenderPass()
	{
		/*auto instance = this;
		Renderer::Submit([instance]()
			{
				auto device = VulkanContext::GetCurrentDevice()->Get();
				vkDestroyRenderPass(device, instance->m_VulkanRenderPass, nullptr);
			});*/
	}

	void VulkanRenderPass::BeginRenderPass(const std::shared_ptr<CommandBuffer>& commandBuffer, b8 isSubpass)
	{
		auto context = VulkanContext::Get();
		auto swapChain = context->GetSwapChain();
		VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();

		u32 width = m_TargetFramebuffer->GetWidth();
		u32 height = m_TargetFramebuffer->GetHeight();

		std::vector<VkClearValue> clearValues;
		if (m_Specification.HasColorAttachment)
		{
			auto clearColorNormalized = Utils::FromRGBA(m_Specification.ClearColor);
			VkClearValue colorClearValue = {};
			colorClearValue.color = { { clearColorNormalized.r, clearColorNormalized.g, clearColorNormalized.b, clearColorNormalized.a } };
			clearValues.push_back(colorClearValue);
		}

		if (m_Specification.HasDepthAttachment)
		{
			VkClearValue depthClearValue = {};
			depthClearValue.depthStencil = { 1.f, 0 };
			clearValues.push_back(depthClearValue);
		}

		VkRenderPassBeginInfo renderPassBeginInfo = {};
		renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassBeginInfo.renderPass = m_VulkanRenderPass;
		renderPassBeginInfo.renderArea.offset.x = 0;
		renderPassBeginInfo.renderArea.offset.y = 0;
		renderPassBeginInfo.renderArea.extent.width = width;
		renderPassBeginInfo.renderArea.extent.height = height;
		renderPassBeginInfo.clearValueCount = clearValues.size(); // Color + depth
		renderPassBeginInfo.pClearValues = clearValues.data();
		renderPassBeginInfo.framebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(m_TargetFramebuffer)->GetVulkanFramebuffer();


		if (isSubpass)
			vkCmdBeginRenderPass(drawCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
		else
			vkCmdBeginRenderPass(drawCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void VulkanRenderPass::EndRenderPass(const std::shared_ptr<CommandBuffer>& commandBuffer)
	{
		VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
		vkCmdEndRenderPass(drawCommandBuffer);
	}

	const VkRenderPass& VulkanRenderPass::GetVulkanRenderPass() const
	{
		return m_VulkanRenderPass;
	}

	const u32 VulkanRenderPass::GetColorFormat() const
	{
		return static_cast<u32>(m_VulkanColorFormat);
	}

	const VkFormat VulkanRenderPass::GetVulkanColorFormat() const
	{
		return m_VulkanColorFormat;
	}

	const VkFormat VulkanRenderPass::GetVulkanDepthFormat() const
	{
		return m_VulkanDepthFormat;
	}

	const VkSampleCountFlagBits VulkanRenderPass::GetVulkanSampleCount() const
	{
		return m_VulkanSampleCount;
	}

	void VulkanRenderPass::AssembleRenderQueue()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->Assemble();
			});
	}

	void VulkanRenderPass::Assemble()
	{
		VkDevice device = VK_NULL_HANDLE;
		if (m_VulkanSpecification.VulkanLogicalDevice)
			device = m_VulkanSpecification.VulkanLogicalDevice;
		else
			device = VulkanContext::GetCurrentDevice()->Get();

		DEB_CORE_ASSERT(device != VK_NULL_HANDLE, "No device specified!");

		m_VulkanColorFormat = DebColorFormatToVulkan(m_Specification.ColorFormat);
		m_VulkanDepthFormat = DebDepthFormatToVulkan(m_Specification.DepthFormat);
		m_VulkanSampleCount = DebMSAATypeToVulkan(m_Specification.MSAAType);
		m_VulkanColorImageFinalLayout = DebImageLayoutToVulkan(m_Specification.FinalLayout);

		VkSubpassDescription subpassDescription = {};
		subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

		std::vector<VkAttachmentDescription> attachmentDescriptions;

		VkAttachmentReference colorAttachmentReference = {};
		if (m_Specification.HasColorAttachment)
		{
			VkAttachmentDescription colorAttachmentDescription{};
			
			colorAttachmentDescription.flags = 0;
			colorAttachmentDescription.format = m_VulkanColorFormat;
			colorAttachmentDescription.samples = m_VulkanSampleCount;
			colorAttachmentDescription.loadOp = m_Specification.ClearOnStart ?  VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			colorAttachmentDescription.finalLayout = m_VulkanColorImageFinalLayout;

			attachmentDescriptions.push_back(colorAttachmentDescription);

			colorAttachmentReference.attachment = 0;
			colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			subpassDescription.colorAttachmentCount = 1;
			subpassDescription.pColorAttachments = &colorAttachmentReference;
		}

		VkAttachmentReference depthAttachmentReference = {};
		if (m_Specification.HasDepthAttachment)
		{
			VkAttachmentDescription depthAttachmentDescription = {};

			depthAttachmentDescription.format = m_VulkanDepthFormat;
			depthAttachmentDescription.samples = m_VulkanSampleCount;
			depthAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			//attachmentDescriptions[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			//attachmentDescriptions[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			depthAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			depthAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			depthAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			depthAttachmentReference.attachment = 1;
			depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			attachmentDescriptions.push_back(depthAttachmentDescription);

			subpassDescription.pDepthStencilAttachment = &depthAttachmentReference;
		}

		// Use subpass dependencies for layout transitions
		std::vector<VkSubpassDependency> dependencies(2);

		dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[0].dstSubpass = 0;
		dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		dependencies[1].srcSubpass = 0;
		dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		// Create the actual render pass
		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = static_cast<u32>(attachmentDescriptions.size());
		renderPassInfo.pAttachments = attachmentDescriptions.data();
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpassDescription;
		renderPassInfo.dependencyCount = static_cast<u32>(dependencies.size());
		renderPassInfo.pDependencies = dependencies.data();

		VK_CHECK_RESULT(vkCreateRenderPass(device, &renderPassInfo, nullptr, &m_VulkanRenderPass));
	}

	std::shared_ptr<VulkanRenderPass> VulkanRenderPass::Create(const VulkanRenderPassSpecification& vulkanSpecification)
	{
		return std::make_unique<VulkanRenderPass>(vulkanSpecification);
	}
}