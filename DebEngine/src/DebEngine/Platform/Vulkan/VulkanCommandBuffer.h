#pragma once

#include "DebEngine/Graphics/CommandBuffer.h"

#include "Vulkan.h"

namespace DebEngine
{
	class VulkanCommandBuffer : public CommandBuffer
	{
	public:
		VulkanCommandBuffer(CommandBufferType commandBufferType); // primary
		VulkanCommandBuffer(void* commandBufferHandle);

		virtual void BeginRecording() override;
		virtual void EndRecording() override;
		CommandBufferType GetType() const override;
		virtual void Flush(b8 endCommandBuffer = true) override;

		VkCommandBuffer& GetVulkanCommandBuffer();


		void ExecuteCommands(std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers) override;

	private:
		VkCommandBuffer m_VulkanCommandBuffer;
		CommandBufferType m_CommandBufferType = CommandBufferType::None;
		VkCommandBufferInheritanceInfo m_VulkanInheritanceInfo;
		std::shared_ptr<RenderPass> m_InheritanceInfoRenderPass;
		b8 m_IsInheritanceInfoSet = false;
	};
}

