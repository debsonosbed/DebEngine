#include "debpch.h"
#include "VulkanContext.h"

#include "DebEngine/Core/Core.h"
#include <SDL_vulkan.h>


namespace DebEngine
{
	static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanDebugReportCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objectType,
		uint64_t object,
		size_t location,
		int32_t messageCode,
		const char* pLayerPrefix,
		const char* pMessage,
		void* pUserData)
	{
		(void)flags; (void)object; (void)messageCode; (void)pUserData; (void)pLayerPrefix;
		DEB_CORE_WARN("VulkanDebugCallback:\n	Object Type: {0}\n	Message: {1}", objectType, pMessage);

		return VK_FALSE;
	}

	static b8 s_Validation = true;

	VkInstance VulkanContext::s_VulkanInstance = nullptr;

	VulkanContext::VulkanContext(SDL_Window* windowHandle) : m_WindowHandle(windowHandle)
	{

	}

	VulkanContext::~VulkanContext()
	{
		m_SwapChain->CleanUp();

		vkDestroyPipelineCache(m_LogicalDevice->Get(), m_VulkanPipelineCache, nullptr);
	}

	void VulkanContext::Create()
	{
		DEB_CORE_LOG("VulkanContext::Create -> Creating Vulkan context...");

		// Application Info
		VkApplicationInfo appInfo{};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.apiVersion = VK_API_VERSION_1_2;
		appInfo.pApplicationName = "DebEngine";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "DebEngine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);

		// Extensions and Validation
		std::vector<const char*> instanceExtensions = {
			VK_KHR_SURFACE_EXTENSION_NAME,
		};

#ifdef DEB_DEBUG
			instanceExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
			instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif

		u32 sdlExtensionCount = 0;
		DEB_CORE_ASSERT(SDL_Vulkan_GetInstanceExtensions(m_WindowHandle, &sdlExtensionCount, nullptr) == SDL_TRUE,
			"Could not get instance extensions!");
		SDL_Vulkan_GetInstanceExtensions(m_WindowHandle, &sdlExtensionCount, nullptr);
		u32 additionalExtensionsCount = instanceExtensions.size();
		instanceExtensions.resize(additionalExtensionsCount + sdlExtensionCount);
		DEB_CORE_ASSERT(SDL_Vulkan_GetInstanceExtensions(
			m_WindowHandle,
			&sdlExtensionCount,
			instanceExtensions.data() + additionalExtensionsCount) == SDL_TRUE,
			"Could not get instance extensions!");

		VkInstanceCreateInfo instanceCreateInfo{};
		instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceCreateInfo.pApplicationInfo = &appInfo;
		instanceCreateInfo.pNext = NULL;
		instanceCreateInfo.enabledExtensionCount = (u32)instanceExtensions.size();
		instanceCreateInfo.ppEnabledExtensionNames = instanceExtensions.data();

		// Validation layers

		const std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };
#ifdef DEB_DEBUG
		if (s_Validation)
		{
			// Request all available layers
			u32 instanceLayerCount;
			vkEnumerateInstanceLayerProperties(&instanceLayerCount, nullptr);
			std::vector<VkLayerProperties> availableInstanceLayers(instanceLayerCount);
			vkEnumerateInstanceLayerProperties(&instanceLayerCount, availableInstanceLayers.data());

			// Check if all the validation layers are available
			for (const auto& layerName : validationLayers) {
				b8 validationLayerPresent = false;

				for (const auto& layerProperties : availableInstanceLayers)
				{
					if (strcmp(layerName, layerProperties.layerName) == 0)
					{
						validationLayerPresent = true;
						break;
					}
				}

				if (validationLayerPresent)
				{
					instanceCreateInfo.ppEnabledLayerNames = validationLayers.data();
					instanceCreateInfo.enabledLayerCount = validationLayers.size();
					break;
				}
			}
		}
		else
		{
			DEB_CORE_ERROR("DebEngine::VulkanContext::Create -> Validation layer not present. Validation is disabled.");
		}
#endif

		VK_CHECK_RESULT(vkCreateInstance(&instanceCreateInfo, nullptr, &s_VulkanInstance));

#ifdef DEB_DEBUG
		// Instance and Surface Creation
		if (s_Validation)
		{
			// TODO: Replace with VkDebugUtilsMesengerCrateInfoEXT?
			auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(s_VulkanInstance, "vkCreateDebugReportCallbackEXT");
			DEB_CORE_ASSERT(vkCreateDebugReportCallbackEXT != NULL, "");
			VkDebugReportCallbackCreateInfoEXT debugReportCallbackCreateInfo{};
			debugReportCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
			debugReportCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
			debugReportCallbackCreateInfo.pfnCallback = VulkanDebugReportCallback;
			debugReportCallbackCreateInfo.pUserData = NULL;
			VK_CHECK_RESULT(vkCreateDebugReportCallbackEXT(s_VulkanInstance, &debugReportCallbackCreateInfo, nullptr, &m_DebugReportCallback));
		}
#endif

		// Physical device

		m_PhysicalDevice = VulkanPhysicalDevice::Select();

		VkPhysicalDeviceFeatures enabledFeatures;
		memset(&enabledFeatures, 0, sizeof(VkPhysicalDeviceFeatures));
		enabledFeatures.samplerAnisotropy = VK_TRUE;
		//enabledFeatures.fillModeNonSolid = VK_TRUE;
		//enabledFeatures.wideLines = VK_TRUE;
		//enabledFeatures.largePoints = VK_TRUE;
		enabledFeatures.sampleRateShading = VK_TRUE;

		m_LogicalDevice = std::make_shared<VulkanDevice>(m_PhysicalDevice, enabledFeatures);

		m_MemoryAllocator = std::make_unique<VulkanMemoryAllocator>(m_LogicalDevice, "DefaultMemoryAllocator");

		m_SwapChain = std::make_unique<VulkanSwapChain>(s_VulkanInstance, m_LogicalDevice);
		//m_SwapChain = SwapChain::Create(swapChainSpecification);
		m_SwapChain->InitSurface(m_WindowHandle);

		u32 width = 1280, height = 720;
		m_SwapChain->Create(&width, &height);

		// Create Pipeline Cache
		VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {};
		pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
		VK_CHECK_RESULT(vkCreatePipelineCache(m_LogicalDevice->Get(), &pipelineCacheCreateInfo, nullptr, &m_VulkanPipelineCache));
	}

	void VulkanContext::BeginFrame()
	{
		m_SwapChain->BeginFrame();
	}

	void VulkanContext::SwapBuffers()
	{
		m_SwapChain->Present();
	}

	void VulkanContext::OnResize(u32 width, u32 height)
	{
		m_SwapChain->OnResize(width, height);
	}

	void VulkanContext::Shutdown()
	{
		//m_SwapChain->CleanUp();

		//vkDestroyPipelineCache(m_LogicalDevice->Get(), m_VulkanPipelineCache, nullptr);
	}
}