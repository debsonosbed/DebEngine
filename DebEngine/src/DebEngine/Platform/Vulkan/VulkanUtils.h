#pragma once

#include "Vulkan.h"

namespace DebEngine
{
	VkSampleCountFlagBits DebMSAATypeToVulkan(MSAA MSAAType);
	VkFormat DebColorFormatToVulkan(ColorFormat format);
	ColorFormat VulkanColorFormatToDeb(VkFormat format);
	VkFormat DebDepthFormatToVulkan(DepthFormat format);
	DepthFormat VulkanDepthFormatToDeb(VkFormat format);
	VkImageLayout DebImageLayoutToVulkan(Layout layout);
}