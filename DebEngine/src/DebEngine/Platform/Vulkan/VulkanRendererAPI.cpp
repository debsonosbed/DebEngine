#include "debpch.h"
#include "VulkanRendererAPI.h"

#include "DebEngine/Graphics/Renderer.h"

#include "Vulkan.h"
#include "VulkanCommandBuffer.h"

namespace DebEngine
{
	void VulkanRendererAPI::Init()
	{

	}

	void VulkanRendererAPI::Clear()
	{

	}

	void VulkanRendererAPI::SetViewport(u32 x, u32 y, u32 width, u32 height, b8 flipViewport)
	{
		Renderer::Submit([x, y, width, height, flipViewport]()
			{
				VkCommandBuffer vulkanCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				// Update dynamic viewport state
				VkViewport viewport{};
				viewport.x = (f32)x;
				viewport.y = flipViewport ? (f32)height : (f32)y;
				viewport.height = flipViewport ? -(f32)height : (f32)height;
				viewport.width = (f32)width;
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				vkCmdSetViewport(vulkanCommandBuffer, 0, 1, &viewport);

				// Update dynamic scissor state
				VkRect2D scissor = {};
				scissor.extent.width = width;
				scissor.extent.height = height;
				scissor.offset.x = 0;
				scissor.offset.y = 0;
				vkCmdSetScissor(vulkanCommandBuffer, 0, 1, &scissor);
			});
	}

	void VulkanRendererAPI::Draw(u32 vertexCount, u32 instancesCount, u32 firstVertex, u32 firstInstance)
	{
		Renderer::Submit([vertexCount, instancesCount, firstVertex, firstInstance]()
			{
				VkCommandBuffer vulkanCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
				vkCmdDraw(vulkanCommandBuffer, vertexCount, instancesCount, firstVertex, firstInstance);
			});
	}

	void VulkanRendererAPI::DrawIndexed(u32 indexCount, u32 instancesCount, u32 baseIndex, u32 baseVertex)
	{
		Renderer::Submit([indexCount, instancesCount, baseIndex, baseVertex]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				vkCmdDrawIndexed(drawCommandBuffer, indexCount, instancesCount, baseIndex, baseVertex, 0);
			});
	}

	void VulkanRendererAPI::SetLineThickness(f32 thickness)
	{
		Renderer::Submit([thickness]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				vkCmdSetLineWidth(drawCommandBuffer, 2.f);
			});
	}

	void VulkanRendererAPI::ExecuteCommands(const std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers)
	{
		Renderer::Submit([commandBuffers]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();

				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				std::vector<VkCommandBuffer> vulkanCommandsBuffers;
				vulkanCommandsBuffers.reserve(commandBuffers.size());

				for (const auto& commandBuffer : commandBuffers)
				{
					vulkanCommandsBuffers.emplace_back(std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer());
				}

				vkCmdExecuteCommands(drawCommandBuffer, vulkanCommandsBuffers.size(), vulkanCommandsBuffers.data());
			});
	}
}
