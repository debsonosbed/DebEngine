#include "debpch.h"
#include "VulkanUtils.h"

#include "DebEngine/Graphics/RenderPass.h"

namespace DebEngine
{
	VkSampleCountFlagBits DebMSAATypeToVulkan(MSAA MSAAType)
	{
		switch (MSAAType)
		{
		case MSAA::X1:
			return VK_SAMPLE_COUNT_1_BIT;
		case MSAA::X2:
			return VK_SAMPLE_COUNT_2_BIT;
		case MSAA::X4:
			return VK_SAMPLE_COUNT_4_BIT;
		case MSAA::X8:
			return VK_SAMPLE_COUNT_8_BIT;
		case MSAA::X16:
			return VK_SAMPLE_COUNT_16_BIT;
		}

		return VK_SAMPLE_COUNT_1_BIT;
	}

	VkFormat DebColorFormatToVulkan(ColorFormat format)
	{
		switch (format)
		{
		case ColorFormat::RGBA8: return VK_FORMAT_R8G8B8A8_UNORM;
		case ColorFormat::BGRA8: return VK_FORMAT_B8G8R8A8_UNORM;
		case ColorFormat::RGBA16F: return VK_FORMAT_R16G16B16A16_SFLOAT;
		case ColorFormat::RGBA32F: return VK_FORMAT_R32G32B32_SFLOAT;
		}

		return VK_FORMAT_MAX_ENUM;
	}

	ColorFormat VulkanColorFormatToDeb(VkFormat format)
	{
		switch (format)
		{
		case VK_FORMAT_R8G8B8A8_UNORM: return ColorFormat::RGBA8;
		case VK_FORMAT_B8G8R8A8_UNORM: return ColorFormat::BGRA8;
		case VK_FORMAT_R16G16B16A16_SFLOAT: return ColorFormat::RGBA16F;
		case VK_FORMAT_R32G32B32_SFLOAT: return ColorFormat::RGBA32F;
		}

		return ColorFormat::None;
	}

	VkFormat DebDepthFormatToVulkan(DepthFormat format)
	{
		switch (format)
		{
		case DepthFormat::D16: return VK_FORMAT_D16_UNORM;
		case DepthFormat::D32F: return VK_FORMAT_D32_SFLOAT;
		case DepthFormat::D32F8S: return VK_FORMAT_D32_SFLOAT_S8_UINT;
		}

		return VK_FORMAT_MAX_ENUM;
	}

	DepthFormat VulkanDepthFormatToDeb(VkFormat format)
	{
		switch (format)
		{
		case VK_FORMAT_D16_UNORM: return DepthFormat::D16;
		case VK_FORMAT_D32_SFLOAT: return DepthFormat::D32F;
		case VK_FORMAT_D32_SFLOAT_S8_UINT: return DepthFormat::D32F8S;
		}

		return DepthFormat::None;
	}

	VkImageLayout DebImageLayoutToVulkan(Layout layout)
	{
		switch (layout)
		{
		case Layout::Present: return VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		case Layout::ReadOnly: return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		case Layout::ColorAttachment: return VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		}

		return VK_IMAGE_LAYOUT_MAX_ENUM;
	}

}
