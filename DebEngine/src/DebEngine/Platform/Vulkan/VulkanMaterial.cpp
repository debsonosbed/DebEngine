#include "debpch.h"
#include "VulkanMaterial.h"

#include "DebEngine/Graphics/Renderer.h"

#include "VulkanTexture.h"
#include "VulkanContext.h"
#include "VulkanCommandBuffer.h"
#include "DebEngine/Core/Profiler.h";

namespace DebEngine
{
	VulkanMaterial::VulkanMaterial(const std::shared_ptr<Pipeline>& pipeline, b8 isMeshPrimary)
		: Material(pipeline)
	{
		m_IsPrimary = isMeshPrimary;
		m_Pipeline = pipeline;
		auto pipelineSpecification = pipeline->GetSpecification();
		// Pipeline must have a shader attached to it
		DEB_CORE_ASSERT(pipelineSpecification.Shader != nullptr);
		m_Shader = pipelineSpecification.Shader;
		m_VulkanShader = std::dynamic_pointer_cast<VulkanShader>(m_Shader);
		m_VulkanPipeline = std::dynamic_pointer_cast<VulkanPipeline>(m_Pipeline);

		// Create Vulkan buffers for uniforms ONLY for the first instance
		// The one limitation is that as for now textures for a mesh cannot be changed after they are loaded
		if (!isMeshPrimary) return;

		// Create a descriptor set
		Renderer::Submit([this]()
			{
				b8 success = m_VulkanShader->CreateDescriptorSet(m_VulkanDescriptorSet);
				// Returns false when shader has no uniforms at all
				if (!success) return;

				auto dummyTexture = std::dynamic_pointer_cast<VulkanTexture2D>(Renderer::GetDummyTexture());
				auto dummyTextureCube = std::dynamic_pointer_cast<VulkanTextureCube>(Renderer::GetDummyTextureCube());
				auto logicalDevice = VulkanContext::GetCurrentDevice();

				auto& shaderBuffers = m_Shader->GetShaderBuffers();
				for (const auto& [name, shaderBuffer] : shaderBuffers)
				{
					switch (shaderBuffer->Type)
					{
					case ShaderBufferType::UniformBufferConstant:
					{
						auto size = shaderBuffer->Size;
						m_VulkanBuffers[name].Allocate(
							VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
							VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
							size);

						auto& wds = m_VulkanShader->GetDescriptorSet(name);
						wds.pBufferInfo = &m_VulkanBuffers[name].vDescriptor;
						m_WDSMap.insert({ std::string(name), wds });

						break;
					}
					case ShaderBufferType::CombinedImageSampler:
					{
						auto& wds = m_VulkanShader->GetDescriptorSet(name);
						wds.pImageInfo = &dummyTexture->GetVulkanDescriptorInfo();
						m_WDSMap.insert({ std::string(name), wds });

						break;
					}
					case ShaderBufferType::CombinedImageSamplerCube:
					{
						auto& wds = m_VulkanShader->GetDescriptorSet(name);
						wds.pImageInfo = &dummyTextureCube->GetVulkanDescriptorInfo();
						m_WDSMap.insert({ std::string(name), wds });

						break;
					}
					}
				}

				if (m_WDSMap.size() > 0)
				{
					std::vector<VkWriteDescriptorSet> wdsContainer;
					wdsContainer.reserve(m_WDSMap.size());
					for (auto& [name, wds] : m_WDSMap)
					{
						wds.dstSet = m_VulkanDescriptorSet;
						wdsContainer.emplace_back(wds);
					}

					vkUpdateDescriptorSets(logicalDevice->Get(), wdsContainer.size(), wdsContainer.data(), 0, nullptr);
				}
			});
	}

	void VulkanMaterial::Bind()
	{
		if (!m_IsPrimary) return;

		b8 updateWDS = false;
		auto& shaderBufers = m_Shader->GetShaderBuffers();
		for (auto& [name, shaderDataDecl] : m_ShaderDataDeclaration)
		{
			if (shaderDataDecl.IsUploaded()) continue;

			shaderDataDecl.SetUploaded(true);

			switch (shaderDataDecl.GetShaderBufferType())
			{
			case ShaderBufferType::PushConstant:
			{
				auto parentName = GetParentName(name);
				auto childName = GetChildName(name);

				auto& shaderBuffer = m_VulkanShader->GetUniformBuffer(parentName);

				auto& buffer = shaderDataDecl.GetBuffer();
				auto dsName = name;

				if (!childName.empty())
				{
					auto& uniformMember = shaderBuffer->GetMember(childName);
					u32 size = uniformMember.Size;
					u32 offset = uniformMember.Offset;
					//m_Pipeline->Push(uniformMember, buffer);
					VkShaderStageFlagBits shaderStage = VulkanShader::ShaderTypeToShaderStage(uniformMember.ShaderType);
					Buffer bufferCopy = Buffer::Copy(buffer);
					Renderer::Submit([this, dsName, shaderStage, offset, size, bufferCopy]() mutable
						{
							auto& drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
							vkCmdPushConstants(
								drawCommandBuffer,
								m_VulkanPipeline->GetVulkanPipelineLayout(),
								shaderStage,
								offset, size,
								(void*)bufferCopy.Data);
							bufferCopy.Clear();
						});
				}
				else
				{
					u32 size = shaderBuffer->Size;
					u32 offset = shaderBuffer->Offset;
					//m_Pipeline->Push(uniformMember, buffer);
					VkShaderStageFlagBits shaderStage = VulkanShader::ShaderTypeToShaderStage(shaderBuffer->ShaderType);
					Buffer bufferCopy = Buffer::Copy(buffer);
					Renderer::Submit([this, dsName, shaderStage, offset, size, bufferCopy]() mutable
						{
							auto& drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
							vkCmdPushConstants(
								drawCommandBuffer,
								m_VulkanPipeline->GetVulkanPipelineLayout(),
								shaderStage,
								offset, size,
								(void*)bufferCopy.Data);
							bufferCopy.Clear();
						});
				}
				break;
			}
			case ShaderBufferType::UniformBufferConstant:
			{
				auto parentName = GetParentName(name);
				auto childName = GetChildName(name);

				auto& shaderBuffer = m_VulkanShader->GetUniformBuffer(parentName);

				auto& buffer = shaderDataDecl.GetBuffer();
				if (!childName.empty())
				{
					auto& shaderBufferMember = shaderBuffer->GetMember(childName);
					u32 offset = shaderBufferMember.Offset;
					Buffer bufferCopy = Buffer::Copy(buffer);
					Renderer::Submit([this, bufferCopy, parentName, offset]() mutable
						{
							auto& vulkanBuffer = m_VulkanBuffers.at(parentName);
							VK_CHECK_RESULT(vulkanBuffer.map(bufferCopy.Size, offset));
							vulkanBuffer.copyTo(bufferCopy);
							vulkanBuffer.unmap();
							bufferCopy.Clear();
						});
				}
				else
				{
					Buffer bufferCopy = Buffer::Copy(buffer);
					Renderer::Submit([this, bufferCopy, parentName]() mutable
						{
							auto& vulkanBuffer = m_VulkanBuffers.at(parentName);
							VK_CHECK_RESULT(vulkanBuffer.map(bufferCopy.Size));
							vulkanBuffer.copyTo(bufferCopy);
							vulkanBuffer.unmap();
							bufferCopy.Clear();
						});
				}

				break;
			}
			case ShaderBufferType::CombinedImageSampler:
			{
				auto tex = std::dynamic_pointer_cast<VulkanTexture2D>(shaderDataDecl.GetTexture());
				auto dsName = name;
				Renderer::Submit([this, dsName, tex]
					{
						auto& wds = m_WDSMap[dsName];
						wds.pImageInfo = &tex->GetVulkanDescriptorInfo();
					});

				updateWDS = true;

				break;
			}
			case ShaderBufferType::CombinedImageSamplerCube:
			{
				auto tex = std::dynamic_pointer_cast<VulkanTextureCube>(shaderDataDecl.GetTextureCube());
				auto dsName = name;
				Renderer::Submit([this, dsName, tex]
					{
						auto& wds = m_WDSMap[dsName];
						wds.pImageInfo = &tex->GetVulkanDescriptorInfo();
					});

				updateWDS = true;

				break;
			}
			}
		}

		// Update descriptor set if needed
		if (updateWDS)
		{
			Renderer::Submit([this]
				{
					std::vector<VkWriteDescriptorSet> wdsContainer;
					wdsContainer.reserve(m_WDSMap.size());
					for (auto& [name, wds] : m_WDSMap)
					{
						//if (wds.descriptorType != VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
						wdsContainer.emplace_back(wds);
					}

					if (wdsContainer.size() > 0)
					{
						auto logicalDevice = VulkanContext::GetCurrentDevice();
						vkUpdateDescriptorSets(logicalDevice->Get(), wdsContainer.size(), wdsContainer.data(), 0, nullptr);
					}
				});
		}

		// Bind that descriptor set
		Renderer::Submit([this]
			{
				if (m_VulkanDescriptorSet != VK_NULL_HANDLE)
				{
					auto& drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
					vkCmdBindDescriptorSets(drawCommandBuffer,
						VK_PIPELINE_BIND_POINT_GRAPHICS,
						m_VulkanPipeline->GetVulkanPipelineLayout(),
						0, 1,
						&m_VulkanDescriptorSet, 0, nullptr);
				}
			});
	}
}
