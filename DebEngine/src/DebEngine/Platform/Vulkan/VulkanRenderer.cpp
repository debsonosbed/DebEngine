#include "debpch.h"
#include "VulkanRenderer.h"

#include "DebEngine/Graphics/Renderer.h"

#include "VulkanContext.h"
#include "VulkanShader.h"
#include "VulkanFramebuffer.h"
#include "VulkanPipeline.h"
#include "VulkanIndexBuffer.h"
#include "VulkanVertexBuffer.h"
#include "VulkanCommandBuffer.h"

#include "backends/imgui_impl_vulkan.h"

#include <glm/gtc/quaternion.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <SDL.h>


namespace DebEngine
{
	static VkCommandBuffer s_ImGuiCommandBuffer = VK_NULL_HANDLE;
	static VkCommandBuffer s_CompositeCommandBuffer = VK_NULL_HANDLE;
	static VkCommandBuffer s_QuadCommandBuffer = VK_NULL_HANDLE;
	static std::shared_ptr<Pipeline> s_MeshPipeline = nullptr;
	static std::shared_ptr<Pipeline> s_CompositePipeline = nullptr;
	static std::shared_ptr<VertexBuffer> s_QuadVertexBuffer = nullptr;
	static std::shared_ptr<IndexBuffer> s_QuadIndexBuffer = nullptr;
	static VkDescriptorSet s_QuadDescriptorSet = VK_NULL_HANDLE;;
	static VkDescriptorSet s_DescriptorSet = VK_NULL_HANDLE;
	static std::vector<VkWriteDescriptorSet> s_WriteDescriptorSets;

	static std::shared_ptr<VertexBuffer> s_TestQuadVertexBuffer = nullptr;
	static std::shared_ptr<IndexBuffer> s_TestQuadIndexBuffer = nullptr;
	static VkDescriptorSet s_TestQuadDescriptorSet = VK_NULL_HANDLE;;
	static std::shared_ptr<Pipeline> s_WireframePipeline = nullptr;


	struct VulkanRenderData
	{
		std::shared_ptr<CommandBuffer> ActiveCommandBuffer;
	};

	static VulkanRenderData s_RenderData = {};

	void VulkanRenderer::Init()
	{
		Renderer::Submit([=]()
			{
				s_ImGuiCommandBuffer = VulkanContext::GetCurrentDevice()->CreateSecondaryCommandBuffer();
				s_CompositeCommandBuffer = VulkanContext::GetCurrentDevice()->CreateSecondaryCommandBuffer();
			});

		{
			s_MeshPipeline = PipelineLibrary::Get("Default");

			s_WireframePipeline = PipelineLibrary::Get("Wireframe");
		}

		{
			FramebufferSpecification spec;
			spec.SwapChainTarget = true;
			auto framebuffer = Framebuffer::Create(spec);

			PipelineSpecification pipelineSpecification;
			pipelineSpecification.Layouts = {
				{
					{ ShaderDataType::Float3, "a_Position" },
					{ ShaderDataType::Float2, "a_TexCoord" }
				}
			};
			pipelineSpecification.Shader = ShaderLibrary::Get("Texture");

			RenderPassSpecification renderPassSpec;
			pipelineSpecification.RenderPass = RenderPass::Create(renderPassSpec);
			s_CompositePipeline = Pipeline::Create(pipelineSpecification);
		}

		// Create fullscreen quad
		float x = -1;
		float y = -1;
		float width = 2, height = 2;
		struct QuadVertex
		{
			glm::vec3 Position;
			glm::vec2 TexCoord;
		};

		std::vector<QuadVertex> quadData(4);

		quadData[0].Position = glm::vec3(x, y, 0.1f);
		quadData[0].TexCoord = glm::vec2(0, 0);

		quadData[1].Position = glm::vec3(x + width, y, 0.1f);
		quadData[1].TexCoord = glm::vec2(1, 0);

		quadData[2].Position = glm::vec3(x + width, y + height, 0.1f);
		quadData[2].TexCoord = glm::vec2(1, 1);

		quadData[3].Position = glm::vec3(x, y + height, 0.1f);
		quadData[3].TexCoord = glm::vec2(0, 1);

		//s_QuadVertexBuffer = VertexBuffer::Create(quadData.data(), quadData.size(), sizeof(QuadVertex));
		std::vector<u32> indices = { 0, 1, 2, 2, 3, 0 };
		//s_QuadIndexBuffer = IndexBuffer::Create(indices.data(), indices.size(), sizeof(u32));


		/*Renderer::Submit([]()
			{
				auto shader = std::dynamic_pointer_cast<VulkanShader>(s_CompositePipeline->GetSpecification().Shader);
				auto framebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(s_MeshPipeline->GetSpecification().RenderPass->GetSpecification().TargetFramebuffer);
				s_QuadDescriptorSet = shader->CreateDescriptorSet();

				// Should be done with shader->SetTexture
				VkWriteDescriptorSet writeDescriptorSet{};
				writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writeDescriptorSet.dstSet = s_QuadDescriptorSet;
				writeDescriptorSet.descriptorCount = 1;
				writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writeDescriptorSet.pImageInfo = &framebuffer->GetVulkanDescriptorInfo();
				writeDescriptorSet.dstBinding = 0;

				auto vulkanDevice = VulkanContext::GetCurrentDevice()->Get();
				vkUpdateDescriptorSets(vulkanDevice, 1, &writeDescriptorSet, 0, nullptr);
			});*/

			/*Renderer::Submit([]()
				{
					auto& shader = std::dynamic_pointer_cast<VulkanShader>(s_MeshPipeline->GetSpecification().Shader);
					s_DescriptorSet = shader->CreateDescriptorSet();

					auto& ub0 = shader->GetUniformBuffer(0);
					VkWriteDescriptorSet writeDescriptorSet = {};
					writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
					writeDescriptorSet.dstSet = s_DescriptorSet;
					writeDescriptorSet.descriptorCount = 1;
					writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
					writeDescriptorSet.pBufferInfo = &ub0.Descriptor;
					writeDescriptorSet.dstBinding = 0;

					auto vulkanDevice = VulkanContext::GetCurrentDevice()->Get();
					vkUpdateDescriptorSets(vulkanDevice, 1, &writeDescriptorSet, 0, nullptr);
				});*/
	}

	void VulkanRenderer::Shutdown()
	{

	}

	void VulkanRenderer::Draw()
	{

		auto context = VulkanContext::Get();

#if 0
		auto swapChain = context->GetSwapChain();
		VkCommandBuffer drawCommandBuffer = swapChain->GetCurrentDrawCommandBuffer();
		s_RenderData.ActiveCommandBuffer = drawCommandBuffer;


		BeginRenderPass(s_CompositePipeline->GetSpecification().RenderPass);
		BeginFrame();
		Renderer::Submit([context]()
			{

				RenderQuad();
			});
		EndFrame();
		EndRenderPass();

#endif


#if 1
		{
			/*auto swapChain = context->GetSwapChain();
			s_RenderData.ActiveCommandBuffer = swapChain->GetCurrnetDrawCommandBuffer();
			BeginRenderPass(Renderer::GetRenderPassScene());

			glm::mat4 transform = glm::mat4(1.0f);
			transform = glm::translate(transform, glm::vec3(0.3f, -0.3f, 0.0f));
			transform = glm::rotate(transform, 0.3f, glm::vec3(0.0f, 0.0f, 1.0f));
			transform = glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f));
			RenderQuad(s_MeshPipeline, transform);

			transform = glm::mat4(1.0f);
			transform = glm::translate(transform, glm::vec3(-0.3f, 0.3f, 0.0f));
			transform = glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f));
			RenderQuad(s_WireframePipeline, transform);

			EndRenderPass();*/
		}

#if 0
		{
			// Here I'm rendering to a custom framebuffer and using custom render pass
			// but it's recorded on the current draw command buffer
			auto swapChain = context->GetSwapChain();
			CommandBuffer cmdBuffer = swapChain->GetCurrnetDrawCommandBuffer();
			s_RenderData.ActiveCommandBuffer = static_cast<VkCommandBuffer>(cmdBuffer.Buffer);

			s_MeshPipeline->GetSpecification().RenderPass->BeginRenderPass(cmdBuffer);
			s_Framebuffer->SetupViewport(cmdBuffer);

			glm::mat4 transform = glm::mat4(1.0f);
			transform = glm::translate(transform, glm::vec3(0.3f, -0.3f, 0.0f));
			transform = glm::rotate(transform, 0.3f, glm::vec3(0.0f, 0.0f, 1.0f));
			transform = glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f));
			RenderQuad(s_MeshPipeline, transform);

			transform = glm::mat4(1.0f);
			transform = glm::translate(transform, glm::vec3(-0.3f, 0.3f, 0.0f));
			transform = glm::scale(transform, glm::vec3(0.5f, 0.5f, 0.5f));
			RenderQuad(s_WireframePipeline, transform);

			s_MeshPipeline->GetSpecification().RenderPass->EndRenderPass(cmdBuffer);
			//vkEndCommandBuffer(s_RenderData.ActiveCommandBuffer);
		}
#endif

#if 0
		// swapChain->RenderToCurrentFramebuffer() ?
		{
			auto swapChain = context->GetSwapChain();
			CommandBuffer cmdBuffer = swapChain->GetCurrnetDrawCommandBuffer();
			s_RenderData.ActiveCommandBuffer = static_cast<VkCommandBuffer>(cmdBuffer.Buffer);

			u32 width = swapChain->GetWidth();
			u32 height = swapChain->GetHeight();

			VkClearValue clearValues[2];
			clearValues[0].color = { {0.1f, 0.1f,0.1f, 1.0f} };
			clearValues[1].depthStencil = { 1.0f, 0 };

			VkRenderPassBeginInfo renderPassBeginInfo = {};
			renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassBeginInfo.pNext = nullptr;
			renderPassBeginInfo.renderPass = static_cast<VkRenderPass>(swapChain->GetRenderPassNative());
			renderPassBeginInfo.renderArea.offset.x = 0;
			renderPassBeginInfo.renderArea.offset.y = 0;
			renderPassBeginInfo.renderArea.extent.width = width;
			renderPassBeginInfo.renderArea.extent.height = height;
			renderPassBeginInfo.clearValueCount = 2; // Color + depth
			renderPassBeginInfo.pClearValues = clearValues;
			renderPassBeginInfo.framebuffer = static_cast<VkFramebuffer>(swapChain->GetCurrentFramebufferNative());

			VkCommandBufferBeginInfo cmdBufInfo = {};
			cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			//vkBeginCommandBuffer(s_RenderData.ActiveCommandBuffer, &cmdBufInfo);
			vkCmdBeginRenderPass(s_RenderData.ActiveCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

			VkCommandBufferInheritanceInfo inheritanceInfo = {};
			inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
			inheritanceInfo.renderPass = static_cast<VkRenderPass>(swapChain->GetRenderPassNative());
			inheritanceInfo.framebuffer = static_cast<VkFramebuffer>(swapChain->GetCurrentFramebufferNative());

			// ImGui Pass
			{
				// TODO: Move to VulkanImGuiLayer
				// Rendering
				ImGui::Render();

				ImDrawData* main_draw_data = ImGui::GetDrawData();
				ImGui_ImplVulkan_RenderDrawData(main_draw_data, s_RenderData.ActiveCommandBuffer);
			}

			std::vector<VkCommandBuffer> commandBuffers;
			CompositeRenderPass(inheritanceInfo);
			commandBuffers.push_back(s_CompositeCommandBuffer);

			//vkCmdExecuteCommands(s_RenderData.ActiveCommandBuffer, commandBuffers.size(), commandBuffers.data());

			vkCmdEndRenderPass(s_RenderData.ActiveCommandBuffer);

			vkEndCommandBuffer(s_RenderData.ActiveCommandBuffer);
		}
#endif

#endif

#if 0
		Renderer::Submit([context]()
			{
				auto swapChain = context->GetSwapChain();


				VkCommandBuffer drawCommandBuffer = static_cast<VkCommandBuffer>(swapChain->GetCurrentDrawCommandBufferNative());
				//s_RenderData.ActiveCommandBuffer = drawCommandBuffer;

				auto framebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(s_Framebuffer);

				uint32_t width = framebuffer->GetSpecification().Width;
				uint32_t height = framebuffer->GetSpecification().Height;
				DEB_CORE_INFO("Width: {0}  Height: {1}", width, height);

				VkRenderPassBeginInfo renderPassBeginInfo = {};
				renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderPassBeginInfo.pNext = nullptr;
				renderPassBeginInfo.renderPass = framebuffer->GetVulkanRenderPass();
				renderPassBeginInfo.renderArea.offset.x = 0;
				renderPassBeginInfo.renderArea.offset.y = 0;
				renderPassBeginInfo.renderArea.extent.width = width;
				renderPassBeginInfo.renderArea.extent.height = height;

				VkClearValue clearValues[2];
				clearValues[0].color = { {0.1f, 0.1f,0.1f, 1.0f} };
				clearValues[1].depthStencil = { 1.0f, 0 };
				renderPassBeginInfo.clearValueCount = 2; // Color + depth
				renderPassBeginInfo.pClearValues = clearValues;
				renderPassBeginInfo.framebuffer = framebuffer->GetVulkanFramebuffer();

				VkCommandBufferBeginInfo cmdBufInfo = {};
				cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				VK_CHECK_RESULT(vkBeginCommandBuffer(drawCommandBuffer, &cmdBufInfo));
				vkCmdBeginRenderPass(drawCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);


				// Update dynamic viewport state
				VkViewport viewport{};
				viewport.x = 0.0f;
				viewport.y = (f32)height;
				viewport.height = -(f32)height;
				viewport.width = (f32)width;
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				vkCmdSetViewport(drawCommandBuffer, 0, 1, &viewport);

				// Update dynamic scissor state
				VkRect2D scissor = {};
				scissor.extent.width = width;
				scissor.extent.height = height;
				scissor.offset.x = 0;
				scissor.offset.y = 0;
				vkCmdSetScissor(drawCommandBuffer, 0, 1, &scissor);

				/*for (auto& mesh : s_Meshes)
					RenderMesh(mesh, drawCommandBuffer);

				s_Meshes.clear();*/

				RenderQuad(drawCommandBuffer);

				vkCmdEndRenderPass(drawCommandBuffer);
			});
#endif
#if 0
		Renderer::Submit([context]()
			{

				auto swapChain = std::dynamic_pointer_cast<VulkanSwapChain>(context->GetSwapChain());

				VkCommandBuffer drawCommandBuffer = static_cast<VkCommandBuffer>(swapChain->GetCurrentDrawCommandBufferNative());

				VkClearValue clearValues[2];
				clearValues[0].color = { {0.1f, 0.1f,0.1f, 1.0f} };
				clearValues[1].depthStencil = { 1.0f, 0 };

				u32 width = swapChain->GetWidth();
				u32 height = swapChain->GetHeight();

				VkRenderPassBeginInfo renderPassBeginInfo = {};
				renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderPassBeginInfo.pNext = nullptr;
				renderPassBeginInfo.renderPass = static_cast<VkRenderPass>(swapChain->GetRenderPassNative());
				renderPassBeginInfo.renderArea.offset.x = 0;
				renderPassBeginInfo.renderArea.offset.y = 0;
				renderPassBeginInfo.renderArea.extent.width = width;
				renderPassBeginInfo.renderArea.extent.height = height;
				renderPassBeginInfo.clearValueCount = 2; // Color + depth
				renderPassBeginInfo.pClearValues = clearValues;
				renderPassBeginInfo.framebuffer = static_cast<VkFramebuffer>(swapChain->GetCurrentFramebufferNative());

				{
					vkCmdBeginRenderPass(drawCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

					VkCommandBufferInheritanceInfo inheritanceInfo = {};
					inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
					inheritanceInfo.renderPass = static_cast<VkRenderPass>(swapChain->GetRenderPassNative());
					inheritanceInfo.framebuffer = static_cast<VkFramebuffer>(swapChain->GetCurrentFramebufferNative());

					std::vector<VkCommandBuffer> commandBuffers;
					CompositeRenderPass(inheritanceInfo);
					commandBuffers.push_back(s_CompositeCommandBuffer);
#if 0
					// ImGui Pass
					{
						VkCommandBufferBeginInfo cmdBufInfo = {};
						cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
						cmdBufInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
						cmdBufInfo.pInheritanceInfo = &inheritanceInfo;

						VK_CHECK_RESULT(vkBeginCommandBuffer(s_ImGuiCommandBuffer, &cmdBufInfo));

						// Update dynamic viewport state
						VkViewport viewport = {};
						viewport.x = 0.0f;
						viewport.y = (f32)height;
						viewport.height = -(f32)height;
						viewport.width = (f32)width;
						viewport.minDepth = 0.0f;
						viewport.maxDepth = 1.0f;
						vkCmdSetViewport(s_ImGuiCommandBuffer, 0, 1, &viewport);

						// Update dynamic scissor state
						VkRect2D scissor = {};
						scissor.extent.width = width;
						scissor.extent.height = height;
						scissor.offset.x = 0;
						scissor.offset.y = 0;
						vkCmdSetScissor(s_ImGuiCommandBuffer, 0, 1, &scissor);

						// TODO: Move to VulkanImGuiLayer
						// Rendering
						ImGui::Render();

						ImDrawData* main_draw_data = ImGui::GetDrawData();
						ImGui_ImplVulkan_RenderDrawData(main_draw_data, s_ImGuiCommandBuffer);

						VK_CHECK_RESULT(vkEndCommandBuffer(s_ImGuiCommandBuffer));

						commandBuffers.push_back(s_ImGuiCommandBuffer);
					}
#endif

					vkCmdExecuteCommands(drawCommandBuffer, commandBuffers.size(), commandBuffers.data());

					vkCmdEndRenderPass(drawCommandBuffer);
				}

				VK_CHECK_RESULT(vkEndCommandBuffer(drawCommandBuffer));
			});
#endif
	}

	void VulkanRenderer::OnWindowResize(u32 width, u32 height)
	{
		/*Renderer::Submit([]()
			{
				auto framebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(s_MeshPipeline->GetSpecification().RenderPass->GetSpecification().TargetFramebuffer);

				VkWriteDescriptorSet writeDescriptorSet = {};
				writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writeDescriptorSet.dstSet = s_QuadDescriptorSet;
				writeDescriptorSet.descriptorCount = 1;
				writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writeDescriptorSet.pImageInfo = &framebuffer->GetVulkanDescriptorInfo();
				writeDescriptorSet.dstBinding = 0;

				auto vulkanDevice = VulkanContext::GetCurrentDevice()->Get();
				vkUpdateDescriptorSets(vulkanDevice, 1, &writeDescriptorSet, 0, nullptr);
			});*/
	}

	void VulkanRenderer::BeginFrame()
	{
		auto context = VulkanContext::Get();
		Renderer::Submit([context]()
			{
				auto swapChain = std::dynamic_pointer_cast<VulkanSwapChain>(context->GetSwapChain());

				VkCommandBufferBeginInfo cmdBufInfo = {};
				cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

				VkCommandBuffer drawCommandBuffer = swapChain->GetVulkanCurrentDrawCommandBuffer();
				VK_CHECK_RESULT(vkBeginCommandBuffer(drawCommandBuffer, &cmdBufInfo));
			});
	}

	void VulkanRenderer::EndFrame()
	{
		Renderer::Submit([]()
			{
			});
	}

	void VulkanRenderer::SubmitQuad()
	{
		struct QuadVertex
		{
			glm::vec3 Position;
			glm::vec4 Color;
		};

		std::vector<QuadVertex> quadData = {
			{{-0.5f, -0.5f, 0.0f }, {1.0f, 0.0f, 0.0f, 1.0f}},
			{{ 0.5f, -0.5f, 0.0f }, {0.0f, 1.0f, 0.0f, 1.0f}},
			{{ 0.5f,  0.5f, 0.0f }, {0.0f, 0.0f, 1.0f, 1.0f}},
			{{-0.5f,  0.5f, 0.0f }, {1.0f, 0.0f, 0.0f, 1.0f}}
		};

		/*QuadVertex data[2] = {
			{{-0.5f, -0.5f, 0.0f }, {1.0f, 0.0f, 0.0f, 1.0f}},
			{{ 0.5f, -0.5f, 0.0f }, {0.0f, 1.0f, 0.0f, 1.0f}},
		};*/


		//s_TestQuadVertexBuffer = VertexBuffer::Create(quadData.data(), quadData.size(), sizeof(QuadVertex));
		std::vector<u32> indices = { 0, 1, 2, 2, 3, 0 };
		//u32 indices[] = { 0, 1 };
		//s_TestQuadIndexBuffer = IndexBuffer::Create(indices.data(), indices.size(), sizeof(u32));
	}

	void VulkanRenderer::BeginRenderPass(const std::shared_ptr<RenderPass>& renderPass)
	{
		auto context = Renderer::GetContext();
		Renderer::Submit([context]()
			{
				auto swapChain = context->GetSwapChain();
				s_RenderData.ActiveCommandBuffer = swapChain->GetCurrnetDrawCommandBuffer();

				s_MeshPipeline->GetSpecification().RenderPass->BeginRenderPass(s_RenderData.ActiveCommandBuffer, false);
			}
		);
	}

	void VulkanRenderer::EndRenderPass()
	{
		/*Renderer::Submit([]()
			{
				// Should draw UI here
				Application::Get().GetImGuiLayer().ImGuiRenderPass(s_RenderData.ActiveCommandBuffer);

				s_MeshPipeline->GetSpecification().RenderPass->EndRenderPass(s_RenderData.ActiveCommandBuffer);
			});*/
	}

	void VulkanRenderer::CompositeRenderPass(VkCommandBufferInheritanceInfo& inheritanceInfo)
	{
		auto context = std::dynamic_pointer_cast<VulkanContext>(Application::Get().GetWindow().GetRendererContext());
		VkCommandBuffer commandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(s_RenderData.ActiveCommandBuffer)->GetVulkanCommandBuffer();

		auto swapChain = std::dynamic_pointer_cast<VulkanSwapChain>(context->GetSwapChain());
		u32 width = swapChain->GetWidth();
		u32 height = swapChain->GetHeight();

		VkCommandBufferBeginInfo cmdBufInfo = {};
		cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cmdBufInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
		cmdBufInfo.pInheritanceInfo = &inheritanceInfo;


		//VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffer, &cmdBufInfo));

		// Update dynamic viewport state
		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = (float)height;
		viewport.height = -(float)height;
		viewport.width = (float)width;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

		// Update dynamic scissor state
		VkRect2D scissor = {};
		scissor.extent.width = width;
		scissor.extent.height = height;
		scissor.offset.x = 0;
		scissor.offset.y = 0;
		vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

		// Copy 3D scene here!
		auto vulkanPipeline = std::dynamic_pointer_cast<VulkanPipeline>(s_CompositePipeline);

		VkPipelineLayout layout = vulkanPipeline->GetVulkanPipelineLayout();

		auto vulkanMeshVB = std::dynamic_pointer_cast<VulkanVertexBuffer>(s_QuadVertexBuffer);
		//VkBuffer vbMeshBuffer = vulkanMeshVB->GetVulkanBuffer();
		VkDeviceSize offsets[1] = { 0 };
		//vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vbMeshBuffer, offsets);

		auto vulkanMeshIB = std::dynamic_pointer_cast<VulkanIndexBuffer>(s_QuadIndexBuffer);
		//VkBuffer ibBuffer = vulkanMeshIB->GetVulkanBuffer();
		//vkCmdBindIndexBuffer(commandBuffer, ibBuffer, 0, VK_INDEX_TYPE_UINT32);

		VkPipeline pipeline = vulkanPipeline->GetVulkanPipeline();
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

		// Bind descriptor sets describing shader binding points
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &s_QuadDescriptorSet, 0, nullptr);

		vkCmdDrawIndexed(commandBuffer, s_QuadIndexBuffer->GetCount(), 1, 0, 0, 0);

		//VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffer));
	}

	void VulkanRenderer::RenderQuad(const std::shared_ptr<Pipeline>& pipeline, glm::mat4 transform)
	{
		Renderer::Submit([pipeline, transform]()
			{
				auto vulkanPipeline = std::dynamic_pointer_cast<VulkanPipeline>(pipeline);
				auto shader = std::dynamic_pointer_cast<VulkanShader>(pipeline->GetSpecification().Shader);
				auto layout = vulkanPipeline->GetVulkanPipelineLayout();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(s_RenderData.ActiveCommandBuffer)->GetVulkanCommandBuffer();

				// pipeline->Bind();
				vkCmdBindPipeline(drawCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vulkanPipeline->GetVulkanPipeline());

				// vertexBuffer->Bind();
				auto vulkanMeshVB = std::dynamic_pointer_cast<VulkanVertexBuffer>(s_TestQuadVertexBuffer);
				//VkBuffer vbMeshBuffer = vulkanMeshVB->GetVulkanBuffer();
				VkDeviceSize offsets[1] = { 0 };

				//vkCmdBindVertexBuffers(drawCommandBuffer, 0, 1, &vbMeshBuffer, offsets);

				// indexBuffer->Bind();
				auto vulkanMeshIB = std::dynamic_pointer_cast<VulkanIndexBuffer>(s_TestQuadIndexBuffer);
				//VkBuffer ibBuffer = vulkanMeshIB->GetVulkanBuffer();
				//vkCmdBindIndexBuffer(drawCommandBuffer, ibBuffer, 0, VK_INDEX_TYPE_UINT32);


				//vkCmdBindDescriptorSets(s_RenderData.ActiveCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &s_DescriptorSet, 0, nullptr);

				{
					// material->Push('name', transform);
					vkCmdPushConstants(drawCommandBuffer, layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4), &transform);
					//shader->SetUniform("Transform", transform);
				}

				/*{
					void* ubPtr = shader->MapUniformBuffer(0);
					glm::mat4 transform = glm::mat4(1.0f);
					transform = glm::translate(transform, glm::vec3(0.3f, -0.3f, 0.0f));
					transform = glm::rotate(transform, 0.3f, glm::vec3(0.0f, 0.0f, 1.0f));
					memcpy(ubPtr, &transform, sizeof(glm::mat4));
					shader->UnmapUniformBuffer(0);
				}*/

				// RendererAPI->SetLineWidth(2.f);
				vkCmdSetLineWidth(drawCommandBuffer, 2.f);
				//vkCmdDraw(s_RenderData.ActiveCommandBuffer, 2, 1, 0, 0);

				//RendererAPI::DrawIndexed(indexBuffer);
				vkCmdDrawIndexed(drawCommandBuffer, s_TestQuadIndexBuffer->GetCount(), 1, 0, 0, 0);
			});
	}

}