#include "debpch.h"
#include "VulkanFramebuffer.h"

#include "DebEngine/Graphics/Renderer.h"
#include "VulkanContext.h"
#include "VulkanCommandBuffer.h"
#include "VulkanRenderPass.h"
#include "VulkanTexture.h"
#include "VulkanUtils.h"

namespace DebEngine
{

	VulkanFramebuffer::VulkanFramebuffer(const FramebufferSpecification& spec)
		: m_Width(spec.Width),
		m_Height(spec.Height)
	{
		m_FramebufferSpecification = spec;
		u32 width = spec.Width;
		u32 height = spec.Height;
		if (spec.Width == 0)
		{
			width = Application::Get().GetWindow().GetWidth() * spec.Scale;
			height = Application::Get().GetWindow().GetHeight() * spec.Scale;
		}

		auto instance = this;
		Renderer::Submit([instance, width, height]()
			{
				instance->Resize(width, height, true);
			});
	}

	VulkanFramebuffer::VulkanFramebuffer(const VulkanFramebufferSpecification& spec)
		: m_VulkanFramebufferSpecification(spec),
		m_Width(spec.Width),
		m_Height(spec.Height)
	{
		if (m_VulkanFramebufferSpecification.Framebuffer)
		{
			m_VulkanFramebufferSpecification.SwapChainTarget = true;
			m_VulkanFramebuffer = m_VulkanFramebufferSpecification.Framebuffer;
		}
		else
		{

			u32 width = spec.Width;
			u32 height = spec.Height;
			if (spec.Width == 0)
			{
				width = Application::Get().GetWindow().GetWidth() * spec.Scale;
				height = Application::Get().GetWindow().GetHeight() * spec.Scale;
			}

			auto instance = this;
			Renderer::Submit([instance, width, height]()
				{
					instance->Resize(width, height, true);
				});
		}
	}

	VulkanFramebuffer::~VulkanFramebuffer()
	{
		// TODO
		/*auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->CleanUp();
			});*/
	}

	void VulkanFramebuffer::Bind() const
	{

	}

	void VulkanFramebuffer::Unbind() const
	{

	}

	void VulkanFramebuffer::SetupViewport(const std::shared_ptr<CommandBuffer>& drawCommandBuffer, b8 flip)
	{

	}

	void VulkanFramebuffer::CleanUp()
	{
		auto logicalDevice = VulkanContext::GetCurrentDevice();
		auto vulkanLogicaldevice = VulkanContext::GetCurrentDevice()->Get();

		vkDestroyImage(vulkanLogicaldevice, m_ColorAttachment.Image, nullptr);
		vkDestroyImageView(vulkanLogicaldevice, m_ColorAttachment.ImageView, nullptr);
		vkDestroyImage(vulkanLogicaldevice, m_DepthAttachment.Image, nullptr);
		vkDestroyImageView(vulkanLogicaldevice, m_DepthAttachment.ImageView, nullptr);
		vkDestroySampler(vulkanLogicaldevice, m_ColorAttachmentSampler, nullptr);
		vkFreeMemory(vulkanLogicaldevice, m_DepthAttachment.DeviceMemory, nullptr);
		vkFreeMemory(vulkanLogicaldevice, m_ColorAttachment.DeviceMemory, nullptr);
		vkDestroyFramebuffer(vulkanLogicaldevice, m_VulkanFramebuffer, nullptr);
	}

	void VulkanFramebuffer::Resize(u32 width, u32 height, b8 forceRecreate /*= false*/)
	{
		m_Width = width;
		m_Height = height;

		if (!m_FramebufferSpecification.SwapChainTarget)
		{
			auto logicalDevice = VulkanContext::GetCurrentDevice();
			auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();

			if (m_VulkanFramebuffer)
			{
				CleanUp();
			}

			DEB_CORE_ASSERT(m_FramebufferSpecification.RenderPass != nullptr, "Render Pass must be specified for a framebuffer!");

			m_RenderPass = m_FramebufferSpecification.RenderPass;

			m_VulkanRenderPass = std::dynamic_pointer_cast<VulkanRenderPass>(m_RenderPass)->GetVulkanRenderPass();

			std::vector<VkImageView> imageViewAttachments;;

			if (m_FramebufferSpecification.RenderPass->GetSpecification().HasColorAttachment)
			{
				if (m_VulkanFramebufferSpecification.ColorAttachment)
					m_ColorAttachment.ImageView = m_VulkanFramebufferSpecification.ColorAttachment;
				else
					CreateColorAttachment();

				imageViewAttachments.push_back(m_ColorAttachment.ImageView);

				// Fill a descriptor for later use in a descriptor set
				m_DescriptorColorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				m_DescriptorColorImageInfo.imageView = m_ColorAttachment.ImageView;
				m_DescriptorColorImageInfo.sampler = m_ColorAttachmentSampler;

				// Create the texture if it doesn't exists
				if (m_FramebufferColorTexture == nullptr)
					m_FramebufferColorTexture = Texture2D::Create(shared_from_this(), TextureType::FramebufferColor);

				m_FramebufferColorTexture->Resize(width, height);
			}


			if (m_FramebufferSpecification.RenderPass->GetSpecification().HasDepthAttachment)
			{
				if (m_VulkanFramebufferSpecification.DepthAttachment)
				{
					m_DepthAttachment.ImageView = m_VulkanFramebufferSpecification.DepthAttachment;
				}
				else
				{
					CreateDepthAttachment();
				}

				imageViewAttachments.push_back(m_DepthAttachment.ImageView);

				// Fill a descriptor for later use in a descriptor set
				m_DescriptorDepthImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				m_DescriptorDepthImageInfo.imageView = m_ColorAttachment.ImageView;

				// Create the texture if it doesn't exists
				if (m_FramebufferDepthTexture == nullptr)
					m_FramebufferDepthTexture = Texture2D::Create(shared_from_this(), TextureType::FramebufferDepth);

				m_FramebufferDepthTexture->Resize(width, height);
			}

			VkFramebufferCreateInfo framebufferCreateInfo{};
			framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferCreateInfo.renderPass = m_VulkanRenderPass;
			framebufferCreateInfo.attachmentCount = imageViewAttachments.size();
			framebufferCreateInfo.pAttachments = imageViewAttachments.data();
			framebufferCreateInfo.width = width;
			framebufferCreateInfo.height = height;
			framebufferCreateInfo.layers = 1;

			VK_CHECK_RESULT(vkCreateFramebuffer(vulkanLogicalDevice, &framebufferCreateInfo, nullptr, &m_VulkanFramebuffer));
		}
		else
		{
			// TODO: Need to be fixed, cant access context before its created - REVIEW
			auto swapChain = std::dynamic_pointer_cast<VulkanSwapChain>(VulkanContext::Get()->GetSwapChain());
			m_VulkanRenderPass = swapChain->GetVulkanRenderPass();
			//m_VulkanFramebuffer = static_cast<VkFramebuffer>(swapChain->GetCurrentFramebufferNative());
		}

		for (auto& resizeCallback : m_ResizeCallbacks)
			resizeCallback(shared_from_this());
	}

	void VulkanFramebuffer::AddResizeCallback(const std::function<void(std::shared_ptr<Framebuffer>)>& resizeCallback)
	{
		m_ResizeCallbacks.push_back(resizeCallback);
	}

	u32 VulkanFramebuffer::GetWidth() const
	{
		return m_Width;
	}

	u32 VulkanFramebuffer::GetHeight() const
	{
		return m_Height;
	}

	const std::shared_ptr<Texture2D> VulkanFramebuffer::GetFramebufferColorTexture() const
	{
		return m_FramebufferColorTexture;
	}

	const std::shared_ptr<DebEngine::Texture2D> VulkanFramebuffer::GetFramebufferDepthTexture() const
	{
		return m_FramebufferDepthTexture;
	}

	void VulkanFramebuffer::InitializeLayout()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto logicalDevice = VulkanContext::GetCurrentDevice();
				auto vulkanCmdBuffer = logicalDevice->CreatePrimaryCommandBuffer(true);
	
				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.image = instance->m_ColorAttachment.Image;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = 0;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_HOST_BIT,
					VK_PIPELINE_STAGE_TRANSFER_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);


				logicalDevice->FlushCommandBuffer(vulkanCmdBuffer);

				instance->m_DescriptorColorImageInfo.imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			});
	}

	void VulkanFramebuffer::ChangeLayoutWrite()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.image = instance->m_ColorAttachment.Image;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = 0;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_HOST_BIT,
					VK_PIPELINE_STAGE_TRANSFER_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanFramebuffer::ChangeLayoutRead()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.image = instance->m_ColorAttachment.Image;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = 0;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_HOST_BIT,
					VK_PIPELINE_STAGE_TRANSFER_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	const VkDescriptorImageInfo& VulkanFramebuffer::GetVulkanDescriptorColorImageInfo() const
	{
		return m_DescriptorColorImageInfo;
	}

	const VkDescriptorImageInfo& VulkanFramebuffer::GetVulkanDescriptorDepthImageInfo() const
	{
		return m_DescriptorDepthImageInfo;
	}

	VkImage& VulkanFramebuffer::GetVulkanColorImage()
	{
		return m_ColorAttachment.Image;
	}

	VkImage& VulkanFramebuffer::GetVulkanDepthImage()
	{
		return m_DepthAttachment.Image;
	}

	std::shared_ptr<RenderPass> VulkanFramebuffer::GetRenderPass() const
	{
		return m_RenderPass;
	}

	VkFramebuffer VulkanFramebuffer::GetVulkanFramebuffer() const
	{
		return m_VulkanFramebuffer;
	}

	void VulkanFramebuffer::SetVulkanFramebuffer(const VkFramebuffer& vulkanFramebuffer)
	{
		m_VulkanFramebuffer = vulkanFramebuffer;
	}

	const VulkanFramebufferSpecification& VulkanFramebuffer::GetVulkanSpecification() const
	{
		return m_VulkanFramebufferSpecification;
	}

	FrameBufferAttachment VulkanFramebuffer::GetVulkanFramebufferColorAttachment() const
	{
		return m_ColorAttachment;
	}

	std::shared_ptr<VulkanFramebuffer> VulkanFramebuffer::Create(const VulkanFramebufferSpecification& vulkanFramebufferSpecification)
	{
		return std::make_shared<VulkanFramebuffer>(vulkanFramebufferSpecification);
	}

	void VulkanFramebuffer::CreateColorAttachment()
	{
		auto& renderPass = std::dynamic_pointer_cast<VulkanRenderPass>(m_RenderPass);
		auto colorFormat = renderPass->GetVulkanColorFormat();
		auto sampleCount = renderPass->GetVulkanSampleCount();

		auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();
		VulkanMemoryAllocator vulkanMemoryAllocator("Framebuffer");

		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = colorFormat;
		imageCreateInfo.extent.width = m_Width;
		imageCreateInfo.extent.height = m_Height;
		imageCreateInfo.extent.depth = 1;
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageCreateInfo.samples = sampleCount;
		imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		// We will sample directly from the color attachment
		imageCreateInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;


		VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCreateInfo, nullptr, &m_ColorAttachment.Image));


		VkMemoryRequirements memReqs;
		vkGetImageMemoryRequirements(vulkanLogicalDevice, m_ColorAttachment.Image, &memReqs);

		// TODO: Review this. Only 'VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT' should be passed - probably only for color picking fb it's different
		vulkanMemoryAllocator.Allocate(memReqs, &m_ColorAttachment.DeviceMemory, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT);

		VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_ColorAttachment.Image, m_ColorAttachment.DeviceMemory, 0));

		VkImageViewCreateInfo colorImageViewCreateInfo{};
		colorImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		colorImageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		colorImageViewCreateInfo.format = colorFormat;
		colorImageViewCreateInfo.subresourceRange = {};
		colorImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		colorImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		colorImageViewCreateInfo.subresourceRange.levelCount = 1;
		colorImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		colorImageViewCreateInfo.subresourceRange.layerCount = 1;
		colorImageViewCreateInfo.image = m_ColorAttachment.Image;
		VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &colorImageViewCreateInfo, nullptr, &m_ColorAttachment.ImageView));

		// Create sampler to sample from the attachment in the fragment shader
		VkSamplerCreateInfo samplerCreateInfo{};
		samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCreateInfo.maxAnisotropy = 1.0f;
		samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
		samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
		samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCreateInfo.addressModeV = samplerCreateInfo.addressModeU;
		samplerCreateInfo.addressModeW = samplerCreateInfo.addressModeU;
		samplerCreateInfo.mipLodBias = 0.0f;
		samplerCreateInfo.maxAnisotropy = 1.0f;
		samplerCreateInfo.minLod = 0.0f;
		samplerCreateInfo.maxLod = 1.0f;
		samplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

		VK_CHECK_RESULT(vkCreateSampler(vulkanLogicalDevice, &samplerCreateInfo, nullptr, &m_ColorAttachmentSampler));
	}

	void VulkanFramebuffer::CreateDepthAttachment()
	{
		auto& renderPass = std::dynamic_pointer_cast<VulkanRenderPass>(m_RenderPass);
		auto depthFormat = renderPass->GetVulkanDepthFormat();
		auto sampleCount = renderPass->GetVulkanSampleCount();

		auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();

		VulkanMemoryAllocator vulkanMemoryAllocator("Framebuffer");
		// DEPTH ATTACHMENT
		{
			VkImageCreateInfo imageCreateInfo{};
			imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
			imageCreateInfo.format = depthFormat;
			imageCreateInfo.extent.width = m_Width;
			imageCreateInfo.extent.height = m_Height;
			imageCreateInfo.extent.depth = 1;
			imageCreateInfo.mipLevels = 1;
			imageCreateInfo.arrayLayers = 1;
			imageCreateInfo.samples = sampleCount;
			imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
			// We will sample directly from the color attachment
			imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

			VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCreateInfo, nullptr, &m_DepthAttachment.Image));
			VkMemoryRequirements memoryRequirements;
			vkGetImageMemoryRequirements(vulkanLogicalDevice, m_DepthAttachment.Image, &memoryRequirements);
			m_Size = memoryRequirements.size;
			vulkanMemoryAllocator.Allocate(memoryRequirements, &m_DepthAttachment.DeviceMemory);

			VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_DepthAttachment.Image, m_DepthAttachment.DeviceMemory, 0));

			VkImageViewCreateInfo depthStencilImageViewCreateInfo{};
			depthStencilImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			depthStencilImageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			depthStencilImageViewCreateInfo.image = m_DepthAttachment.Image;
			depthStencilImageViewCreateInfo.format = depthFormat;
			depthStencilImageViewCreateInfo.flags = 0;
			depthStencilImageViewCreateInfo.subresourceRange = {};
			depthStencilImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
			depthStencilImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			depthStencilImageViewCreateInfo.subresourceRange.levelCount = 1;
			depthStencilImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			depthStencilImageViewCreateInfo.subresourceRange.layerCount = 1;

			VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &depthStencilImageViewCreateInfo, nullptr, &m_DepthAttachment.ImageView));
		}
	}
}