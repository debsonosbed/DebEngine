#include "debpch.h"
#include "VulkanShader.h"

#include "VulkanContext.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanTexture.h"

#define NOMINMAX

#include <shaderc/shaderc.hpp>

#undef max // SPIRV should work without it, what causes that?
#include <spirv_glsl.hpp>

#include <filesystem>

#define SHADER_CACHED_FOLDER_NAME "cached"
#define SHADER_CACHED_VERT_EXT ".cached.vert"
#define SHADER_CACHED_FRAG_EXT ".cached.frag"

namespace DebEngine
{
	static ShaderUniformType SPIRTypeToShaderUniformType(spirv_cross::SPIRType type)
	{
		switch (type.basetype)
		{
		case spirv_cross::SPIRType::Boolean:  return ShaderUniformType::Bool;
		case spirv_cross::SPIRType::Int:      return ShaderUniformType::Int;
		case spirv_cross::SPIRType::Float:
			if (type.vecsize == 1)            return ShaderUniformType::Float;
			if (type.vecsize == 2)            return ShaderUniformType::Vec2;
			if (type.vecsize == 3)            return ShaderUniformType::Vec3;
			if (type.vecsize == 4)            return ShaderUniformType::Vec4;
			if (type.columns == 3)            return ShaderUniformType::Mat3;
			if (type.columns == 4)            return ShaderUniformType::Mat4;
			break;
		}
		//DEB_CORE_ASSERT(false, "Unknown type!");
		return ShaderUniformType::None;
	}

	static std::string ReadShaderFromFile(const std::string& filepath)
	{
		std::string result;
		std::ifstream in(filepath, std::ios::in | std::ios::binary);
		if (in)
		{
			in.seekg(0, std::ios::end);
			result.resize(in.tellg());
			in.seekg(0, std::ios::beg);
			in.read(&result[0], result.size());
		}
		else
		{
			DEB_CORE_ASSERT(false, "Could not load shader!");
		}
		in.close();
		return result;
	}

	static VkShaderStageFlagBits ShaderTypeFromString(const std::string& type)
	{
		if (type == "vertex")                       return VK_SHADER_STAGE_VERTEX_BIT;
		if (type == "fragment" || type == "pixel")  return VK_SHADER_STAGE_FRAGMENT_BIT;
		if (type == "compute")
		{
			DEB_CORE_ASSERT(false);
		}

		return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
	}

	const std::unordered_map<std::string, std::shared_ptr<ShaderBuffer>>& VulkanShader::GetShaderBuffers() const
	{
		return m_ShaderBuffers;
	}

	VulkanShader::VulkanShader(const std::string& path) : m_ShaderPath(path)
	{
		// TODO: This should be more "general"
		size_t found = path.find_last_of("/\\");
		m_Name = found != std::string::npos ? path.substr(found + 1) : path;
		found = m_Name.find_last_of(".");
		m_Name = found != std::string::npos ? m_Name.substr(0, found) : m_Name;

		Reload();
	}

	VulkanShader::~VulkanShader()
	{
		// TODO: investigate - it gives an error
		/*auto instance = this;
		Renderer::Submit([instance]() mutable
			{
				VkDevice device = VulkanContext::GetCurrentDevice()->Get();
				for (const auto& shaderStage : instance->m_ShaderStages)
					vkDestroyShaderModule(device, shaderStage.module, nullptr);
			});*/
	}

	void VulkanShader::Reload()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				std::string source = ReadShaderFromFile(instance->m_ShaderPath);
				instance->m_ShaderSource = instance->ExtractShaderByType(source);
				instance->m_ShaderStages.resize(2);
				std::vector<std::vector<u32>> shaderData(2);
				instance->Compile(shaderData);
				if (shaderData.size() > 1)
				{
					instance->Load(instance->m_ShaderStages[0], ShaderTypeVertex, shaderData[0]);
					instance->Load(instance->m_ShaderStages[1], ShaderTypeFragment, shaderData[1]);
					instance->ExtractShaderLayout(VK_SHADER_STAGE_VERTEX_BIT, shaderData[0]);
					instance->ExtractShaderLayout(VK_SHADER_STAGE_FRAGMENT_BIT, shaderData[1]);
					instance->CreateDescriptors();
					instance->Clean();
				}
			});
	}

	void VulkanShader::Bind()
	{

	}

	const std::vector<VkPipelineShaderStageCreateInfo>& VulkanShader::GetShaderStages() const
	{
		return m_ShaderStages;
	}

	std::shared_ptr<ShaderBuffer> VulkanShader::GetUniformBuffer(const std::string& name)
	{
		DEB_CORE_ASSERT(m_ShaderBuffers.find(name) != m_ShaderBuffers.end());
		return m_ShaderBuffers.at(name);
	}

	const std::vector<VulkanShader::PushConstantRange>& VulkanShader::GetPushConstantRanges() const
	{
		return m_PushContantRanges;
	}

	b8 VulkanShader::CreateDescriptorSet(VkDescriptorSet& descriptorSet)
	{
		if (m_DescriptorPool == VK_NULL_HANDLE)
			return false;

		VkDevice device = VulkanContext::GetCurrentDevice()->Get();

		// Allocate a new descriptor set from the global descriptor pool
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = m_DescriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = &m_DescriptorSetLayout;

		VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet));

		return true;
	}

	VkWriteDescriptorSet& VulkanShader::GetDescriptorSet(const std::string& name)
	{
		DEB_CORE_ASSERT(m_WriteDescriptorSets.find(name) != m_WriteDescriptorSets.end(), "Shader does not contain requested descriptor set");
		return m_WriteDescriptorSets.at(name);
	}

	VkDescriptorSetLayout VulkanShader::GetDescriptorSetLayout() const
	{
		return m_DescriptorSetLayout;
	}

	const std::vector<VkDescriptorSetLayout>& VulkanShader::GetDescriptorSetsLayouts() const
	{
		return m_DescriptorSetsLayouts;
	}

	std::unordered_map<VkShaderStageFlagBits, std::string> VulkanShader::ExtractShaderByType(const std::string& source)
	{
		// All the shaders are placed in one '.glsl' file, and they are separated by
		// '#type {shaderType}' specifcier
		// Basing on that, extract shader under it's type definition and save it
		std::unordered_map<VkShaderStageFlagBits, std::string> shaderSources;

		const char* typeToken = "#type";
		size_t typeTokenLength = strlen(typeToken);
		size_t pos = source.find(typeToken, 0);
		while (pos != std::string::npos)
		{
			size_t endOfLine = source.find_first_of("\r\n", pos);
			DEB_CORE_ASSERT(endOfLine != std::string::npos, "Syntax error");
			size_t begin = pos + typeTokenLength + 1;
			std::string type = source.substr(begin, endOfLine - begin);
			DEB_CORE_ASSERT(type == "vertex" || type == "fragment" || type == "pixel" || type == "compute", "Invalid shader type specified");

			size_t nextLinePos = source.find_first_not_of("\r\n", endOfLine);
			pos = source.find(typeToken, nextLinePos);
			auto shaderType = ShaderTypeFromString(type);
			shaderSources[shaderType] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
		}

		return shaderSources;
	}

	void VulkanShader::Compile(std::vector<std::vector<u32>>& outputBinary)
	{
		std::filesystem::path p = m_ShaderPath;

		auto pathVert = p.parent_path() / SHADER_CACHED_FOLDER_NAME / (p.filename().string() + SHADER_CACHED_VERT_EXT);
		std::string cachedVertFilePath = pathVert.string();
		// Try to read cached Vertex Shader from the file
		{
			FILE* f = fopen(cachedVertFilePath.c_str(), "rb");
			if (f)
			{
				fseek(f, 0, SEEK_END);
				u64 size = ftell(f);
				fseek(f, 0, SEEK_SET);
				outputBinary[0] = std::vector<u32>(size / sizeof(u32));
				fread(outputBinary[0].data(), sizeof(u32), outputBinary[0].size(), f);
				fclose(f);
			}
		}

		auto pathFrag = p.parent_path() / SHADER_CACHED_FOLDER_NAME / (p.filename().string() + SHADER_CACHED_FRAG_EXT);
		std::string cachedFragFilePath = pathFrag.string();
		// Try to read cached Fragment Shader from the file
		{
			FILE* f = fopen(cachedFragFilePath.c_str(), "rb");
			if (f)
			{
				fseek(f, 0, SEEK_END);
				u64 size = ftell(f);
				fseek(f, 0, SEEK_SET);
				outputBinary[1] = std::vector<u32>(size / sizeof(u32));
				fread(outputBinary[1].data(), sizeof(u32), outputBinary[1].size(), f);
				fclose(f);
			}
		}

		// Load and save binaries to the file(cache) for Vertex and Fragment Shaders
		if (outputBinary[0].empty())
		{
			shaderc::Compiler compiler;
			shaderc::CompileOptions options;
			options.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_2);

			//options.SetOptimizationLevel(shaderc_optimization_level_performance);

			// Vertex Shader
			{
				auto& shaderSource = m_ShaderSource.at(VK_SHADER_STAGE_VERTEX_BIT);
				shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(shaderSource, shaderc_vertex_shader, m_ShaderPath.c_str(), options);

				if (module.GetCompilationStatus() != shaderc_compilation_status_success)
				{
					DEB_CORE_ASSERT(false, module.GetErrorMessage());
				}

				const u8* begin = (const u8*)module.cbegin();
				const u8* end = (const u8*)module.cend();

				outputBinary[0] = std::vector<u32>(module.cbegin(), module.cend());

				auto cachedVertFile = std::fstream(cachedVertFilePath, std::ios::out | std::ios::binary);
				cachedVertFile.write((char*)outputBinary[0].data(), outputBinary[0].size() * sizeof(u32));
				cachedVertFile.close();
			}

			// Should save binary to a file, so that it can be reused later
		}

		if (outputBinary[1].empty())
		{
			shaderc::Compiler compiler;
			shaderc::CompileOptions options;
			options.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_2);

			//options.SetOptimizationLevel(shaderc_optimization_level_performance);

			// Fragment Shader
			{
				auto& shaderSource = m_ShaderSource.at(VK_SHADER_STAGE_FRAGMENT_BIT);
				shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(shaderSource, shaderc_fragment_shader, m_ShaderPath.c_str(), options);

				if (module.GetCompilationStatus() != shaderc_compilation_status_success)
				{
					DEB_CORE_ASSERT(false, module.GetErrorMessage());
				}

				const u8* begin = (const u8*)module.cbegin();
				const u8* end = (const u8*)module.cend();

				outputBinary[1] = std::vector<u32>(module.cbegin(), module.cend());

				auto cachedFragFile = std::fstream(cachedFragFilePath, std::ios::out | std::ios::binary);
				cachedFragFile.write((char*)outputBinary[1].data(), outputBinary[1].size() * sizeof(u32));
				cachedFragFile.close();
			}

			// Should save binary to a file, so that it can be reused later
		}
	}

	void VulkanShader::Load(VkPipelineShaderStageCreateInfo& shaderStageCreateInfo, ShaderType shaderType, const std::vector<u32> shaderData)
	{
		VkDevice device = VulkanContext::GetCurrentDevice()->Get();
		DEB_CORE_ASSERT(shaderData.size());
		// Create a new shader module that will be used for pipeline creation
		VkShaderModuleCreateInfo moduleCreateInfo{};
		moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		moduleCreateInfo.codeSize = shaderData.size() * sizeof(u32);
		moduleCreateInfo.pCode = shaderData.data();

		auto& shaderModule = m_ShaderModules.emplace_back();
		VK_CHECK_RESULT(vkCreateShaderModule(device, &moduleCreateInfo, NULL, &shaderModule));

		shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStageCreateInfo.stage = ShaderTypeToShaderStage(shaderType);
		shaderStageCreateInfo.module = shaderModule;
		shaderStageCreateInfo.pName = "main";
	}

	void VulkanShader::ExtractShaderLayout(VkShaderStageFlagBits shaderStage, const std::vector<u32>& shaderData)
	{
		VkDevice device = VulkanContext::GetCurrentDevice()->Get();

		DEB_CORE_TRACE("===========================");
		DEB_CORE_TRACE(" Vulkan Shader Layout");
		DEB_CORE_TRACE(" {0}", m_ShaderPath);
		DEB_CORE_TRACE("===========================");

		spirv_cross::Compiler compiler(shaderData);
		auto resources = compiler.get_shader_resources();


		DEB_CORE_TRACE("Storage Buffers:");
		for (const auto& resource : resources.storage_buffers)
		{
			const auto& bufferName = resource.name;
			auto& bufferType = compiler.get_type(resource.base_type_id);
			s32 memberCount = bufferType.member_types.size();
			u32 bindingPoint = compiler.get_decoration(resource.id, spv::DecorationBinding);
			u32 size = compiler.get_declared_struct_size(bufferType);

			// There can't be the same binding points
			DEB_CORE_ASSERT(m_ShaderBuffers.find(bufferName) == m_ShaderBuffers.end());

			// Save uniforms informations
			ShaderType shaderType = ShaderStageToShaderType(shaderStage);
			auto buffer = ShaderBuffer::Create();
			buffer->Type = ShaderBufferType::StorageBuffers;
			buffer->BindingPoint = bindingPoint;
			buffer->Size = size;
			buffer->Name = bufferName;
			buffer->ShaderType = shaderType;

			m_ShaderBuffers.insert({ bufferName, buffer });

			m_UniformLocations[bufferName] = bindingPoint;

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tMember Count: {0}", memberCount);
			DEB_CORE_TRACE("\tBinding Point: {0}", bindingPoint);
			DEB_CORE_TRACE("\tSize: {0}", size);
			DEB_CORE_TRACE("-------------------");

			DEB_CORE_TRACE("\t\t--- Members");
			for (int i = 0; i < memberCount; i++)
			{
				auto type = compiler.get_type(bufferType.member_types[i]);
				const auto& memberName = compiler.get_member_name(bufferType.self, i);
				u32 size = static_cast<u32>(compiler.get_declared_struct_member_size(bufferType, i));
				u32 offsetMember = static_cast<u32>(compiler.type_struct_member_offset(bufferType, i));

				std::string uniformName = memberName;
				buffer->Uniforms[uniformName] = { uniformName, SPIRTypeToShaderUniformType(type), size, offsetMember, shaderType, buffer };

				DEB_CORE_TRACE("\t\tName: {0}", uniformName);
				DEB_CORE_TRACE("\t\tSize: {0}", size);
				DEB_CORE_TRACE("\t\tOffset: {0}", offsetMember);
			}
		}

		DEB_CORE_TRACE("Uniform Buffers:");

		for (const auto& resource : resources.uniform_buffers)
		{
			const auto& bufferName = resource.name;
			auto& bufferType = compiler.get_type(resource.base_type_id);
			s32 memberCount = bufferType.member_types.size();
			u32 bindingPoint = compiler.get_decoration(resource.id, spv::DecorationBinding);
			u32 size = compiler.get_declared_struct_size(bufferType);

			// There can't be the same binding points
			DEB_CORE_ASSERT(m_ShaderBuffers.find(bufferName) == m_ShaderBuffers.end());

			// Save uniforms informations
			ShaderType shaderType = ShaderStageToShaderType(shaderStage);
			auto buffer = ShaderBuffer::Create();
			buffer->Type = ShaderBufferType::UniformBufferConstant;
			buffer->BindingPoint = bindingPoint;
			buffer->Size = size;
			buffer->Name = bufferName;
			buffer->ShaderType = shaderType;

			m_ShaderBuffers.insert({ bufferName, buffer });

			m_UniformLocations[bufferName] = bindingPoint;

			m_UBOCount++;

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tMember Count: {0}", memberCount);
			DEB_CORE_TRACE("\tBinding Point: {0}", bindingPoint);
			DEB_CORE_TRACE("\tSize: {0}", size);
			DEB_CORE_TRACE("-------------------");

			DEB_CORE_TRACE("\t\t--- Members");
			for (int i = 0; i < memberCount; i++)
			{
				auto type = compiler.get_type(bufferType.member_types[i]);
				const auto& memberName = compiler.get_member_name(bufferType.self, i);
				u32 size = static_cast<u32>(compiler.get_declared_struct_member_size(bufferType, i));
				u32 offsetMember = static_cast<u32>(compiler.type_struct_member_offset(bufferType, i));

				std::string uniformName = memberName;
				buffer->Uniforms[uniformName] = { uniformName, SPIRTypeToShaderUniformType(type), size, offsetMember, shaderType, buffer };

				DEB_CORE_TRACE("\t\tName: {0}", uniformName);
				DEB_CORE_TRACE("\t\tSize: {0}", size);
				DEB_CORE_TRACE("\t\tOffset: {0}", offsetMember);
			}
		}

		DEB_CORE_TRACE("Push Constant Buffers:");
		for (const auto& resource : resources.push_constant_buffers)
		{
			const auto& bufferName = resource.name;
			auto& bufferType = compiler.get_type(resource.base_type_id);
			s32 memberCount = bufferType.member_types.size();
			u32 offset = 0;

			// Determine offset of new push constant
			if (m_PushContantRanges.size())
				offset = m_PushContantRanges.back().Offset + m_PushContantRanges.back().Size;

			auto bufferSize = compiler.get_declared_struct_size(bufferType) - offset;

			auto& pushConstantRange = m_PushContantRanges.emplace_back();
			pushConstantRange.Size = bufferSize;
			pushConstantRange.Offset = offset;
			pushConstantRange.ShaderStage = shaderStage;

			// Skip empty push constant buffers, since these are for the renderer only
			if (bufferName.empty())
				continue;

			// Save buffer informations
			ShaderType shaderType = ShaderStageToShaderType(shaderStage);
			auto buffer = ShaderBuffer::Create();
			buffer->Type = ShaderBufferType::PushConstant;
			buffer->Name = bufferName;
			buffer->Size = bufferSize;
			buffer->Offset = offset;
			buffer->ShaderType = shaderType;

			m_ShaderBuffers.insert({ bufferName, buffer });

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tMember Count: {0}", memberCount);
			DEB_CORE_TRACE("\tSize: {0}", bufferSize);
			DEB_CORE_TRACE("\tOffset: {0}", offset);

			DEB_CORE_TRACE("\t--- Members");
			for (int i = 0; i < memberCount; i++)
			{
				auto type = compiler.get_type(bufferType.member_types[i]);
				const auto& memberName = compiler.get_member_name(bufferType.self, i);
				u32 size = static_cast<u32>(compiler.get_declared_struct_member_size(bufferType, i));
				u32 offsetMember = static_cast<u32>(compiler.type_struct_member_offset(bufferType, i));

				std::string uniformName = memberName;
				buffer->Uniforms[uniformName] = { uniformName, SPIRTypeToShaderUniformType(type), size, offsetMember, shaderType, buffer };

				DEB_CORE_TRACE("\t\tName: {0}", uniformName);
				DEB_CORE_TRACE("\t\tSize: {0}", size);
				DEB_CORE_TRACE("\t\tOffset: {0}", offsetMember);
			}
		}

		DEB_CORE_TRACE("Samplers:");
		for (const auto& resource : resources.separate_samplers)
		{
			const auto& bufferName = resource.name;
			auto& type = compiler.get_type(resource.base_type_id);
			auto bindingPoint = compiler.get_decoration(resource.id, spv::DecorationBinding);
			u32 dimension = type.image.dim;

			auto buffer = ShaderBuffer::Create();
			buffer->Type = ShaderBufferType::Sampler;
			buffer->BindingPoint = bindingPoint;
			buffer->Name = bufferName;
			buffer->ShaderType = ShaderStageToShaderType(shaderStage);

			m_ShaderBuffers.insert({ bufferName, buffer });

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tBinding Point: {0}", bindingPoint);
		}

		DEB_CORE_TRACE("Separate images:");
		for (const auto& resource : resources.separate_images)
		{
			const auto& bufferName = resource.name;
			auto& type = compiler.get_type(resource.base_type_id);
			auto bindingPoint = compiler.get_decoration(resource.id, spv::DecorationBinding);
			u32 dimension = type.image.dim;

			u32 size = type.array[0];

			auto buffer = ShaderBuffer::Create();
			switch (dimension)
			{
			case spv::Dim::Dim2D:
				buffer->Type = ShaderBufferType::ImageSampler;
				break;

			case spv::Dim::DimCube:
				buffer->Type = ShaderBufferType::ImageSamplerCube;
				break;
			}
			buffer->BindingPoint = bindingPoint;
			buffer->Name = bufferName;
			buffer->ShaderType = ShaderStageToShaderType(shaderStage);

			m_ShaderBuffers.insert({ bufferName, buffer });

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tBinding Point: {0}", bindingPoint);
		}

		DEB_CORE_TRACE("Sampled Images:");
		for (const auto& resource : resources.sampled_images)
		{
			const auto& bufferName = resource.name;
			auto& type = compiler.get_type(resource.base_type_id);
			auto bindingPoint = compiler.get_decoration(resource.id, spv::DecorationBinding);
			u32 dimension = type.image.dim;

			auto buffer = ShaderBuffer::Create();
			switch (dimension)
			{
			case spv::Dim::Dim2D:
				buffer->Type = ShaderBufferType::CombinedImageSampler;
				break;

			case spv::Dim::DimCube:
				buffer->Type = ShaderBufferType::CombinedImageSamplerCube;
				break;
			}

			buffer->BindingPoint = bindingPoint;
			buffer->Name = bufferName;
			buffer->ShaderType = ShaderStageToShaderType(shaderStage);

			m_ShaderBuffers.insert({ bufferName, buffer });

			m_SamplersCount++;

			DEB_CORE_TRACE("\tName: {0}", bufferName);
			DEB_CORE_TRACE("\tBinding Point: {0}", bindingPoint);
		}

		DEB_CORE_TRACE("===========================");
	}

	void VulkanShader::CreateDescriptors()
	{
		VkDevice device = VulkanContext::GetCurrentDevice()->Get();

		const u32 MAX_OBJECTS_PER_SHADER = 100000;

		// Descriptor Pool
		// We need to tell the API the number of max. requested descriptors per type

		std::vector<VkDescriptorPoolSize> poolSizes;
		if (m_UBOCount > 0)
		{
			VkDescriptorPoolSize& poolSize = poolSizes.emplace_back();
			poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			poolSize.descriptorCount = MAX_OBJECTS_PER_SHADER * m_UBOCount;
		}
		if (m_SamplersCount > 0)
		{
			VkDescriptorPoolSize& poolSize = poolSizes.emplace_back();
			poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			poolSize.descriptorCount = MAX_OBJECTS_PER_SHADER * m_SamplersCount;
		}

		// TODO: global descriptor pool?
		if (poolSizes.size() > 0)
		{
			VkDescriptorPoolCreateInfo descriptorPoolInfo = {};
			descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			descriptorPoolInfo.poolSizeCount = poolSizes.size();
			descriptorPoolInfo.pPoolSizes = poolSizes.data();
			descriptorPoolInfo.maxSets = MAX_OBJECTS_PER_SHADER;

			VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolInfo, nullptr, &m_DescriptorPool));
		}

		// Descriptor Set Layout
		std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
		for (auto& [name, shaderBuffer] : m_ShaderBuffers)
		{
			// Ignore push constants buffers
			if (shaderBuffer->Type == ShaderBufferType::PushConstant)
				continue;

			auto& layoutBinding = layoutBindings.emplace_back();
			layoutBinding.descriptorCount = 1;
			layoutBinding.stageFlags = ShaderTypeToShaderStage(shaderBuffer->ShaderType);
			layoutBinding.pImmutableSamplers = nullptr;
			layoutBinding.binding = shaderBuffer->BindingPoint;
			switch (shaderBuffer->Type)
			{
			case ShaderBufferType::StorageBuffers:
			{
				layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
				break;
			}
			case ShaderBufferType::UniformBufferConstant:
			{
				layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				break;
			}
			case ShaderBufferType::Sampler:
			{
				layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
				break;
			}
			case ShaderBufferType::CombinedImageSampler:
			case ShaderBufferType::CombinedImageSamplerCube:
			{
				layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				break;
			}
			}

			VkWriteDescriptorSet& set = m_WriteDescriptorSets[shaderBuffer->Name];
			set = {};
			set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			set.descriptorType = layoutBinding.descriptorType;
			set.descriptorCount = 1;
			set.dstBinding = layoutBinding.binding;
		}

		VkDescriptorSetLayoutCreateInfo descriptorLayout = {};
		descriptorLayout.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		descriptorLayout.pNext = nullptr;
		descriptorLayout.bindingCount = layoutBindings.size();
		descriptorLayout.pBindings = layoutBindings.data();

		VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorLayout, nullptr, &m_DescriptorSetLayout));
	}

	void VulkanShader::Clean()
	{
		m_ShaderSource.clear();
		/*VkDevice device = VulkanContext::GetCurrentDevice()->Get();
		for (auto& shaderModule : m_ShaderModules)
			vkDestroyShaderModule(device, shaderModule, nullptr);*/
	}

	////////////////////////////////////////////////////////////////////////////////
	// Static methods //////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	VkShaderStageFlagBits VulkanShader::ShaderTypeToShaderStage(ShaderType shaderType)
	{
		switch (shaderType)
		{
		case ShaderTypeVertex:
			return VK_SHADER_STAGE_VERTEX_BIT;
		case ShaderTypeFragment:
			return VK_SHADER_STAGE_FRAGMENT_BIT;
		}

		DEB_CORE_ASSERT(false);

		return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
	}

	ShaderType VulkanShader::ShaderStageToShaderType(VkShaderStageFlagBits shaderStage)
	{
		switch (shaderStage)
		{
		case VK_SHADER_STAGE_VERTEX_BIT:
			return ShaderTypeVertex;
		case VK_SHADER_STAGE_FRAGMENT_BIT:
			return ShaderTypeFragment;
		}

		DEB_CORE_ASSERT(false);

		return ShaderTypeNone;
	}
}