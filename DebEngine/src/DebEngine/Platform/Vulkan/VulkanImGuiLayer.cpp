#include "debpch.h"
#include "VulkanImGuiLayer.h"

#include "imgui.h"
#include "DebEngine/ImGui/ImGuizmo.h"

#include "backends/imgui_impl_sdl.h"
#include "backends/imgui_impl_vulkan.h"

#include <SDL.h>

#include "DebEngine/Graphics/Renderer.h"
#include "DebEngine/Core/Profiler.h"

#include "VulkanContext.h"
#include "VulkanCommandBuffer.h"

namespace DebEngine
{

	static void VkCheckResult(VkResult err)
	{
		VK_CHECK_RESULT(err);
	}

	VulkanImGuiLayer::VulkanImGuiLayer()
	{
		m_CommandBuffer = Renderer::CreateSecondaryCommandBuffer();
	}

	VulkanImGuiLayer::VulkanImGuiLayer(const std::string& name)
	{

	}

	void VulkanImGuiLayer::Begin()
	{
		ImGui_ImplVulkan_NewFrame();
		Application& app = Application::Get();
		ImGui_ImplSDL2_NewFrame(static_cast<SDL_Window*>(app.GetWindow().GetNativeWindow()));
		ImGui::NewFrame();
		ImGuizmo::BeginFrame();
		//ImGui::ShowDemoWindow();
	}

	void VulkanImGuiLayer::End()
	{
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		Application& app = Application::Get();
		io.DisplaySize = ImVec2(app.GetWindow().GetWidth(), app.GetWindow().GetHeight());

		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
		}
	}

	void VulkanImGuiLayer::OnAttach()
	{
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		auto& io = ImGui::GetIO(); (void)io;
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
		//io.ConfigViewportsNoAutoMerge = true;
		//io.ConfigViewportsNoTaskBarIcon = true;

		ImFont* pFont = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
		io.FontDefault = io.Fonts->Fonts.back();

		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		//ImGui::StyleColorsClassic();

		// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
		ImGuiStyle& style = ImGui::GetStyle();
		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			style.WindowRounding = 0.0f;
			style.Colors[ImGuiCol_WindowBg].w = 1.0f;
		}
		style.Colors[ImGuiCol_WindowBg] = ImVec4(0.15f, 0.15f, 0.15f, style.Colors[ImGuiCol_WindowBg].w);

		auto instance = this;
		Renderer::Submit([instance]()
			{
				Application& app = Application::Get();
				SDL_Window* window = static_cast<SDL_Window*>(app.GetWindow().GetNativeWindow());

				auto vulkanContext = VulkanContext::Get();
				auto device = VulkanContext::GetCurrentDevice()->Get();

				// Create Descriptor Pool
				VkDescriptorPoolSize pool_sizes[] =
				{
					{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
					{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
					{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
					{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
					{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
					{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
					{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
					{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
					{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
					{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
					{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
				};
				VkDescriptorPoolCreateInfo pool_info = {};
				pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
				pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
				pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
				pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
				pool_info.pPoolSizes = pool_sizes;
				auto err = vkCreateDescriptorPool(device, &pool_info, nullptr, &instance->m_DescriptorPool);
				VkCheckResult(err);

				// Setup Platform/Renderer bindings
				ImGui_ImplSDL2_InitForVulkan(window);
				ImGui_ImplVulkan_InitInfo initInfo = {};
				initInfo.Instance = VulkanContext::GetInstance();
				initInfo.PhysicalDevice = VulkanContext::GetCurrentDevice()->GetPhysicalDevice()->Get();
				initInfo.Device = device;
				initInfo.QueueFamily = VulkanContext::GetCurrentDevice()->GetPhysicalDevice()->GetQueueFamilyIndices().Graphics;
				initInfo.Queue = VulkanContext::GetCurrentDevice()->GetQueue();
				initInfo.PipelineCache = nullptr;
				initInfo.DescriptorPool = instance->m_DescriptorPool;
				initInfo.Allocator = nullptr;
				initInfo.MinImageCount = 2;
				initInfo.ImageCount = vulkanContext->GetSwapChain()->GetImageCount();
				//initInfo.MSAASamples = VK_SAMPLE_COUNT_8_BIT;
				initInfo.CheckVkResultFn = VkCheckResult;

				auto& vulkanSwapchain = std::dynamic_pointer_cast<VulkanSwapChain>(vulkanContext->GetSwapChain());

				ImGui_ImplVulkan_Init(&initInfo, vulkanSwapchain->GetVulkanRenderPass());

				// Load Fonts
				// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
				// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
				// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
				// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
				// - Read 'docs/FONTS.md' for more instructions and details.
				// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
				//io.Fonts->AddFontDefault();
				//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
				//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
				//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
				//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
				//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
				//IM_ASSERT(font != NULL);

				ImGuiIO& io = ImGui::GetIO();
				AddFont("Roboto", io.Fonts->AddFontFromFileTTF("assets/fonts/roboto.ttf", 14.0f));
				AddFont("Roboto-Bold", io.Fonts->AddFontFromFileTTF("assets/fonts/roboto_bold.ttf", 14.0f));

				// Upload Fonts
				{
					// Use any command queue

					VkCommandBuffer commandBuffer = vulkanContext->GetCurrentDevice()->CreatePrimaryCommandBuffer(true);
					ImGui_ImplVulkan_CreateFontsTexture(commandBuffer);
					vulkanContext->GetCurrentDevice()->FlushCommandBuffer(commandBuffer);

					err = vkDeviceWaitIdle(device);
					VkCheckResult(err);
					ImGui_ImplVulkan_DestroyFontUploadObjects();
				}
			});
	}

	void VulkanImGuiLayer::OnUpdate()
	{

	}

	void VulkanImGuiLayer::OnEvent(Event& event)
	{

	}

	void VulkanImGuiLayer::OnDetach()
	{
		Renderer::Submit([=]()
			{
				auto device = VulkanContext::GetCurrentDevice()->Get();

				auto err = vkDeviceWaitIdle(device);
				VkCheckResult(err);
				ImGui_ImplVulkan_Shutdown();
				ImGui_ImplSDL2_Shutdown();
				ImGui::DestroyContext();
				vkDestroyDescriptorPool(device, m_DescriptorPool, nullptr);
			});
	}

	void VulkanImGuiLayer::OnImGuiRender()
	{

	}


	void VulkanImGuiLayer::ImGuiRenderPass()
	{
		auto& renderPassSwapchain = Renderer::GetRenderPassSwapChain();
		m_CommandBuffer->SetTargetRenderPass(renderPassSwapchain);

		Renderer::BeginRecording(m_CommandBuffer);

		auto context = Renderer::GetContext();
		auto swapChain = context->GetSwapChain();
		u32 width = swapChain->GetWidth();
		u32 height = swapChain->GetHeight();
		Renderer::GetAPI().SetViewport(0, 0, width, height, true);

		Renderer::Submit([=]()
			{
				//Profiler::StartMeasurement("ImGuiRenderPass");
				auto& vulkanCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(m_CommandBuffer)->GetVulkanCommandBuffer();
				ImGui::Render();
				ImDrawData* main_draw_data = ImGui::GetDrawData();
				ImGui_ImplVulkan_RenderDrawData(main_draw_data, vulkanCommandBuffer);

				//Profiler::EndMeasurement();
			});

		Renderer::EndRecording(m_CommandBuffer);
	}
}