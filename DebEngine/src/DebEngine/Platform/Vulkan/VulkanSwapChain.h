#pragma once

#include "DebEngine/Core/Core.h"

#include "DebEngine/Graphics/SwapChain.h"
#include "DebEngine/Graphics/RenderPass.h"

#include "Vulkan.h"
#include "VulkanDevice.h"
#include "VulkanMemoryAllocator.h"

struct SDL_Window;

namespace DebEngine
{
	class VulkanSwapChain : public SwapChain
	{
		friend class VulkanContext;
	public:
		VulkanSwapChain() = default;
		VulkanSwapChain(VkInstance instance, const std::shared_ptr<VulkanDevice>& logicalDevice);

		virtual void InitSurface(SDL_Window* windowHandle);
		virtual void Create(u32* width, u32* height, b8 vsync = false);

		virtual void BeginFrame();
		virtual void Present();

		virtual void OnResize(u32 width, u32 height);

		virtual u32 GetImageCount() const override;
		virtual u32 GetWidth() const override;
		virtual u32 GetHeight() const override;
		virtual void* GetSurfaceNative() const override;
		virtual std::shared_ptr<RenderPass>& GetRenderPass() override;
		VkRenderPass GetVulkanRenderPass() const;
		VkFramebuffer GetVulkanFramebuffer(u32 index) const;
		VkCommandBuffer GetVulkanDrawCommandBuffer(u32 index) const;
		VkFramebuffer GetVulkanCurrentFramebuffer() const;
		VkCommandBuffer GetVulkanCurrentDrawCommandBuffer() const;
		VkSwapchainKHR GetVulkanSwapChain() const;
		virtual std::shared_ptr<CommandBuffer>& GetCurrnetDrawCommandBuffer() override;
		virtual std::shared_ptr<Framebuffer> GetFramebuffer(u32 index) const override;
		virtual std::shared_ptr<Framebuffer> GetCurrentFramebuffer() const override;

		virtual void CleanUp();
	private:
		VkResult AcquireNextImage(VkSemaphore presentCompleteSemaphore, u32* imageIndex);
		VkResult QueuePresent(VkQueue queue, u32 imageIndex, VkSemaphore waitSemaphore = VK_NULL_HANDLE);

		void CreateDrawCommandBuffers();
		void CreateDepthStencil();
		void CreateFramebuffers();
		std::pair<VkFormat, VkColorSpaceKHR> FindImageFormatAndColorSpace();
	private:
		u32 m_ImageCount = 0;
		u32 m_QueueNodeIndex = UINT32_MAX;
		u32 m_Width = 0, m_Height = 0;
		u32 m_CurrentBufferIndex = 0;

		VkInstance m_Instance;
		VkSwapchainKHR m_VulkanSwapChain = VK_NULL_HANDLE;
		VkSurfaceKHR m_Surface;
		VkSubmitInfo m_SubmitInfo;
		VkRenderPass m_VulkanRenderPass;
		std::shared_ptr<RenderPass> m_RenderPass;
		VkCommandPool m_CommandPool;
		VkColorSpaceKHR m_ColorSpace;

		VkFormat m_VulkanColorFormat;
		VkFormat m_VulkanDepthFormat;

		std::shared_ptr<CommandBuffer> m_CurrentCommandBuffer;

		std::shared_ptr<VulkanDevice> m_LogicalDevice;
		std::shared_ptr<VulkanPhysicalDevice> m_PhysicalDevice;
		std::unique_ptr<VulkanMemoryAllocator> m_MemoryAllocator;

		std::vector<VkImage> m_Images;
		std::vector<VkFence> m_WaitFences;
		std::vector<VkFramebuffer> m_VulkanFrameBuffers;
		std::vector<std::shared_ptr<Framebuffer>> m_FrameBuffers;
		std::vector<VkCommandBuffer> m_VulkanDrawCommandBuffers;
		std::vector<std::shared_ptr<CommandBuffer>> m_DrawCommandBuffers;


		struct SwapChainBuffer
		{
			VkImage Image;
			VkImageView ImageView;
		};
		std::vector<SwapChainBuffer> m_Buffers;

		struct DepthStencil
		{
			VkImage Image;
			VkDeviceMemory DeviceMemory;
			VkImageView ImageView;
		} m_DepthStencil;

		struct Semaphores
		{
			// Swap chain
			VkSemaphore PresentComplete;
			// Command buffer
			VkSemaphore RenderComplete;
		} m_Semaphores;
	};
}
