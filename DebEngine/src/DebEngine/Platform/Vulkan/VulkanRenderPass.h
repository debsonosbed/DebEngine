#pragma once

#include "Vulkan.h"

#include "DebEngine/Graphics/RenderPass.h"

namespace DebEngine
{
	struct VulkanRenderPassSpecification : public RenderPassSpecification
	{
		VkDevice VulkanLogicalDevice = VK_NULL_HANDLE;
	};

	class VulkanRenderPass : public RenderPass
	{
	public:
		VulkanRenderPass(const RenderPassSpecification& specification);
		VulkanRenderPass(const VulkanRenderPassSpecification& vulkanSpecification);
		virtual ~VulkanRenderPass();

		virtual void BeginRenderPass(const std::shared_ptr<CommandBuffer>& cmdBuffer, b8 isSubpass = false) override;
		virtual void EndRenderPass(const std::shared_ptr<CommandBuffer>& cmdBuffer) override;

		virtual const RenderPassSpecification& GetSpecification() const override { return m_Specification; };
		const VkRenderPass& GetVulkanRenderPass() const;
		virtual const u32 GetColorFormat() const override;
		const VkFormat GetVulkanColorFormat() const;
		const VkFormat GetVulkanDepthFormat() const;
		const VkSampleCountFlagBits GetVulkanSampleCount() const;

		static std::shared_ptr<VulkanRenderPass> Create(const VulkanRenderPassSpecification& vulkanSpecification);
	private:
		void AssembleRenderQueue();
		void Assemble();
	private:
		RenderPassSpecification m_Specification;
		VulkanRenderPassSpecification m_VulkanSpecification;
		VkRenderPass m_VulkanRenderPass;
		VkFormat m_VulkanColorFormat;
		VkFormat m_VulkanDepthFormat;
		VkSampleCountFlagBits m_VulkanSampleCount;
		VkImageLayout m_VulkanColorImageFinalLayout;
	};
}

