#pragma once

#include "Vulkan.h"
#include "VulkanDevice.h"

#include <memory>

namespace DebEngine
{
	class VulkanMemoryAllocator
	{
	public:
		VulkanMemoryAllocator() = default;
		VulkanMemoryAllocator(const std::shared_ptr<VulkanDevice>& logicalDevice, const std::string& tag);
		VulkanMemoryAllocator(const std::string& tag);
		~VulkanMemoryAllocator();

		void Allocate(VkMemoryRequirements requirements, VkDeviceMemory* dest, VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	private:
		std::shared_ptr<VulkanDevice> m_LogicalDevice;
		std::string m_Tag;
	};
}
