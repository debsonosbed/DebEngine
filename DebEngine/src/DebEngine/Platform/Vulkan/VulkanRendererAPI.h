#pragma once

#include "DebEngine/Graphics/RendererAPI.h"

namespace DebEngine
{
	class VulkanRendererAPI : public RendererAPI
	{
	public:
		virtual void Init() override;
		virtual void Clear() override;
		virtual void SetViewport(u32 x, u32 y, u32 width, u32 height, b8 flipViewport = true) override;
		virtual void Draw(u32 vertexCount, u32 instancesCount = 1, u32 firstVertex = 0, u32 firstInstance = 0) override;
		virtual void DrawIndexed(u32 indexCount, u32 instancesCount = 0, u32 baseIndex = 0, u32 baseVertex = 0) override;
		virtual void SetLineThickness(f32 thickness) override;
		virtual void ExecuteCommands(const std::vector<std::shared_ptr<CommandBuffer>>& commandBuffers) override;
	};
}