#pragma once

#include "DebEngine/Core/Buffer.h"

#include "DebEngine/Graphics/Texture.h"

#include "Vulkan.h"
#include "VulkanFramebuffer.h"

namespace DebEngine
{
	class VulkanTexture2D : public Texture2D
	{
	public:
		VulkanTexture2D(const std::string& path, TextureType textureType);
		VulkanTexture2D(const u32 width, const u32 height, ColorFormat colorFormat = ColorFormat::BGRA8);
		VulkanTexture2D(const std::shared_ptr<Framebuffer>& framebuffer, TextureType textureType);
		~VulkanTexture2D();

		void Bind() override;

		virtual u32 GetWidth() const override;
		virtual u32 GetHeight() const override;
		virtual ImTextureID GetImGuiTexture() override;
		virtual b8 IsLoaded() const override;
		virtual u32 GetMipLevelCount() const override;
		virtual Buffer& GetImageBuffer(u32 x = 0, u32 y = 0, u32 width = 0, u32 height = 0);
		VkDescriptorImageInfo& GetVulkanDescriptorInfo();
		virtual glm::uvec4 GetPixelValue(u32 x, u32 y) override;
		virtual b8 Compare(const std::shared_ptr<Texture> texture) const override;
		virtual void Destroy() override;

		virtual void Resize(u32 width, u32 height) override;
		virtual void ChangeLayoutWrite() override; // TEmp
		virtual void ChangeLayoutRead() override; // TEmp
		virtual void CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel) override;

	private:
		void Load();
		void LoadEmpty();
		void CreateImGuiTexture();

	private:
		u32 m_MipMapLevel = 0;
		u32 m_Width;
		u32 m_Height;
		u32 m_Channels;
		std::string m_Path;
		ImTextureID m_ImGuiTextureId = nullptr;
		b8 m_LoadedFromFile = false;
		ColorFormat m_ColorFormat = ColorFormat::BGRA8;

		b8 m_IsLoaded = false;

		Buffer m_ImageBuffer;

		VkDeviceMemory m_VulkanDeviceMemory;
		VkImage m_VulkanImage;

		VkDescriptorImageInfo m_VulkanDescriptorImageInfo{};
	};

	class VulkanTextureCube : public TextureCube
	{
	public:
		VulkanTextureCube(u32 width, u32 height);
		VulkanTextureCube(const std::string& path);
		VulkanTextureCube(const std::vector<std::string>& facesPaths);
		~VulkanTextureCube();


		void Bind() override;
		u32 GetWidth() const override;
		u32 GetHeight() const override;
		u32 GetMipLevelCount() const override;
		Buffer& GetImageBuffer(u32 x = 0, u32 y = 0, u32 width = 0, u32 height = 0) override;
		VkDescriptorImageInfo& GetVulkanDescriptorInfo();
		VkImage& GetVulkanImage();
		bool Serialize(YAML::Emitter& outStream) override;
		void Resize(u32 width, u32 height) override;
		virtual b8 Compare(const std::shared_ptr<Texture> texture) const override;
		virtual void Destroy() override;

		virtual void ChangeLayoutWrite() override; // TEmp
		virtual void ChangeLayoutRead() override; // TEmp
		virtual void CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel) override;

	private:
		void Load();
		void LoadEmpty();
	private:
		u32 m_MipMapLevel = 0;
		u32 m_FaceWidth = 0;
		u32 m_FaceHeight = 0;

		u32 m_Channels;
		std::string m_Path;
		b8 m_IsLoaded = false;

		std::vector<u8*> m_FacesData = std::vector<u8*>(6);

		Buffer m_ImageBuffer;

		VkDeviceMemory m_VulkanDeviceMemory;
		VkImage m_VulkanImage;
		VkDescriptorImageInfo m_VulkanDescriptorImageInfo{};
	};
}
