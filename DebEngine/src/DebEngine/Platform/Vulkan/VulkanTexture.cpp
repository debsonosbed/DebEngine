#include "debpch.h"
#include "VulkanTexture.h"

#include "DebEngine/Core/Utils.h"
#include "DebEngine/Graphics/Renderer.h"

#include "VulkanContext.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanFramebuffer.h"
#include "VulkanCommandBuffer.h"
#include "VulkanUtils.h"

#include "stb_image.h"

#include "backends/imgui_impl_vulkan.h"

namespace DebEngine
{
	VulkanTexture2D::VulkanTexture2D(const std::string& path, TextureType textureType)
		: m_Path(path)
	{
		s32 width, height, channels;
		stbi_set_flip_vertically_on_load(1);
		m_ImageBuffer.Data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);
		m_ImageBuffer.Size = width * height * STBI_rgb_alpha;
		DEB_CORE_ASSERT(m_ImageBuffer.Data, "Failed to load image!");
		m_Width = width;
		m_Height = height;
		m_Channels = channels;

		m_IsLoaded = true;

		m_TextureType = textureType;

		// As for now only support RGBA images
		//DEB_CORE_ASSERT(channels == 4);

		m_LoadedFromFile = true;

		// Create Vulkan texture on renderer thread
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->Load();

				stbi_image_free(instance->m_ImageBuffer.Data);
			});
	}

	VulkanTexture2D::VulkanTexture2D(const u32 width, const u32 height, ColorFormat colorFormat)
		: m_Width(width), m_Height(height), m_ColorFormat(colorFormat)
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->LoadEmpty();
			});
	}

	VulkanTexture2D::VulkanTexture2D(const std::shared_ptr<Framebuffer>& framebuffer, TextureType textureType)
	{
		m_Framebuffer = framebuffer;
		m_TextureType = textureType;

		Resize(framebuffer->GetSpecification().Width, framebuffer->GetSpecification().Height);
	}

	VulkanTexture2D::~VulkanTexture2D()
	{
		//Destroy();
	}

	void VulkanTexture2D::Bind()
	{

	}

	u32 VulkanTexture2D::GetWidth() const
	{
		return m_Height;
	}

	u32 VulkanTexture2D::GetHeight() const
	{
		return m_Width;
	}

	ImTextureID VulkanTexture2D::GetImGuiTexture()
	{
		if (m_ImGuiTextureId == nullptr)
			CreateImGuiTexture();

		// TODO: Update only if needed

		return ImGui_ImplVulkan_UpdateTexture(
			(VkDescriptorSet)m_ImGuiTextureId,
			m_VulkanDescriptorImageInfo.sampler,
			m_VulkanDescriptorImageInfo.imageView,
			m_VulkanDescriptorImageInfo.imageLayout);
	}

	u32 VulkanTexture2D::GetMipLevelCount() const
	{
		return m_MipMapLevel;
	}

	Buffer& VulkanTexture2D::GetImageBuffer(u32 x, u32 y, u32 width, u32 height)
	{
		u32 imageWidth = width > 0 ? width : m_Width;
		u32 imageHeight = height > 0 ? height : m_Height;

		if (!m_LoadedFromFile && m_Framebuffer)
		{
			auto& vulkanFramebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(m_Framebuffer);

			auto srcImage = vulkanFramebuffer->GetVulkanColorImage();

			auto logicalDevice = VulkanContext::GetCurrentDevice();
			auto vulkanLogicalDevice = logicalDevice->Get();
			VulkanMemoryAllocator vulkanMemoryAllocator("Texture screenshot");

			const VkFormat colorFormat = static_cast<VkFormat>(vulkanFramebuffer->GetRenderPass()->GetColorFormat());

			VkImageCreateInfo imageCreateInfo{};
			imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
			// Note that vkCmdBlitImage (if supported) will also do format conversions if the swapchain color format would differ
			imageCreateInfo.format = colorFormat;
			imageCreateInfo.extent.width = imageWidth;
			imageCreateInfo.extent.height = imageHeight;
			imageCreateInfo.extent.depth = 1;
			imageCreateInfo.arrayLayers = 1;
			imageCreateInfo.mipLevels = 1;
			imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
			imageCreateInfo.tiling = VK_IMAGE_TILING_LINEAR;
			imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;

			// Create the image
			VkImage dstImage;

			VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCreateInfo, nullptr, &dstImage));

			VkDeviceMemory dstImageMemory;
			VkMemoryRequirements memReqs;
			vkGetImageMemoryRequirements(vulkanLogicalDevice, srcImage, &memReqs);

			u32 imageSize = memReqs.size;

			vulkanMemoryAllocator.Allocate(memReqs, &dstImageMemory, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT);

			VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, dstImage, dstImageMemory, 0));

			VkCommandBuffer copyCmd = logicalDevice->CreatePrimaryCommandBuffer(true);

			VkImageMemoryBarrier imageMemoryBarrier{};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.srcAccessMask = 0;
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			imageMemoryBarrier.image = dstImage;
			imageMemoryBarrier.subresourceRange = VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

			vkCmdPipelineBarrier(
				copyCmd,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &imageMemoryBarrier);


			imageMemoryBarrier = {};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			imageMemoryBarrier.image = srcImage;
			imageMemoryBarrier.subresourceRange = VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

			vkCmdPipelineBarrier(
				copyCmd,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &imageMemoryBarrier);


			VkOffset3D blitStartSrc = {};
			blitStartSrc.x = x;
			blitStartSrc.y = y;
			blitStartSrc.z = 0;

			VkOffset3D blitEndSrc = {};
			blitEndSrc.x = x + imageWidth;
			blitEndSrc.y = y + imageHeight;
			blitEndSrc.z = 1;

			VkOffset3D blitStartDst = {};
			blitStartDst.x = 0;
			blitStartDst.y = 0;
			blitStartDst.z = 0;

			VkOffset3D blitEndDst = {};
			blitEndDst.x = imageWidth;
			blitEndDst.y = imageHeight;
			blitEndDst.z = 1;

			VkImageBlit imageBlitRegion{};
			imageBlitRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageBlitRegion.srcSubresource.layerCount = 1;
			imageBlitRegion.srcOffsets[0] = blitStartSrc;
			imageBlitRegion.srcOffsets[1] = blitEndSrc;
			imageBlitRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageBlitRegion.dstSubresource.layerCount = 1;
			imageBlitRegion.dstOffsets[0] = blitStartDst;
			imageBlitRegion.dstOffsets[1] = blitEndDst;

			// Issue the blit command
			vkCmdBlitImage(
				copyCmd,
				srcImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&imageBlitRegion,
				VK_FILTER_NEAREST);

			// Transition destination image to general layout, which is the required layout for mapping the image memory later on
			imageMemoryBarrier = {};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
			imageMemoryBarrier.image = dstImage;
			imageMemoryBarrier.subresourceRange = VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

			vkCmdPipelineBarrier(
				copyCmd,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &imageMemoryBarrier);


			// Transition back the color attachment image after the blit is done
			imageMemoryBarrier = {};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageMemoryBarrier.image = srcImage;
			imageMemoryBarrier.subresourceRange = VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

			vkCmdPipelineBarrier(
				copyCmd,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &imageMemoryBarrier);

			logicalDevice->FlushCommandBuffer(copyCmd);


			VkImageSubresource subResource{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 0 };
			VkSubresourceLayout subResourceLayout;
			vkGetImageSubresourceLayout(vulkanLogicalDevice, dstImage, &subResource, &subResourceLayout);

			// Map image memory so we can start copying from it
			const char* data;
			vkMapMemory(vulkanLogicalDevice, dstImageMemory, 0, VK_WHOLE_SIZE, 0, (void**)&data);

			data += subResourceLayout.offset;

			std::vector<VkFormat> formatsBGR = { VK_FORMAT_B8G8R8A8_SRGB, VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_B8G8R8A8_SNORM };
			b8 colorSwizzle = (std::find(formatsBGR.begin(), formatsBGR.end(), colorFormat) != formatsBGR.end());

			u32 colorCount = 4;
			m_ImageBuffer.Clear();
			m_ImageBuffer.Allocate(imageWidth * imageHeight * colorCount);
			for (u32 y = 0; y < imageHeight; y++)
			{
				u32* row = (u32*)data;
				u32 columnIndex = 0;
				for (uint32_t x = 0; x < imageWidth; x++)
				{
					if (colorSwizzle)
					{
						// BGRA
						m_ImageBuffer[y * (imageWidth * colorCount) + columnIndex] = *((u8*)row + 2);		// R
						m_ImageBuffer[y * (imageWidth * colorCount) + columnIndex + 1] = *((u8*)row + 1);	// G
						m_ImageBuffer[y * (imageWidth * colorCount) + columnIndex + 2] = *((u8*)row);		// B
						m_ImageBuffer[y * (imageWidth * colorCount) + columnIndex + 3] = *((u8*)row + 3);	// A
					}
					else
					{
						// RGBA
						memcpy((void*)(m_ImageBuffer + y * (imageWidth * colorCount) + columnIndex), (void*)row, 4);
					}
					columnIndex += colorCount;
					row++;
				}
				data += subResourceLayout.rowPitch;
			}

			// Clean up resources
			vkUnmapMemory(vulkanLogicalDevice, dstImageMemory);
			vkFreeMemory(vulkanLogicalDevice, dstImageMemory, nullptr);
			vkDestroyImage(vulkanLogicalDevice, dstImage, nullptr);
		}

		return m_ImageBuffer;
	}

	VkDescriptorImageInfo& VulkanTexture2D::GetVulkanDescriptorInfo()
	{
		return m_VulkanDescriptorImageInfo;
	}

	void VulkanTexture2D::Resize(u32 width, u32 height)
	{
		m_Width = width;
		m_Height = height;

		if (m_Framebuffer != nullptr)
		{
			auto& vulkanFramebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(m_Framebuffer);
			switch (m_TextureType)
			{
			case TextureType::FramebufferColor:
				// Create texture

				m_VulkanDescriptorImageInfo = vulkanFramebuffer->GetVulkanDescriptorColorImageInfo();
				break;
			case TextureType::FramebufferDepth:
				m_VulkanDescriptorImageInfo = vulkanFramebuffer->GetVulkanDescriptorDepthImageInfo();
				break;
			}
			auto vulkanDeviceMemory = vulkanFramebuffer->GetVulkanFramebufferColorAttachment().DeviceMemory;
			if (vulkanDeviceMemory)
			{
				/*auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();
				m_ImageBuffer.Size = 100;
				void* mappedData;
				vkMapMemory(vulkanLogicalDevice, vulkanDeviceMemory, 0, VK_WHOLE_SIZE, 0, &mappedData);
				memcpy(m_ImageBuffer.Data, mappedData, m_ImageBuffer.Size);
				vkUnmapMemory(vulkanLogicalDevice, vulkanDeviceMemory);*/
			}
		}
	}

	void VulkanTexture2D::ChangeLayoutWrite()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.image = instance->m_VulkanImage;;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = 0;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanTexture2D::ChangeLayoutRead()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.image = instance->m_VulkanImage;;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanTexture2D::CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel)
	{
		auto instance = this;
		Renderer::Submit([instance, framebuffer, width, height, currentLayer, currentMipMapLevel]()
			{
				auto& vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
				auto& vulkanFramebufferImage = std::dynamic_pointer_cast<VulkanFramebuffer>(framebuffer)->GetVulkanColorImage();

				// Need to change framebuffers color attachment layout to transfer
				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.image = vulkanFramebufferImage;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);

				// Copy image
				VkImageCopy copyRegion = {};

				copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.srcSubresource.baseArrayLayer = 0;
				copyRegion.srcSubresource.mipLevel = 0;
				copyRegion.srcSubresource.layerCount = 1;
				copyRegion.srcOffset = { 0, 0, 0 };

				copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.dstSubresource.baseArrayLayer = currentLayer;
				copyRegion.dstSubresource.mipLevel = currentMipMapLevel;
				copyRegion.dstSubresource.layerCount = 1;
				copyRegion.dstOffset = { 0, 0, 0 };

				copyRegion.extent.width = width;
				copyRegion.extent.height = height;
				copyRegion.extent.depth = 1;

				vkCmdCopyImage(
					vulkanCmdBuffer,
					vulkanFramebufferImage,
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					instance->m_VulkanImage,
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&copyRegion);

				// Set color attachment layout back

				imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	glm::uvec4 VulkanTexture2D::GetPixelValue(u32 x, u32 y)
	{
		auto& imageBuffer = GetImageBuffer(x, y, 1, 1);

		auto& vulkanFramebuffer = std::dynamic_pointer_cast<VulkanFramebuffer>(m_Framebuffer);

		const VkFormat colorFormat = static_cast<VkFormat>(vulkanFramebuffer->GetRenderPass()->GetColorFormat());

		glm::vec4 pixelValue = Utils::GetRGBAFromHex(*(u32*)imageBuffer.Data);

		return pixelValue;
	}

	b8 VulkanTexture2D::Compare(const std::shared_ptr<Texture> texture) const
	{
		auto& vulkanTexture = std::dynamic_pointer_cast<VulkanTexture2D>(texture);
		return vulkanTexture->GetVulkanDescriptorInfo().imageView == m_VulkanDescriptorImageInfo.imageView;
	}

	void VulkanTexture2D::Destroy()
	{
		Renderer::SubmitPostFrame([this]
			{
				auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();

				vkDestroyImageView(vulkanLogicalDevice, m_VulkanDescriptorImageInfo.imageView, nullptr);
				vkDestroyImage(vulkanLogicalDevice, m_VulkanImage, nullptr);
				vkDestroySampler(vulkanLogicalDevice, m_VulkanDescriptorImageInfo.sampler, nullptr);
				vkFreeMemory(vulkanLogicalDevice, m_VulkanDeviceMemory, nullptr);
			});
	}

	void VulkanTexture2D::Load()
	{
		auto logicalDevice = VulkanContext::GetCurrentDevice();
		auto vulkanLogicalDevice = logicalDevice->Get();

		VkDeviceSize size = m_ImageBuffer.Size;

		VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

		VkMemoryAllocateInfo memoryAllocateInfo{};
		memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memoryRequirements{};
		memoryRequirements.size = size;

		// Copy data to an optimal tiled image
		// This loads the texture data into a host local buffer that is copied to the optimal tiled image on the device

		// Create a host-visible staging buffer that contains the raw image data
		// This buffer will be the data source for copying texture data to the optimal tiled image on the device
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingMemory;

		VulkanMemoryAllocator allocator("Texture2D");

		VkBufferCreateInfo bufferCreateInfo{};
		bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferCreateInfo.size = size;
		// This buffer is used as a transfer source for the buffer copy
		bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		VK_CHECK_RESULT(vkCreateBuffer(vulkanLogicalDevice, &bufferCreateInfo, nullptr, &stagingBuffer));
		vkGetBufferMemoryRequirements(vulkanLogicalDevice, stagingBuffer, &memoryRequirements);
		allocator.Allocate(memoryRequirements, &stagingMemory, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		VK_CHECK_RESULT(vkBindBufferMemory(vulkanLogicalDevice, stagingBuffer, stagingMemory, 0));

		// Copy texture data into host local staging buffer
		uint8_t* destData;
		VK_CHECK_RESULT(vkMapMemory(vulkanLogicalDevice, stagingMemory, 0, memoryRequirements.size, 0, (void**)&destData));
		memcpy(destData, m_ImageBuffer.Data, size);
		vkUnmapMemory(vulkanLogicalDevice, stagingMemory);

		VkBufferImageCopy bufferCopyRegion{};
		bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		bufferCopyRegion.imageSubresource.mipLevel = 0;
		bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
		bufferCopyRegion.imageSubresource.layerCount = 1;
		bufferCopyRegion.imageExtent.width = m_Width;
		bufferCopyRegion.imageExtent.height = m_Height;
		bufferCopyRegion.imageExtent.depth = 1;
		bufferCopyRegion.bufferOffset = 0;

		// Create optimal tiled target image on the device
		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = format;
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		// Set initial layout of the image to undefined
		imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageCreateInfo.extent = { m_Width, m_Height, 1 };
		imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
		VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCreateInfo, nullptr, &m_VulkanImage));

		vkGetImageMemoryRequirements(vulkanLogicalDevice, m_VulkanImage, &memoryRequirements);
		allocator.Allocate(memoryRequirements, &m_VulkanDeviceMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_VulkanImage, m_VulkanDeviceMemory, 0));

		VkCommandBuffer copyCmd = logicalDevice->CreatePrimaryCommandBuffer(true);

		// Image memory barriers for the texture image

		// The sub resource range describes the regions of the image that will be transitioned using the memory barriers below
		VkImageSubresourceRange subresourceRange{};
		// Image only contains color data
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		// Start at first mip level
		subresourceRange.baseMipLevel = 0;
		// We will transition on all mip levels
		subresourceRange.levelCount = 1; // TODO: Support mips
		// The 2D texture only has one layer
		subresourceRange.layerCount = 1;

		// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
		VkImageMemoryBarrier imageMemoryBarrier{};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = m_VulkanImage;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
		// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_HOST_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Copy mip levels from staging buffer
		vkCmdCopyBufferToImage(
			copyCmd,
			stagingBuffer,
			m_VulkanImage,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1,
			&bufferCopyRegion);

		// Once the data has been uploaded we transfer to the texture image to the shader read layout, so it can be sampled from
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		// Destination pipeline stage fragment shader access (VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Store current layout for later reuse
		m_VulkanDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		logicalDevice->FlushCommandBuffer(copyCmd);

		// Clean up staging resources
		vkFreeMemory(vulkanLogicalDevice, stagingMemory, nullptr);
		vkDestroyBuffer(vulkanLogicalDevice, stagingBuffer, nullptr);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// CREATE TEXTURE SAMPLER
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a texture sampler
		// In Vulkan textures are accessed by samplers
		// This separates all the sampling information from the texture data. This means you could have multiple sampler objects for the same texture with different settings
		// Note: Similar to the samplers available with OpenGL 3.3
		VkSamplerCreateInfo sampler{};
		sampler.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		sampler.maxAnisotropy = 1.0f;
		sampler.magFilter = VK_FILTER_NEAREST;
		sampler.minFilter = VK_FILTER_LINEAR;
		sampler.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		sampler.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		sampler.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		sampler.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		sampler.mipLodBias = 0.0f;
		sampler.compareOp = VK_COMPARE_OP_NEVER;
		sampler.minLod = 0.0f;
		// Set max level-of-detail to mip level count of the texture
		sampler.maxLod = 1.0f;
		// Enable anisotropic filtering
		// This feature is optional, so we must check if it's supported on the device

		// TODO:
		/*if (vulkanDevice->features.samplerAnisotropy) {
			// Use max. level of anisotropy for this example
			sampler.maxAnisotropy = 1.0f;// vulkanDevice->properties.limits.maxSamplerAnisotropy;
			sampler.anisotropyEnable = VK_TRUE;
		}
		else {
			// The device does not support anisotropic filtering
			sampler.maxAnisotropy = 1.0;
			sampler.anisotropyEnable = VK_FALSE;
		}*/
		sampler.maxAnisotropy = 1.0;
		sampler.anisotropyEnable = VK_FALSE;
		sampler.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		VK_CHECK_RESULT(vkCreateSampler(vulkanLogicalDevice, &sampler, nullptr, &m_VulkanDescriptorImageInfo.sampler));

		// Create image view
		// Textures are not directly accessed by the shaders and
		// are abstracted by image views containing additional
		// information and sub resource ranges
		VkImageViewCreateInfo view{};
		view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		view.viewType = VK_IMAGE_VIEW_TYPE_2D;
		view.format = format;
		view.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
		// The subresource range describes the set of mip levels (and array layers) that can be accessed through this image view
		// It's possible to create multiple image views for a single image referring to different (and/or overlapping) ranges of the image
		view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		view.subresourceRange.baseMipLevel = 0;
		view.subresourceRange.baseArrayLayer = 0;
		view.subresourceRange.layerCount = 1;
		// Linear tiling usually won't support mip maps
		// Only set mip map count if optimal tiling is used
		view.subresourceRange.levelCount = 1;
		// The view will be based on the texture's image
		view.image = m_VulkanImage;
		VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &view, nullptr, &m_VulkanDescriptorImageInfo.imageView));
	}

	void VulkanTexture2D::LoadEmpty()
	{
		auto logicalDevice = VulkanContext::GetCurrentDevice();
		auto vulkanLogicalDevice = logicalDevice->Get();

		const VkFormat format = DebColorFormatToVulkan(m_ColorFormat);

		// Pre-filtered cube map
		// Image
		VkImageCreateInfo imageCI = {};
		imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCI.imageType = VK_IMAGE_TYPE_2D;
		imageCI.format = format;
		imageCI.extent.width = m_Width;
		imageCI.extent.height = m_Height;
		imageCI.extent.depth = 1;
		imageCI.mipLevels = 1;
		imageCI.arrayLayers = 1;
		imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCI.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCI, nullptr, &m_VulkanImage));

		VulkanMemoryAllocator allocator("Texture2D");

		VkMemoryRequirements memReqs;
		vkGetImageMemoryRequirements(vulkanLogicalDevice, m_VulkanImage, &memReqs);
		allocator.Allocate(memReqs, &m_VulkanDeviceMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_VulkanImage, m_VulkanDeviceMemory, 0));

		// Image view
		VkImageViewCreateInfo viewCI = {};
		viewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewCI.format = format;
		viewCI.subresourceRange = {};
		viewCI.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewCI.subresourceRange.levelCount = 1;
		viewCI.subresourceRange.layerCount = 1;
		viewCI.image = m_VulkanImage;
		VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &viewCI, nullptr, &m_VulkanDescriptorImageInfo.imageView));

		// Sampler
		VkSamplerCreateInfo samplerCI = {};
		samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCI.maxAnisotropy = 1.0f;
		samplerCI.magFilter = VK_FILTER_LINEAR;
		samplerCI.minFilter = VK_FILTER_LINEAR;
		samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.minLod = 0.0f;
		samplerCI.maxLod = 1.0f;
		samplerCI.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		VK_CHECK_RESULT(vkCreateSampler(vulkanLogicalDevice, &samplerCI, nullptr, &m_VulkanDescriptorImageInfo.sampler));

		VkCommandBuffer copyCmd = logicalDevice->CreatePrimaryCommandBuffer(true);

		// Image memory barriers for the texture image

		// The sub resource range describes the regions of the image that will be transitioned using the memory barriers below
		VkImageSubresourceRange subresourceRange{};
		// Image only contains color data
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		// Start at first mip level
		subresourceRange.baseMipLevel = 0;
		// We will transition on all mip levels
		subresourceRange.levelCount = 1; // TODO: Support mips
		// The 2D texture only has one layer
		subresourceRange.layerCount = 1;

		// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
		VkImageMemoryBarrier imageMemoryBarrier{};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = m_VulkanImage;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
		// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_HOST_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Store current layout for later reuse
		m_VulkanDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		logicalDevice->FlushCommandBuffer(copyCmd);
	}

	void VulkanTexture2D::CreateImGuiTexture()
	{
		m_ImGuiTextureId = ImGui_ImplVulkan_AddTexture(
			m_VulkanDescriptorImageInfo.sampler,
			m_VulkanDescriptorImageInfo.imageView,
			m_VulkanDescriptorImageInfo.imageLayout);
	}

	b8 VulkanTexture2D::IsLoaded() const
	{
		return m_IsLoaded;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// VulkanTextureCube ////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	VulkanTextureCube::VulkanTextureCube(const std::string& path)
	{
		s32 width, height, channels;
		stbi_set_flip_vertically_on_load(0);
		m_ImageBuffer.Data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);
		DEB_CORE_ASSERT(m_ImageBuffer.Data, "Failed to load image!");
		m_Channels = channels;
		m_ImageBuffer.Size = width * height * 4;
		m_IsLoaded = true;

		m_FaceWidth = width / 6;
		m_FaceHeight = height;

		for (size_t i = 0; i < m_FacesData.size(); i++)
			m_FacesData[i] = new u8[m_FaceWidth * m_FaceHeight * 4]; // 3 BPP

		u32 faceIndex = 0;

		for (size_t i = 0; i < 6; i++)
		{
			for (size_t y = 0; y < m_FaceHeight; y++)
			{
				size_t yOffset = y;
				for (size_t x = 0; x < m_FaceWidth; x++)
				{
					size_t xOffset = x + i * m_FaceWidth;
					m_FacesData[faceIndex][(x + y * m_FaceWidth) * 4 + 0] = m_ImageBuffer[(xOffset + yOffset * width) * 4 + 0];
					m_FacesData[faceIndex][(x + y * m_FaceWidth) * 4 + 1] = m_ImageBuffer[(xOffset + yOffset * width) * 4 + 1];
					m_FacesData[faceIndex][(x + y * m_FaceWidth) * 4 + 2] = m_ImageBuffer[(xOffset + yOffset * width) * 4 + 2];
					m_FacesData[faceIndex][(x + y * m_FaceWidth) * 4 + 3] = m_ImageBuffer[(xOffset + yOffset * width) * 4 + 3];
				}
			}
			faceIndex++;
		}

		m_TextureType = TextureType::CubeMap;
		// +X  -X  +Y  -Y  +Z  -Z

		// Create Vulkan texture on renderer thread
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->Load();

				for (size_t i = 0; i < instance->m_FacesData.size(); i++)
					delete[] instance->m_FacesData[i];

				stbi_image_free(instance->m_ImageBuffer.Data);
			});
	}

	VulkanTextureCube::VulkanTextureCube(const std::vector<std::string>& facesPaths)
	{
		stbi_set_flip_vertically_on_load(1);
		s32 width, height, channels;
		u32 faceIndex = 0;
		for (const auto& path : facesPaths)
		{
			m_ImageBuffer.Data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);
			DEB_CORE_ASSERT(m_ImageBuffer.Data, "Failed to load image!");
			DEB_CORE_ASSERT(width == height, "Width and height must be equal for a cube map face!");
			m_FaceWidth = width;
			m_FaceHeight = height;
			m_Channels = channels;


			u32 faceSize = width * height * 4;
			m_ImageBuffer.Size += faceSize;

			m_FacesData[faceIndex] = new u8[faceSize];
			memcpy(m_FacesData[faceIndex], m_ImageBuffer.Data, faceSize);
			stbi_image_free(m_ImageBuffer.Data);
			faceIndex++;
		}
		m_IsLoaded = true;

		m_TextureType = TextureType::CubeMap;

		// Create Vulkan texture on renderer thread
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->Load();

				for (size_t i = 0; i < instance->m_FacesData.size(); i++)
					delete[] instance->m_FacesData[i];
			});
	}

	VulkanTextureCube::VulkanTextureCube(u32 width, u32 height)
	{
		m_FaceWidth = width;
		m_FaceHeight = height;

		m_MipMapLevel = static_cast<uint32_t>(floor(log2(m_FaceWidth))) + 1;

		m_TextureType = TextureType::CubeMap;

		// Create Vulkan texture on renderer thread
		auto instance = this;
		Renderer::Submit([instance]()
			{
				instance->LoadEmpty();
			});
	}

	VulkanTextureCube::~VulkanTextureCube()
	{
		//Destroy();
	}

	void VulkanTextureCube::Bind()
	{

	}

	u32 VulkanTextureCube::GetWidth() const
	{
		return m_FaceWidth;
	}

	u32 VulkanTextureCube::GetHeight() const
	{
		return m_FaceHeight;
	}

	u32 VulkanTextureCube::GetMipLevelCount() const
	{
		return m_MipMapLevel;
	}

	Buffer& VulkanTextureCube::GetImageBuffer(u32 x, u32 y, u32 width, u32 height)
	{
		return m_ImageBuffer;
	}

	VkDescriptorImageInfo& VulkanTextureCube::GetVulkanDescriptorInfo()
	{
		return m_VulkanDescriptorImageInfo;
	}

	VkImage& VulkanTextureCube::GetVulkanImage()
	{
		return m_VulkanImage;
	}

	bool VulkanTextureCube::Serialize(YAML::Emitter& outStream)
	{
		return true;
	}

	void VulkanTextureCube::Resize(u32 width, u32 height)
	{

	}

	b8 VulkanTextureCube::Compare(const std::shared_ptr<Texture> texture) const
	{
		auto& vulkanTexture = std::dynamic_pointer_cast<VulkanTextureCube>(texture);
		return vulkanTexture->GetVulkanDescriptorInfo().imageView == m_VulkanDescriptorImageInfo.imageView;
	}

	void VulkanTextureCube::Destroy()
	{
		Renderer::SubmitPostFrame([this]
			{
				auto vulkanLogicalDevice = VulkanContext::GetCurrentDevice()->Get();

				vkDestroyImageView(vulkanLogicalDevice, m_VulkanDescriptorImageInfo.imageView, nullptr);
				vkDestroyImage(vulkanLogicalDevice, m_VulkanImage, nullptr);
				vkDestroySampler(vulkanLogicalDevice, m_VulkanDescriptorImageInfo.sampler, nullptr);
				vkFreeMemory(vulkanLogicalDevice, m_VulkanDeviceMemory, nullptr);
			});
	}

	void VulkanTextureCube::ChangeLayoutWrite()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = instance->m_MipMapLevel;
				subresourceRange.layerCount = 6;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.image = instance->m_VulkanImage;;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = 0;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanTextureCube::ChangeLayoutRead()
	{
		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();

				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = instance->m_MipMapLevel;
				subresourceRange.layerCount = 6;

				// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				imageMemoryBarrier.image = instance->m_VulkanImage;;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

				// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
				// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
				// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanTextureCube::CopyImageFromFramebuffer(const std::shared_ptr<Framebuffer>& framebuffer, u32 width, u32 height, u32 currentLayer, u32 currentMipMapLevel)
	{
		auto instance = this;
		Renderer::Submit([instance, framebuffer, width, height, currentLayer, currentMipMapLevel]()
			{
				auto& vulkanCmdBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(Renderer::GetCurrentCommandBuffer())->GetVulkanCommandBuffer();
				auto& vulkanFramebufferImage = std::dynamic_pointer_cast<VulkanFramebuffer>(framebuffer)->GetVulkanColorImage();

				// Need to change framebuffers color attachment layout to transfer
				VkImageSubresourceRange subresourceRange = {};
				subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				subresourceRange.baseMipLevel = 0;
				subresourceRange.levelCount = 1;
				subresourceRange.layerCount = 1;

				VkImageMemoryBarrier imageMemoryBarrier{};
				imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				imageMemoryBarrier.image = vulkanFramebufferImage;
				imageMemoryBarrier.subresourceRange = subresourceRange;
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);


				// Copy image
				VkImageCopy copyRegion = {};

				copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.srcSubresource.baseArrayLayer = 0;
				copyRegion.srcSubresource.mipLevel = 0;
				copyRegion.srcSubresource.layerCount = 1;
				copyRegion.srcOffset = { 0, 0, 0 };

				copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.dstSubresource.baseArrayLayer = currentLayer;
				copyRegion.dstSubresource.mipLevel = currentMipMapLevel;
				copyRegion.dstSubresource.layerCount = 1;
				copyRegion.dstOffset = { 0, 0, 0 };

				copyRegion.extent.width = width;
				copyRegion.extent.height = height;
				copyRegion.extent.depth = 1;

				vkCmdCopyImage(
					vulkanCmdBuffer,
					vulkanFramebufferImage,
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					instance->m_VulkanImage,
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&copyRegion);

				// Set color attachment layout back

				imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
				imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

				imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				vkCmdPipelineBarrier(
					vulkanCmdBuffer,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					0,
					0, nullptr,
					0, nullptr,
					1, &imageMemoryBarrier);
			});
	}

	void VulkanTextureCube::Load()
	{
		auto logicalDevice = VulkanContext::GetCurrentDevice();
		auto vulkanLogicalDevice = logicalDevice->Get();

		VkDeviceSize size = m_ImageBuffer.Size;

		VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

		VkMemoryAllocateInfo memoryAllocateInfo{};
		memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memoryRequirements{};

		// Copy data to an optimal tiled image
		// This loads the texture data into a host local buffer that is copied to the optimal tiled image on the device

		// Create a host-visible staging buffer that contains the raw image data
		// This buffer will be the data source for copying texture data to the optimal tiled image on the device
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingMemory;

		VulkanMemoryAllocator allocator("TextureCube");

		VkBufferCreateInfo bufferCreateInfo{};
		bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferCreateInfo.size = size;
		// This buffer is used as a transfer source for the buffer copy
		bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		VK_CHECK_RESULT(vkCreateBuffer(vulkanLogicalDevice, &bufferCreateInfo, nullptr, &stagingBuffer));
		vkGetBufferMemoryRequirements(vulkanLogicalDevice, stagingBuffer, &memoryRequirements);
		allocator.Allocate(memoryRequirements, &stagingMemory, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		VK_CHECK_RESULT(vkBindBufferMemory(vulkanLogicalDevice, stagingBuffer, stagingMemory, 0));

		// Copy texture data into host local staging buffer
		u8* destData;
		VK_CHECK_RESULT(vkMapMemory(vulkanLogicalDevice, stagingMemory, 0, memoryRequirements.size, 0, (void**)&destData));

		u32 faceSize = m_FaceWidth * m_FaceHeight * 4;
		for (int face = 0; face < m_FacesData.size(); face++)
		{
			memcpy(destData + faceSize * face, m_FacesData[face], faceSize);
		}

		vkUnmapMemory(vulkanLogicalDevice, stagingMemory);

		// Setup buffer copy regions for each face
		std::vector<VkBufferImageCopy> bufferCopyRegions;

		for (u32 face = 0; face < 6; face++)
		{
			u32 offset = face * m_ImageBuffer.Size / 6;

			VkBufferImageCopy bufferCopyRegion{};
			bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			bufferCopyRegion.imageSubresource.mipLevel = 0;
			bufferCopyRegion.imageSubresource.baseArrayLayer = face;
			bufferCopyRegion.imageSubresource.layerCount = 1;
			bufferCopyRegion.imageExtent.width = m_FaceWidth;
			bufferCopyRegion.imageExtent.height = m_FaceHeight;
			bufferCopyRegion.imageExtent.depth = 1;
			bufferCopyRegion.bufferOffset = offset;

			bufferCopyRegions.push_back(bufferCopyRegion);
		}

		// Create optimal tiled target image on the device
		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = format;
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 6;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		// Set initial layout of the image to undefined
		imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageCreateInfo.extent = { m_FaceWidth, m_FaceHeight, 1 };
		imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
		imageCreateInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCreateInfo, nullptr, &m_VulkanImage));

		vkGetImageMemoryRequirements(vulkanLogicalDevice, m_VulkanImage, &memoryRequirements);
		allocator.Allocate(memoryRequirements, &m_VulkanDeviceMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_VulkanImage, m_VulkanDeviceMemory, 0));

		VkCommandBuffer copyCmd = logicalDevice->CreatePrimaryCommandBuffer(true);

		// Image memory barriers for the texture image

		// The sub resource range describes the regions of the image that will be transitioned using the memory barriers below
		VkImageSubresourceRange subresourceRange{};
		// Image only contains color data
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		// Start at first mip level
		subresourceRange.baseMipLevel = 0;
		// We will transition on all mip levels
		subresourceRange.levelCount = 1; // TODO: Support mips
		// The 2D texture only has one layer
		subresourceRange.layerCount = 6;

		// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
		VkImageMemoryBarrier imageMemoryBarrier{};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = m_VulkanImage;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
		// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_HOST_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Copy mip levels from staging buffer
		vkCmdCopyBufferToImage(
			copyCmd,
			stagingBuffer,
			m_VulkanImage,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			bufferCopyRegions.size(),
			bufferCopyRegions.data());

		// Once the data has been uploaded we transfer to the texture image to the shader read layout, so it can be sampled from
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		// Destination pipeline stage fragment shader access (VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Store current layout for later reuse
		m_VulkanDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		logicalDevice->FlushCommandBuffer(copyCmd);

		// Clean up staging resources
		vkFreeMemory(vulkanLogicalDevice, stagingMemory, nullptr);
		vkDestroyBuffer(vulkanLogicalDevice, stagingBuffer, nullptr);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// CREATE TEXTURE SAMPLER
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a texture sampler
		// In Vulkan textures are accessed by samplers
		// This separates all the sampling information from the texture data. This means you could have multiple sampler objects for the same texture with different settings
		// Note: Similar to the samplers available with OpenGL 3.3
		VkSamplerCreateInfo sampler{};
		sampler.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		sampler.maxAnisotropy = 1.0f;
		sampler.magFilter = VK_FILTER_LINEAR;
		sampler.minFilter = VK_FILTER_LINEAR;
		sampler.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		sampler.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler.mipLodBias = 0.0f;
		sampler.compareOp = VK_COMPARE_OP_NEVER;
		sampler.minLod = 0.0f;
		// Set max level-of-detail to mip level count of the texture
		sampler.maxLod = 1.0f;
		// Enable anisotropic filtering
		// This feature is optional, so we must check if it's supported on the device

		// TODO:
		/*if (vulkanDevice->features.samplerAnisotropy) {
			// Use max. level of anisotropy for this example
			sampler.maxAnisotropy = 1.0f;// vulkanDevice->properties.limits.maxSamplerAnisotropy;
			sampler.anisotropyEnable = VK_TRUE;
		}
		else {
			// The device does not support anisotropic filtering
			sampler.maxAnisotropy = 1.0;
			sampler.anisotropyEnable = VK_FALSE;
		}*/
		sampler.maxAnisotropy = 1.0;
		sampler.anisotropyEnable = VK_TRUE;
		sampler.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		VK_CHECK_RESULT(vkCreateSampler(vulkanLogicalDevice, &sampler, nullptr, &m_VulkanDescriptorImageInfo.sampler));

		// Create image view
		// Textures are not directly accessed by the shaders and
		// are abstracted by image views containing additional
		// information and sub resource ranges
		VkImageViewCreateInfo view{};
		view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		view.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
		view.format = format;
		view.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
		// The subresource range describes the set of mip levels (and array layers) that can be accessed through this image view
		// It's possible to create multiple image views for a single image referring to different (and/or overlapping) ranges of the image
		view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		view.subresourceRange.baseMipLevel = 0;
		view.subresourceRange.levelCount = 1;
		view.subresourceRange.baseArrayLayer = 0;
		view.subresourceRange.layerCount = 6;
		// Linear tiling usually won't support mip maps
		// Only set mip map count if optimal tiling is used
		view.subresourceRange.levelCount = 1;
		// The view will be based on the texture's image
		view.image = m_VulkanImage;
		VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &view, nullptr, &m_VulkanDescriptorImageInfo.imageView));
	}

	void VulkanTextureCube::LoadEmpty()
	{
		auto logicalDevice = VulkanContext::GetCurrentDevice();
		auto vulkanLogicalDevice = logicalDevice->Get();

		//const VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
		const VkFormat format = VK_FORMAT_R16G16B16A16_SFLOAT;

		// Pre-filtered cube map
		// Image
		VkImageCreateInfo imageCI = {};
		imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCI.imageType = VK_IMAGE_TYPE_2D;
		imageCI.format = format;
		imageCI.extent.width = m_FaceWidth;
		imageCI.extent.height = m_FaceHeight;
		imageCI.extent.depth = 1;
		imageCI.mipLevels = m_MipMapLevel;
		imageCI.arrayLayers = 6;
		imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCI.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		imageCI.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		VK_CHECK_RESULT(vkCreateImage(vulkanLogicalDevice, &imageCI, nullptr, &m_VulkanImage));

		VulkanMemoryAllocator allocator("TextureCube");

		VkMemoryRequirements memReqs;
		vkGetImageMemoryRequirements(vulkanLogicalDevice, m_VulkanImage, &memReqs);
		allocator.Allocate(memReqs, &m_VulkanDeviceMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		VK_CHECK_RESULT(vkBindImageMemory(vulkanLogicalDevice, m_VulkanImage, m_VulkanDeviceMemory, 0));

		// Image view
		VkImageViewCreateInfo viewCI = {};
		viewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewCI.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
		viewCI.format = format;
		viewCI.subresourceRange = {};
		viewCI.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewCI.subresourceRange.levelCount = m_MipMapLevel;
		viewCI.subresourceRange.layerCount = 6;
		viewCI.image = m_VulkanImage;
		VK_CHECK_RESULT(vkCreateImageView(vulkanLogicalDevice, &viewCI, nullptr, &m_VulkanDescriptorImageInfo.imageView));

		// Sampler
		VkSamplerCreateInfo samplerCI = {};
		samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCI.maxAnisotropy = 1.0f;
		samplerCI.magFilter = VK_FILTER_LINEAR;
		samplerCI.minFilter = VK_FILTER_LINEAR;
		samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerCI.minLod = 0.0f;
		samplerCI.maxLod = static_cast<f32>(m_MipMapLevel);
		samplerCI.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		VK_CHECK_RESULT(vkCreateSampler(vulkanLogicalDevice, &samplerCI, nullptr, &m_VulkanDescriptorImageInfo.sampler));


		VkCommandBuffer copyCmd = logicalDevice->CreatePrimaryCommandBuffer(true);

		// Image memory barriers for the texture image

		// The sub resource range describes the regions of the image that will be transitioned using the memory barriers below
		VkImageSubresourceRange subresourceRange{};
		// Image only contains color data
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		// Start at first mip level
		subresourceRange.baseMipLevel = 0;
		// We will transition on all mip levels
		subresourceRange.levelCount = m_MipMapLevel; // TODO: Support mips
		// The 2D texture only has one layer
		subresourceRange.layerCount = 6;

		// Transition the texture image layout to transfer target, so we can safely copy our buffer data to it.
		VkImageMemoryBarrier imageMemoryBarrier{};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = m_VulkanImage;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// Insert a memory dependency at the proper pipeline stages that will execute the image layout transition 
		// Source pipeline stage is host write/read exection (VK_PIPELINE_STAGE_HOST_BIT)
		// Destination pipeline stage is copy command exection (VK_PIPELINE_STAGE_TRANSFER_BIT)
		vkCmdPipelineBarrier(
			copyCmd,
			VK_PIPELINE_STAGE_HOST_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier);

		// Store current layout for later reuse
		m_VulkanDescriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		logicalDevice->FlushCommandBuffer(copyCmd);
	}
}
