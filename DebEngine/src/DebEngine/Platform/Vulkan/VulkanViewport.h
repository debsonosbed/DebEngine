#pragma once

#include "DebEngine/Graphics/Viewport.h"

namespace DebEngine
{
	class VulkanViewport : public Viewport
	{
	public:
		VulkanViewport(u32 x, u32 y, u32 width, u32 height);

		virtual void SetViewport(u32 width, u32 height) override;
	private:
		u32 m_Width = 0;
		u32 m_Height = 0;
		u32 m_PosX = 0;
		u32 m_PosY = 0;
	};
}

