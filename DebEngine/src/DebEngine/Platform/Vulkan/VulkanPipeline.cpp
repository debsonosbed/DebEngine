#include "debpch.h"
#include "VulkanPipeline.h"

#include "Vulkan.h"
#include "VulkanShader.h"
#include "VulkanFramebuffer.h"
#include "VulkanContext.h"
#include "VulkanCommandBuffer.h"
#include "VulkanRenderPass.h"
#include "VulkanUtils.h"

namespace DebEngine
{
	static VkFormat ShaderDataTypeToVulkanFormat(ShaderDataType type)
	{
		switch (type)
		{
		case ShaderDataType::Float:     return VK_FORMAT_R32_SFLOAT;
		case ShaderDataType::Float2:    return VK_FORMAT_R32G32_SFLOAT;
		case ShaderDataType::Float3:    return VK_FORMAT_R32G32B32_SFLOAT;
		case ShaderDataType::Float4:    return VK_FORMAT_R32G32B32A32_SFLOAT;
		}
		DEB_CORE_ASSERT(false);
		return VK_FORMAT_UNDEFINED;
	}

	VulkanPipeline::VulkanPipeline(const PipelineSpecification& pipelineSpecification, const std::string& name)
		: m_PipelineSpecification(pipelineSpecification)
	{
		m_Name = name;
		Assemble();
	}

	void VulkanPipeline::Bind() const
	{
		m_PipelineSpecification.Shader->Bind();

		auto instance = this;
		Renderer::Submit([instance]()
			{
				auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
				VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
				vkCmdBindPipeline(drawCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, instance->m_VulkanPipeline);
			});
	}

	void VulkanPipeline::Assemble()
	{
		auto instance = this;
		auto context = Renderer::GetContext();
		Renderer::Submit([instance, context]() mutable
			{
				VkDevice device = VulkanContext::GetCurrentDevice()->Get();

				DEB_CORE_ASSERT(instance->m_PipelineSpecification.Shader);
				auto vulkanShader = std::dynamic_pointer_cast<VulkanShader>(instance->m_PipelineSpecification.Shader);
				// Right now pipeline is created from swap chain's frame buffers and render pass
				// TODO: Should support custom frame buffers and render passes.

				auto vulkanRenderPass = std::dynamic_pointer_cast<VulkanRenderPass>(instance->m_PipelineSpecification.RenderPass)->GetVulkanRenderPass();
				//auto vulkanSwapchain = std::dynamic_pointer_cast<VulkanSwapChain>(context->GetSwapChain());
				//auto vulkanFramebuffer = vulkanSwapchain->GetVulkanCurrentFramebuffer();
				//auto vulkanRenderPass = vulkanSwapchain->GetVulkanRenderPass();

				auto descriptorSetLayout = vulkanShader->GetDescriptorSetLayout();

				const auto& pushConstantRanges = vulkanShader->GetPushConstantRanges();

				const auto& pipelineSpecification = instance->GetSpecification();

				std::vector<VkPushConstantRange> vulkanPushConstantRanges(pushConstantRanges.size());
				for (uint32_t i = 0; i < pushConstantRanges.size(); i++)
				{
					const auto& pushConstantRange = pushConstantRanges[i];
					auto& vulkanPushConstantRange = vulkanPushConstantRanges[i];

					vulkanPushConstantRange.stageFlags = pushConstantRange.ShaderStage;
					// Ideally it should come from the shader
					//vulkanPushConstantRange.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
					vulkanPushConstantRange.offset = pushConstantRange.Offset;
					vulkanPushConstantRange.size = pushConstantRange.Size;
				}

				// Create the pipeline layout that is used to generate the rendering pipelines that are based on this descriptor set layout
				// In a more complex scenario you would have different pipeline layouts for different descriptor set layouts that could be reused
				VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
				pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
				pipelineLayoutCreateInfo.pNext = nullptr;
				pipelineLayoutCreateInfo.setLayoutCount = 1;
				pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
				pipelineLayoutCreateInfo.pushConstantRangeCount = vulkanPushConstantRanges.size();
				pipelineLayoutCreateInfo.pPushConstantRanges = vulkanPushConstantRanges.data();

				VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &instance->m_PipelineLayout));

				// Create the graphics pipeline used in this example
				// Vulkan uses the concept of rendering pipelines to encapsulate fixed states, replacing OpenGL's complex state machine
				// A pipeline is then stored and hashed on the GPU making pipeline changes very fast
				// Note: There are still a few dynamic states that are not directly part of the pipeline (but the info that they are used is)

				VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
				pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
				// The layout used for this pipeline (can be shared among multiple pipelines using the same layout)
				pipelineCreateInfo.layout = instance->m_PipelineLayout;
				// Renderpass this pipeline is attached to
				pipelineCreateInfo.renderPass = vulkanRenderPass;

				// Construct the different states making up the pipeline

				// Input assembly state describes how primitives are assembled
				// This pipeline will assemble vertex data as a triangle lists (though we only use one triangle)
				VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {};
				inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
				inputAssemblyState.topology = instance->GetToplogy();

				// Rasterization state
				VkPipelineRasterizationStateCreateInfo rasterizationState = {};
				rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
				rasterizationState.polygonMode = instance->GetPolygonMode();
				rasterizationState.cullMode = instance->GetCullMode(instance->m_PipelineSpecification.CullMode);
				/*if (instance->m_PipelineSpecification.CullingEnabled)
					rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;*/
				rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
				rasterizationState.depthClampEnable = VK_FALSE;
				rasterizationState.rasterizerDiscardEnable = VK_FALSE;
				rasterizationState.depthBiasEnable = VK_FALSE;
				rasterizationState.lineWidth = 1.0f;
				rasterizationState.flags = 0;

				// Color blend state describes how blend factors are calculated (if used)
				// We need one blend attachment state per color attachment (even if blending is not used)
				VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
				colorBlendAttachment.colorWriteMask = 0xF;
				colorBlendAttachment.blendEnable = pipelineSpecification.BlendEnabled ? VK_TRUE : VK_FALSE;
				colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
				colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
				colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
				colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
				colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
				colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

				VkPipelineColorBlendStateCreateInfo colorBlendState = {};
				colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
				colorBlendState.attachmentCount = 1;
				colorBlendState.pAttachments = &colorBlendAttachment;

				// Viewport state sets the number of viewports and scissor used in this pipeline
				// Note: This is actually overridden by the dynamic states (see below)
				VkPipelineViewportStateCreateInfo viewportState = {};
				viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
				viewportState.viewportCount = 1;
				viewportState.scissorCount = 1;

				// Enable dynamic states
				// Most states are baked into the pipeline, but there are still a few dynamic states that can be changed within a command buffer
				// To be able to change these we need do specify which dynamic states will be changed using this pipeline. Their actual states are set later on in the command buffer.
				// For this example we will set the viewport and scissor using dynamic states
				std::vector<VkDynamicState> dynamicStateEnables = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
				//dynamicStateEnables.push_back(VK_DYNAMIC_STATE_LINE_WIDTH);
				VkPipelineDynamicStateCreateInfo dynamicState = {};
				dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
				dynamicState.pDynamicStates = dynamicStateEnables.data();
				dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

				// Depth and stencil state containing depth and stencil compare and test operations
				// We only use depth tests and want depth tests and writes to be enabled and compare with less or equal
				VkPipelineDepthStencilStateCreateInfo depthStencilState = instance->BuildDepthStenilState();

				// Multi sampling state
				// This example does not make use of multi sampling (for anti-aliasing), the state must still be set and passed to the pipeline

				auto sampleCount = std::dynamic_pointer_cast<VulkanRenderPass>(instance->m_PipelineSpecification.RenderPass)->GetVulkanSampleCount();
				VkPipelineMultisampleStateCreateInfo multisampleState = {};
				multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
				multisampleState.rasterizationSamples = sampleCount;
				if (sampleCount > 1)
				{
					multisampleState.sampleShadingEnable = VK_TRUE;
					// Minimum fraction for sample shading
					multisampleState.minSampleShading = 0.8f;
				}

				// Vertex input descriptions
				// Specifies the vertex input parameters for a pipeline

				auto& layouts = instance->m_PipelineSpecification.Layouts;

				// Vertex input binding
				// This example uses a single vertex input binding at binding point 0 (see vkCmdBindVertexBuffers)
				std::vector<VkVertexInputBindingDescription> vertexInputBindingDescriptions;
				std::vector<VkVertexInputAttributeDescription> vertexInputAttributes;
				vertexInputBindingDescriptions.resize(layouts.size());

				u32 bindingPoint = 0;
				u32 location = 0;
				for (const auto& layout : layouts)
				{
					vertexInputBindingDescriptions[bindingPoint] = {};
					vertexInputBindingDescriptions[bindingPoint].binding = bindingPoint;
					vertexInputBindingDescriptions[bindingPoint].stride = layout.GetStride();
					vertexInputBindingDescriptions[bindingPoint].inputRate = layout.IsInstanceLayout() ? VK_VERTEX_INPUT_RATE_INSTANCE : VK_VERTEX_INPUT_RATE_VERTEX;

					// Input attribute bindings describe shader attribute locations and memory layouts

					for (const auto& element : layout)
					{
						VkVertexInputAttributeDescription vertexInputAttribute = {};
						vertexInputAttribute.binding = bindingPoint;
						vertexInputAttribute.location = location;
						vertexInputAttribute.format = ShaderDataTypeToVulkanFormat(element.Type);
						vertexInputAttribute.offset = element.Offset;
						vertexInputAttributes.push_back(vertexInputAttribute);
						location++;
					}
					bindingPoint++;
				}

				// Vertex input state used for pipeline creation
				VkPipelineVertexInputStateCreateInfo vertexInputState = {};
				vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
				vertexInputState.vertexBindingDescriptionCount = vertexInputBindingDescriptions.size();
				vertexInputState.pVertexBindingDescriptions = vertexInputBindingDescriptions.data();
				vertexInputState.vertexAttributeDescriptionCount = vertexInputAttributes.size();
				vertexInputState.pVertexAttributeDescriptions = vertexInputAttributes.data();

				const auto& shaderStages = vulkanShader->GetShaderStages();

				// Set pipeline shader stage info
				pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
				pipelineCreateInfo.pStages = shaderStages.data();

				// Assign the pipeline states to the pipeline creation info structure
				pipelineCreateInfo.pVertexInputState = &vertexInputState;
				pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
				pipelineCreateInfo.pRasterizationState = &rasterizationState;
				pipelineCreateInfo.pColorBlendState = &colorBlendState;
				pipelineCreateInfo.pMultisampleState = &multisampleState;
				pipelineCreateInfo.pViewportState = &viewportState;
				pipelineCreateInfo.pDepthStencilState = &depthStencilState;
				pipelineCreateInfo.renderPass = vulkanRenderPass;
				pipelineCreateInfo.pDynamicState = &dynamicState;

				VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {};
				pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
				VkPipelineCache pipelineCache;
				VK_CHECK_RESULT(vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));

				// Create rendering pipeline using the specified states
				VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &instance->m_VulkanPipeline));
			});
	}

	void VulkanPipeline::Push(const ShaderBufferMember& uniformMember, Buffer& buffer)
	{
		auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
		VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
		VkShaderStageFlagBits shaderStage = VulkanShader::ShaderTypeToShaderStage(uniformMember.ShaderType);
		vkCmdPushConstants(drawCommandBuffer, m_PipelineLayout, shaderStage, uniformMember.Offset, uniformMember.Size, (void*)buffer.Data);
		// We are working on copy of source buffers. These needs to be deleted
	}

	void VulkanPipeline::Push(const std::shared_ptr<ShaderBuffer> shaderBuffer, Buffer& buffer)
	{
		auto& commandBuffer = Renderer::GetCurrentCommandBuffer();
		VkCommandBuffer drawCommandBuffer = std::dynamic_pointer_cast<VulkanCommandBuffer>(commandBuffer)->GetVulkanCommandBuffer();
		VkShaderStageFlagBits shaderStage = VulkanShader::ShaderTypeToShaderStage(shaderBuffer->ShaderType);
		vkCmdPushConstants(drawCommandBuffer, m_PipelineLayout, shaderStage, shaderBuffer->Offset, shaderBuffer->Size, (void*)buffer.Data);
	}

	PipelineSpecification& VulkanPipeline::GetSpecification()
	{
		return m_PipelineSpecification;
	}

	VkPipeline VulkanPipeline::GetVulkanPipeline() const
	{
		return m_VulkanPipeline;
	}

	VkPipelineLayout VulkanPipeline::GetVulkanPipelineLayout() const
	{
		return m_PipelineLayout;
	}

	VkPrimitiveTopology VulkanPipeline::GetToplogy() const
	{
		switch (m_PipelineSpecification.PipelineTopology) {
		case PipelineTopology::PointList:
			return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		case PipelineTopology::LineList:
			return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
		case PipelineTopology::TriangleList:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		}

		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	}

	VkPolygonMode VulkanPipeline::GetPolygonMode() const
	{
		switch (m_PipelineSpecification.PolygonMode)
		{
		case PolygonMode::Fill: return VK_POLYGON_MODE_FILL;
		case PolygonMode::Line: return VK_POLYGON_MODE_LINE;
		}

		return VK_POLYGON_MODE_FILL;
	}

	VkPipelineDepthStencilStateCreateInfo VulkanPipeline::BuildDepthStenilState() const
	{
		VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo{};
		depthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

		auto& depthStencilState = m_PipelineSpecification.DepthStencilState;

		depthStencilStateCreateInfo.stencilTestEnable = depthStencilState.StencilTestEnabled ? VK_TRUE : VK_FALSE;
		depthStencilStateCreateInfo.depthTestEnable = depthStencilState.DepthTestEnabled ? VK_TRUE : VK_FALSE;
		depthStencilStateCreateInfo.depthWriteEnable = depthStencilState.DepthWriteEnabled ? VK_TRUE : VK_FALSE;
		depthStencilStateCreateInfo.depthBoundsTestEnable = depthStencilState.DepthBoundsTestEnabled ? VK_TRUE : VK_FALSE;
		//depthStencilStateCreateInfo.minDepthBounds = -1.f;
		//depthStencilStateCreateInfo.maxDepthBounds = 1.f;

		depthStencilStateCreateInfo.depthCompareOp = GetCompareOp(depthStencilState.DepthCompareOp);

		depthStencilStateCreateInfo.front.compareOp = GetCompareOp(depthStencilState.Front.CompareOp);
		depthStencilStateCreateInfo.front.failOp = GetStencilOp(depthStencilState.Front.FailOp);
		depthStencilStateCreateInfo.front.depthFailOp = GetStencilOp(depthStencilState.Front.FailOp);
		depthStencilStateCreateInfo.front.passOp = GetStencilOp(depthStencilState.Front.PassOp);
		depthStencilStateCreateInfo.front.compareMask = depthStencilState.Front.CompareMask;
		depthStencilStateCreateInfo.front.writeMask = depthStencilState.Front.WriteMask;
		depthStencilStateCreateInfo.front.reference = depthStencilState.Front.Reference;

		depthStencilStateCreateInfo.back.compareOp = GetCompareOp(depthStencilState.Back.CompareOp);
		depthStencilStateCreateInfo.back.failOp = GetStencilOp(depthStencilState.Back.FailOp);
		depthStencilStateCreateInfo.back.depthFailOp = GetStencilOp(depthStencilState.Back.FailOp);
		depthStencilStateCreateInfo.back.passOp = GetStencilOp(depthStencilState.Back.PassOp);
		depthStencilStateCreateInfo.back.compareMask = depthStencilState.Back.CompareMask;
		depthStencilStateCreateInfo.back.writeMask = depthStencilState.Back.WriteMask;
		depthStencilStateCreateInfo.back.reference = depthStencilState.Back.Reference;


		return depthStencilStateCreateInfo;
	}

	VkCompareOp VulkanPipeline::GetCompareOp(CompareOp compareOp) const
	{
		switch (compareOp)
		{
		case CompareOp::Never:
			return VK_COMPARE_OP_NEVER;
		case CompareOp::Less:
			return VK_COMPARE_OP_LESS;
		case CompareOp::LessOrEqual:
			return VK_COMPARE_OP_LESS_OR_EQUAL;
		case CompareOp::Greater:
			return VK_COMPARE_OP_GREATER;
		case CompareOp::NotEqual:
			return VK_COMPARE_OP_NOT_EQUAL;
		case CompareOp::GreaterOrEqual:
			return VK_COMPARE_OP_GREATER_OR_EQUAL;
		case CompareOp::Always:
			return VK_COMPARE_OP_ALWAYS;
		}
		return VK_COMPARE_OP_MAX_ENUM;
	}

	VkStencilOp VulkanPipeline::GetStencilOp(StencilOp stencilOp) const
	{
		switch (stencilOp)
		{
		case StencilOp::Keep:
			return VK_STENCIL_OP_KEEP;
		case StencilOp::Zero:
			return VK_STENCIL_OP_ZERO;
		case StencilOp::Replace:
			return VK_STENCIL_OP_REPLACE;
		}

		return VK_STENCIL_OP_MAX_ENUM;
	}

	VkCullModeFlagBits VulkanPipeline::GetCullMode(CullMode cullMode) const
	{
		switch (cullMode)
		{
		case CullMode::FrontFace:
			return VK_CULL_MODE_FRONT_BIT;
		case CullMode::BackFace:
			return VK_CULL_MODE_BACK_BIT;
		}

		return VK_CULL_MODE_NONE;
	}

}