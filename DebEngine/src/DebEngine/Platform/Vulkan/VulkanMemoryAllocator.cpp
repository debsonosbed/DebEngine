#include "debpch.h"
#include "VulkanMemoryAllocator.h"
#include "VulkanContext.h"
#include "DebEngine/Core/Core.h"

namespace DebEngine
{
	VulkanMemoryAllocator::VulkanMemoryAllocator(const std::shared_ptr<VulkanDevice>& logicalDevice, const std::string& tag)
		: m_LogicalDevice(logicalDevice), m_Tag(tag)
	{

	}

	VulkanMemoryAllocator::VulkanMemoryAllocator(const std::string& tag)
		: m_LogicalDevice(VulkanContext::GetCurrentDevice()), m_Tag(tag)
	{

	}

	VulkanMemoryAllocator::~VulkanMemoryAllocator()
	{

	}

	void VulkanMemoryAllocator::Allocate(VkMemoryRequirements requirements, VkDeviceMemory* destination, VkMemoryPropertyFlags flags)
	{
		LogCore::Message("VulkanMemoryAllocator::Allocate -> [{:s}]: Allocating {:d} Bytes ({:.2f} MB).", m_Tag, requirements.size, BYTES_TO_MB(requirements.size));

		VkMemoryAllocateInfo memoryAllocateInfo{};
		memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memoryAllocateInfo.allocationSize = requirements.size;
		memoryAllocateInfo.memoryTypeIndex = m_LogicalDevice->GetPhysicalDevice()->GetMemoryTypeIndex(requirements.memoryTypeBits, flags);

		VK_CHECK_RESULT(vkAllocateMemory(m_LogicalDevice->Get(), &memoryAllocateInfo, nullptr, destination));
	}
}