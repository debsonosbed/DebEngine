#pragma once

#include "DebEngine/Core/Buffer.h"
#include "DebEngine/Graphics/Framebuffer.h"
#include "DebEngine/Graphics/RenderPass.h"
#include "VulkanMemoryAllocator.h"


#include "Vulkan.h"

namespace DebEngine
{
	struct VulkanFramebufferSpecification : public FramebufferSpecification
	{
		VkImageView ColorAttachment = VK_NULL_HANDLE;
		VkImageView DepthAttachment = VK_NULL_HANDLE;
		VkFramebuffer Framebuffer = VK_NULL_HANDLE;
	};

	struct FrameBufferAttachment
	{
		VkImage Image = VK_NULL_HANDLE;
		VkDeviceMemory DeviceMemory = VK_NULL_HANDLE;
		VkImageView ImageView = VK_NULL_HANDLE;
	};

	class VulkanFramebuffer : public Framebuffer, public std::enable_shared_from_this<VulkanFramebuffer>
	{
	public:
		VulkanFramebuffer(const FramebufferSpecification& spec);
		VulkanFramebuffer(const VulkanFramebufferSpecification& spec);
		virtual ~VulkanFramebuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;
		virtual void SetupViewport(const std::shared_ptr<CommandBuffer>& drawCommandBuffer, b8 flip = false) override;
		virtual void Resize(u32 width, u32 height, b8 cleanupPostFrame = false) override;
		virtual void AddResizeCallback(const std::function<void(std::shared_ptr<Framebuffer>)>& resizeCallback) override;
		virtual u32 GetWidth() const override;
		virtual u32 GetHeight() const override;
		virtual const std::shared_ptr<Texture2D> GetFramebufferColorTexture() const override;
		virtual const std::shared_ptr<Texture2D> GetFramebufferDepthTexture() const override;

		/// <summary>
		/// As default framebuffer color attachement has VK_IMAGE_LAYOUT_UNDEFINED
		/// In order to perform operations such as copy etc. it has to be initialized to 
		/// a correct layout, which in this case is VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		/// </summary>
		virtual void InitializeLayout() override;

		virtual void ChangeLayoutWrite() override; // TEmp
		virtual void ChangeLayoutRead() override; // TEmp

		//virtual Buffer& GetColorScreenshotBuffer() override;

		const VkDescriptorImageInfo& GetVulkanDescriptorColorImageInfo() const;
		const VkDescriptorImageInfo& GetVulkanDescriptorDepthImageInfo() const;

		VkImage& GetVulkanColorImage();
		VkImage& GetVulkanDepthImage();

		std::shared_ptr<RenderPass> GetRenderPass() const override;
		VkFramebuffer GetVulkanFramebuffer() const;
		void SetVulkanFramebuffer(const VkFramebuffer& vulkanFramebuffer);

		const VulkanFramebufferSpecification& GetVulkanSpecification() const;

		FrameBufferAttachment GetVulkanFramebufferColorAttachment() const;

		static std::shared_ptr<VulkanFramebuffer> Create(const VulkanFramebufferSpecification& vulkanFramebufferSpecification);

	private:
		void CleanUp();
		void CreateColorAttachment();
		void CreateDepthAttachment();
	private:
		VulkanFramebufferSpecification m_VulkanFramebufferSpecification;

		u32 m_RendererID = 0;
		std::vector<std::function<void(std::shared_ptr<Framebuffer>)>> m_ResizeCallbacks;
		u32 m_Width = 0;
		u32 m_Height = 0;
		std::shared_ptr<Texture2D> m_FramebufferColorTexture;
		std::shared_ptr<Texture2D> m_FramebufferDepthTexture;

		Buffer m_ColorBuffer;

		FrameBufferAttachment m_ColorAttachment{}, m_DepthAttachment{};
		VkSampler m_ColorAttachmentSampler = VK_NULL_HANDLE;
		VkRenderPass m_VulkanRenderPass = VK_NULL_HANDLE;
		std::shared_ptr<RenderPass> m_RenderPass;
		VkFramebuffer m_VulkanFramebuffer = VK_NULL_HANDLE;
		VkDescriptorImageInfo m_DescriptorColorImageInfo = {};
		VkDescriptorImageInfo m_DescriptorDepthImageInfo = {};
	};
}
