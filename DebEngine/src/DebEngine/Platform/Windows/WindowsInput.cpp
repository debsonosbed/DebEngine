#include "debpch.h"
#include "WindowsInput.h"

#include "DebEngine/Core/Application.h"
#include <SDL.h>

namespace DebEngine
{
	Input* Input::s_Instance = new WindowsInput();

	b8 WindowsInput::IsKeyDownImpl(KeyCode keyCode)
	{
		return m_CurrentKeyState[(s32)keyCode];
	}

	b8 WindowsInput::IsKeyPressedImpl(KeyCode keyCode)
	{
		return m_CurrentKeyState[(u32)keyCode] && !m_PreviousKeyState[(u32)keyCode];
	}

	b8 WindowsInput::IsMouseButtonDownImpl(KeyCode keyCode)
	{
		return m_CurrentMouseState & SDL_BUTTON((u32)keyCode);
	}

	b8 WindowsInput::IsMouseButtonPressedImpl(KeyCode keyCode)
	{
		return m_CurrentMouseState & SDL_BUTTON((u32)keyCode) && !(m_PreviousMouseState & SDL_BUTTON((u32)keyCode));;
	}

	glm::ivec2 WindowsInput::GetMousePositionImpl() const
	{
		return m_CurrentMousePosition;
	}

	glm::ivec2 WindowsInput::GetGlobalMousePositionImpl() const
	{
		s32 x, y;
		SDL_GetGlobalMouseState(&x, &y);

		return { x, y };
	}

	glm::ivec2 WindowsInput::GetGlobalMousePositionDeltaImpl() const
	{
		return m_FirstFrame ? glm::ivec2(0) : m_CurrentGlobalMousePosition - m_PreviousGlobalMousePosition;
	}

	s32 WindowsInput::GetMouseXImpl() const
	{
		auto pos = GetMousePositionImpl();
		return pos.x;
	}

	s32 WindowsInput::GetMouseYImpl() const
	{
		auto pos = GetMousePositionImpl();
		return pos.y;
	}

	glm::ivec2 WindowsInput::GetMousePositionDeltaImpl() const
	{
		return m_MousePositionDelta;
	}

	glm::ivec2 WindowsInput::GetMouseScrollDeltaImpl() const
	{
		return m_ScrollValue;
	}

	void WindowsInput::UpdateKeyboardStateImpl()
	{
		memcpy(m_PreviousKeyState, m_CurrentKeyState, (u32)KeyCode::NUM_SCANCODES);
		auto keyboardState = SDL_GetKeyboardState(NULL);
		memcpy(m_CurrentKeyState, keyboardState, (u32)KeyCode::NUM_SCANCODES);
	}

	void WindowsInput::UpdateMouseStateImpl()
	{
		m_ScrollValue = { 0, 0 };
		m_PreviousMousePosition = m_CurrentMousePosition;
		m_PreviousMouseState = m_CurrentMouseState;
		m_CurrentMouseState = SDL_GetMouseState(&m_CurrentMousePosition.x, &m_CurrentMousePosition.y);

		m_PreviousGlobalMousePosition = m_CurrentGlobalMousePosition;
		SDL_GetGlobalMouseState(&m_CurrentGlobalMousePosition.x, &m_CurrentGlobalMousePosition.y);

		m_MousePositionDelta = { 0, 0 };

		m_FirstFrame = false;
	}

	void WindowsInput::UpdateMouseScrollValueImpl(u32 x, u32 y)
	{
		m_ScrollValue = { x, y };
	}

	void WindowsInput::UpdateMousePositionDeltaImpl(u32 x, u32 y)
	{
		m_MousePositionDelta = { x, y };
	}
}