#pragma once

#include "DebEngine/Core/Window.h"
#include "DebEngine/Graphics/RendererContext.h"

#include "SDL.h"

namespace DebEngine {

	class WindowsWindow : public Window
	{
	public:
		WindowsWindow(const WindowSettings& settings);
		virtual ~WindowsWindow();

		virtual void ProcessEvents() override;
		virtual void SwapBuffers() override;

		virtual u32 GetWidth() const override { return m_Data.Width; }
		virtual u32 GetHeight() const override { return m_Data.Height; }
		virtual u32 GetAspectRatio() const override;
		virtual void SetMouseVisible(b8 isVisible) override;
		virtual b8 GetMouseVisible() const override;
		virtual void TrapMouse(b8 enabled) override;
		virtual void SetVSync(b8 enabled) override;
		virtual b8 IsVSync() const override;
		virtual void* GetNativeWindow() const { return m_Window; }
		virtual void SetEventCallback(const EventCallbackFn& callback) override { m_Data.EventCallback = callback; }

		virtual std::shared_ptr<RendererContext> GetRendererContext() override
		{
			return m_RendererContext;
		}
	private:
		virtual void Init(const WindowSettings& settings);
		virtual void Shutdown();
	private:
		SDL_Window* m_Window;
		SDL_Event m_Event;
		std::shared_ptr<RendererContext> m_RendererContext;

		struct WindowData
		{
			std::string Title;
			u32 Width, Height;
			b8 Vsync;

			EventCallbackFn EventCallback;
		};

		WindowData m_Data;
	};

}
