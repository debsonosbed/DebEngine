#include "debpch.h"
#include "WindowsWindow.h"

#include "DebEngine/Core/Input/Input.h"
#include "DebEngine/Core/Input/Events/WindowEvent.h"
#include "DebEngine/Core/Input/Events/MouseEvent.h"
#include "DebEngine/Core/Input/Events/KeyEvent.h"

#include "backends/imgui_impl_sdl.h"

#include <shaderc/shaderc.hpp>

namespace DebEngine
{
	static bool s_SDLInitialized = false;

	std::unique_ptr<Window> Window::Create(const WindowSettings& options)
	{
		return std::make_unique<WindowsWindow>(options);
	}

	WindowsWindow::WindowsWindow(const WindowSettings& settings)
	{
		Init(settings);
	}

	WindowsWindow::~WindowsWindow()
	{
		Shutdown();
	}

	void WindowsWindow::Init(const WindowSettings& settings)
	{
		shaderc::Compiler compiler;

		m_Data.Title = settings.Title;
		m_Data.Width = settings.Width;
		m_Data.Height = settings.Height;


		if (!s_SDLInitialized)
		{
			int success = SDL_Init(SDL_INIT_VIDEO);
			DEB_CORE_ASSERT(success == 0, "Could not initialize SDL!");

			s_SDLInitialized = true;
		}
		DEB_CORE_LOG("SDL initialized succesfully!");

		DEB_CORE_LOG("Creating Window {0} ({1}, {2})", settings.Title, settings.Width, settings.Height);
		m_Window = SDL_CreateWindow(
			settings.Title.c_str(),
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			settings.Width,
			settings.Height,
			SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);


		DEB_CORE_ASSERT(m_Window != NULL, "Could not create SDL window!");
		DEB_CORE_LOG("SDL Window created succesfully!");

		SDL_SetWindowData(m_Window, VAR_NAME(m_Data), &m_Data);

		// Create renderer context
		m_RendererContext = RendererContext::Create(m_Window);
		m_RendererContext->Create();
	}

	void WindowsWindow::Shutdown()
	{
		m_RendererContext->Shutdown();
		SDL_DestroyWindow(m_Window);
	}

	void WindowsWindow::ProcessEvents()
	{
		Input::UpdateKeyboardState();
		Input::UpdateMouseState();

		while (SDL_PollEvent(&m_Event))
		{
			// Process events for ImGui
			ImGui_ImplSDL2_ProcessEvent(&m_Event);

			WindowData& data = *(WindowData*)SDL_GetWindowData(m_Window, VAR_NAME(m_Data));

			switch (m_Event.type)
			{
			case SDL_QUIT:
			{
				WindowClosedEvent event;
				data.EventCallback(event);
				break;
			}
			case SDL_WINDOWEVENT:
			{
				// Register events from main window only (AS FOR NOW)
				if (m_Event.window.windowID != 1)  break;

				switch (m_Event.window.event)
				{
				case SDL_WINDOWEVENT_SIZE_CHANGED: {
					data.Width = m_Event.window.data1;
					data.Height = m_Event.window.data2;

					WindowResizedEvent event(m_Event.window.data1, m_Event.window.data2);
					data.EventCallback(event);
					break;
				}
				case SDL_WINDOWEVENT_MINIMIZED: {
					WindowMinimizedEvent event;
					data.EventCallback(event);
					break;
				}
				case SDL_WINDOWEVENT_RESTORED: {
					WindowRestoredEvent event;
					data.EventCallback(event);
					break;
				}
				}
				break;
			}
			case SDL_KEYDOWN:
			case SDL_KEYUP:
			{
				switch (m_Event.key.state)
				{
				case SDL_PRESSED:
				{
					KeyPressedEvent event((KeyCode)m_Event.key.keysym.scancode, m_Event.key.repeat);
					data.EventCallback(event);
					break;
				}
				case SDL_RELEASED:
				{
					KeyReleasedEvent event((KeyCode)m_Event.key.keysym.scancode);
					data.EventCallback(event);
					break;
				}
				}
				break;
			}
			case SDL_MOUSEWHEEL:
			{
				MouseScrollEvent event((f32)m_Event.wheel.x, (f32)m_Event.wheel.y);
				data.EventCallback(event);
				break;
			}
			case SDL_MOUSEMOTION:
			{
				MouseMovedEvent event((f32)m_Event.motion.x, (f32)m_Event.motion.y);
				data.EventCallback(event);

				Input::UpdateMousePositionDelta(m_Event.motion.xrel, m_Event.motion.yrel);
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
			{

				switch (m_Event.button.state)
				{
				case SDL_PRESSED:
				{
					MouseButtonPressedEvent event(m_Event.button.button);
					data.EventCallback(event);
					break;
				}
				case SDL_RELEASED:
				{
					MouseButtonReleasedEvent event(m_Event.button.button);
					data.EventCallback(event);
					break;
				}
				}
				break;
			}
			}
		}
	}

	void WindowsWindow::SwapBuffers()
	{
		m_RendererContext->SwapBuffers();
	}

	u32 WindowsWindow::GetAspectRatio() const
	{
		return m_Data.Width / m_Data.Height;
	}

	void WindowsWindow::SetMouseVisible(b8 isVisible)
	{
		SDL_ShowCursor((int)isVisible);
	}

	b8 WindowsWindow::GetMouseVisible() const
	{
		return true;
	}

	void WindowsWindow::TrapMouse(b8 enabled)
	{
		SDL_SetRelativeMouseMode((SDL_bool)enabled);
	}

	void WindowsWindow::SetVSync(b8 enabled)
	{

	}

	b8 WindowsWindow::IsVSync() const
	{
		return m_Data.Vsync;
	}
}
