#include "debpch.h"
#include "WindowsFilesystem.h"
#include "DebEngine/Core/Application.h"

#include <filesystem>

#include "SDL.h"
#include "SDL_syswm.h"

namespace DebEngine
{
	std::unique_ptr<Filesystem> Filesystem::s_Instance = std::make_unique<WindowsFilesystem>();


	std::string WindowsFilesystem::OpenFileDialogImpl(const std::string& initialDir, const char* filter)
	{
		OPENFILENAMEA ofn;       // common dialog box structure
		CHAR szFile[260] = { 0 };       // if using TCHAR macros

		auto nativeWindow = Application::Get().GetWindow().GetNativeWindow();
		SDL_SysWMinfo wmInfo;
		SDL_VERSION(&wmInfo.version);
		if (SDL_GetWindowWMInfo(static_cast<SDL_Window*>(nativeWindow), &wmInfo))
		{
			// Initialize OPENFILENAME
			ZeroMemory(&ofn, sizeof(OPENFILENAME));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = wmInfo.info.win.window;
			ofn.hInstance = wmInfo.info.win.hinstance;
			ofn.lpstrTitle = "DebEngine - Open file";
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = filter;
			ofn.nFilterIndex = 1;
			ofn.lpstrInitialDir = initialDir.c_str();
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

			if (GetOpenFileNameA(&ofn) == TRUE)
			{
				return ofn.lpstrFile;
			}
		}
		return std::string();
	}
}
