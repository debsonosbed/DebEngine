#pragma once

#include "DebEngine/Core/Filesystem.h"

namespace DebEngine
{
	class WindowsFilesystem : public Filesystem
	{
	private:
		virtual std::string OpenFileDialogImpl(const std::string& initialDir, const char* filter) override;
		//virtual void SaveFileImpl(const File& file, const Directory& directory) override;
	};
}