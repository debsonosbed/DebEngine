#pragma once

#include "DebEngine/Core/Core.h"
#include "DebEngine/Core/Input/Input.h"

namespace DebEngine {

	class WindowsInput : public Input
	{
	protected:
		virtual b8 IsKeyDownImpl(KeyCode keyCode) override;
		virtual b8 IsKeyPressedImpl(KeyCode keyCode) override;
		virtual b8 IsMouseButtonDownImpl(KeyCode keyCode) override;
		virtual b8 IsMouseButtonPressedImpl(KeyCode keyCode) override;
		virtual glm::ivec2 GetMousePositionImpl() const override;
		virtual glm::ivec2 GetGlobalMousePositionImpl() const override;
		virtual glm::ivec2 GetGlobalMousePositionDeltaImpl() const override;
		virtual s32 GetMouseXImpl() const override;
		virtual s32 GetMouseYImpl() const override;
		virtual glm::ivec2 GetMousePositionDeltaImpl() const override;
		virtual glm::ivec2 GetMouseScrollDeltaImpl() const override;

		virtual void UpdateKeyboardStateImpl() override;
		virtual void UpdateMouseStateImpl() override;
		virtual void UpdateMouseScrollValueImpl(u32 x, u32 y) override;
		virtual void UpdateMousePositionDeltaImpl(u32 x, u32 y) override;

	private:
		u8 m_CurrentKeyState[(u32)KeyCode::NUM_SCANCODES];
		u8 m_PreviousKeyState[(u32)KeyCode::NUM_SCANCODES];
		u32 m_CurrentMouseState = 0;
		u32 m_PreviousMouseState = 0;

		b8 m_FirstFrame = true;
		glm::ivec2 m_CurrentGlobalMousePosition{ 0, 0 };
		glm::ivec2 m_PreviousGlobalMousePosition{ 0, 0 };

		glm::ivec2 m_CurrentMousePosition{ 0, 0 };
		glm::ivec2 m_PreviousMousePosition{ 0, 0 };
		glm::ivec2 m_ScrollValue{ 0, 0 };

		glm::ivec2 m_MousePositionDelta{0, 0};
	};
}

