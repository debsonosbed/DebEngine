#include <DebEngine.h>
#include <DebEngine/EntryPoint.h>
#include <DebEditor.h>

#include "Sandbox/SandboxLayer.h"

class DebApp : public DebEngine::Application
{
public:
	DebApp(const DebEngine::ApplicationSettings& options) : Application(options) { }

	virtual void OnInit() override
	{
		auto sandboxLayer = new SandboxLayer();
		PushLayer(new DebEditor::EditorLayer(sandboxLayer->GetScene()));
		PushLayer(sandboxLayer);
	}
};

std::unique_ptr<DebEngine::Application> DebEngine::CreateApplication()
{
	const ApplicationSettings appSettings = { "DebApp", 1280, 720 };
	return std::make_unique<DebApp>(appSettings);
}