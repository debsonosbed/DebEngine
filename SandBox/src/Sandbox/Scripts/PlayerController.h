#pragma once

#include <DebEngine.h>
using namespace DebEngine;

class PlayerController: public ScriptableEntity
{
public:
	virtual ~PlayerController() = default;

	ASSIGN_NAME(PlayerController)
protected:
	void OnCreate() override;
	void OnUpdate() override;
	void OnDestroy() override;
};

