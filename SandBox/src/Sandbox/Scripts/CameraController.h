#pragma once

#include <DebEngine.h>
using namespace DebEngine;

class CameraController: public ScriptableEntity
{
public:
	virtual ~CameraController() = default;

	ASSIGN_NAME(CameraController)
protected:
	void OnCreate() override;
	void OnUpdate() override;
	void OnDestroy() override;

private:
	Entity m_PlayerEntity;
};

