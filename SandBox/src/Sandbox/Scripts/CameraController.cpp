#include "CameraController.h"

void CameraController::OnCreate()
{
	auto& cameraPosition = GetComponent<TransformComponent>().Position;
	cameraPosition = glm::vec3(0.f, 0.f, 5.f);

	m_PlayerEntity = FindByTagName("Player");
}

void CameraController::OnUpdate()
{
	if (m_PlayerEntity)
	{
		auto& pt = m_PlayerEntity.GetComponent<TransformComponent>();
		auto& ct = GetComponent<TransformComponent>();

		// Primitive player follow
		ct.Position = glm::vec3(pt.Position.x, pt.Position.y + 1, pt.Position.z + 2);
		ct.Rotation = pt.Rotation;
	}
}

void CameraController::OnDestroy()
{

}
