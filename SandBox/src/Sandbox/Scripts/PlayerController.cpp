#include "PlayerController.h"

void PlayerController::OnCreate()
{
	Log::Message("PlayerController script Created");

	auto& playerPosition = GetComponent<TransformComponent>().Position;
	playerPosition = glm::vec3(1.5f, 0.f, 0.f);
}

void PlayerController::OnUpdate()
{
	auto& transform = GetComponent<TransformComponent>();

	// Rotation
	auto& posDelta = Input::GetMousePositionDelta();
	f32 sensitivity = 1.0f;
	f32 pitch = posDelta.x * Time::DeltaTime() * sensitivity;
	f32 roll = posDelta.y * Time::DeltaTime() * sensitivity;
	transform.Rotation.y -= pitch;
	transform.Rotation.x -= roll;

	// Limit roll rotation
	f32 limitRotationRad = glm::radians(90.0f);
	if (transform.Rotation.x > limitRotationRad)
		transform.Rotation.x = limitRotationRad;
	if (transform.Rotation.x < -limitRotationRad)
		transform.Rotation.x = -limitRotationRad;

	// Position
	f32 speed = 5.f;
	f32 deltaTime = Time::DeltaTime();

	if (Input::IsKeyDown(KeyCode::W))
		transform.Position += glm::vec3(0, 0, -1) * speed * deltaTime;
	if (Input::IsKeyDown(KeyCode::A))
		transform.Position += glm::vec3(-1, 0, 0) * speed * deltaTime;
	if (Input::IsKeyDown(KeyCode::S))
		transform.Position += glm::vec3(0, 0, 1) * speed * deltaTime;
	if (Input::IsKeyDown(KeyCode::D))
		transform.Position += glm::vec3(1, 0, 0) * speed * deltaTime;

}

void PlayerController::OnDestroy()
{
	Log::Message("PlayerController script destroyed");
}
