#include "SandboxLayer.h"

// Scripts
#include "Scripts/PlayerController.h"
#include "Scripts/CameraController.h"
#include "Scripts/PrimitiveController.h"

#include <random>

SandboxLayer::SandboxLayer()
	: m_Scene(new Scene())
{

}

enum class MeshType
{
	Cube, Sphere, PrimitiveMix
};

void SandboxLayer::OnAttach()
{
	// Register scripts - review later
	ScriptRegistry::Add<PlayerController>();
	ScriptRegistry::Add<CameraController>();
	ScriptRegistry::Add<PrimitiveController>();

#define SCENE_GENERATOR_ENABLED 0

#if SCENE_GENERATOR_ENABLED
	std::random_device device;
	auto RNG = std::mt19937(device());
	std::uniform_real_distribution<f64> distribution(0.0, 1.0);

	u32 cubeCounter = 0;
	u32 primitiveCount = 1000;

	MeshType meshType = MeshType::PrimitiveMix;
	b8 randomMeshColour = true;
	b8 attachPrimitiveScript = true;

	m_CubesContainer.reserve(primitiveCount);
	// Cube size is 1x1x1
	for (u32 i = 0; i < primitiveCount; i++)
	{
		auto& primitive = m_CubesContainer.emplace_back(m_Scene->CreateEntity("Mesh" + std::to_string(cubeCounter)));

		switch (meshType)
		{
		case MeshType::Cube:
		{
			primitive.AddComponent<MeshComponent>("assets/models/cube.fbx");
			break;
		}
		case MeshType::Sphere:
		{
			primitive.AddComponent<MeshComponent>("assets/models/sphere.fbx");
			break;
		}
		case MeshType::PrimitiveMix:
		{
			if (cubeCounter % 2 == 0)
				primitive.AddComponent<MeshComponent>("assets/models/cube.fbx");
			else
				primitive.AddComponent<MeshComponent>("assets/models/sphere.fbx");

			break;
		}
		}

		if (attachPrimitiveScript)
			primitive.AddComponent<NativeScriptComponent>().Bind<PrimitiveController>();

		if (randomMeshColour)
		{
			f64 r = distribution(RNG);
			f64 g = distribution(RNG);
			f64 b = distribution(RNG);

			glm::vec4 color = { r, g, b, 1.0f };
			auto& mesh = primitive.GetComponent<MeshComponent>().Mesh;
			mesh->SetColor(color);
		}

		cubeCounter++;
	}

	// Cerberus scene
	/*{
		auto& gun = m_Scene->CreateEntity("Cerberus");
		gun.AddComponent<MeshComponent>("assets/models/cerberus/cerberus.fbx");
	}*/


	Log::Message("Created {0} entites with cube component!", cubeCounter);
	{
		m_Player = m_Scene->CreateEntity("Player");
		auto& mesh = m_Player.AddComponent<MeshComponent>("assets/models/cube.fbx").Mesh;
		m_Player.AddComponent<NativeScriptComponent>().Bind<PlayerController>();

		glm::vec4 color = { 1.0f, 0.0f, 0.0f, 1.0f };
		mesh->SetColor(color);
	}

	{
		m_Camera = m_Scene->CreateEntity("Main Camera");
		m_Camera.AddComponent<CameraComponent>();
		m_Camera.AddComponent<NativeScriptComponent>().Bind<CameraController>();
	}
#endif

	m_Scene->LoadEnvironmentMap("assets/environment/default.hdr");
}

void SandboxLayer::OnDetach()
{

}

void SandboxLayer::OnUpdate()
{
	m_Scene->OnUpdate();
	m_Scene->OnRender();
}

void SandboxLayer::OnImGuiRender()
{
}

void SandboxLayer::OnEvent(Event& event)
{
	m_Scene->OnEvent(event);
}

DebEngine::Scene* SandboxLayer::GetScene()
{
	return m_Scene;
}
