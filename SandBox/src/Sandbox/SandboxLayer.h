#pragma once

#include <DebEngine.h>
#include <DebEditor.h>

using namespace DebEngine;
using namespace DebEditor;

class SandboxLayer : public Layer
{
public:
	SandboxLayer();

	void OnAttach() override;
	void OnDetach() override;
	void OnUpdate() override;
	void OnImGuiRender() override;
	void OnEvent(Event& event) override;

	Scene* GetScene();
private:
	Scene* m_Scene;

	std::shared_ptr<Material> m_Material;
	Entity m_Player;
	Entity m_Gun;
	Entity m_Cube;
	Entity m_DirectionalLight;
	Entity m_Camera;
	Entity m_Sprite;
	std::vector<Entity> m_CubesContainer;
};

