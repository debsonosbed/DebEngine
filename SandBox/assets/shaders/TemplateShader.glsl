#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

/////////////////////////////////////////////////////
// Push Constant Definition /////////////////////////
/////////////////////////////////////////////////////
struct DataRef
{
	int DataID;
	int SamplerID;
	int CombinedSamplerID;
};

layout (push_constant) uniform PushConstantBlock
{
	DataRef Scene;
	DataRef Pass;
	DataRef Object;
} pcVert;
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Storage Buffers Definition ///////////////////////
/////////////////////////////////////////////////////

struct SceneData
{
	int dummy;
};

struct PassData
{
	int dummy;
};

struct PerObjectData
{
	int dummy;
};

layout (set = 0, std140, binding = 0) buffer SceneStorageBufferVert
{
	layout (offset = 256)
	SceneData data[ ];
} u_Scene;


layout (set = 1, std140, binding = 5) buffer PassStorageBufferVert
{
	layout (offset = 256)
	PassData data[ ];
} u_Pass;


layout (set = 2, std140, binding = 10) buffer ObjectsStorageBufferVert
{
	layout (offset = 256)
	PerObjectData data[ ];
} u_PerObject;


void main()
{
	gl_Position = vec4(a_Position, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

/////////////////////////////////////////////////////
// Push Constant Definition /////////////////////////
/////////////////////////////////////////////////////
struct DataRef
{
	int DataID;
	int SamplerID;
	int CombinedSamplerID;
};

layout (push_constant) uniform PushConstantBlockFrag
{
	DataRef Scene;
	DataRef Pass;
	DataRef Object;
} pcFrag;
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Storage Buffers Definition ///////////////////////
/////////////////////////////////////////////////////

struct SceneData
{
	int dummy;
};

struct PassData
{
	int dummy;
};

struct PerObjectData
{
	int dummy;
};

layout (set = 0, std140, binding = 1) buffer SceneStorageBufferFrag
{
	layout (offset = 256)
	SceneData data[ ];
} u_Scene;


layout (set = 1, std140, binding = 6) buffer PassStorageBufferFrag
{
	layout (offset = 256)
	PassData data[ ];
} u_Pass;


layout (set = 2, std140, binding = 11) buffer ObjectsStorageBufferFrag
{
	layout (offset = 256)
	PerObjectData data[ ];
} u_PerObject;


/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Samplers Definition //////////////////////////////
/////////////////////////////////////////////////////
#define MAX_COMBINED_SAMPLED_IMAGES_COUNT 10
#define MAX_SAMPLED_IMAGE_COUNT 16384

// Scene Samplers ///////////////////////////////////
layout(set = 0, binding = 2) uniform sampler SceneSampler;
layout(set = 0, binding = 3) uniform textureCube SceneCombinedSampledImages[MAX_COMBINED_SAMPLED_IMAGES_COUNT];
layout(set = 0, binding = 4) uniform texture2D SceneSampledImages[MAX_SAMPLED_IMAGE_COUNT];
/////////////////////////////////////////////////////

// Pass Samplers ////////////////////////////////////
layout(set = 1, binding = 7) uniform sampler PassSampler;
layout(set = 1, binding = 8) uniform textureCube PassCombinedSampledImages[MAX_COMBINED_SAMPLED_IMAGES_COUNT];
layout(set = 1, binding = 9) uniform texture2D PassSampledImages[MAX_SAMPLED_IMAGE_COUNT];
/////////////////////////////////////////////////////

// Objects Samplers ///////////////////////////////////
layout(set = 2, binding = 12) uniform sampler ObjectsSampler;
layout(set = 2, binding = 13) uniform texture2D ObjectsSampledImages[MAX_SAMPLED_IMAGE_COUNT];
/////////////////////////////////////////////////////

void main()
{
	o_Color = vec4(1.0);
}
