#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout (push_constant) uniform Transform
{
	mat4 ViewProjection;
} pc_Transform;

struct OutputBlock
{
	vec3 TexCoord;
};

layout (location = 0) out OutputBlock Output;


out gl_PerVertex 
{
	vec4 gl_Position;
};

void main()
{
	vec3 texCoord = a_Position;

	Output.TexCoord = texCoord;
	gl_Position = pc_Transform.ViewProjection * vec4(a_Position.xyz, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

struct OutputBlock
{
	vec3 TexCoord;
};

layout (location = 0) in OutputBlock Input;

layout (push_constant) uniform Properties
{
	layout (offset = 64) float cubeMapLOD;
} pc_Properties;

layout(binding = 0) uniform samplerCube u_TextureCubeMap;

vec3 Uncharted2Tonemap(vec3 color)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	float W = 11.2;
	return ((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}

void main()
{
	/*vec3 color = texture(u_SamplerCube, Input.TexCoord).rgb;
	// Tone mapping
	color = Uncharted2Tonemap(color * 1.5f);
	color = color * (1.0f / Uncharted2Tonemap(vec3(11.2f)));	
	// Gamma correction
	color = pow(color, vec3(1.0f / 1.3f));*/

	//o_Color = vec4(color, 1.0);
	o_Color = textureLod(u_TextureCubeMap, Input.TexCoord, pc_Properties.cubeMapLOD);
}