#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec2 a_TexCoord;


layout (push_constant) uniform Transform
{
	mat4 Transform;
	mat4 ViewProjection;
} pc_Transform;

struct VertexOutput
{
	vec2 TexCoord;
};

layout (location = 0) out VertexOutput Output;


void main()
{
	gl_PointSize = 1.0;
	Output.TexCoord = a_TexCoord;
	gl_Position = pc_Transform.ViewProjection * pc_Transform.Transform * vec4(a_Position, 1.0);
	//gl_Position.y = -gl_Position.y;
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

/*layout (push_constant) uniform Material
{
	layout(offset = 64) vec4 Color;
} u_Material;*/


layout (binding = 1) uniform Material
{
	vec4 Color;
} u_Material;

layout (binding = 2) uniform sampler2D u_AlbedoTexture;

struct VertexOutput
{
	vec2 TexCoord;
};

layout (location = 0) in VertexOutput Input;


void main()
{
	//o_Color = vec4(1.0, 0.0, 0.0, 1.0);
	o_Color = u_Material.Color;
	//o_Color = u_MaterialUniform.Color;
	//o_Color = u_Material.Color;
	//vec3 albedo = texture(u_AlbedoTexture, Input.TexCoord).rgb;
	//o_Color = vec4(albedo, 1);
}