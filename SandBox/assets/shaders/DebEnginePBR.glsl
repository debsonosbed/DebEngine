#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout(location = 5) in vec4 a_InstanceTransform1;
layout(location = 6) in vec4 a_InstanceTransform2;
layout(location = 7) in vec4 a_InstanceTransform3;
layout(location = 8) in vec4 a_InstanceTransform4;
layout(location = 9) in vec4 a_InstanceColor;
layout(location = 10) in float a_InstanceRoughness;
layout(location = 11) in float a_InstanceMetalness;

layout (push_constant) uniform Transform
{
	mat4 ViewProjection;
} pc_Transform;

struct VertexOutput
{
	vec3 WorldPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
	mat3 WorldTransform;
	vec3 Binormal;
	vec4 Color;
	float Roughness;
	float Metalness;
};

layout (location = 0) out VertexOutput vs_Output;

void main()
{
	gl_PointSize = 1.0;
	vec4 position = vec4(a_Position, 1.0);

	mat4 instanceTransform = 
							{
								a_InstanceTransform1, 
								a_InstanceTransform2, 
								a_InstanceTransform3,
								a_InstanceTransform4 
							};

	mat4 baseTransform = mat4(1.0);
	mat4 transform = baseTransform * instanceTransform;

	vs_Output.WorldPosition = vec3(transform * position);
	vs_Output.Normal = mat3(transform) * a_Normal;
	vs_Output.TexCoord = a_TexCoord;
	vs_Output.WorldNormals = mat3(transform) * mat3(a_Tangent, a_Binormal, a_Normal);
	vs_Output.WorldTransform = mat3(transform);
	vs_Output.Binormal = a_Binormal;

	// Material properties
	vs_Output.Color = a_InstanceColor;
	vs_Output.Roughness = a_InstanceRoughness;
	vs_Output.Metalness = a_InstanceMetalness;

	gl_Position = pc_Transform.ViewProjection * transform * position;
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

struct VertexInput
{
	vec3 WorldPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
	mat3 WorldTransform;
	vec3 Binormal;
	vec4 Color;
	float Roughness;
	float Metalness;
};

layout (location = 0) in VertexInput vs_Input;

layout (binding = 1) uniform sampler2D u_TextureAlbedo;
layout (binding = 2) uniform sampler2D u_TextureNormals;
layout (binding = 3) uniform sampler2D u_TextureRoughness;
layout (binding = 4) uniform sampler2D u_TextureNormalCamera;
layout (binding = 5) uniform sampler2D u_TextureMetalness;
layout (binding = 6) uniform samplerCube u_TextureRadiance;
layout (binding = 7) uniform samplerCube u_TextureIrradiance;
layout (binding = 8) uniform sampler2D u_TextureBRDF;


layout (binding = 9) uniform Material
{
	vec3 Color;
	float Metalness;
	float Roughness;
} u_Material;

layout (binding = 10) uniform TextureToggles
{
	int hasAlbedo;
	int hasNormals;
	int hasRoughness;
	int hasMetalness;
} u_TextureToggles;

layout (binding = 11) uniform DirectionalLight
{
	vec3 Direction;
	vec3 Radiance;
	float Intensity;
} u_DirectionalLight;

layout (binding = 12) uniform Camera
{
	vec3 Position;
} u_Camera;

layout (binding = 13) uniform Environment
{
	float EnvMapRotation;
} u_Environment;


const float PI = 3.141592;
const float Epsilon = 0.00001;

const int LightCount = 1;

// Constant normal incidence Fresnel factor for all dielectrics.
const vec3 Fdielectric = vec3(0.04);

struct PBRParameters
{
	vec3 Albedo;
	float Roughness;
	float Metalness;

	vec3 Normal;
	vec3 View;
	float NdotV;
};

PBRParameters m_Params;

float ndfGGX(float cosLh, float roughness)
{
	float alpha = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float gaSchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float gaSchlickGGX(float cosLi, float NdotV, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0; // Epic suggests using this roughness remapping for analytic lights.
	return gaSchlickG1(cosLi, k) * gaSchlickG1(NdotV, k);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

// Shlick's approximation of the Fresnel factor.
vec3 fresnelSchlick(vec3 F0, float cosTheta)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
} 

vec3 RotateVectorAboutY(float angle, vec3 vec)
{
    angle = radians(angle);
    mat3x3 rotationMatrix ={vec3(cos(angle),0.0,sin(angle)),
                            vec3(0.0,1.0,0.0),
                            vec3(-sin(angle),0.0,cos(angle))};
    return rotationMatrix * vec;
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

vec3 Lighting(vec3 F0)
{
	vec3 result = vec3(0.0);
	for(int i = 0; i < LightCount; i++)
	{
		//vec3 Li = normalize(u_DirectionalLight.Position - vs_Input.WorldPosition);
		vec3 Li = -u_DirectionalLight.Direction;
		vec3 Lradiance = u_DirectionalLight.Radiance * u_DirectionalLight.Intensity;
		vec3 Lh = normalize(Li + m_Params.View);

		/*float distance = length(u_DirectionalLight.Position - vs_Input.WorldPosition);
        float attenuation = 1.0 / (distance * distance);
		radiance *= attenuation;*/

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(m_Params.Normal, Li));
		float cosLh = max(0.0, dot(m_Params.Normal, Lh));

		vec3 F = fresnelSchlick(F0, max(0.0, dot(Lh, m_Params.View)));
		float D = ndfGGX(cosLh, m_Params.Roughness);
		float G = gaSchlickGGX(cosLi, m_Params.NdotV, m_Params.Roughness);

		vec3 kd = (1.0 - F) * (1.0 - m_Params.Metalness);
		vec3 diffuseBRDF = kd * m_Params.Albedo;

		// Cook-Torrance
		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * m_Params.NdotV);

		//result += (kD * m_Params.Albedo / PI + specular) * radiance * NdotL;

		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;
}

vec3 IBL(vec3 F0, vec3 Lr)
{
	vec3 irradiance = texture(u_TextureIrradiance, m_Params.Normal).rgb;
	vec3 F = fresnelSchlickRoughness(F0, m_Params.NdotV, m_Params.Roughness);
	vec3 kd = (1.0 - F) * (1.0 - m_Params.Metalness);
	vec3 diffuseIBL = m_Params.Albedo * irradiance;

	int envRadianceTexLevels = textureQueryLevels(u_TextureRadiance);
	float NoV = clamp(m_Params.NdotV, 0.0, 1.0);
	vec3 R = 2.0 * dot(m_Params.View, m_Params.Normal) * m_Params.Normal - m_Params.View;
	vec3 specularIrradiance = textureLod(u_TextureRadiance, RotateVectorAboutY(u_Environment.EnvMapRotation, Lr), (m_Params.Roughness) * envRadianceTexLevels).rgb;

	// Sample BRDF Lut
	vec2 specularBRDF = texture(u_TextureBRDF, vec2(m_Params.Roughness)).rg;
	vec3 specularIBL = specularIrradiance * (F * specularBRDF.x + specularBRDF.y);

	return kd * diffuseIBL + specularIBL;
}

void main()
{
	m_Params.Albedo = u_TextureToggles.hasAlbedo > 0 ? texture(u_TextureAlbedo, vs_Input.TexCoord).rgb : vs_Input.Color.rgb; 
	m_Params.Metalness = u_TextureToggles.hasMetalness > 0 ? texture(u_TextureMetalness, vs_Input.TexCoord).r : vs_Input.Metalness;
	m_Params.Roughness = u_TextureToggles.hasRoughness > 0 ? texture(u_TextureRoughness, vs_Input.TexCoord).r : vs_Input.Roughness;
    m_Params.Roughness = max(m_Params.Roughness, 0.05); // Minimum roughness of 0.05 to keep specular highlight

	// Normals (either from vertex or map)
	m_Params.Normal = normalize(vs_Input.Normal);
	if (u_TextureToggles.hasNormals > 0)
	{
		m_Params.Normal = normalize(2.0 * texture(u_TextureNormals, vs_Input.TexCoord).rgb - 1.0);
		m_Params.Normal = normalize(vs_Input.WorldNormals * m_Params.Normal);
	}

	m_Params.View = normalize(u_Camera.Position - vs_Input.WorldPosition);
	m_Params.NdotV = max(dot(m_Params.Normal, m_Params.View), 0.0);

	// Specular reflection vector
	vec3 Lr = 2.0 * m_Params.NdotV * m_Params.Normal - m_Params.View;

	// Fresnel reflectance, metals use albedo
	vec3 F0 = mix(Fdielectric, m_Params.Albedo, m_Params.Metalness);

	vec3 lightContribution = Lighting(F0);
	vec3 iblContribution = IBL(F0, Lr);

	o_Color = vec4(lightContribution + iblContribution, 1.0);
}