#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec2 a_TexCoord;


layout (push_constant) uniform Transform
{
	mat4 ViewProjection;
	mat4 Transform;
} pc_Transform;

struct VertexOutput
{
	vec2 TexCoord;
};

layout (location = 0) out VertexOutput Output;

void main()
{
	vec4 position = pc_Transform.ViewProjection * pc_Transform.Transform * vec4(a_Position, 1.0);
	gl_Position = position;

	Output.TexCoord = a_TexCoord;
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

layout (binding = 0) uniform Material
{
	float Scale;
	float Resolution;
} u_Material;

struct VertexOutput
{
	vec2 TexCoord;
};

layout (location = 0) in VertexOutput Input;

float grid(vec2 st, float res)
{
	vec2 grid = fract(st);
	return step(res, grid.x) * step(res, grid.y);
}

void main()
{
	float scale = u_Material.Scale;
	float resolution = u_Material.Resolution;

	float x = grid(Input.TexCoord * scale, resolution);
	o_Color = vec4(vec3(0.1), 0.5) * (1.0 - x);
}
