#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout (push_constant) uniform PushConstantBlock
{
	mat4 ViewProjection;
} pc_Transform;

struct OutputBlock
{
	vec3 LocalPosition;
};

layout (location = 0) out OutputBlock Output;

void main()
{
	Output.LocalPosition = a_Position;
	gl_Position = pc_Transform.ViewProjection * vec4(a_Position.xyz, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

layout(binding = 0) uniform sampler2D u_Texture;

struct InputBlock
{
	vec3 LocalPosition;
};

layout (location = 0) in InputBlock Input;

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}
/////////////////////////////////////////////////////

void main()
{
	vec2 uv = SampleSphericalMap(normalize(Input.LocalPosition)); 
	o_Color = vec4(texture(u_Texture, uv).rgb,
	1.0);
}
