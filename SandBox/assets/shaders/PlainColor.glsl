#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout(location = 5) in vec4 a_InstanceTransform1;
layout(location = 6) in vec4 a_InstanceTransform2;
layout(location = 7) in vec4 a_InstanceTransform3;
layout(location = 8) in vec4 a_InstanceTransform4;
layout(location = 9) in vec4 a_InstanceColor;

layout (push_constant) uniform Transform
{
	mat4 ViewProjection;
} pc_Transform;

struct VertexOutput
{
	vec4 Color;
};

layout (location = 0) out VertexOutput vs_Output;

void main()
{
	vs_Output.Color = a_InstanceColor;
	mat4 instanceTransform = 
							{
								a_InstanceTransform1, 
								a_InstanceTransform2, 
								a_InstanceTransform3,
								a_InstanceTransform4 
							};

	mat4 baseTransform = mat4(1.0);
	mat4 transform = baseTransform * instanceTransform;
	gl_Position = pc_Transform.ViewProjection * transform * vec4(a_Position, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

struct VertexInput
{
	vec4 Color;
};

layout (location = 0) in VertexInput vs_Input;

void main()
{
	o_Color = vs_Input.Color;
}
