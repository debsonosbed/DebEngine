#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout(binding = 0) uniform Settings
{
	float OutlineThickness;
} u_Settings;

layout (push_constant) uniform Transform
{
	mat4 ViewProjection;
	mat4 Transform;
} pc_Transform;


void main()
{
	vec4 position = pc_Transform.ViewProjection *
					pc_Transform.Transform *
					vec4(a_Position * u_Settings.OutlineThickness, 1.0);
	gl_Position = position;
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;

layout (binding = 1) uniform Material
{
	vec3 Color;
} u_Material;

void main()
{
	o_Color = vec4(u_Material.Color, 1.0);
}
