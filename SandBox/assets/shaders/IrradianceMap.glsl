#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec3 a_Tangent;
layout(location = 4) in vec3 a_Binormal;

layout (push_constant) uniform PushConstantBlock
{
	mat4 ViewProjection;
} pc_Transform;

struct OutputBlock
{
	vec3 LocalPosition;
};

layout (location = 0) out OutputBlock Output;


void main()
{
	Output.LocalPosition = a_Position;
	gl_Position = pc_Transform.ViewProjection * vec4(a_Position.xyz, 1.0);
	//gl_Position = vec4(a_Position.xyz, 1.0);
}

#type fragment
#version 450 core

// Generates an irradiance cube from an environment map using convolution

layout(location = 0) out vec4 o_Color;


struct InputBlock
{
	vec3 LocalPosition;
};

layout (location = 0) in InputBlock Input;


layout(binding = 0) uniform samplerCube u_TextureCubeMap;


#define PI 3.1415926535897932384626433832795

void main()
{
	vec3 N = normalize(Input.LocalPosition);
	vec3 up = vec3(0.0, 1.0, 0.0);
	vec3 right = normalize(cross(up, N));
	up = cross(N, right);

	const float TWO_PI = PI * 2.0;
	const float HALF_PI = PI * 0.5;

	const float deltaPhi = (2.0 * PI) / 180.0;
	const float deltaTheta = (0.5 * PI) / 64.0;

	vec3 color = vec3(0.0);
	uint sampleCount = 0u;
	for (float phi = 0.0; phi < TWO_PI; phi += deltaPhi) 
	{
		for (float theta = 0.0; theta < HALF_PI; theta += deltaTheta) 
		{
			vec3 tempVec = cos(phi) * right + sin(phi) * up;
			vec3 sampleVector = cos(theta) * N + sin(theta) * tempVec;
			color += texture(u_TextureCubeMap, sampleVector).rgb * cos(theta) * sin(theta);
			sampleCount++;
		}
	}

	o_Color = vec4(PI * color / float(sampleCount), 1.0);
	//o_Color = texture(u_CubeMap, Input.LocalPosition);
}
